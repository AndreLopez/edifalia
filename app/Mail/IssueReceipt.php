<?php

namespace Edifalia\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class IssueReceipt extends Mailable
{
    use Queueable, SerializesModels;

    public $issueReceipt_id;
    protected $pdf_route;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($issueReceipt_id, $pdf_route)
    {
        $this->issueReceipt_id = $issueReceipt_id; // for view emails.receipt
        $this->pdf_route = $pdf_route;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('info@edifalia.com', trans('email.issue_receipt.name'))
            ->subject(trans('email.issue_receipt.receipt'))->view('emails.receipt')
            ->attach($this->pdf_route);
    }
}

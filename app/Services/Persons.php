<?php
/**
 * Created by PhpStorm.
 * User: gamg
 * Date: 26/11/16
 * Time: 10:40 AM
 */

namespace Edifalia\Services;


class Persons
{
    public function get()
    {
        $persons = [];
        foreach (\DB::table('persons')->get() as $person){
            $persons[$person->id] = $person->first_name;
        }
        return $persons;
    }
}
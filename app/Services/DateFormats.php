<?php

namespace Edifalia\Services;

use Edifalia\Models\DateFormat;

class DateFormats
{
    public function get()
    {
        $formats[''] = trans('dashboard.fields.select_date_format');

        foreach (DateFormat::get() as $format){
            $formats[$format->id] = $format->format;
        }
        return $formats;
    }
}
<?php

namespace Edifalia\Services;

use Edifalia\Models\Continent;

class Continents
{
    public function get()
    {
        $continents[''] = trans('dashboard.fields.select_continent');
        foreach (Continent::get() as $continent){
            $continents[$continent->id] = $continent->name;
        }
        return $continents;
    }
}
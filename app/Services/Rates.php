<?php

namespace Edifalia\Services;

use Edifalia\Models\Rate;

class Rates
{
    public function get()
    {
        $rates[''] = trans('dashboard.fields.select_rate');
        foreach (Rate::get() as $rate){
            $rates[$rate->id] = $rate->name;
        }
        return $rates;
    }
}
<?php

namespace Edifalia\Services;

use Edifalia\Models\Reason;

class Reasons
{
    public function get()
    {
        $reasons[''] = trans('dashboard.fields.select_reason');

        foreach (Reason::get() as $reason){
            $reasons[$reason->id] = $reason->name;
        }
        return $reasons;
    }
}
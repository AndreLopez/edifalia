<?php
/**
 * Created by PhpStorm.
 * User: gamg
 * Date: 25/11/16
 * Time: 09:31 PM
 */

namespace Edifalia\Services;

use Edifalia\Models\Page;

class Pages
{
    public function get()
    {
        $pages[''] = trans('dashboard.fields.select_page');
        foreach (Page::get() as $page){
            $pages[$page->id] = $page->name;
        }
        return $pages;
    }
}
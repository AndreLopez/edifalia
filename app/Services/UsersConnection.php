<?php

namespace Edifalia\Services;

use Illuminate\Support\Facades\Auth;
use Edifalia\Models\Connection;
use Edifalia\Models\Access;
use DateTime;
use \Torann\GeoIP\Facades\GeoIP;

class UsersConnection {

    public function save()
    {
        if(Auth::user()->isEmployee() || Auth::user()->isAdmin()){
            $current_time = time();
            $time_min = $current_time - (config('session.lifetime')*60);
            $expired = Connection::where('last_time', '<=', $time_min)->get();

            // se guardan como acceso de usuario aquellas conexiones caducadas
            foreach ($expired as $expiredConnection){
                $start = new DateTime($expiredConnection->created_at);
                $end = new DateTime($expiredConnection->updated_at);
                $interval = $start->diff($end);
                Access::create([
                    'user_id' => $expiredConnection->user->id,
                    'start_date' => $expiredConnection->created_at,
                    'ip_address' => $expiredConnection->ip_address,
                    'country' => $expiredConnection->country,
                    'end_date' => $expiredConnection->updated_at,
                    'time' => $interval->format('%H:%I:%S'),
                ]);
            }

            // se eliminan conexiones caducadas
            $deletedRows = Connection::where('last_time', '<=', $time_min)->delete();

            // se guarda por primera vez o se actualiza conexión del usuario
            if (Connection::where('user_id', Auth::id())->exists()){
                $connection = Connection::where('user_id', Auth::id())->first();
                $connection->last_time = $current_time;
                $connection->save();
            }
            else {
                $location = GeoIP::getLocation();
                Connection::create([
                    'user_id' => Auth::id(),
                    'ip_address' => $this->getIP(),
                    'country' => $location['country'],
                    'last_time' => $current_time,
                ]);
            }
        }
    }

    private function getIP()
    {
        if (getenv('HTTP_CLIENT_IP')){
            $ip = getenv('HTTP_CLIENT_IP');
        }
        elseif (getenv('HTTP_X_FORWARDED_FOR')){
            $ip = getenv('HTTP_X_FORWARDED_FOR');
        }
        elseif (getenv('HTTP_X_FORWARDED')){
            $ip = getenv('HTTP_X_FORWARDED');
        }
        elseif (getenv('HTTP_FORWARDED_FOR')){
            $ip = getenv('HTTP_FORWARDED_FOR');
        }
        elseif (getenv('HTTP_FORWARDED')){
            $ip = getenv('HTTP_FORWARDED');
        }
        else {
            $ip = $_SERVER["REMOTE_ADDR"];
        }
        return $ip;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: gamg
 * Date: 25/11/16
 * Time: 09:31 PM
 */

namespace Edifalia\Services;

use Edifalia\Models\Tag;

class Tags
{
    public function get()
    {
        $tags[''] = trans('dashboard.fields.select_tag');
        foreach (Tag::get() as $tag){
            $tags[$tag->id] = $tag->name;
        }
        return $tags;
    }
}
<?php

namespace Edifalia\Services;

use Edifalia\Models\NumberFormat;

class NumberFormats
{
    public function get()
    {
        $formats[''] = trans('dashboard.fields.select_number_format');

        foreach (NumberFormat::get() as $format){
            $formats[$format->id] = $format->format;
        }
        return $formats;
    }
}
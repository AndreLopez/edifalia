<?php

namespace Edifalia\Services;

class PostalCodes {

    public function get()
    {
        $codes[''] = trans('dashboard.fields.select_postal_code');
        foreach (\DB::table('postal_codes')->select('id', 'code', 'town', 'province')->get() as $code){
            $codes[$code->id] = $code->code.' ('.$code->town.', '.$code->province.')';
        }
        return $codes;
    }
}
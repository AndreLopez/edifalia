<?php

namespace Edifalia\Services;

use Edifalia\Models\Reading;

class Readings
{
    public function get()
    {
        $data = Reading::get();
        if ($data->count()){
            $readings[''] = trans('dashboard.fields.select_reading');
            foreach ($data as $reading){
                $readings[$reading->id] = $reading->title;
            }
        }
        else {
            $readings[''] = trans('dashboard.fields.no_reading');
        }
        return $readings;
    }
}
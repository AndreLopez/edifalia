<?php

namespace Edifalia\Services;

use Edifalia\Models\Budget;

class Budgets
{
    public function get()
    {
        $data = Budget::get();
        if ($data->count()){
            $budgets[''] = trans('dashboard.fields.select_budget');
            foreach ($data as $budget){
                $budgets[$budget->id] = $budget->title;
            }
        }
        else {
            $budgets[''] = trans('dashboard.fields.no_budget');
        }
        return $budgets;
    }
}
<?php

namespace Edifalia\Services;

use Edifalia\Models\Currency;

class Currencies
{
    public function get()
    {
        $currencies[''] = trans('dashboard.fields.select_currency');
        foreach (Currency::get() as $currency){
            $currencies[$currency->id] = $currency->name;
        }
        return $currencies;
    }
}
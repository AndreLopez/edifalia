<?php

namespace Edifalia\Services;

use Edifalia\Models\Office;

class Offices {

    public function get()
    {
        $offices[''] = trans('dashboard.fields.select_office');

        foreach (Office::get() as $office){
            $offices[$office->id] = $office->name;
        }
        return $offices;
    }
}
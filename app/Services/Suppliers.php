<?php

namespace Edifalia\Services;

use Edifalia\Models\Backend\Supplier;

class Suppliers
{
    public function get()
    {
        $suppliers[''] = trans('dashboard.fields.select_supplier');

        foreach (Supplier::get() as $supplier){
            $suppliers[$supplier->id] = $supplier->tradename;
        }
        return $suppliers;
    }
}
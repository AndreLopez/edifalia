<?php

namespace Edifalia\Services;

use Edifalia\Models\Reception;

class Receptions
{
    public function get()
    {
        $receptions[''] = trans('dashboard.fields.select_reception');

        foreach (Reception::get() as $reception){
            $receptions[$reception->id] = $reception->name;
        }
        return $receptions;
    }
}
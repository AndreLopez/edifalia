<?php

namespace Edifalia\Services;

use Edifalia\Models\InvoicingConcept;

class InvoicingConcepts
{
    public function get()
    {
        $invoicingConcepts[''] = trans('dashboard.fields.select_invoicing_concept');
        foreach (InvoicingConcept::get() as $invoicingConcept){
            $invoicingConcepts[$invoicingConcept->id] = $invoicingConcept->name;
        }
        return $invoicingConcepts;
    }
}
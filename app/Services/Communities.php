<?php

namespace Edifalia\Services;

use Edifalia\Models\Backend\Community;
use Illuminate\Support\Facades\Auth;

class Communities {

    public function all()
    {
        return $this->getCommunities(Community::get());
    }

    public function get($office_id = null)
    {
        if (is_null($office_id)){
            $communities = Community::whereNull('office_id')->get();
        }
        else{
            $communities = Community::whereNull('office_id')
                ->orWhere('office_id', $office_id)->get();
        }
        return $communities;
    }

    public function ofEmployee()
    {
        $employee = Auth::user()->employee;
        return $this->getCommunities($employee->communities);
    }

    private function getCommunities($data)
    {
        $communities[''] = trans('dashboard.fields.select_community');
        foreach ($data as $community){
            $communities[$community->id] = $community->name;
        }
        return $communities;
    }
}
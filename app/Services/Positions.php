<?php

namespace Edifalia\Services;

use Edifalia\Models\Position;

class Positions {

    public function get()
    {
        $positions[''] = trans('dashboard.fields.select_position');
        foreach (Position::get() as $position){
            $positions[$position->id] = $position->name;
        }
        return $positions;
    }
}
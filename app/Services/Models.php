<?php

namespace Edifalia\Services;

use Edifalia\Models\ManagementModel;

class Models
{
    public function get()
    {
        $models[''] = trans('dashboard.fields.select_model');
        foreach (ManagementModel::get() as $model){
            $models[$model->id] = $model->name;
        }
        return $models;
    }
}
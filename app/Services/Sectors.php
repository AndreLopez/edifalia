<?php

namespace Edifalia\Services;

use Edifalia\Models\Sector;

class Sectors
{
    public function get()
    {
        $sectors[''] = trans('dashboard.fields.select_sector');

        foreach (Sector::get() as $sector){
            $sectors[$sector->id] = $sector->name;
        }
        return $sectors;
    }
}
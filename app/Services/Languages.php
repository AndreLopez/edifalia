<?php

namespace Edifalia\Services;

use Edifalia\Models\Common\Language;

class Languages
{
    public function get()
    {
        $languages[''] = trans('dashboard.fields.select_language');
        foreach (Language::get() as $language){
            $languages[$language->id] = $language->name;
        }
        return $languages;
    }
}
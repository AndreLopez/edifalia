<?php

namespace Edifalia\Services;

use Edifalia\Models\CommunicatorType;

class CommunicatorTypes
{
    public function get()
    {
        $types[''] = trans('dashboard.fields.select_communicator_type');

        foreach (CommunicatorType::get() as $type){
            $types[$type->id] = $type->name;
        }
        return $types;
    }
}
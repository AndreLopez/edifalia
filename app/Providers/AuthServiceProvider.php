<?php

namespace Edifalia\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'Edifalia\Models\IssueReceipt' => 'Edifalia\Policies\IssueReceiptPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('has', function ($user, $user_id) {
            return ($user->id == $user_id || $user->isAdmin());
        });

        Gate::define('admin', function ($user) {
            return $user->isAdmin();
        });

        Gate::define('employee', function ($user) {
            return $user->isEmployee();
        });

        Gate::define('owner', function ($user) {
            return $user->isOwner();
        });

        Gate::define('supplier', function ($user) {
            return $user->isSupplier();
        });

        // For user access
        Gate::define('access_data', function ($user) {
            return $user->employee->access_data;
        });
        Gate::define('access_management', function ($user) {
            return $user->employee->access_management;
        });
        Gate::define('access_mail', function ($user) {
            return $user->employee->access_mail;
        });
        Gate::define('access_reports', function ($user) {
            return $user->employee->access_reports;
        });
        Gate::define('access_accounting', function ($user) {
            return $user->employee->access_accounting;
        });
    }
}

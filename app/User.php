<?php

namespace Edifalia;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;
use Edifalia\Notifications\ResetPasswordEmail;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use RevisionableTrait;

    protected $dates = ['deleted_at'];

    protected $revisionCreationsEnabled = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'name', 'email', 'password', 'active',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($value){
        if(!empty($value)){
            $this->attributes['password'] = \Hash::make($value);
        }
    }

    /**
     * Main user record associated with the user.
     */
    public function mainUser()
    {
        return $this->hasOne('Edifalia\Models\MainUser');
    }

    /**
     * Employee record associated with the user.
     */
    public function employee()
    {
        return $this->hasOne('Edifalia\Models\Employee');
    }

    /**
     * owner(person) record associated with the user.
     */
    public function owner()
    {
        return $this->hasOne('Edifalia\Models\Backend\Person', 'user_id');
    }

    /**
     * Supplier record associated with the user.
     */
    public function supplier()
    {
        return $this->hasOne('Edifalia\Models\Supplier');
    }

    /**
     * Connection record associated with the user.
     */
    public function connection()
    {
        return $this->hasOne('Edifalia\Models\Connection');
    }

    /**
     * Message (emails received) associated with the user.
     */

    public function receivedMessages()
    {
        return $this->belongsToMany('Edifalia\Models\Common\Message', 
            'message_recipent','user_id', 'message_id')
                    ->withPivot('read')
                    ->withTimestamps();
    }

    /**
     *  Message (emails send) associated with the user.
     */

    public function sentMessages()
    {
        return $this->hasOne('Edifalia\Models\Common\Message');
    }

    /**
     * Get the issueReceipts for the user.
     */
    public function issueReceipts()
    {
        return $this->hasMany('Edifalia\Models\IssueReceipt');
    }

    /**
     * Get the receiptsToBanks for the user.
     */
    public function receiptsToBanks()
    {
        return $this->hasMany('Edifalia\Models\ReceiptsToBank');
    }

    /**
     * Get the charges for the user.
     */
    public function charges()
    {
        return $this->hasMany('Edifalia\Models\Charge');
    }

    /**
     * Get the documents for the user.
     */
    public function documents()
    {
        return $this->hasMany('Edifalia\Models\Document');
    }

    /**
     * Get the templates for the user.
     */
    public function templates()
    {
        return $this->hasMany('Edifalia\Models\Template');
    }

    /**
     * Get the incidences for the user.
     */
    public function incidences()
    {
        return $this->hasMany('Edifalia\Models\Incidence');
    }

    /**
     * Get the budgets for the user.
     */
    public function budgets()
    {
        return $this->hasMany('Edifalia\Models\Budget');
    }

    /**
     * Get the readings for the user.
     */
    public function readings()
    {
        return $this->hasMany('Edifalia\Models\Reading');
    }

    /**
     * Verify if the user is an admin
     */
    public function isAdmin()
    {
        return ($this->type == 0) ? true : false;
    }

    /**
     * Verify if the user is an employee
     */
    public function isEmployee()
    {
        return ($this->type == 1) ? true : false;
    }

    /**
     * Verify if the user is an Owner
     */
    public function isOwner()
    {
        return ($this->type == 2) ? true : false;
    }

    /**
     * Verify if the user is an supplier
     */
    public function isSupplier()
    {
        return ($this->type == 3) ? true : false;
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordEmail($token));
    }
}

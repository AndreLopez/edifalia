<?php

namespace Edifalia\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;
use Carbon\Carbon;

class Reading extends Model
{
    use SoftDeletes;
    use RevisionableTrait;

    protected $revisionCreationsEnabled = true;

    protected $fillable = [
        'title',
        'community_id',
        'invoicing_concept_id',
        'rate_id',
        'periodicity',
        'month',
        'year',
        'concept_receipt',
        'date',
    ];

    protected $dates = ['date', 'deleted_at'];

    /**
     *  Date Mutator
     */
    public function setDateAttribute($value)
    {
        $this->attributes['date'] = Carbon::createFromFormat('m/d/Y', $value)->format('Y-m-d');
    }

    /**
     * Publication date Accessor
     */
    public function getDateAttribute($value)
    {
        return Carbon::createFromFormat('Y-m-d', $value)->format('m/d/Y');
    }

    /**
     * Get the user that owns the reading.
     */
    public function user()
    {
        return $this->belongsTo('Edifalia\User');
    }

    public function community()
    {
        return $this->belongsTo('Edifalia\Models\Backend\Community');
    }

    public function invoicingConcept()
    {
        return $this->belongsTo(InvoicingConcept::class);
    }

    public function rate()
    {
        return $this->belongsTo(Rate::class);
    }

    public function properties()
    {
        return $this->belongsToMany('Edifalia\Models\Common\Property', 'property_reading')
            ->withPivot('previous', 'current', 'consumption');
    }
}

<?php

namespace Edifalia\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;
use Carbon\Carbon;

class IssueReceipt extends Model
{
    use SoftDeletes;
    use RevisionableTrait;

    protected $revisionCreationsEnabled = false;

    protected $fillable = ['date', 'message'];

    protected $dates = ['deleted_at'];

    /**
     * Publication date Mutator
     */
    public function setDateAttribute($value)
    {
        $this->attributes['date'] = Carbon::createFromFormat('m/d/Y', $value)->format('Y-m-d');
    }

    /**
     * Publication date Accessor
     */
    public function getDateAttribute($value)
    {
        return Carbon::createFromFormat('Y-m-d', $value)->format('m/d/Y');
    }

    /**
     * The communities that belong to the IssueReceipt.
     */
    public function communities()
    {
        return $this->belongsToMany('Edifalia\Models\Backend\Community')->withTimestamps();
    }

    /**
     * The InvoicingConcepts that belong to the IssueReceipt.
     */
    public function invoicingConcepts()
    {
        return $this->belongsToMany('Edifalia\Models\InvoicingConcept')->withTimestamps();
    }

    /**
     * Get the user that owns the IssueReceipt.
     */
    public function user()
    {
        return $this->belongsTo('Edifalia\User');
    }

    /**
     * Get the issueReceiptPdfs for the IssueReceipt.
     */
    public function issueReceiptPdfs()
    {
        return $this->hasMany('Edifalia\Models\IssueReceiptPdf');
    }

    /*public function communitySaved($id)
    {
        return $this->communities()->where('community_id', $id)->exists();
    }*/

    /*public function invoicingConceptSaved($id)
    {
        return $this->invoicingConcepts()->where('invoicing_concept_id', $id)->exists();
    }*/
}

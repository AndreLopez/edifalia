<?php

namespace Edifalia\Models;

use Illuminate\Database\Eloquent\Model;
use \Venturecraft\Revisionable\RevisionableTrait;

class IssueReceiptPdf extends Model
{
    use RevisionableTrait;

    protected $revisionCreationsEnabled = true;

    protected $fillable = ['property_id', 'pdf_name'];

    /**
     * Get the property that owns the IssueReceiptPdf.
     */
    public function issueReceipt()
    {
        return $this->belongsTo('Edifalia\Models\IssueReceipt');
    }
}

<?php 

namespace Edifalia\Models\Pdf;

use PDF;

class EdifaliaPDF extends PDF {

    private static $title;    

    public static function SetTitle($text){
        static::$title = $text;
    }

    //Page header
    public static function Header() {
        // // Set Image
        // $image = public_path().'/static/images/logo-horizontal.png';
        // PDF::Image($image,
        //     $x = '10',
        //     $y = '7',
        //     $w = 30,
        //     $h = 0,
        //     $type = 'PNG',
        //     $link = '',
        //     $align = 'N',
        //     $resize = false,
        //     $dpi = 300,
        //     $palign = 'L',
        //     $ismask = false,
        //     $imgmask = false,
        //     $border = 0,
        //     $fitbox = false,
        //     $hidden = false,
        //     $fitonpage = false,
        //     $alt = false,
        //     $altimgs = array());

        // Custom Header
        PDF::setHeaderCallback(function($pdf) {
            // Set Padding Y
            $pdf->SetY(21);
            // Set font
            $pdf->SetFont('helvetica', 'B', 20);
            // Title
            $pdf->Cell(0, 15, static::$title, 0, false, 'C', 0, '', 0, false, 'M', 'M');
        });
    }

    public static function Footer(){
        // Custom Footer
        PDF::setFooterCallback(function($pdf) {
            // Position at 15 mm from bottom
            $pdf->SetY(-15);
            // Set font
            $pdf->SetFont('helvetica', 'I', 8);
            // Page number
            $pdf->Cell(0, 10, 'Pagina '.$pdf->getAliasNumPage().'/'.$pdf->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        });
    }
}

<?php

namespace Edifalia\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MeetingVoting extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'description',
        'voting',
        'meeting_id',
        'agreement'        
    ];

    protected $dates = ['deleted_at'];   
}

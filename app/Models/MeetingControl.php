<?php

namespace Edifalia\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MeetingControl extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'title',
        'person_id'        
    ];

    protected $dates = ['deleted_at'];   
}

<?php

namespace Edifalia\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;

class MainUser extends Model
{
    use SoftDeletes;
    use RevisionableTrait;

    protected $revisionCreationsEnabled = true;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        //'user_id',
        'company',
        'first_name',
        'last_name',
        'dni',
        'address',
        'postal_code_id',
        'phone',
        'fax',
        'signature',
        'ip_list_option',
        'collegiate_number',
        'aaff_program',
        'aaff_email',
        'letterhead',
        'language_id',
        'currency_id',
        'number_format_id',
        'date_format_id',
        'continent_id',
        'zone_id',
        'coordinates',
    ];

    /**
     * Get the amounts value.
     *
     * @return string
     */
    public function getAmountsAttribute()
    {
        $currencySymbol = ($this->currency()->exists()) ? $this->currency->symbol : '';
        $numberFormat = ($this->numberFormat()->exists())? $this->numberFormat->format : '';
        return $numberFormat.' '.$currencySymbol;
    }

    /**
     * User record associated with the main_user.
     */
    public function user()
    {
        return $this->belongsTo('Edifalia\User');
    }

    /**
     * currency record associated with the main_user.
     */
    public function currency()
    {
        return $this->belongsTo('Edifalia\Models\Currency');
    }

    /**
     * number format record associated with the main_user.
     */
    public function numberFormat()
    {
        return $this->belongsTo('Edifalia\Models\NumberFormat');
    }

    /**
     * Get the ipAccess for main user.
     */
    public function ipAccesses()
    {
        return $this->hasMany('Edifalia\Models\IpAccess');
    }
}

<?php

namespace Edifalia\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;

class Zone extends Model
{
    use SoftDeletes;
    use RevisionableTrait;

    protected $revisionCreationsEnabled = true;

    protected $fillable = [ 'name', 'continent_id'];

    protected $dates = ['deleted_at'];

    public function continent()
    {
        return $this->belongsTo(Continent::class);
    }
}

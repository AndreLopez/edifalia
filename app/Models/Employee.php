<?php

namespace Edifalia\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;

class Employee extends Model
{
    use SoftDeletes;
    use RevisionableTrait;

    protected $revisionCreationsEnabled = true;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'dni',
        'department',
        'phone',
        'extension',
        'position_id',
        'office_id',
        'access_data',
        'access_management',
        'access_mail',
        'access_incidents',
        'access_reports',
        'access_accounting',
        'signature',
    ];


    /**
     * Get the position that owns the employee.
     */
    public function position()
    {
        return $this->belongsTo(Position::class);
    }

    /**
     * Get the office that owns the employee.
     */
    public function office()
    {
        return $this->belongsTo(Office::class);
    }

    /**
     * User record associated with the employee.
     */
    public function user()
    {
        return $this->belongsTo('Edifalia\User');
    }

    /**
     * The communities that belong to the employee.
     */
    public function communities()
    {
        return $this->belongsToMany('Edifalia\Models\Backend\Community');
    }

    /**
     * synchronize permissions
     */
    public function synchronizePermissions($request)
    {
        if ($this->access_data){
            if(is_null($request->access_data))
                $this->access_data = false;
        }
        if ($this->access_management){
            if(is_null($request->access_management))
                $this->access_management = false;
        }
        if ($this->access_mail){
            if(is_null($request->access_mail))
                $this->access_mail = false;
        }
        if ($this->access_incidents){
            if(is_null($request->access_incidents))
                $this->access_incidents = false;
        }
        if ($this->access_reports){
            if(is_null($request->access_reports))
                $this->access_reports = false;
        }
        if ($this->access_accounting){
            if(is_null($request->access_accounting))
                $this->access_accounting = false;
        }
    }

    public static function filterEmployees($name)
    {
        return Employee::name($name)->paginate(10);
    }

    public function scopeName($query, $name)
    {
        if (trim($name) != ""){
            $query->where('first_name', "LIKE", "%$name%");
        }
    }
}
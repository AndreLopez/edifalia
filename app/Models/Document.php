<?php

namespace Edifalia\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;
use Carbon\Carbon;

class Document extends Model
{
    use SoftDeletes;
    use RevisionableTrait;

    protected $revisionCreationsEnabled = true;

    protected $dates = ['publication_date', 'created_at', 'deleted_at'];

    protected $fillable = [
        'community_id',
        //'document',
        'title',
        'publication_date',
        'page_id',
        'tag_id',
    ];

    /**
     * Publication date Mutator
     */
    public function setPublicationDateAttribute($value)
    {
        $this->attributes['publication_date'] = Carbon::createFromFormat('m/d/Y', $value)->format('Y-m-d');
    }

    /**
     * Publication date Accessor
     */
    public function getPublicationDateAttribute($value)
    {
        return Carbon::createFromFormat('Y-m-d', $value)->format('m/d/Y');
    }

    /**
     * Get the community that owns the document.
     */
    public function community()
    {
        return $this->belongsTo('Edifalia\Models\Backend\Community');
    }

    /**
     * Get the page that owns the document.
     */
    public function page()
    {
        return $this->belongsTo(Page::class);
    }

    /**
     * Get the tag that owns the document.
     */
    public function tag()
    {
        return $this->belongsTo(Tag::class);
    }

    /**
     * Get the user that owns the document.
     */
    public function user()
    {
        return $this->belongsTo('Edifalia\User');
    }

    /**
     * The persons that belong to the document.
     */
    public function persons()
    {
        return $this->belongsToMany('Edifalia\Models\Backend\Person');
    }

    public function existsFile()
    {
        return \Storage::disk('local_documents')->exists($this->document);
    }

    public function personSaved($id)
    {
        return $this->persons()->where('person_id', $id)->exists();
    }
}

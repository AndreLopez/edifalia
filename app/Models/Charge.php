<?php

namespace Edifalia\Models;

use Edifalia\Models\Common\Property;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;
use Illuminate\Support\Facades\Auth;

class Charge extends Model
{
    use SoftDeletes;
    use RevisionableTrait;

    protected $revisionCreationsEnabled = false;

    protected $fillable = ['community_id', 'property_id', 'issue_receipt_id', 'pending', 'charge_type'];

    protected $dates = ['deleted_at'];

    /**
     * Get the user that owns the Charge.
     */
    public function user()
    {
        return $this->belongsTo('Edifalia\User');
    }

    /**
     * Get the community that owns the Charge.
     */
    public function community()
    {
        return $this->belongsTo('Edifalia\Models\Backend\Community');
    }

    /**
     * Get the property that owns the Charge.
     */
    public function property()
    {
        return $this->belongsTo('Edifalia\Models\Common\Property');
    }

    /**
     * Get the issueReceipt that owns the Charge.
     */
    public function issueReceipt()
    {
        return $this->belongsTo('Edifalia\Models\IssueReceipt');
    }

    /**
     * full name Accessor
     */
    public function getOwnerFullNameAttribute()
    {
        return $this->property->person->first_name.' '.$this->property->person->last_name;
    }

    /**
     * State Accessor
     */
    public function getStateAttribute()
    {
        return ($this->attributes['pending'] <= 0) ? "check" : "close";
    }

    /**
     * State class Accessor
     */
    public function getStateClassAttribute()
    {
        return ($this->attributes['pending'] <= 0) ? "success" : "danger";
    }

    /**
     * amount Accessor
     */
    public function getAmountAttribute()
    {
        $property = Property::find($this->attributes['property_id']);
        $receiptDues = $property->receiptDues()->where('issue_receipt_id', $this->attributes['issue_receipt_id'])->get();
        $sum = 0;
        foreach ($receiptDues as $receiptDue) {
            $sum += $receiptDue->pivot->amount;
        }
        return $sum;
    }

    //// Filters
    public static function filterCharges($searchFields, $paginate = true, $pagesNumber = 10)
    {
        if (empty($searchFields)) {
            if (Auth::user()->can('admin'))
                return ($paginate) ? Charge::paginate($pagesNumber) : Charge::all();
            else
                return ($paginate) ? Auth::user()->charges()->paginate($pagesNumber) : Auth::user()->charges()->get();
        }

        if (Auth::user()->can('admin')){
            $q = Charge::state($searchFields)->community($searchFields['community'])
                ->date($searchFields);
        } else {
            $q = Charge::where('user_id', Auth::user()->id)
                ->state($searchFields)
                ->community($searchFields['community'])
                ->date($searchFields);
        }

        return ($paginate) ? $q->paginate($pagesNumber) : $q->get();
    }

    public function scopeState($query, $searchFields)
    {
        $state_value = (!empty($searchFields['state'])) ? $searchFields['state'] : "";
        if ($state_value == 1){
            $query->where('pending', '<=', '0');
        } else if($state_value == 2) {
            $query->where('pending', '>', '0');
        }
    }

    public function scopeCommunity($query, $community_id)
    {
        if (!empty($community_id) && is_numeric(trim($community_id))){
            $query->where('community_id', $community_id);
        }
    }

    public function scopeDate($query, $searchFields)
    {
        if (!empty($searchFields['date_from']) || !empty($searchFields['date_until'])){
            $date_from =  (!empty($searchFields['date_from'])) ? $searchFields['date_from'] : "";
            $date_until =  (!empty($searchFields['date_until'])) ? $searchFields['date_until'] : "";
            $chargeIds = $this->getChargeIds($date_from, $date_until);
            $query->whereIn('id', $chargeIds);
        }
    }

    private function getChargeIds($date_from, $date_until)
    {
        $chargeIds = [];
        if (!empty($date_from) || !empty($date_until)){
            $charges = (Auth::user()->can('admin')) ? Charge::all() : Auth::user()->charges()->get();
            foreach ($charges as $charge){
                if (!empty($date_from) && !empty($date_until)){
                    if ($charge->issueReceipt->date >= $date_from && $charge->issueReceipt->date <= $date_until)
                        $chargeIds[] = $charge->id;
                } elseif (!empty($date_from) && empty($date_until)) {
                    if ($charge->issueReceipt->date >= $date_from)
                        $chargeIds[] = $charge->id;
                } elseif (empty($date_from) && !empty($date_until)) {
                    if ($charge->issueReceipt->date <= $date_until)
                        $chargeIds[] = $charge->id;
                }
            }
        }
        return $chargeIds;
    }
}

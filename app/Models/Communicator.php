<?php

namespace Edifalia\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;

class Communicator extends Model
{
    use SoftDeletes;
    use RevisionableTrait;

    protected $revisionCreationsEnabled = true;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'type_id',
        'name',
        'last_name',
        'address',
        'email',
        'phone',
        'other_phone',
    ];

    /**
     * Get the incidence record associated with the Communicator.
     */
    public function incidence()
    {
        return $this->hasOne(Incidence::class);
    }

    /**
     * Get the type that owns the communicator.
     */
    public function type()
    {
        return $this->belongsTo(CommunicatorType::class);
    }
}

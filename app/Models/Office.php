<?php

namespace Edifalia\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;

class Office extends Model
{
    use SoftDeletes;
    use RevisionableTrait;

    protected $revisionCreationsEnabled = true;

    protected $fillable = [
        'identifier',
        'name',
        'address',
        'postal_code_id',
        'phone1',
        'phone2',
        'fax',
        'email',
    ];

    protected $dates = ['deleted_at'];

    public function communities()
    {   // una oficina puede tener muchas comunidades
        return $this->hasMany('Edifalia\Models\Backend\Community');
    }

    public function postalCode()
    {
        return $this->belongsTo('Edifalia\Models\Common\PostalCode');
    }

    public static function filterOffices($name)
    {
        return Office::name($name)->paginate(10);
    }

    public function scopeName($query, $name)
    {
        if (trim($name) != ""){
            $query->where('name', "LIKE", "%$name%");
        }
    }
}

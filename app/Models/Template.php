<?php

namespace Edifalia\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;

class Template extends Model
{
    use SoftDeletes;
    use RevisionableTrait;

    protected $revisionCreationsEnabled = true;

    protected $fillable = ['title', 'model_id'];

    protected $dates = ['deleted_at'];

    /**
     * Get the user that owns the template.
     */
    public function user()
    {
        return $this->belongsTo('Edifalia\User');
    }

    /**
     * Get the model that owns the template.
     */
    public function model()
    {
        return $this->belongsTo(ManagementModel::class);
    }

    /**
     * Get the office that owns the template.
     */
    public function office()
    {
        return $this->belongsTo(Office::class);
    }

    public function existsFile()
    {
        return \Storage::disk('local_templates')->exists($this->file);
    }
}

<?php

namespace Edifalia\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Query\Builder;


class ReceiptDue extends Model
{
    use SoftDeletes;
    use RevisionableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'community_id',
        'invoicing_concept_id',
        'total_amount',
        'month',
        'year'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * prevent timestamps
     * @var boolean
     */
    public $timestamps = false;

    /**
     * All receipt dues records
     * 
     * @return array
     */
    protected function receipt_due_all()
    {
        return DB::select(
            'SELECT receipt_dues.*, invoicing_concepts.name
            FROM receipt_dues, invoicing_concepts
            WHERE receipt_dues.invoicing_concept_id = invoicing_concepts.id');
    }

    /**
     * Get all communities
     * 
     * @return array
     */
    protected function get_communities()
    {
        return DB::table('community_employee')
                ->join('communities', 'communities.id', '=', 'community_employee.community_id')
                ->join('employees', 'employees.id', '=', 'community_employee.employee_id')
                ->join('users', 'users.id', '=', 'employees.user_id')
                ->select('communities.name', 'communities.id')
                ->where('users.id', Auth::id())
                ->get();
    }

    /**
     * Get record of a receipt due
     * 
     * @param  int $id
     * @return array
     */
    protected function get_receipt_due($id)
    {
        return DB::select(
                'SELECT receipt_dues.*, communities.name AS community_name, invoicing_concepts.name AS invoicing_concept_name
                FROM receipt_dues, communities, invoicing_concepts
                WHERE receipt_dues.invoicing_concept_id = invoicing_concepts.id
                AND receipt_dues.community_id = communities.id
                AND receipt_dues.id =  ?', [$id]);
    }

    /**
     * Get properties of a community
     * 
     * @param  int $user
     * @param  int $id
     * @return array
     */
    protected function get_properties($user, $id)
    {
        return DB::table(DB::raw('properties, persons, communities'))
                    ->join('community_employee', 'community_employee.community_id', '=', 'communities.id')
                    ->join('employees', 'employees.id', '=', 'community_employee.employee_id')
                    ->join('users', 'users.id', '=', 'employees.user_id')
                    ->select('properties.*', 'persons.first_name', 'persons.last_name')
                    ->where('users.id', $user)
                    ->where('persons.user_id', $user)
                    ->where('properties.community_id', $id)
                    ->get();
    }

    /**
     * The properties that belong to the ReceiptDue.
     */
    public function properties()
    {
        return $this->belongsToMany('Edifalia\Models\Common\Property', 'receipt_due_items')
            ->withPivot('amount');
    }

    /**
     * Get the invoicingConcept that owns the ReceiptDue.
     */
    public function invoicingConcept()
    {
        return $this->belongsTo('Edifalia\Models\InvoicingConcept');
    }
}

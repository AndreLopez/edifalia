<?php

namespace Edifalia\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;

class CommunicatorType extends Model
{
    use SoftDeletes;
    use RevisionableTrait;

    protected $table = "communicator_types";

    protected $revisionCreationsEnabled = true;

    protected $dates = ['deleted_at'];

    protected $fillable = ['name'];
}

<?php

namespace Edifalia\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class CommunityBankAccount extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['bank_account_id', 'community_id'];
}

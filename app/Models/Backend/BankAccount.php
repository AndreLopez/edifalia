<?php

namespace Edifalia\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;

class BankAccount extends Model
{
    use SoftDeletes;
    use RevisionableTrait;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'iban', 
        'code_a', 
        'code_b', 
        'code_c', 
        'code_d', 
        'code_e', 
        'sufix', 
        'bic', 
        'accounting_account_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     *  Relationships
    */
    public function accountingAccount()
    {
        return $this->belongsTo('Edifalia\Models\Backend\AccountingAccount', 'accounting_account_id');
    }


}
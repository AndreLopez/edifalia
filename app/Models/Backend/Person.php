<?php

namespace Edifalia\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;
use Edifalia\Models\SearchTrait;
class Person extends Model
{
    use SoftDeletes;
    use RevisionableTrait;
    use SearchTrait;

    protected $revisionCreationsEnabled = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'persons';

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'first_name',
       'last_name',
       'address',
       'dni',
       'reference',
       'strength_language',
       'message_email_only',
       'group_properties',
       'group_invoice_concepts',
       'dont_get_email',
       'can_update_own_data',
       'exclusiveness',
       'virtual_office_access',
       'update_data_from_virtual_office',
       'notes',
       'postal_code_id',
       'language_id',
       'payment_method_id',
       'user_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

     /**
     * The fields to filter items in searches.
     *
     * @var array
    */
    protected $filters = [
      'invoicing_concept_ids', 
      'people_without_property',
      'building_number',
      'language_ids',
      'message_email_only',
      'community_ids'
    ];

    /**
     * Relationships
    */

    public function accountingAccounts()
    {
      return $this->belongsToMany('Edifalia\Models\Backend\AccountingAccount', 
        'accounting_account_person')
          ->withTimestamps();
    }

     public function bankAccounts()
    {
      return $this->belongsToMany('Edifalia\Models\Backend\BankAccount', 
        'bank_account_person')
          ->withTimestamps();
    }

    public function properties()
    {
        return $this->hasMany('Edifalia\Models\Common\Property', 'person_id');
    }

    public function user()
    {
      return  $this->belongsTo('Edifalia\User');
    }


    /**
     *  Accessors
    */
    public function getFullNameAttribute()
    {
      return $this->first_name .' '. $this->last_name;
    }


    public function scopeFilterFullname($query, $string)
    {
        return $query->where('first_name', 'LIKE', '%' . $string . '%')
                      ->orWhere('last_name', 'LIKE', '%' . $string . '%');
    }
    
    /**
     * The documents that belong to the person.
     */
    public function documents()
    {
        return $this->belongsToMany('Edifalia\Models\Document');
    }

    public function scopeLanguageIds($query, $data = array())
    {
      $query->whereIn('language_id', $data['language_ids']);
    }

    public function scopeCommunityIds($query, $data = array())
    {
      $query->join('properties', 'persons.id', '=', 'properties.person_id')
        ->join('communities', 'properties.community_id', '=', 'communities.id')
        ->whereIn('communities.id', $data['community_ids'])
        ->select('persons.*');
    }

    public function scopePeopleWithoutProperty($query, $data = array())
    {
      $query->doesntHave('properties');
    }

    public function scopeMessageEmailOnly($query, $data = array())
    {
      $query->where('message_email_only', '=', $data['message_email_only']);
    }

    public function scopeBuildingNumber($query, $data = array())
    {
      $query->whereHas('properties', function ($q) use ($data){
        $q->where('building_number', '=', $data['building_number']);
      });
    }

    public function scopeInvoicingConceptIds($query, $data = array())
    {
      $query->join('properties', 'persons.id', '=', 'properties.person_id')
        ->join('budget_property', 'properties.id', '=', 'budget_property.property_id')
        ->join('budgets', 'budget_property.budget_id', '=', 'budgets.id')
        ->whereIn('budgets.community_id', $data['community_ids'])
        ->whereIn('budgets.invoicing_concept_id', $data['invoicing_concept_ids'])
        ->select('persons.*');
    }

    /**
     * Scopes
      invoicingConceptIds
      peopleWithoutProperty
      buildingNumber
      languageIds
      messageEmailOnly
      communityIds

     */

}
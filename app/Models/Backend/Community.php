<?php

namespace Edifalia\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;

class Community extends Model
{
	use SoftDeletes;
    use RevisionableTrait;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 
        'name', 
        'address', 
        'nif', 
        'structure_year', 
        'elevator_numbers', 
        'platform_accounting',
        'longitude',
        'latitude',
        'notes', 
        'status_id', 
        'postal_code_id', 
        'president_id', 
        'banck_account_id',
        'accounting_account_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function hasProperties()
    {
        return count($this->properties) > 0;
    }

    public function office()
    {
        return $this->belongsTo(Offices::class);
    }

    public function hasOffice($office_id)
    {
        return ($this->office_id == $office_id) ? true : null;
    }

    public function getCodeNameAttribute(){
        return $this->code." - ".$this->name;
    }

    public function bankAccounts()
    {
        return $this->belongsToMany(
            'Edifalia\Models\Common\BankAccount', 'community_bank_accounts', 'community_id', 'bank_account_id'
        )->withTimestamps();
    }

    public function accountingAccounts()
    {
        return $this->belongsToMany(
            'Edifalia\Models\Common\AccountingAccount', 'community_accounting_accounts', 
            'community_id', 'accounting_account_id'
        )->withTimestamps();
    }

    public function properties()
    {
        return $this->hasMany('Edifalia\Models\Common\Property', 'community_id');
    }

    /**
     * The IssueReceipts that belong to the community.
     */
    public function issueReceipts()
    {
        return $this->belongsToMany('Edifalia\Models\IssueReceipt');
    }

    /**
     * Get the receiptDues for the Community.
     */
    public function receiptDues()
    {
        return $this->hasMany('Edifalia\Models\ReceiptDue');
    }
}

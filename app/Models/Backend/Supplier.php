<?php

namespace Edifalia\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;

class Supplier extends Model
{
    use SoftDeletes;
    use RevisionableTrait;

    protected $revisionCreationsEnabled = true;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'suppliers'; 



    protected $fillable = [
        'cif', 'business_name','tradename','address',
        'website','contact_name','update_data_from_virtual_office','virtual_office','notes','postal_code_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

     public function bankAccounts()
    {
       return $this->belongsToMany('Edifalia\Models\Backend\BankAccount', 
        'supplier_bank_accounts')
          ->withTimestamps();
    }

    public function SupplierActivities ()
    {
        return $this->belongsToMany('Edifalia\Models\Common\SupplierActivitie','supplier_activities')
        ->withTimestamps(); 
    }
    
}

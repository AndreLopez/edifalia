<?php

namespace Edifalia\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;

class AccountingAccount extends Model
{
    use SoftDeletes;
    use RevisionableTrait;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'title'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     *  Relationships
    */

    public function bankAccount()
    {
        return $this->hasOne('Edifalia\Models\Backend\BankAccount', 'accounting_account_id');
    }

    public function hasBankAccount()
    {
        return count($this->bankAccount) > 0;
    }
}

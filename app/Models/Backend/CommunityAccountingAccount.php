<?php

namespace Edifalia\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class CommunityAccountingAccount extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['accounting_account_id', 'community_id'];
}

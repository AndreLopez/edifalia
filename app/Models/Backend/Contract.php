<?php

namespace Edifalia\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contract extends Model
{
    use SoftDeletes;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reference','description','jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec','amount','minimum','maximum','contract_file','publish_virtual_office','contract_file','publish_virtual_office','contract_date','duration','duration_type','observations','accounting_account_id','community_id','supplier_id','payment_method_id','bank_account_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
 public function bankAccounts()
    {
        return $this->belongsToMany(
            'Edifalia\Models\Common\BankAccount', 'community_bank_accounts', 'community_id', 'bank_account_id'
        )->withTimestamps();
    }

    public function accountingAccounts()
    {
        return $this->belongsToMany(
            'Edifalia\Models\Common\AccountingAccount', 'community_accounting_accounts', 
            'community_id', 'accounting_account_id'
        )->withTimestamps();
    }
}


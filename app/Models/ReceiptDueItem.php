<?php

namespace Edifalia\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Query\Builder;

class ReceiptDueItem extends Model
{
    use SoftDeletes;
    use RevisionableTrait;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'receipt_due_id',
        'property_id',
        'amount'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * prevent timestamps
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Get all items of a Receipt Due
     * 
     * @param  int $id
     * @param  int $user
     * @return array
     */
    protected function get_receipt_due_items($user, $id)
    {
        return DB::select(
                'SELECT receipt_due_items.*, pro.*, per.first_name, per.last_name
                FROM receipt_due_items, properties AS pro, persons AS per, communities
                JOIN community_employee ON communities.id = community_employee.community_id
                JOIN employees ON community_employee.employee_id = employees.id
                JOIN users ON employees.user_id = users.id
                WHERE receipt_due_items.property_id = pro.id
                AND per.user_id = ?
                AND receipt_due_items.receipt_due_id = ?', [$user, $id]);
    }

    /**
     * Get receipt due of last month
     * 
     * @param  int $user
     * @param  int $concept_id
     * @param  int $receipt_due_id
     * @return array
     */
    protected function get_last_month_records($user, $concept_id, $receipt_due_id)
    {
        return DB::select(
            'SELECT
                receipt_dues.*,
                receipt_due_items.*,
                pro.*,
                per.first_name,
                per.last_name,
                invoicing_concepts.name AS concept,
                communities.name AS community
            FROM
                receipt_dues,
                receipt_due_items,
                properties AS pro,
                persons AS per,
                invoicing_concepts,
                communities
            JOIN community_employee
                ON communities.id = community_employee.community_id
            JOIN employees
                ON community_employee.employee_id = employees.id
            JOIN users
                ON employees.user_id = users.id

            WHERE per.user_id = ?
            AND invoicing_concepts.id = ?
            AND receipt_due_items.receipt_due_id = ?
            AND receipt_due_items.property_id = pro.id
            AND receipt_dues.id = ?',
            [$user, $concept_id, $receipt_due_id, $receipt_due_id]);
    }
}

<?php

namespace Edifalia\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;

class Incidence extends Model
{
    use SoftDeletes;
    use RevisionableTrait;

    protected $revisionCreationsEnabled = true;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'office_id',
        'community_id',
        'communicator_id',
        'priority',
        'reception_id',
        'reason_id',
        'controls',
        'publish',
        'conclude',
        'title',
        'details',
        'send',
        'sector_id',
        'supplier_id',
        'other_annotations',
    ];

    /**
     * Get the user that owns the Charge.
     */
    public function user()
    {
        return $this->belongsTo('Edifalia\User');
    }

    /**
     * Get the office that owns the incidence.
     */
    public function office()
    {
        return $this->belongsTo(Office::class);
    }

    /**
     * Get the community that owns the incidence.
     */
    public function community()
    {
        return $this->belongsTo('Edifalia\Models\Backend\Community');
    }

    /**
     * Get the communicator that owns the incidence.
     * one to one
     */
    public function communicator()
    {
        return $this->belongsTo(Communicator::class);
    }

    /**
     * Get the reception that owns the incidence.
     */
    public function reception()
    {
        return $this->belongsTo(Reception::class);
    }

    /**
     * Get the reason that owns the incidence.
     */
    public function reason()
    {
        return $this->belongsTo(Reason::class);
    }

    /**
     * Get the sector that owns the incidence.
     */
    public function sector()
    {
        return $this->belongsTo(Sector::class);
    }

    /**
     * Get the supplier that owns the incidence.
     */
    public function supplier()
    {
        return $this->belongsTo('Edifalia\Models\Backend\Supplier');
    }

    /**
     * Get the files that owns the incidence.
     */
    public function files()
    {
        return $this->hasMany(IncidenceFile::class);
    }
}

<?php

namespace Edifalia\Models\Common;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;

class Message extends Model
{
	use SoftDeletes;
    use RevisionableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'from',
    	'subject', 
    	'priority', 
    	'body', 
    	'user_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    public function hasFiles()
    {
        return count($this->files) > 0;
    }

    public function countFiles()
    {
        return count($this->files);
    }


    /**
     * Relationships
    */

    public function user()
    {
    	return $this->belongsTo('Edifalia\User');
    }

     /**
     * Message (emails received) associated with the user.
     */

    public function recipients()
    {
        return $this->belongsToMany('Edifalia\User', 
            'message_recipent','message_id', 'user_id')
        ->withPivot('read')
        ->withTimestamps();
    }

    /**
     *  Files associated with the message.
     */
    public function files()
    {
        return $this->hasMany('Edifalia\Models\Common\MessageFile');
    }

    public function getReadAttribute()
    {
        $messageRecipent = $this->recipients()
            ->withTrashed()
            ->whereUserId(\Auth::user()->id)
            ->first();
        return ($messageRecipent->pivot->read ? 
            trans('common.affirmative_answer') : trans('common.negative_answer'));
    }

    public function getReadCounterAttribute()
    {
        return $this->whereHas('recipients', function($q) {
            $q->where('read', '=', true);
        })->count();
    }

}

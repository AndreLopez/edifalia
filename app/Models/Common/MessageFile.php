<?php

namespace Edifalia\Models\Common;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;
use Edifalia\Models\Upload\Upload;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\Storage;

class MessageFile extends Model
{
	use SoftDeletes;
    use RevisionableTrait;
    protected $upload;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'filename',
    	'original_filename', 
    	'mime', 
        'extension',
    	'complete_path', 
    	'message_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Relationships
    */


    public function registerFile(UploadedFile $file, $data = array())
    {
        $this->upload = new Upload($file);
        $this->upload->process();
        $this->create([
            'filename' => $this->upload->getFile()->getFilename(),
            'original_filename' => $this->upload->getFile()->getClientOriginalName(), 
            'mime' => $this->upload->getFile()->getClientMimeType(),
            'extension' => $this->upload->getFile()->getClientOriginalExtension(), 
            'complete_path' => public_path().'/storage', 
            'message_id' => $data['message_id']
        ]);
    }

    public function getMimeTypeAttribute()
    {
         return Storage::mimeType($this->filename.'.'.$this->extension);
    }

    public function getSizeAttribute()
    {
        return Storage::size($this->filename.'.'.$this->extension);
    }

    public function getFilenameExtensionAttribute()
    {
        return $this->filename.'.'.$this->extension;
    }

}

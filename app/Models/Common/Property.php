<?php


namespace Edifalia\Models\Common;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;

class Property extends Model
{
    use SoftDeletes;
    use RevisionableTrait;

    protected $revisionCreationsEnabled = true;

	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'properties';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'building_number', 
    	'property_number', 
    	'registered_property', 
    	'property_type_id',
    	'status',
    	'coefficient',
    	'group_properties',
    	'group_concepts',
    	'person_id',
        'payment_method_id',
        'community_id'
    ];

     /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */

    protected $appends = [
   		'status_format',
        'group_properties_format',
        'group_concepts_format'
    ];

    /**
     * Relationships
    */

    public function accountingAccounts()
    {
      return $this->belongsToMany('Edifalia\Models\Backend\AccountingAccount', 
        'accounting_account_property')
          ->withTimestamps();
    }

     public function bankAccounts()
    {
      return $this->belongsToMany('Edifalia\Models\Backend\BankAccount', 
        'bank_account_property')
          ->withTimestamps();
    }

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function getStatusFormatAttribute()
    {
        return ($this->status ? trans('common.affirmative_answer') : trans('common.negative_answer'));
    }

    public function getGroupPropertiesFormatAttribute()
    {
        return ($this->group_properties ? trans('common.affirmative_answer') : trans('common.negative_answer'));
    }

    public function getGroupConceptsFormatAttribute()
    {
        return ($this->group_concepts ? trans('common.affirmative_answer') : trans('common.negative_answer'));
    }

    /**
     *Relationships
     */

    public function type()
    {
        return  $this->belongsTo('Edifalia\Models\Common\PropertyType', 'property_type_id');
    }

    public function person()
    {
        return  $this->belongsTo('Edifalia\Models\Backend\Person', 'person_id');
    }

    public function accountingAccount()
    {
        return  $this->belongsTo('Edifalia\Models\Common\AccountingAccount', 'accounting_account_id');
    }

    public function paymentMethod()
    {
        return  $this->belongsTo('Edifalia\Models\Common\PaymentMethod', 'payment_method_id');
    }

    public function community()
    {
        return  $this->belongsTo('Edifalia\Models\Backend\Community', 'community_id');
    }

    public function budgets()
    {
        return $this->belongsToMany('Edifalia\Models\Budget', 'budget_property')
            ->withPivot('general', 'amount', 'coefficient');
    }

    public function readings()
    {
        return $this->belongsToMany('Edifalia\Models\Reading', 'property_reading')
            ->withPivot('previous', 'current', 'consumption');
    }

    /**
     * The receiptDues that belong to the property.
     */
    public function receiptDues()
    {
        return $this->belongsToMany('Edifalia\Models\ReceiptDue', 'receipt_due_items')
            ->withPivot('amount');
    }
}

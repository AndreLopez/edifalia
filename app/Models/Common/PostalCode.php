<?php

namespace Edifalia\Models\Common;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;

class PostalCode extends Model
{
	use SoftDeletes;
    use RevisionableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'town', 'province', 'region', 'country_id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Get the Country of the State
     */
	public function country()
    {
        return $this->belongsTo('Edifalia\Models\Common\Country', 'country_id');
    }
}

<?php


namespace Edifalia\Models\Common;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;

class AccountingAccount extends Model
{
    
    use SoftDeletes;
    use RevisionableTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'accounting_accounts';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code','title'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}

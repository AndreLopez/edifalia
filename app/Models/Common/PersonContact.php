<?php

namespace Edifalia\Models\Common;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;

class PersonContact extends Model
{
     
    use SoftDeletes;
    use RevisionableTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'persons_contacts';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['contact','notes','contact_type_id','person_id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}

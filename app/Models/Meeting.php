<?php

namespace Edifalia\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Meeting extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'type',
        'title',
        'date_in',
        'place',
        'order_day',
        'time_one',
        'time_two',
        'time_end',
        'community_id',
        'minute'
    ];

    protected $dates = ['deleted_at']; 

    public static function getType()
    {
        $array = [
            1 => trans('report.meeting.type.ordinary'),
            2 => trans('report.meeting.type.extra_ordinary'),
            3 => trans('report.meeting.type.directive'),
            4 => trans('report.meeting.type.others'),
        ];
        return $array;
    }
}

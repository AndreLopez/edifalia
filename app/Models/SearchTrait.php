<?php  
namespace Edifalia\Models;

trait SearchTrait {

	public function scopeSearch($query, array $data = array()){

		$data = array_only($data, $this->filters);

		$data = array_filter($data);

		foreach($data as $field => $value)
		{
			$method = studly_case($field);
			$filterMethod = lcfirst($method);
			$scopeMethod = 'scope'.$method;
			if(method_exists(get_called_class(), $scopeMethod))
			{
				//echo $filterMethod.'<br/>';
				$query->$filterMethod($data);
			}
		}
		return $query;
	}

}

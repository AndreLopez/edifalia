<?php

namespace Edifalia\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

class InvoicingConcept extends Model
{
    use SoftDeletes;
    use RevisionableTrait;

    protected $revisionCreationsEnabled = true;

    protected $fillable = ['name'];

    protected $dates = ['deleted_at'];

    /**
     * The IssueReceipts that belong to the InvoicingConcept.
     */
    public function issueReceipts()
    {
        return $this->belongsToMany('Edifalia\Models\IssueReceipt');
    }
}

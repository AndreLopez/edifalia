<?php

namespace Edifalia\Models;

use Illuminate\Database\Eloquent\Model;

class Access extends Model
{
    protected $fillable = ['user_id', 'start_date', 'ip_address', 'end_date', 'time'];

    protected $dates = ['start_date', 'end_date', 'created_at', 'updated_at'];

    public function user()
    {   // un acceso pertenece a un solo usuario
        return $this->belongsTo('Edifalia\User');
    }
}

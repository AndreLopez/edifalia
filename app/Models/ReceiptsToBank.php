<?php

namespace Edifalia\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;
use Carbon\Carbon;

class ReceiptsToBank extends Model
{
    use SoftDeletes;
    use RevisionableTrait;

    protected $revisionCreationsEnabled = false;

    protected $fillable = [
        'file_format',
        'state',
        'date_sent_to_bank',
        'charge_date',
        'bank_request',
        'payment_date',
    ];

    protected $dates = ['deleted_at'];

    /**
     * date_sent_to_bank Mutator
     */
    public function setDateSentToBankAttribute($value)
    {
        $this->attributes['date_sent_to_bank'] = Carbon::createFromFormat('m/d/Y', $value)
            ->format('Y-m-d');
    }
    /**
     * date_sent_to_bank Accessor
     */
    public function getDateSentToBankAttribute($value)
    {
        return Carbon::createFromFormat('Y-m-d', $value)->format('m/d/Y');
    }

    /**
     * charge_date Mutator
     */
    public function setChargeDateAttribute($value)
    {
        $this->attributes['charge_date'] = Carbon::createFromFormat('m/d/Y', $value)
            ->format('Y-m-d');
    }
    /**
     * charge_date Accessor
     */
    public function getChargeDateAttribute($value)
    {
        return Carbon::createFromFormat('Y-m-d', $value)->format('m/d/Y');
    }

    /**
     * payment_date Mutator
     */
    public function setPaymentDateAttribute($value)
    {
        $this->attributes['payment_date'] = Carbon::createFromFormat('m/d/Y', $value)
            ->format('Y-m-d');
    }

    /**
     * payment_date Accessor
     */
    public function getPaymentDateAttribute($value)
    {
        return Carbon::createFromFormat('Y-m-d', $value)->format('m/d/Y');
    }

    /**
     * state and delivery_to_bank_date Accessor
     */
    public function getStateAndDateAttribute()
    {
        $date = Carbon::createFromFormat('Y-m-d', $this->attributes['date_sent_to_bank'])->format('m/d/Y');
        if($this->attributes['state'] == 1){
            $state = trans('dashboard.state.previous');
        } else if($this->attributes['state'] == 2){
            $state = trans('dashboard.state.later');
        }
        return $state.trans('dashboard.tables.to').$date;
    }

    /**
     * Get the user that owns the ReceiptsToBank.
     */
    public function user()
    {
        return $this->belongsTo('Edifalia\User');
    }

    /**
     * The communities that belong to the ReceiptsToBank.
     */
    public function communities()
    {
        return $this->belongsToMany('Edifalia\Models\Backend\Community')
            ->withTimestamps();
    }

    /**
     * The presenters that belong to the ReceiptsToBank.
     */
    /*public function presenters()
    {
        return $this->belongsToMany('Edifalia\Models\')->withTimestamps();
    }*/
}

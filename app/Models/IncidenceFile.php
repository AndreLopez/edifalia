<?php

namespace Edifalia\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;

class IncidenceFile extends Model
{
    use SoftDeletes;
    use RevisionableTrait;

    protected $table = "incidence_files";

    protected $revisionCreationsEnabled = true;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'incidence_id',
        'file',
        'title',
        'publish',
    ];

    /**
     * Get the incidence that owns the file.
     */
    public function incidence()
    {
        return $this->belongsTo(Office::class);
    }

    public function existsFile()
    {
        return \Storage::disk('local_incidences')->exists($this->file);
    }
}

<?php

namespace Edifalia\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Venturecraft\Revisionable\RevisionableTrait;

class Budget extends Model
{
    use SoftDeletes;
    use RevisionableTrait;

    protected $revisionCreationsEnabled = true;

    protected $fillable = [
        'title',
        'community_id',
        'invoicing_concept_id',
        'periodicity',
        'rounding',
        'duration',
        'month',
        'year',
    ];

    protected $dates = ['deleted_at'];

    /**
     * Get the user that owns the budget.
     */
    public function user()
    {
        return $this->belongsTo('Edifalia\User');
    }

    public function community()
    {
        return $this->belongsTo('Edifalia\Models\Backend\Community');
    }

    public function invoicingConcept()
    {
        return $this->belongsTo(InvoicingConcept::class);
    }

    public function properties()
    {
        return $this->belongsToMany('Edifalia\Models\Common\Property', 'budget_property')
            ->withPivot('general', 'amount', 'coefficient');
    }
}

<?php

namespace Edifalia\Models;

use Illuminate\Database\Eloquent\Model;

class Connection extends Model
{
    protected $fillable = ['user_id', 'ip_address', 'last_time'];

    protected $dates = ['created_at', 'updated_at'];

    public function user()
    {   // una conexion pertenece a un solo usuario
        return $this->belongsTo('Edifalia\User');
    }
}

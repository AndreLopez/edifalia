<?php

namespace Edifalia\Models;

use Illuminate\Database\Eloquent\Model;
use \Venturecraft\Revisionable\RevisionableTrait;

class IpAccess extends Model
{
    use RevisionableTrait;

    protected $revisionCreationsEnabled = true;

    protected $fillable = ['main_user_id', 'ip'];

    /**
     * Get the main user that owns the ipAccess.
     */
    public function mainUser()
    {
        return $this->belongsTo('Edifalia\Models\MainUser');
    }
}

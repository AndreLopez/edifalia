<?php

namespace Edifalia\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MeetingVotingDetail extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'voting',
        'voting_id',
        'property_id'
    ];

    public function property()
    {
        return  $this->belongsTo('Edifalia\Models\Common\Property', 'property_id');
    }

    protected $dates = ['deleted_at'];   
}

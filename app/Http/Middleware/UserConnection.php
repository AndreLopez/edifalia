<?php

namespace Edifalia\Http\Middleware;

use Closure;
use Edifalia\Services\UsersConnection;
use Illuminate\Support\Facades\Auth;

class UserConnection
{
    private $connection;

    public function __construct(UsersConnection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check())
            $this->connection->save();

        return $next($request);
    }
}

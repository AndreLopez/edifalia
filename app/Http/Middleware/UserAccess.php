<?php

namespace Edifalia\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Gate;

class UserAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $access_type)
    {
        if(Gate::denies('admin')){
            if(Gate::denies('employee') || Gate::denies($access_type)){
                return abort(404);
            }
        }
        return $next($request);
    }
}

<?php

namespace Edifalia\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class DisableRegister
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check())
            return abort(404);

        return $next($request);
    }
}

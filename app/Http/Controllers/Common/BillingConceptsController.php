<?php

namespace Edifalia\Http\Controllers\Common;

use Illuminate\Http\Request;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\Common\BillingConcept;

class BillingConceptsController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
   		$data = [
            'breadcrumb_title' => trans('common.billing_concept_management_title'),
            'breadcrumb_subtitle' => trans('common.billing_concept_list_title'),
            'panel_title' => trans('common.lst_billing_concept_title'),
            'url_new' => route('billing-concept.create'),
            'theads' => [
                ['align' => 'text-center', 'name' => 'common.lst_col_name'],
                ['align' => 'text-center', 'name' => 'billing_concept.lst_col_title_1'],
                ['align' => 'text-center', 'name' => 'billing_concept.lst_col_title_2']
            ],
            'tbody' => []
        ];
        $billingConcepts = BillingConcept::all();
        foreach ($billingConcepts as $billingConcept) {
            $id = $billingConcept->id;
            $cols  = "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . $billingConcept->name . "</td>";
            $cols  .= "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . $billingConcept->reduced_title_1 . "</td>";
           	$cols  .= "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . $billingConcept->reduced_title_2 . "</td>";

            array_push($data['tbody'], [
                'cols' => $cols, 
                'url_edit' => route('billing-concept.edit', $id),
                'url_delete' => 'billing-concept.destroy', 
                'record_id' => $id
            ]);
        }
        
        return view('layouts.list-records')->with('data', $data);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $data = [
            'breadcrumb_title' => trans('common.billing_concept_management_title'),
            'breadcrumb_subtitle' => trans('common.billing_concept_add_title'),
            'breadcrumb_link' => route('billing-concept.index'),
            'panel_title' => trans('common.new_billing_concept_type_title'),
            'btn_record_submit' => trans('common.btn_save_text'),
        ];
        $header = [
            'route' => 'billing-concept.store', 
            'method' => 'POST', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];
        return view('layouts.forms')->with('data', $data)->with('new', true)->with('header', $header)
                                    ->with('fields_template', 'common.billing-concept-fields');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        	'name' => 'required|unique:billing_concepts,name|max:255', 
        	'reduced_title_1' => 'max:45', 
        	'reduced_title_2' => 'max:45'
        ]);

        $billingConcept = new BillingConcept();
        $billingConcept->fill($request->all());
        $billingConcept->save();

        $request->session()->flash('message', [
            'alert' => 'success', 'text' => trans('dashboard.messages.created')
        ]);

        return view('common.panel-info');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $billingConcept = BillingConcept::findOrfail($id);
        $data = [
            'breadcrumb_title' => trans('common.billing_concept_management_title'),
            'breadcrumb_subtitle' => trans('common.billing_concept_upd_title'),
            'breadcrumb_link' => route('billing-concept.index'),
            'panel_title' => trans('common.edit_billing_concept_type_title'),
            'btn_record_submit' => trans('common.edit_record_submit'),
        ];
        $header = [
            'route' => ['billing-concept.update', $billingConcept->id], 
            'method' => 'PUT', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];
        return view('layouts.forms')->with('data', $data)->with('edit', true)->with('header', $header)
                                    ->with('model', $billingConcept)
                                    ->with('fields_template', 'common.billing-concept-fields');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            //'type' => 'required|max:45|unique:contact_types,type,' . $id,
            'name' => 'required|max:255|unique:billing_concepts,name,' . $id, 
        	'reduced_title_1' => 'max:45', 
        	'reduced_title_2' => 'max:45'
        ]);

        $billingConcept = BillingConcept::find($id);
        if (!is_null($billingConcept)) {
            $billingConcept->fill($request->all());
            $billingConcept->save();
            $request->session()->flash('message', [
                'alert' => 'success', 'text' => trans('dashboard.messages.updated')
            ]);
        }
        return redirect()->route('billing-concept.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $billingConcept = BillingConcept::find($id);
            if (!is_null($billingConcept)) {
                $billingConcept->delete();
                $request->session()->flash('message', [
                    'alert' => 'success', 'text' => trans('dashboard.messages.deleted')
                ]);
                return response()->json(['result' => true]);
            }
            return response()->json(['result' => false]);
        }
        return redirect()->route('billing-concept.index');
    }

}

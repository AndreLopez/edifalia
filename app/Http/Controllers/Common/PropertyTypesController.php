<?php

namespace Edifalia\Http\Controllers\Common;

use Illuminate\Http\Request;

use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\Common\PropertyType;


class PropertyTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */

     public function index()
    {
        $data = [
            'breadcrumb_title' => trans('common.property_type_management_title'),
            'breadcrumb_subtitle' => trans('common.property_type_list_title'),
            'panel_title' => trans('common.lst_property_type_title'),
            'url_new' => route('propertyType.create'),
            'theads' => [
            	[ 'align' => 'text-center', 'name' => 'common.lst_col_name'],
            ],
            'tbody' => []
        ];
        $propertyTypes = PropertyType::all();
        foreach ($propertyTypes as $propertyType) {
            $id = $propertyType->id;
            $cols  = "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . $propertyType->name . "</td>";
            
            array_push($data['tbody'], 
            	[
	             'cols' => $cols, 
	             'url_edit' => route('propertyType.edit', $id),
	             'url_delete' => 'propertyType.destroy', 
	             'record_id' => $id
            	]
            );
        }
        
        return view('layouts.list-records')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'breadcrumb_title' => trans('common.property_type_management_title'),
            'breadcrumb_subtitle' => trans('common.property_type_add_title'),
            'breadcrumb_link' => route('propertyType.index'),
            'panel_title' => trans('common.new_property_type_title'),
            'btn_record_submit' => trans('common.btn_save_text'),
        ];
        $header = [
            'route' => 'propertyType.store', 
            'method' => 'POST', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];
        return view('layouts.forms')->with('data', $data)->with('new', true)->with('header', $header)
                                    ->with('fields_template', 'common.property-type-fields');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
     		'name' => 'required|max:45|unique:property_types',
        ]);

        $propertyType = new PropertyType();
        $propertyType->fill($request->all());
        $propertyType->save();

        $request->session()->flash('message', [
            'alert' => 'success', 'text' => trans('dashboard.messages.created')
        ]);

        return view('common.panel-info');
    }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $propertyType = PropertyType::findOrfail($id);
        $data = [
            'breadcrumb_title' => trans('common.property_type_management_title'),
            'breadcrumb_subtitle' => trans('common.property_type_upd_title'),
            'breadcrumb_link' => route('propertyType.index'),
            'panel_title' => trans('common.edit_property_type_title'),
            'btn_record_submit' => trans('common.edit_record_submit'),
        ];
        $header = [
            'route' => ['propertyType.update', $propertyType->id], 
            'method' => 'PUT', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];
        return view('layouts.forms')->with('data', $data)->with('edit', true)->with('header', $header)
                                    ->with('model', $propertyType)
                                    ->with('fields_template', 'common.property-type-fields');
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
          'name' => 'required|max:45|unique:property_types,name,' .$id,
        ]);

        $propertyType = PropertyType::find($id);
        if (!is_null($propertyType)) {
            $propertyType->fill($request->all());
            $propertyType->save();
            $request->session()->flash('message', [
                'alert' => 'success', 'text' => trans('dashboard.messages.updated')
            ]);
        }
        return redirect()->route('propertyType.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $propertyType = PropertyType::find($id);
            if (!is_null($propertyType)) {
                $propertyType->delete();
                $request->session()->flash('message', [
                    'alert' => 'success', 'text' => trans('dashboard.messages.deleted')
                ]);
                return response()->json(['result' => true]);
            }
            return response()->json(['result' => false]);
        }
        return redirect()->route('property.index');
    }



}

<?php


namespace Edifalia\Http\Controllers\Common;

use Illuminate\Http\Request;
use Edifalia\Http\Requests;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\Common\ContactType; 
use Edifalia\Models\BackEnd\Person;
use Edifalia\Models\Common\PersonContact;

class PersonsContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'breadcrumb_title' => trans('common.person_contact_management_title'),
            'breadcrumb_subtitle' => trans('common.person_contact_list_title'),
            'panel_title' => trans('common.lst_person_contact_title'),
            'url_new' => route('person-contact.create'),
            'theads' => [
                ['align' => 'text-center', 'contact' => 'common.lst_col_type'],
            ],
            'tbody' => []
        ];

        $person_contact = PersonContact::all();

        foreach ($person_contact as $pc) {
            $id = $pc->id;
            $cols  = "<td class='col-xs-2 col-sm-2 col-md-2 col-lg-2    '>" . $pc->contact . "</td>";
            array_push($data['tbody'], [
                'cols' => $cols, 'url_edit' => route('person-contact.edit', $id),
                'url_delete' => 'person-contact.destroy', 'record_id' => $id
            ]);
        }

        return view('layouts.list-records_person_contacts')->with('data', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'breadcrumb_title' => trans('common.person_contact_management_title'),
            'breadcrumb_subtitle' => trans('common.person_contact_listing_title'),
            'breadcrumb_link' => route('person-contact.index'),
            'panel_title' => trans('person-contact.new_title'),
            'btn_record_submit' => trans('common.btn_save_text'),
        ];

        $header = [
            'route' => 'person-contact.store', 
            'method' => 'POST', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];

        $contacts_type = ContactType::all();
        $ct_options = [];
        foreach ($contacts_type as $ct) {
            $ct_options[$ct->id] = $ct->type; 
        }

        $persons = Person::all();
        $person_options = [];
        foreach ($persons as $person) {
            $person_options[$person->id] = $person->first_name;
        }

        return view('layouts.forms')->with('data', $data)->with('new', true)
                                    ->with('header', $header)->with('contacts_type',$ct_options)
                                    ->with('persons', $person_options)
                                    ->with('fields_template', 'common.person_contacts-fields');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'contact' => 'required|max:75',
            'notes' => 'nullable',
        ]);

        $person_contact = new PersonContact();
        $person_contact->fill($request->all()); 
        $person_contact->save(); 

        $request->session()->flash('message', [
            'alert' => 'success', 'text' => trans('dashboard.messages.created')
        ]);

        return redirect()->route('person-contact.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $person_contact = PersonContact::findOrfail($id);

        $data = [
            'breadcrumb_title' => trans('common.person_contact_management_title'),
            'breadcrumb_subtitle' => trans('common.person_contact_listing_title'),
            'breadcrumb_link' => route('person-contact.index'),
            'panel_title' => trans('person-contact.new_title'),
            'btn_record_submit' => trans('common.edit_record_submit'),
        ];

        $header = [
            'route' => ['person-contact.update', $person_contact->id], 
            'method' => 'PUT', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];

        $contacts_type = ContactType::all();
        $ct_options = [];
        foreach ($contacts_type as $ct) {
            $ct_options[$ct->id] = $ct->type; 
        }

        $persons = Person::all();
        $person_options = [];
        foreach ($persons as $person) {
            $person_options[$person->id] = $person->first_name;
        }

        return view('layouts.forms')->with('data', $data)->with('edit', true)->with('header', $header)
                                    ->with('model', $person_contact)->with('contacts_type',$ct_options)
                                    ->with('persons', $person_options)
                                    ->with('fields_template', 'common.person_contacts-fields');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'contact' => 'required|max:75',
            'notes' => 'nullable',
        ]);

        $person_contact = PersonContact::find($id);
        if (!is_null($person_contact)) {
            $person_contact->fill($request->all());
            $person_contact->save();
            $request->session()->flash('message', [
                'alert' => 'success', 'text' => trans('dashboard.messages.updated')
            ]);
        }
        return redirect()->route('person-contact.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
     {
        if ($request->ajax()) {
            $person_contact = PersonContact::find($id);
            if (!is_null($person_contact)) {
                $person_contact->delete();
                $request->session()->flash('message', [
                    'alert' => 'success', 'text' => trans('dashboard.messages.deleted')
                ]);
                return response()->json(['result' => true]);
            }
            return response()->json(['result' => false]);
        }
        return redirect()->route('person-contact.index');
    }
}

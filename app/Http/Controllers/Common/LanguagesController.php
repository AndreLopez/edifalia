<?php

namespace Edifalia\Http\Controllers\Common;

use Illuminate\Http\Request;

use Edifalia\Http\Requests;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\Common\Language;

class LanguagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'breadcrumb_title' => trans('common.lang_management_title'),
            'breadcrumb_subtitle' => trans('common.lang_list_title'),
            'panel_title' => trans('common.lst_language_title'),
            'url_new' => route('language.create'),
            'theads' => [
                ['align' => 'text-center', 'name' => 'common.lst_col_name'],
                ['align' => 'text-center', 'name' => 'common.lst_col_acronym'],
            ],
            'tbody' => []
        ];
        $languages = Language::all();
        foreach ($languages as $lang) {
            $id = $lang->id;
            $cols  = "<td class='col-xs-9 col-sm-9 col-md-9 col-lg-9'>" . $lang->name . "</td>";
            $cols .= "<td class='col-xs-1 col-sm-1 col-md-1 col-lg-1'>" . $lang->acronym . "</td>";
            array_push($data['tbody'], [
                'cols' => $cols, 'url_edit' => route('language.edit', $id),
                'url_delete' => 'language.destroy', 'record_id' => $id
            ]);
        }
        
        return view('layouts.list-records')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'breadcrumb_title' => trans('common.lang_management_title'),
            'breadcrumb_subtitle' => trans('common.lang_add_title'),
            'breadcrumb_link' => route('language.index'),
            'panel_title' => trans('common.new_language_title'),
            'btn_record_submit' => trans('common.btn_save_text'),
        ];
        $header = [
            'route' => 'language.store', 
            'method' => 'POST', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];
        return view('layouts.forms')->with('data', $data)->with('new', true)->with('header', $header)
                                    ->with('fields_template', 'common.language-fields');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:45|unique:languages,name',
            'acronym' => 'required|max:2|unique:languages,acronym',
        ]);

        $lang = new Language();
        $lang->fill($request->all());
        $lang->save();

        $request->session()->flash('message', [
            'alert' => 'success', 'text' => trans('dashboard.messages.created')
        ]);

        return view('common.panel-info');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lang = Language::findOrfail($id);
        $data = [
            'breadcrumb_title' => trans('common.lang_management_title'),
            'breadcrumb_subtitle' => trans('common.lang_upd_title'),
            'breadcrumb_link' => route('language.index'),
            'panel_title' => trans('common.edit_language_title'),
            'btn_record_submit' => trans('common.edit_record_submit'),
        ];
        $header = [
            'route' => ['language.update', $lang->id], 
            'method' => 'PUT', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];
        return view('layouts.forms')->with('data', $data)->with('edit', true)->with('header', $header)
                                    ->with('model', $lang)
                                    ->with('fields_template', 'common.language-fields');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name' => 'required|max:45|unique:languages,name,' . $id,
            'acronym' => 'required|max:2|unique:languages,acronym,' . $id,
        ]);

        $language = Language::find($id);
        if (!is_null($language)) {
            $language->fill($request->all());
            $language->save();
            $request->session()->flash('message', [
                'alert' => 'success', 'text' => trans('dashboard.messages.updated')
            ]);
            
        }
        return redirect()->route('language.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $language = Language::find($id);
            if (!is_null($language)) {
                $language->delete();
                $request->session()->flash('message', [
                    'alert' => 'success', 'text' => trans('dashboard.messages.deleted')
                ]);
                
                return response()->json(['result' => true]);
            }
            return response()->json(['result' => false]);
        }
        return redirect()->route('language.index');
    }
}

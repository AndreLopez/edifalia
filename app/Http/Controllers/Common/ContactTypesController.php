<?php

namespace Edifalia\Http\Controllers\Common;

use Illuminate\Http\Request;

use Edifalia\Http\Requests;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\Common\ContactType;

class ContactTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'breadcrumb_title' => trans('common.contact_type_management_title'),
            'breadcrumb_subtitle' => trans('common.contact_type_list_title'),
            'panel_title' => trans('common.lst_contact_type_title'),
            'url_new' => route('contact-type.create'),
            'theads' => [
                ['align' => 'text-center', 'name' => 'common.lst_col_type'],
            ],
            'tbody' => []
        ];
        $contact_types = ContactType::all();
        foreach ($contact_types as $ct) {
            $id = $ct->id;
            $cols  = "<td class='col-xs-10 col-sm-10 col-md-10 col-lg-10'>" . $ct->type . "</td>";
            array_push($data['tbody'], [
                'cols' => $cols, 'url_edit' => route('contact-type.edit', $id),
                'url_delete' => 'contact-type.destroy', 'record_id' => $id
            ]);
        }
        
        return view('layouts.list-records')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'breadcrumb_title' => trans('common.contact_type_management_title'),
            'breadcrumb_subtitle' => trans('common.contact_type_add_title'),
            'breadcrumb_link' => route('contact-type.index'),
            'panel_title' => trans('common.new_contact_type_title'),
            'btn_record_submit' => trans('common.btn_save_text'),
        ];
        $header = [
            'route' => 'contact-type.store', 
            'method' => 'POST', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];
        return view('layouts.forms')->with('data', $data)->with('new', true)->with('header', $header)
                                    ->with('fields_template', 'common.contact-type-fields');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'type' => 'required|unique:contact_types,type|max:45',
        ]);

        $contact_types = new ContactType();
        $contact_types->fill($request->all());
        $contact_types->save();

        $request->session()->flash('message', [
            'alert' => 'success', 'text' => trans('dashboard.messages.created')
        ]);

        return view('common.panel-info');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contact_type = ContactType::findOrfail($id);
        $data = [
            'breadcrumb_title' => trans('common.contact_type_management_title'),
            'breadcrumb_subtitle' => trans('common.contact_type_upd_title'),
            'breadcrumb_link' => route('contact-type.index'),
            'panel_title' => trans('common.edit_contact_type_title'),
            'btn_record_submit' => trans('common.edit_record_submit'),
        ];
        $header = [
            'route' => ['contact-type.update', $contact_type->id], 
            'method' => 'PUT', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];
        return view('layouts.forms')->with('data', $data)->with('edit', true)->with('header', $header)
                                    ->with('model', $contact_type)
                                    ->with('fields_template', 'common.contact-type-fields');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'type' => 'required|max:45|unique:contact_types,type,' . $id,
        ]);

        $contact_type = ContactType::find($id);
        if (!is_null($contact_type)) {
            $contact_type->fill($request->all());
            $contact_type->save();
            $request->session()->flash('message', [
                'alert' => 'success', 'text' => trans('dashboard.messages.updated')
            ]);
        }
        return redirect()->route('contact-type.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $contact_type = ContactType::find($id);
            if (!is_null($contact_type)) {
                $contact_type->delete();
                $request->session()->flash('message', [
                    'alert' => 'success', 'text' => trans('dashboard.messages.deleted')
                ]);
                return response()->json(['result' => true]);
            }
            return response()->json(['result' => false]);
        }
        return redirect()->route('contact-type.index');
    }
}

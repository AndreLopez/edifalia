<?php

namespace Edifalia\Http\Controllers\Common;

use Illuminate\Http\Request;

use Edifalia\Http\Requests;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\Common\Country;

class CountriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'breadcrumb_title' => trans('common.country_management_title'),
            'breadcrumb_subtitle' => trans('common.country_list_title'),
            'panel_title' => trans('common.lst_country_title'),
            'url_new' => route('country.create'),
            'theads' => [
                ['align' => 'text-center', 'name' => 'common.lst_col_name'],
            ],
            'tbody' => []
        ];
        $countries = Country::all();
        foreach ($countries as $country) {
            $id = $country->id;
            $cols  = "<td class='col-xs-10 col-sm-10 col-md-10 col-lg-10'>" . $country->name . "</td>";
            array_push($data['tbody'], [
                'cols' => $cols, 'url_edit' => route('country.edit', $id),
                'url_delete' => 'country.destroy', 'record_id' => $id
            ]);
        }
        
        return view('layouts.list-records')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'breadcrumb_title' => trans('common.country_management_title'),
            'breadcrumb_subtitle' => trans('common.country_add_title'),
            'breadcrumb_link' => route('country.index'),
            'panel_title' => trans('common.new_country_title'),
            'btn_record_submit' => trans('common.btn_save_text'),
        ];
        $header = [
            'route' => 'country.store', 
            'method' => 'POST', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];
        return view('layouts.forms')->with('data', $data)->with('new', true)->with('header', $header)
                                    ->with('fields_template', 'common.country-fields');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:countries|max:45',
        ]);

        $lang = new Country();
        $lang->fill($request->all());
        $lang->save();

        $request->session()->flash('message', [
            'alert' => 'success', 'text' => trans('dashboard.messages.created')
        ]);

        return view('common.panel-info');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country = Country::findOrfail($id);
        $data = [
            'breadcrumb_title' => trans('common.country_management_title'),
            'breadcrumb_subtitle' => trans('common.country_upd_title'),
            'breadcrumb_link' => route('country.index'),
            'panel_title' => trans('common.edit_country_title'),
            'btn_record_submit' => trans('common.edit_record_submit'),
        ];
        $header = [
            'route' => ['country.update', $country->id], 
            'method' => 'PUT', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];
        return view('layouts.forms')->with('data', $data)->with('edit', true)->with('header', $header)
                                    ->with('model', $country)
                                    ->with('fields_template', 'common.country-fields');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:45|unique:countries,name,' . $id,
        ]);

        $country = Country::find($id);
        if (!is_null($country)) {
            $country->fill($request->all());
            $country->save();
            $request->session()->flash('message', [
                'alert' => 'success', 'text' => trans('dashboard.messages.updated')
            ]);
        }
        return redirect()->route('country.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $country = Country::find($id);
            if (!is_null($country)) {
                $country->delete();
                $request->session()->flash('message', [
                    'alert' => 'success', 'text' => trans('dashboard.messages.deleted')
                ]);
                return response()->json(['result' => true]);
            }
            return response()->json(['result' => false]);
        }
        return redirect()->route('country.index');
    }
}

<?php


namespace Edifalia\Http\Controllers\Common;

use Illuminate\Http\Request;

use Edifalia\Http\Requests;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\Common\AccountingAccount;

class AccountingAccountsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $data = [
            'breadcrumb_title' => trans('common.accounting_accounts_management_title'),
            'breadcrumb_subtitle' => trans('common.accounting_account_list_title'),
            'panel_title' => trans('common.lst_accounting_accounts_title'),
            'url_new' => route('accounting-account.create'),
            'theads' => [
                ['align' => 'text-center', 'title' => 'common.accounting_account_code_title'],
                ['align' => 'text-center', 'title' => 'common.accounting_account_title_title'],
            ],
            'tbody' => []
        ];
        $accounting_account = AccountingAccount::all();
        foreach ($accounting_account as $ac) {
            $id = $ac->id;
            $bold = '';
            if (strlen($ac->code) == 1) {
                $bold = "style='font-weight: bold'";
            }
            $cols = "<td class='col-xs-2 col-sm-2 col-md-2 col-lg-2' $bold>" . $ac->code . "</td>";
            $cols .= "<td class='col-xs-8 col-sm-8 col-md-8 col-lg-8' $bold>" . $ac->title . "</td>";

            array_push($data['tbody'], [
                'cols' => $cols, 'url_edit' => route('accounting-account.edit', $id),
                'url_delete' => 'accounting-account.destroy', 'record_id' => $id
            ]);
        }
        
        return view('layouts.list-records_accountingc')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'breadcrumb_title' => trans('common.accounting_accounts_management_title'),
            'breadcrumb_subtitle' => trans('common.accounting_account_list_title'),
            'breadcrumb_link' => route('accounting-account.index'),
            'panel_title' => trans('common.new_payment_method_title'),
            'btn_record_submit' => trans('common.btn_save_text'),
        ];
        $header = [
            'route' => 'accounting-account.store', 
            'method' => 'POST', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];
        return view('layouts.forms')->with('data', $data)->with('new', true)->with('header', $header)
                                    ->with('fields_template', 'common.accounting-account-fields');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'code' => 'required|unique:accounting_accounts|max:15',
          'title' => 'required|max:45',
        ]);

        $accounting_accounts = new AccountingAccount();
        $accounting_accounts->fill($request->all());
        $accounting_accounts->save();

        $request->session()->flash('message', [
            'alert' => 'success', 'text' => trans('dashboard.messages.created')
        ]);

        return view('common.panel-info');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ac = AccountingAccount::findOrfail($id);
        $data = [
            'breadcrumb_title' => trans('common.accounting_accounts_management_title'),
            'breadcrumb_subtitle' => trans('common.accounting_account_list_title'),
            'breadcrumb_link' => route('accounting-account.index'),
            'panel_title' => trans('common.edit_accounting_account_title'),
            'btn_record_submit' => trans('common.edit_record_submit'),
        ];
        $header = [
            'route' => ['accounting-account.update', $ac->id], 
            'method' => 'PUT', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];

      return view('layouts.forms')->with('data', $data)->with('edit', true)->with('header', $header)
                                    ->with('model', $ac)
                                    ->with('fields_template', 'common.accounting-account-fields');



    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          $accounting_account = AccountingAccount::findOrfail($id);
        $data = [
             'breadcrumb_title' => trans('common.accounting_accounts_management_title'),
            'breadcrumb_subtitle' => trans('common.accounting_account_list_title'),
            'breadcrumb_link' => route('accounting-account.index'),
            'panel_title' => trans('common.new_payment_method_title'),
               'btn_record_submit' => trans('common.edit_record_submit'),
        ];
       $header = [
            'route' => ['accounting-account.update', $accounting_account->id], 
            'method' => 'PUT', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];
        
       return view('layouts.forms')->with('data', $data)->with('edit', true)->with('header', $header)
                                    ->with('model', $accounting_account)
                                    ->with('fields_template', 'common.accounting-account-fields');
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request, [
                   'code' => 'required|unique:accounting_accounts|max:15',
          'title' => 'required|max:45',
        ]);

        $accounting_account = AccountingAccount::find($id);
        if (!is_null($accounting_account)) {
            $accounting_account->fill($request->all());
            $accounting_account->save();
            $request->session()->flash('message', [
                'alert' => 'success', 'text' => trans('dashboard.messages.updated')
            ]);
        }
        return redirect()->route('accounting-account.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         if ($request->ajax()) {
            $accounting_account = AccountingAccount::find($id);
            if (!is_null($accounting_account)) {
                $accounting_account->delete();
                $request->session()->flash('message', [
                    'alert' => 'success', 'text' => trans('dashboard.messages.deleted')
                ]);
                return response()->json(['result' => true]);
            }
            return response()->json(['result' => false]);
        }
        return redirect()->route('accounting-account.index');
    }
    
}

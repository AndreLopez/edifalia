<?php

namespace Edifalia\Http\Controllers\Common;

use Illuminate\Http\Request;

use Edifalia\Http\Requests;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\Common\Status;

class StatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'breadcrumb_title' => trans('common.status_management_title'),
            'breadcrumb_subtitle' => trans('common.status_list_title'),
            'panel_title' => trans('common.lst_status_title'),
            'url_new' => route('status.create'),
            'theads' => [
                ['align' => 'text-center', 'name' => 'common.lst_col_name'],
            ],
            'tbody' => []
        ];
        $status = Status::all();
        foreach ($status as $st) {
            $id = $st->id;
            $cols  = "<td class='col-xs-10 col-sm-10 col-md-10 col-lg-10'>" . $st->name . "</td>";
            array_push($data['tbody'], [
                'cols' => $cols, 'url_edit' => route('status.edit', $id),
                'url_delete' => 'status.destroy', 'record_id' => $id
            ]);
        }
        
        return view('layouts.list-records')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'breadcrumb_title' => trans('common.status_management_title'),
            'breadcrumb_subtitle' => trans('common.status_add_title'),
            'breadcrumb_link' => route('status.index'),
            'panel_title' => trans('common.new_status_title'),
            'btn_record_submit' => trans('common.btn_save_text'),
        ];
        $header = [
            'route' => 'status.store', 
            'method' => 'POST', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];
        return view('layouts.forms')->with('data', $data)->with('new', true)->with('header', $header)
                                    ->with('fields_template', 'common.status-fields');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:status|max:45',
        ]);

        $status = new Status();
        $status->fill($request->all());
        $status->save();

        $request->session()->flash('message', [
            'alert' => 'success', 'text' => trans('dashboard.messages.created')
        ]);

        return view('common.panel-info');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $status = Status::findOrfail($id);
        $data = [
            'breadcrumb_title' => trans('common.status_management_title'),
            'breadcrumb_subtitle' => trans('common.status_upd_title'),
            'breadcrumb_link' => route('status.index'),
            'panel_title' => trans('common.edit_status_title'),
            'btn_record_submit' => trans('common.edit_record_submit'),
        ];
        $header = [
            'route' => ['status.update', $status->id], 
            'method' => 'PUT', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];
        return view('layouts.forms')->with('data', $data)->with('edit', true)->with('header', $header)
                                    ->with('model', $status)
                                    ->with('fields_template', 'common.status-fields');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:45|unique:status,name,' . $id,
        ]);

        $status = Status::find($id);
        if (!is_null($status)) {
            $status->fill($request->all());
            $status->save();
            $request->session()->flash('message', [
                'alert' => 'success', 'text' => trans('dashboard.messages.updated')
            ]);
        }
        return redirect()->route('status.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $status = Status::find($id);
            if (!is_null($status)) {
                $status->delete();
                $request->session()->flash('message', [
                    'alert' => 'success', 'text' => trans('dashboard.messages.deleted')
                ]);
                return response()->json(['result' => true]);
            }
            return response()->json(['result' => false]);
        }
        return redirect()->route('status.index');
    }
}

<?php

namespace Edifalia\Http\Controllers\Common;
use Illuminate\Http\Request;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\Common\SupplierContact;

use Edifalia\Models\Common\Status;
use Edifalia\Models\Common\ContactType;
use Edifalia\Models\Backend\Supplier;

class SuppliersContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $data = [
            'breadcrumb_title' => trans('common.supplier_contact_management_title'),
            'breadcrumb_subtitle' => trans('common.supplier_contact_list_title'),
            'panel_title' => trans('common.lst_supplier_contact_title'),
            'url_new' => route('supplier_contact.create'),
            'theads' => [
                ['align' => 'text-center', 'contact' => 'common.lst_supplier_contact_name'],
            ],
            'tbody' => []
        ];
        $suppliers_contacts = SupplierContact::all();
        foreach ($suppliers_contacts as $supplier_contact) {
            $id = $supplier_contact->id;
            $cols  = "<td class='col-xs-10 col-sm-10 col-md-10 col-lg-10'>" . $supplier_contact->contact . "</td>";
            array_push($data['tbody'], [
                'cols' => $cols, 'url_edit' => route('supplier_contact.edit', $id),
                'url_delete' => 'supplier_contact.destroy', 'record_id' => $id
            ]);
        }
        
        return view('layouts.list-records_supplier_contact')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          $data = [
            'breadcrumb_title' => trans('common.supplier_contact_management_title'),
            'breadcrumb_subtitle' => trans('common.suppliers_contacts_add_title'),
            'breadcrumb_link' => route('supplier_contact.index'),
            'panel_title' => trans('common.suppliers_contacts_new_title'),
            'btn_record_submit' => trans('common.btn_save_text'),
        ];
        $header = [
            'route' => 'supplier_contact.store', 
            'method' => 'POST', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];
        $estatus = Status::all();
        $st_options = [];
        foreach ($estatus as $st) {
            $st_options[$st->id] = $st->name;
        }
       $contacts_type = ContactType::all();
        $contacttype_options = [];
        foreach ($contacts_type as $contacttype) {
            $contacttype_options[$contacttype->id] = $contacttype->type;
        }
      
        $suppliers = Supplier::all();
        $supplier_options = [];
        foreach($suppliers as $supplier) {
           $supplier_options[$supplier->id] = $supplier->business_name; 
        }


        return view('layouts.forms')->with('data', $data)->with('new', true)->with('header', $header)
        ->with('status', $st_options)->with('contacts_type', $contacttype_options)->with('suppliers', $supplier_options)
      ->with('fields_template', 'common.supplierc-fields');
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request, [
            'contact' => 'required|max:75',
             'notes' => 'nullable',
            
        ]);

       $supplier_contact = new  SupplierContact();
        $supplier_contact->fill($request->all());
        $supplier_contact->save();

        $request->session()->flash('message', [
            'alert' => 'success', 'text' => trans('dashboard.messages.created')
        ]);

        return redirect()->route('supplier_contact.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supplier_contact = SupplierContact::findOrfail($id);

        $data = [
            'breadcrumb_title' => trans('common.supplier_contact_management_title'),
            'breadcrumb_subtitle' => trans('common.suppliers_contacts_add_title'),
            'breadcrumb_link' => route('supplier_contact.index'),
            'panel_title' => trans('common.suppliers_contacts_new_title'),
            'btn_record_submit' => trans('common.btn_save_text'),
        ];
        $header = [
            'route' => ['supplier_contact.update', $supplier_contact->id], 
            'method' => 'PUT', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];
       $estatus = Status::all();
        $st_options = [];
        foreach ($estatus as $st) {
            $st_options[$st->id] = $st->name;
        }
       $contacts_type = ContactType::all();
        $contacts_type_options = [];
        foreach ($contacts_type as $contact_type) {
            $contacts_type_options[$contact_type->id] = $contact_type->type;
        }

        $suppliers = Supplier::all();
        $suppliers_options = [];
        foreach ($suppliers as $supplier) {
            $suppliers_options[$supplier->id] = $supplier->id;
        }       
      
      return view('layouts.forms')->with('data', $data)->with('edit', true)->with('header', $header)
    ->with('model', $supplier_contact)->with('status', $st_options)->with('contacts_type', $contacts_type_options)
    ->with('suppliers', $suppliers_options)
    ->with('fields_template', 'common.supplierc-fields');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request, [
            'contact' => 'required|max:75',
             'notes' => 'nullable',
            
        ]);

         $supplierc = SupplierContact::find($id); 
         if(!is_null($supplierc)){
            $supplierc->fill($request->all());
            $supplierc->save();
            $request->session()->flash('message', [
                'alert' => 'success', 'text' => trans('dashboard.messages.updated')
            ]);
         }
         return redirect()->route('supplier_contact.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SupplierContact::destroy($id);
        $request->session()->flash('message', [
            'alert' => 'success', 'text' => trans('dashboard.messages.deleted')
        ]);
        return redirect()->route('supplier_contact.index');
    }
}

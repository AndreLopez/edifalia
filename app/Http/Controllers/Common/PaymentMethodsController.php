<?php

namespace Edifalia\Http\Controllers\Common;

use Illuminate\Http\Request;

use Edifalia\Http\Requests;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\Common\PaymentMethod;

class PaymentMethodsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'breadcrumb_title' => trans('common.payment_method_management_title'),
            'breadcrumb_subtitle' => trans('common.payment_method_list_title'),
            'panel_title' => trans('common.lst_payment_method_title'),
            'url_new' => route('payment-method.create'),
            'theads' => [
                ['align' => 'text-center', 'name' => 'common.lst_col_type'],
            ],
            'tbody' => []
        ];
        $payment_method = PaymentMethod::all();
        foreach ($payment_method as $pm) {
            $id = $pm->id;
            $cols  = "<td class='col-xs-10 col-sm-10 col-md-10 col-lg-10'>" . $pm->type . "</td>";
            array_push($data['tbody'], [
                'cols' => $cols, 'url_edit' => route('payment-method.edit', $id),
                'url_delete' => 'payment-method.destroy', 'record_id' => $id
            ]);
        }
        
        return view('layouts.list-records')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'breadcrumb_title' => trans('common.payment_method_management_title'),
            'breadcrumb_subtitle' => trans('common.payment_method_add_title'),
            'breadcrumb_link' => route('payment-method.index'),
            'panel_title' => trans('common.new_payment_method_title'),
            'btn_record_submit' => trans('common.btn_save_text'),
        ];
        $header = [
            'route' => 'payment-method.store', 
            'method' => 'POST', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];
        return view('layouts.forms')->with('data', $data)->with('new', true)->with('header', $header)
                                    ->with('fields_template', 'common.payment-method-fields');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'type' => 'required|unique:payment_methods,type|max:45',
        ]);

        $payment_method = new PaymentMethod();
        $payment_method->fill($request->all());
        $payment_method->save();

        $request->session()->flash('message', [
            'alert' => 'success', 'text' => trans('dashboard.messages.created')
        ]);

        return view('common.panel-info');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $payment_method = PaymentMethod::findOrfail($id);
        $data = [
            'breadcrumb_title' => trans('common.payment_method_management_title'),
            'breadcrumb_subtitle' => trans('common.payment_method_upd_title'),
            'breadcrumb_link' => route('payment-method.index'),
            'panel_title' => trans('common.edit_payment_method_title'),
            'btn_record_submit' => trans('common.edit_record_submit'),
        ];
        $header = [
            'route' => ['payment-method.update', $payment_method->id], 
            'method' => 'PUT', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];
        return view('layouts.forms')->with('data', $data)->with('edit', true)->with('header', $header)
                                    ->with('model', $payment_method)
                                    ->with('fields_template', 'common.payment-method-fields');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'type' => 'required|max:45|unique:payment_methods,type,' . $id,
        ]);

        $payment_method = PaymentMethod::find($id);
        if (!is_null($payment_method)) {
            $payment_method->fill($request->all());
            $payment_method->save();
            $request->session()->flash('message', [
                'alert' => 'success', 'text' => trans('dashboard.messages.updated')
            ]);
        }
        return redirect()->route('payment-method.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $payment_method = PaymentMethod::find($id);
            if (!is_null($payment_method)) {
                $payment_method->delete();
                $request->session()->flash('message', [
                    'alert' => 'success', 'text' => trans('dashboard.messages.deleted')
                ]);
                return response()->json(['result' => true]);
            }
            return response()->json(['result' => false]);
        }
        return redirect()->route('payment-method.index');
    }
}

<?php

namespace Edifalia\Http\Controllers\Common;
use Illuminate\Http\Request;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\Common\SupplierActivitie;
use Edifalia\Models\Common\Status;



class SupplierActivitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
           $data = [
            'breadcrumb_title' => trans('common.supplier_activitie_title'),
            'breadcrumb_subtitle' => trans('common.supplier_activitie_list_title'),
            'panel_title' => trans('common.lst_supplier_activitie_title'),
            'url_new' => route('supplier_activitie.create'),
            'theads' => [
                ['align' => 'text-center', 'name' => 'common.lst_supplier_activitie_name'],
            ],
            'tbody' => []
        ];
        $supplier_activities = SupplierActivitie::all();
        foreach ($supplier_activities as $supplier_activitie) {
            $id = $supplier_activitie->id;
            $cols  = "<td class='col-xs-10 col-sm-10 col-md-10 col-lg-10'>" . $supplier_activitie->name . "</td>";
            array_push($data['tbody'], [
                'cols' => $cols, 'url_edit' => route('supplier_activitie.edit', $id),
                'url_delete' => 'supplier_activitie.destroy', 'record_id' => $id
            ]);
        }
        
        return view('layouts.list-records_supplier_activitie')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $data = [
            'breadcrumb_title' => trans('common.supplier_activitie_title'),
            'breadcrumb_subtitle' => trans('common.supplier_activitie_list_title'),
            'breadcrumb_link' => route('supplier_activitie.index'),
            'panel_title' => trans('common.supplier_activitie_new_title'),
            'btn_record_submit' => trans('common.btn_save_text'),
        ];
        $header = [
            'route' => 'supplier_activitie.store', 
            'method' => 'POST', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];
        $estatus = Status::all();
        $st_options = [];
        foreach ($estatus as $st) {
            $st_options[$st->id] = $st->name;
        }

        return view('layouts.forms')->with('data', $data)->with('new', true)->with('header', $header)
        ->with('status', $st_options)
      ->with('fields_template', 'common.supplier_activitie-fields');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request, [
            'name' => 'required|max:45',

            
        ]);

       $supplier_activitie = new  SupplierActivitie();
        $supplier_activitie->fill($request->all());
        $supplier_activitie->save();

        $request->session()->flash('message', [
            'alert' => 'success', 'text' => trans('dashboard.messages.created')
        ]);

        return redirect()->route('supplier_activitie.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supplier_a = SupplierActivitie::findOrfail($id); 

         $data = [
            'breadcrumb_title' => trans('common.supplier_activitie_title'),
            'breadcrumb_subtitle' => trans('common.supplier_activitie_list_title'),
            'breadcrumb_link' => route('supplier_activitie.index'),
            'panel_title' => trans('common.supplier_activitie_new_title'),
            'btn_record_submit' => trans('common.btn_save_text'),
        ];

          $header = [
            'route' => ['supplier_activitie.update', $supplier_a->id ], 
            'method' => 'PUT', 
            'role' => 'form',
            'class' => 'form-horizontal',

         ];   

        $estatus = Status::all();
        $st_options = [];
        foreach ($estatus as $st) {
            $st_options[$st->id] = $st->name;
        }
   
      
      return view('layouts.forms')->with('data', $data)->with('edit', true)->with('header', $header) ->with('model', $supplier_a)
    ->with('fields_template', 'common.supplier_activitie-fields');
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         SupplierActivitie::destroy($id);
        $request->session()->flash('message', [
            'alert' => 'success', 'text' => trans('dashboard.messages.deleted')
        ]);

        return redirect()->route('supplier_activitie.index');
    }
}

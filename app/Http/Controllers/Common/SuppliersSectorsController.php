<?php

namespace Edifalia\Http\Controllers\Common;
use Illuminate\Http\Request;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\Common\SupplierSector;
use Edifalia\Models\Common\Status;
use Edifalia\Models\Backend\Supplier;
use Edifalia\Models\Common\SupplierActivitie;

class SuppliersSectorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          
            $data = [
            'breadcrumb_title' => trans('common.supplier_sector_title'),
            'breadcrumb_subtitle' => trans('common.supplier_sector_list_title'),
            'panel_title' => trans('common.lst_supplier_sector_title'),
            'url_new' => route('supplier_sector.create'),
            'theads' => [
                ['align' => 'text-center', 'supplier_id' => 'common.lst_supplier_id_name'],
            ],
            'tbody' => []
        ];
        $suppliers_sectors = SupplierSector::all();
        foreach ($suppliers_sectors as $supplier_sector) {
            $id = $supplier_sector->id;
            $cols  = "<td class='col-xs-10 col-sm-10 col-md-10 col-lg-10'>" . $supplier_sector->supplier_id . "</td>";
            array_push($data['tbody'], [
                'cols' => $cols, 'url_edit' => route('supplier_sector.edit', $id),
                'url_delete' => 'supplier_sector.destroy', 'record_id' => $id
            ]);
        }
        
        return view('layouts.list-records_supplier_sector')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      
       $data = [
            'breadcrumb_title' => trans('common.supplier_contact_management_title'),
            'breadcrumb_subtitle' => trans('common.suppliers_sector_add_title'),
            'breadcrumb_link' => route('supplier_sector.index'),
            'panel_title' => trans('common.suppliers_sector_new_title'),
            'btn_record_submit' => trans('common.btn_save_text'),
        ];
        $header = [
            'route' => 'supplier_sector.store', 
            'method' => 'POST', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];
        $estatus = Status::all();
        $st_options = [];
        foreach ($estatus as $st) {
            $st_options[$st->id] = $st->name;
        }
       $suppliers_activities = SupplierActivitie::all();
        $suppliers_activities_options = [];
        foreach ($suppliers_activities as $supplier_activitie) {
            $suppliers_activities_options[$supplier_activitie->id] = $supplier_activitie->name;
        }
      
        $suppliers = Supplier::all();
        $supplier_options = [];
        foreach($suppliers as $supplier) {
           $supplier_options[$supplier->id] = $supplier->business_name; 
        }


        return view('layouts.forms')->with('data', $data)->with('new', true)->with('header', $header)
        ->with('status', $st_options)->with('suppliers_activities', $suppliers_activities_options)->with('suppliers', $supplier_options)
      ->with('fields_template', 'common.supplier_sector-fields');
    

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $supplier_sector = new  SupplierSector();
        $supplier_sector->fill($request->all());
        $supplier_sector->save();

        $request->session()->flash('message', [
            'alert' => 'success', 'text' => trans('dashboard.messages.created')
        ]);

        return redirect()->route('supplier_sector.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supplier_sector = SupplierSector::findOrfail($id);

        $data = [
            'breadcrumb_title' => trans('common.supplier_sector_title'),
            'breadcrumb_subtitle' => trans('common.supplier_sector_list_title'),
            'breadcrumb_link' => route('supplier_sector.index'),
            'panel_title' => trans('common.suppliers_sector_edit_title'),
            'btn_record_submit' => trans('common.btn_save_text'),

       ];

        $header = [
            'route' => ['supplier_sector.update', $supplier_sector->id], 
            'method' => 'PUT', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];

         $estatus = Status::all();
        $st_options = [];
        foreach ($estatus as $st) {
            $st_options[$st->id] = $st->name;
        }
       $suppliers_activities = SupplierActivitie::all();
        $suppliers_activities_options = [];
        foreach ($suppliers_activities as $supplier_activitie) {
            $suppliers_activities_options[$supplier_activitie->id] = $supplier_activitie->name;
        }
      
        $suppliers = Supplier::all();
        $supplier_options = [];
        foreach($suppliers as $supplier) {
           $supplier_options[$supplier->id] = $supplier->business_name; 
        }

         return view('layouts.forms')->with('data', $data)->with('edit', true)->with('header', $header)->with('model', $supplier_sector)
        ->with('status', $st_options)->with('suppliers_activities', $suppliers_activities_options)->with('suppliers', $supplier_options)
      ->with('fields_template', 'common.supplier_sector-fields');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $supplier_sector = SupplierSector::find($id); 
         if(!is_null($supplier_sector)){
            $supplier_sector->fill($request->all());
            $supplier_sector->save();
            $request->session()->flash('message', [
                'alert' => 'success', 'text' => trans('dashboard.messages.updated')
            ]);
         }
          return redirect()->route('supplier_sector.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

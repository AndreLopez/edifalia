<?php

namespace Edifalia\Http\Controllers\Common;

use Illuminate\Http\Request;
use Edifalia\Models\Common\TextMessage;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\InvoicingConcept;
use Edifalia\Models\Common\Language;
use Edifalia\Models\Backend\Person;
use Edifalia\User;
use Edifalia\Models\Backend\Community;
use Twilio;

class TextMessageController extends Controller
{
   public function create()
   {
      $communities = [];
      $users = [];
      $invoicingConcepts = [];
      $languages = [];

  		$data = [
  			'breadcrumb_title' => trans('common.property_management_title'),
  			'breadcrumb_subtitle' => trans('common.property_add_title'),
  			'breadcrumb_link' => route('property.index'),
  			'panel_title' => trans('common.new_property_title'),
  			'btn_send_email' => 'Enviar',
  		];

  		$header = [
  			'route' => 'text.messages.store', 
  			'method' => 'POST', 
  			'role' => 'form',
  			'class' => 'form-horizontal',
  			'files' => true
  		];

      if (Community::all()->count() > 0)
        $communities = Community::all()->pluck('name', 'id');

      if (User::all()->count() > 0)
        $users = User::all()->pluck('email', 'id');

      if (InvoicingConcept::all()->count() > 0)
        $invoicingConcepts = InvoicingConcept::all()->pluck('name', 'id');

      if (Language::all()->count() > 0)
        $languages = Language::all()->pluck('name', 'id');

      return view('layouts.forms')
      ->with('data', $data)
      ->with('communities', $communities)
      ->with('users', $users)
      ->with('invoicingConcepts', $invoicingConcepts)
      ->with('languages', $languages)
      ->with('new', true)
      ->with('header', $header)
      ->with('fields_template', 'common.text-message-fields');
   }

   public function store(Request $request)
   {

   		 $this->validate($request, [
            'phone_number' => 'required',
            'body' => 'required|max:160',
        ]);

       $result = Person::search($request->all());
        $persons = $result->get();
        foreach ($persons as $person) {
          Twilio::message($person->phone, $request->input('body'));  
        }

      if ($request->has('phone_number')) {
        Twilio::message($request->input('phone_number'), $request->input('body'));  
      }  

   		$textMessage = new TextMessage();
   		$textMessage->phone_number = $request->input('phone_number');
   		$textMessage->body = $request->input('body');
   		$textMessage->user_id = \Auth::user()->id;
   		$textMessage->save();

   		return redirect()->route('text.messages.create');
   }

}

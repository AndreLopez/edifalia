<?php

namespace Edifalia\Http\Controllers\Common;

use Illuminate\Http\Request;

use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\Backend\Community;
use Edifalia\Models\Common\Message;
use Edifalia\Models\Common\MessageFile;
use Illuminate\Support\Facades\Storage;
use Edifalia\User;
use Edifalia\Http\Requests\CreateEmailRequest;
use Edifalia\Models\InvoicingConcept;
use Edifalia\Models\Common\Language;
use Edifalia\Models\Backend\Person;

class MessagesController extends Controller
{
   

	public function received()
	{
		$data = [
        'breadcrumb_title' => trans('common.property_management_title'),
        'breadcrumb_subtitle' => trans('common.property_list_title'),
        'panel_title' => trans('common.lst_property_title'),
        'url_new' => route('property.create'),
        'theads' => [
          [ 'align' => 'text-center', 'name' => 'Fecha'],
          [ 'align' => 'text-center', 'name' => 'De'],
          [ 'align' => 'text-center', 'name' => 'Desde'],
          //[ 'align' => 'text-center', 'name' => 'Para'],
          [ 'align' => 'text-center', 'name' => 'Asunto'],
          [ 'align' => 'text-center', 'name' => 'Prioridad'],
          [ 'align' => 'text-center', 'name' => 'Conf. Lectura'],
          [ 'align' => 'text-center', 'name' => 'Adjuntos'],
        ],
        'tbody' => []
      ];

      $messages =  \Auth::user()->receivedMessages()->withTrashed()->get();


      foreach ($messages as $message)
      {

      	$id = $message->id;
      	$cols  = "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . $message->created_at . "</td>";
      	$cols  .= "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . $message->user->owner->full_name . "</td>";
      	$cols  .= "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . $message->from . "</td>";
      	//$cols  .= "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . "Para" . "</td>";
      	$cols  .= "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . $message->subject . "</td>";
      	$cols  .= "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . $message->priority . "</td>";
      	$cols  .= "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" .$message->read. "</td>";
      	$cols  .= "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . $message->countFiles() . "</td>";

      	array_push($data['tbody'], 
      	[
      		'cols' => $cols, 
      		'url_show' => route('show.received.messages', $id),
      		'url_delete' => 'delete.received.message', 
      		'record_id' => $id
      	]);
      }

      return view('layouts.list-records')->with('data', $data);
	}

	public function sent()
	{
		$data = [
			'breadcrumb_title' => trans('common.property_management_title'),
			'breadcrumb_subtitle' => trans('common.property_list_title'),
			'panel_title' => trans('common.lst_property_title'),
			'url_new' => route('message.create'),
			'theads' => [
				[ 'align' => 'text-center', 'name' => 'Fecha'],
				[ 'align' => 'text-center', 'name' => 'De'],
				[ 'align' => 'text-center', 'name' => 'Desde'],
				//[ 'align' => 'text-center', 'name' => 'Para'],
				[ 'align' => 'text-center', 'name' => 'Asunto'],
				[ 'align' => 'text-center', 'name' => 'Prioridad'],
				[ 'align' => 'text-center', 'name' => 'Conf. Lectura'],
				[ 'align' => 'text-center', 'name' => 'Adjuntos'],
			],
			'tbody' => []
		];

		$messages =  \Auth::user()->sentMessages()->get();
		foreach ($messages as $message)
		{
			$id = $message->id;
			$cols  = "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . $message->created_at . "</td>";
			$cols  .= "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . $message->user->owner->full_name . "</td>";
			$cols  .= "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . $message->from . "</td>";
			//$cols  .= "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . "Para" . "</td>";
			$cols  .= "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . $message->subject . "</td>";
			$cols  .= "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . $message->priority . "</td>";
			$cols  .= "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" .$message->read_counter. "</td>";
			$cols  .= "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . $message->countFiles() . "</td>";

			array_push($data['tbody'], 
			[
				'cols' => $cols, 
				'url_show' => route('show.sent.messages', $id),
				'url_delete' => 'delete.sent.message', 
				'record_id' => $id
			]);
		}

		return view('layouts.list-records')->with('data', $data);
	}

	public function create()
	{
		$communities = [];
		$users = [];
		$invoicingConcepts = [];
		$languages = [];

		$priorities =  [
			'high' => 'Alta', 
			'medium' => 'Media', 
			'normal' => 'Normal'
		];

		$data = [
			'breadcrumb_title' => trans('common.property_management_title'),
			'breadcrumb_subtitle' => trans('common.property_add_title'),
			'breadcrumb_link' => route('property.index'),
			'panel_title' => trans('common.new_property_title'),
			'btn_send_email' => 'Enviar',
		];

		$header = [
			'route' => 'message.store', 
			'method' => 'POST', 
			'role' => 'form',
			'class' => 'form-horizontal',
			'files' => true
		];

		if (Community::all()->count() > 0)
        	$communities = Community::all()->pluck('name', 'id');

      	if (User::all()->count() > 0)
      		$users = User::all()->pluck('email', 'id');

      	if (InvoicingConcept::all()->count() > 0)
      		$invoicingConcepts = InvoicingConcept::all()->pluck('name', 'id');

      	if (Language::all()->count() > 0)
      		$languages = Language::all()->pluck('name', 'id');

		return view('layouts.forms')
        ->with('data', $data)
        ->with('communities', $communities)
        ->with('priorities', $priorities)
        ->with('users', $users)
        ->with('invoicingConcepts', $invoicingConcepts)
        ->with('languages', $languages)
        ->with('new', true)
        ->with('header', $header)
        ->with('fields_template', 'common.message-fields');

	}

	public function store(CreateEmailRequest $request)
	{
		$data = array();
		$userEmailLogged = \Auth::user()->email;

		if ($request->has('from') ) {
			$userId = $request->input('from');
			$user = User::find($userId);
			$email = $user->email;
		}

		$message = new Message();
		$message->from = isset($email) ? $email : $userEmailLogged;
		$message->subject = $request->input('subject');
		$message->priority = $request->input('priority');
		$message->body = $request->input('body');
		$message->user_id = isset($user) ? $user->id : \Auth::user()->id;
		$message->save(); 


		/*if ($request->has('community_ids')) {
			$communityIds = array_filter($request->input('community_ids'));
			foreach ($communityIds as $key => $communityId) 
			{
				$community = Community::find($communityId);
				if ($community->hasProperties()) {
					foreach ($community->properties as $key => $property) {
						$property->person->user->receivedMessages()->attach($message->id);
					}
				}
			}
		}*/

		$result = Person::search($request->all());
        $persons = $result->get();
        foreach ($persons as $person) {
       		$person->user->receivedMessages()->attach($message->id);
        }
		
		if ($request->has('users_id')) {
			$userIds = array_filter($request->input('users_id'));
			foreach ($userIds as $key => $userId) {
				$user = User::find($userId);
				$received = $user->receivedMessages()->whereMessageId($message->id)->first();
				if (count($received) == 0) {
					$user->receivedMessages()->attach($message->id);
				}
			}
		}

		$data['message_id'] = $message->id;

		if ($request->hasFile('files')) {
			foreach ($request->file('files') as $key => $file) {
				$messageFile = new MessageFile();
				$messageFile->registerFile($file, $data);
			}
		}

		$request->session()->flash('message', [
        	'type' => 'store',
        	'alert' => 'success',
        	'text' => trans('dashboard.messages.created')
        ]); 

        return redirect()->route('message.create');

        /*$result = Person::search($request->all());
        $persons = $result->get();
        foreach ($persons as $person) {
        	echo $person->user->email .'<br/>';
        }
        dd($persons);*/
	}

	public function showSent(Request $request, $id)
	{
		$message =  \Auth::user()->sentMessages()->whereId($id)->first();

		$data = [
			'breadcrumb_title' => trans('common.property_management_title'),
			'breadcrumb_subtitle' => trans('common.property_add_title'),
			'breadcrumb_link' => route('property.index'),
			'panel_title' => trans('common.new_property_title'),
			'btn_record_submit' => trans('common.btn_save_text'),
		];

		return view('layouts.messages')
        ->with('data', $data)
        ->with('message', $message);
	}

	public function showReceived(Request $request, $id)
	{
		$message =  \Auth::user()
			->receivedMessages()
			->withTrashed()
			->whereMessageId($id)
			->first();

		if (!$message->pivot->read) {
			$message->recipients()
			->withTrashed()
			->updateExistingPivot(\Auth::user()->id, ['read' => true]);
		}
		
		$data = [
			'breadcrumb_title' => trans('common.property_management_title'),
			'breadcrumb_subtitle' => trans('common.property_add_title'),
			'breadcrumb_link' => route('property.index'),
			'panel_title' => trans('common.new_property_title'),
			'btn_record_submit' => trans('common.btn_save_text'),
		];

		return view('layouts.messages')
        ->with('data', $data)
        ->with('message', $message);
	}


	public function destroySent(Request $request, $id)
    {
    	if ($request->ajax()) {
            $message =  \Auth::user()->sentMessages()->whereId($id)->first();
            if (!is_null($message)) {
                $message->delete();
                $request->session()->flash('message', [
                    'alert' => 'success', 'text' => trans('dashboard.messages.deleted')
                ]);
                return response()->json(['result' => true]);
            }
            return response()->json(['result' => false]);
        }
        return redirect()->route('sent.messages');
    }

    public function destroyReceived(Request $request, $id)
    {
    	if ($request->ajax()) 
    	{
    		\Auth::user()->receivedMessages()->detach([$id]);
    		$request->session()->flash('message', [
    			'type' => 'destroy',
    			'alert' => 'success',
    			'text' => trans('dashboard.messages.deleted')
    		]);
    		return response()->json(['result' => true]);
    	}
    	return redirect()->route('received.messages');
    }

	public function download($fileName)
	{
		$public_path = public_path();
		$url = $public_path.'/storage/'.$fileName;
		if (Storage::exists($fileName))
		{
			return response()->download($url);
		}
		abort(404);
	}

}

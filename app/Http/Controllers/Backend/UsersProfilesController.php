<?php

namespace Edifalia\Http\Controllers\Backend;
use Illuminate\Http\Request;

use Edifalia\Http\Controllers\Controller;
use Illuminate\Database\Query\Builder;
use Edifalia\User; 
use Illuminate\Support\Facades\Auth;

class UsersProfilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $data = [
            'breadcrumb_title' => trans('user-profile.user_profile_management_title'),
            'panel_title' => trans('user-profile.user_information_title'),

            'theads' => [
                ['align' => 'text-center', 'name' => 'common.lst_col_name'],
            ],
            'tbody' => []
        ];
              
 
      return view('auth.list-users')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          $users = User::findOrfail($id);
          $data = [
            'breadcrumb_title' => trans('user-profile.user_profile_management_title'),
            'panel_title' => trans('user-profile.user_information_title'),
            'panel_title' => trans('community.new_title'),
              'breadcrumb_link' => route('user-profile.index'),
            'btn_record_submit' => trans('common.btn_save_text'),
        ];

           $header = [
            'route' => ['user-profile.update', $users->id], 
            'method' => 'PUT', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];

        return view('layouts.forms')->with('data', $data)->with('edit', true)->with('header', $header)->with('model', $users)->with('fields_template', 'auth.user-profile-fields');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }
   

    public function update(Request $request, $id)
    {
         $user = User::find($id); 
       if (!is_null($user)) {
      $user->email = $request->input('email');
      $user->password = bcrypt($request->input('password'));
      $user->save();
 
         $request->session()->flash('message', [
                'alert' => 'success', 'text' => trans('dashboard.messages.updated')
            ]);
    }
        return redirect()->route('user-profile.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace Edifalia\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Edifalia\Http\Requests;
use Edifalia\Http\Controllers\Controller;
use Venturecraft\Revisionable\Revision;

class ActionHistoryController extends Controller
{
    public function getHistory()
    {
        $revisions = Revision::latest()->paginate(15);
        
        return view('backend.utilities.action_history.index')
            ->with('revisions', $revisions);
    }
}

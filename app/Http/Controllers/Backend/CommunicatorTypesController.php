<?php

namespace Edifalia\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\CommunicatorType;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

class CommunicatorTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $communicatorTypes = CommunicatorType::paginate(10);
        return view('backend.management.communicator_types.index')
            ->with('communicatorTypes', $communicatorTypes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $header = [
            'route' => 'communicator-types.store',
            'method' => 'POST',
            'role' => 'form',
        ];
        return view('backend.management.communicator_types.create_or_edit')
            ->with('header', $header)->with('new', true)
            ->with('title', trans('dashboard.titles.add_communicator_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Validator::make($request->all(), [
            'name' => 'required|max:200|unique:communicator_types'
        ])->validate();

        $communicatorType = new CommunicatorType();
        $communicatorType->fill($request->all());
        $communicatorType->save();

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.created')
        ]);

        return redirect()->route('communicator-types.edit', $communicatorType->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $communicatorType = CommunicatorType::find($id);
        if (is_null($communicatorType))
            return redirect()->route('communicator-types.index');

        $header = [
            'route' => ['communicator-types.update', $communicatorType->id],
            'method' => 'PUT',
            'role' => 'form'
        ];

        return view('backend.management.communicator_types.create_or_edit')
            ->with('header', $header)
            ->with('title', trans('dashboard.titles.edit_communicator_type'))
            ->with('communicatorType', $communicatorType)->with('edit', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $communicatorType = CommunicatorType::find($id);
        if (is_null($communicatorType))
            return redirect()->route('communicator-types.index');

        \Validator::make($request->all(), [
            'name' => [
                'required',
                'max:200',
                Rule::unique('communicator_types', 'name')->ignore($communicatorType->id)]
        ])->validate();

        $communicatorType->fill($request->all());
        $communicatorType->save();

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.updated')
        ]);
        return redirect()->route('communicator-types.edit', $communicatorType->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $communicatorType = CommunicatorType::find($id);
            if (!is_null($communicatorType)){
                $communicatorType->delete();
                return response()->json([
                    'data' => true,
                    'id' => $id,
                    'message' => trans('dashboard.messages.deleted'),
                    'undo' => trans('dashboard.buttons.undo')
                ]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }

    public function postUndelete(Request $request, $id)
    {
        if($request->ajax()){
            $communicatorType = CommunicatorType::onlyTrashed()->where('id', $id)->first();
            if (!is_null($communicatorType)){
                $communicatorType->restore();
                return response()->json(['data' => true, 'id' => $id]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }
}

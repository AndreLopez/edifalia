<?php

namespace Edifalia\Http\Controllers\Backend;

use Edifalia\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Edifalia\Http\Requests\Backup\BackupRequest;
use Chumper\Zipper\Zipper;
use Spatie\DbDumper\Databases\MySql;

class BackupsController extends Controller
{
    public function getIndex()
    {
        $tables[''] = trans('dashboard.fields.select_tables');
        $all= \DB::select('SHOW TABLES');
        $connection = config('database.default');
        $db_name = config('database.connections.'.$connection.'.database');
        $database = 'Tables_in_'.$db_name;
        foreach ($all as $table){
            $tables[$table->$database] = $table->$database;
        }
        $this->deleteFiles();
        return view('backend.utilities.backups.index')->with('tables', $tables);
    }

    public function postExport(BackupRequest $request)
    {
        $time = time();
        if ($request->get('format') == 'xls'){
            Excel::create('backup_'.$time, function($excel) use($request) {
                foreach ($request->get('tables') as $table){
                    $excel->sheet($table, function($sheet) use($table) {
                        $rows = \DB::table($table)->get();
                        $data = $rows->toJson();
                        $sheet->fromArray(json_decode($data, true));
                    });
                }
            })->export($request->get('format'));
        }
        elseif ($request->get('format') == 'csv'){

            foreach ($request->get('tables') as $table) {
                $rows = \DB::table($table)->get();
                $data = $rows->toJson();
                Excel::create($table.'_'.$time, function ($excel) use ($request, $data) {
                    $excel->sheet('sheet1', function ($sheet) use ($data) {
                        $sheet->fromArray(json_decode($data, true));
                    });
                })->store($request->get('format'));

            }
            $files = glob(storage_path('exports/*'.$time.'.csv'));
            $zip = new Zipper;
            $zip->make(storage_path('exports/backup_'.$time.'.zip'))->add($files);
            $zip->close();
            return response()->download(storage_path('exports/backup_'.$time.'.zip'));
        }
        else {
            MySql::create()->setDbName(config('database.connections.'.config('database.default').'.database'))
                ->setUserName(config('database.connections.'.config('database.default').'.username'))
                ->setPassword(config('database.connections.'.config('database.default').'.password'))
                ->includeTables($request->get('tables'))
                ->dumpToFile('backup_'.$time.'.sql');

            return response()->download(public_path('backup_'.$time.'.sql'));
        }
    }

    private function deleteFiles()
    {
        $filesXlsAndCsv = glob(storage_path('exports/*'));
        foreach ($filesXlsAndCsv as $file){
            unlink($file);
        }
        $filesSql = glob(public_path('*.sql'));
        foreach ($filesSql as $file){
            unlink($file);
        }
    }
}

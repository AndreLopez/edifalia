<?php

namespace Edifalia\Http\Controllers\Backend;

use Carbon\Carbon;
use Edifalia\Http\Requests\Budget\CreateEditRequest;
use Edifalia\Models\Common\Property;
use Illuminate\Http\Request;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\Budget;
use Illuminate\Support\Facades\Session;
use Edifalia\Models\Backend\Community;
use Illuminate\Support\Facades\Gate;
use Edifalia\Repositories\BudgetRepository;

class BudgetsController extends Controller
{
    protected $budgetRepo;

    public function __construct(BudgetRepository $budgetRepo)
    {
        $this->budgetRepo = $budgetRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Gate::allows('admin')){
            $budgets = $this->budgetRepo->getAll();
        } else if(Gate::allows('employee')){
            $budgets = $this->budgetRepo->getEmployeeBudgets();
        }
        return view('backend.management.budgets.index')->with('budgets', $budgets);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $header = [
            'route' => 'budgets.store',
            'method' => 'POST',
            'role' => 'form',
        ];
        return view('backend.management.budgets.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.add_budget'))->with('new', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEditRequest $request)
    {
        if($this->budgetRepo->checkCommunity($request->get('community_id'))){
            $budget = $this->budgetRepo->saveNewWithUserAndBudget($request);
        } else {
            return redirect()->route('budgets.create');
        }

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.created')
        ]);

        return redirect()->route('budgets.edit', $budget->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $budget = $this->budgetRepo->find($id);
        if (is_null($budget) || Gate::denies('has', $budget->user_id))
            return redirect()->route('budgets.index');

        $header = [
            'route' => ['budgets.update', $budget->id],
            'method' => 'PUT',
            'role' => 'form'
        ];

        return view('backend.management.budgets.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.edit_budget'))
            ->with('budget', $budget)->with('edit', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateEditRequest $request, $id)
    {
        $budget = $this->budgetRepo->find($id);
        if (is_null($budget) || Gate::denies('has', $budget->user_id))
            return redirect()->route('budgets.index');

        if($this->budgetRepo->checkCommunity($request->get('community_id'))){
            $this->budgetRepo->updateBudget($budget, $request);
        } else {
            return redirect()->route('budgets.edit', $budget->id);
        }

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.updated')
        ]);
        return redirect()->route('budgets.edit', $budget->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $budget = $this->budgetRepo->find($id);
            if (!is_null($budget) && Gate::allows('has', $budget->user_id)){
                $this->budgetRepo->delete($budget);
                return response()->json([
                    'data' => true,
                    'id' => $id,
                    'message' => trans('dashboard.messages.deleted'),
                    'undo' => trans('dashboard.buttons.undo')
                ]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }

    public function postUndelete(Request $request, $id)
    {
        if($request->ajax()){
            $budget = $this->budgetRepo->recordInTrash($id);
            if (!is_null($budget)){
                $this->budgetRepo->restore($budget);
                return response()->json(['data' => true, 'id' => $id]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }

    public function getBudgetData(Request $request)
    {
        if ($request->ajax())
        {
            $budget = $this->budgetRepo->find($request->get('budget_id'));
            if(!is_null($budget)){
                return response()->json([
                    'data' => true,
                    'title' => $budget->title,
                    'invoicing' => $budget->invoicingConcept->id,
                    'periodicity' => $budget->periodicity,
                    'rounding' => $budget->rounding,
                    'duration' => $budget->duration,
                    'month' => $budget->month,
                    'year' => $budget->year
                ]);
            }
            else{
                return response()->json([
                    'data' => false,
                    'message' => trans('dashboard.messages.error')
                ]);
            }
        }
    }

    public function getCommunityProperties(Request $request)
    {
        if($request->ajax()){
            $community = Community::find($request->community_id);
            if(!is_null($community)){
                $properties = $community->properties()->get();
                $html = '';
                if($properties->count()){
                    $general = trans('dashboard.fields.general');
                    $amount = trans('dashboard.fields.amount');
                    foreach ($properties as $property){
                        $generalValue = '';
                        $amountValue = '';
                        if($request->has('objective_id')){
                            $budget = $property->budgets()
                                ->where('budget_id',$request->get('objective_id'))
                                ->first();
                            if(!is_null($budget)){
                                $generalValue = $budget->pivot->general;
                                $amountValue = $budget->pivot->amount;
                            }
                        }
                        $html .= "<div class='row'>";
                        $html .= "<div class='form-group col-md-4'>";
                        $html .= "<p><strong>{$property->property_number} - {$property->building_number}</strong></p>";
                        $html .= "</div><div class='form-group col-md-4'>";
                        $html .= "<input type='text' name='general[$property->id]' class='form-control' placeholder='{$general}' value='{$generalValue}'>";
                        $html .= "</div><div class='form-group col-md-4'>";
                        $html .= "<input type='text' name='amount[]' class='form-control' placeholder='{$amount}' value='{$amountValue}' disabled>";
                        $html .= "</div>";
                        $html .= "</div>";
                    };
                }
                else {
                    $html .= "<p class='text-yellow'>".trans('dashboard.messages.no_properties')."</p>";
                }
                return response()->json(['data' => true, 'html' => $html]);
            }
            else {
                return response()->json([
                    'data' => false,
                    'message' => trans('dashboard.messages.error')
                ]);
            }
        }
    }
}

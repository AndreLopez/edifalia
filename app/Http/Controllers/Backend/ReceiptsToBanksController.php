<?php

namespace Edifalia\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Repositories\ReceiptsToBankRepository;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Edifalia\Http\Requests\ReceiptsToBank\CreateRequest;

class ReceiptsToBanksController extends Controller
{
    protected $receiptsToBankRepo;

    public function __construct(ReceiptsToBankRepository $receiptsToBankRepo)
    {
        $this->receiptsToBankRepo = $receiptsToBankRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Gate::allows('admin')){
            $receiptsToBanks = $this->receiptsToBankRepo->getAll();
        } else if(Gate::allows('employee')){
            $receiptsToBanks = $this->receiptsToBankRepo->getEmployeeReceiptsToBanks();
        }
        return view('backend.management.receipts_to_banks.index', compact('receiptsToBanks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $header = [
            'route' => 'receipts-banks.store',
            'method' => 'POST',
            'role' => 'form',
        ];
        return view('backend.management.receipts_to_banks.create')
            ->with('header', $header)
            ->with('title', trans('dashboard.titles.add_receipts_to_bank'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        if($this->receiptsToBankRepo->checkCommunities($request->get('communities'))){
            $receiptsToBank = $this->receiptsToBankRepo->saveNewWithUser($request->all());
            $this->receiptsToBankRepo->saveCommunities($receiptsToBank,
                $request->get('communities'));
        } else {
            return redirect()->route('receipts-banks.create');
        }

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.created')
        ]);

        return redirect()->route('receipts-banks.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $receiptsToBank = $this->receiptsToBankRepo->find($id);

        if (is_null($receiptsToBank) || Gate::denies('has', $receiptsToBank->user_id))
            return redirect()->route('receipts-banks.index');

        return view('backend.management.receipts_to_banks.show')
            ->with('receiptsToBank', $receiptsToBank);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $receiptsToBank = $this->receiptsToBankRepo->find($id);
            if (!is_null($receiptsToBank) && Gate::allows('has', $receiptsToBank->user_id)){
                $this->receiptsToBankRepo->delete($receiptsToBank);
                $array = [
                    'data' => true,
                    'id' => $id,
                    'message' => trans('dashboard.messages.deleted'),
                    'undo' => trans('dashboard.buttons.undo')
                ];
            } else {
                $array = [
                    'data' => false,
                    'message' => trans('dashboard.messages.error')
                ];
            }
            return response()->json($array);
        }
    }

    public function postUndelete(Request $request, $id)
    {
        if($request->ajax()){
            $receiptsToBank = $this->receiptsToBankRepo->recordInTrash($id);
            if (!is_null($receiptsToBank)){
                $this->receiptsToBankRepo->restore($receiptsToBank);
                $array = ['data' => true, 'id' => $id];
            } else {
                $array = [
                    'data' => false,
                    'message' => trans('dashboard.messages.error')
                ];
            }
            return response()->json($array);
        }
    }
}

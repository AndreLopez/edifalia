<?php

namespace Edifalia\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\InvoicingConcept;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

class InvoicingConceptsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoicingConcepts = InvoicingConcept::paginate(10);
        return view('backend.management.invoicing_concepts.index')
            ->with('invoicingConcepts', $invoicingConcepts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $header = [
            'route' => 'invoicing-concepts.store',
            'method' => 'POST',
            'role' => 'form',
        ];
        return view('backend.management.invoicing_concepts.create_or_edit')
            ->with('header', $header)
            ->with('title', trans('dashboard.titles.add_invoicing_concept'))
            ->with('new', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Validator::make($request->all(), [
            'name' => ['required', 'max:255', 'unique:invoicing_concepts']
        ])->validate();

        $invoicingConcept = new InvoicingConcept();
        $invoicingConcept->fill($request->all());
        $invoicingConcept->save();

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.created')
        ]);

        return redirect()->route('invoicing-concepts.edit', $invoicingConcept->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $invoicingConcept = InvoicingConcept::find($id);
        if (is_null($invoicingConcept))
            return redirect()->route('invoicing-concepts.index');

        $header = [
            'route' => ['invoicing-concepts.update', $invoicingConcept->id],
            'method' => 'PUT',
            'role' => 'form'
        ];

        return view('backend.management.invoicing_concepts.create_or_edit')
            ->with('header', $header)
            ->with('title', trans('dashboard.titles.edit_invoicing_concept'))
            ->with('invoicingConcept', $invoicingConcept)->with('edit', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $invoicingConcept = InvoicingConcept::find($id);
        if (is_null($invoicingConcept))
            return redirect()->route('invoicing-concepts.index');

        \Validator::make($request->all(), [
            'name' => [
                'required',
                'max:255',
                Rule::unique('invoicing_concepts', 'name')->ignore($invoicingConcept->id)]
        ])->validate();

        $invoicingConcept->fill($request->all());
        $invoicingConcept->save();

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.updated')
        ]);
        return redirect()->route('invoicing-concepts.edit', $invoicingConcept->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $invoicingConcept = InvoicingConcept::find($id);
            if (!is_null($invoicingConcept)){
                $invoicingConcept->delete();
                return response()->json([
                    'data' => true,
                    'id' => $id,
                    'message' => trans('dashboard.messages.deleted'),
                    'undo' => trans('dashboard.buttons.undo')
                ]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }

    public function postUndelete(Request $request, $id)
    {
        if($request->ajax()){
            $invoicingConcept = InvoicingConcept::onlyTrashed()->where('id', $id)->first();
            if (!is_null($invoicingConcept)){
                $invoicingConcept->restore();
                return response()->json(['data' => true, 'id' => $id]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }
}

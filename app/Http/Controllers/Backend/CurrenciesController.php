<?php

namespace Edifalia\Http\Controllers\Backend;

use Edifalia\Repositories\CurrencyRepository;
use Illuminate\Http\Request;
use Edifalia\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

class CurrenciesController extends Controller
{
    protected $currencyRepo;

    public function __construct(CurrencyRepository $currencyRepo)
    {
        $this->currencyRepo = $currencyRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currencies = $this->currencyRepo->getAll();
        return view('backend.utilities.currencies.index', compact('currencies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $header = [
            'route' => 'currencies.store',
            'method' => 'POST',
            'role' => 'form',
        ];
        return view('backend.utilities.currencies.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.add_currency'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Validator::make($request->all(), [
            'name' => 'required|max:30|unique:currencies',
            'symbol' => 'required|max:5',
        ])->validate();

        $currency = $this->currencyRepo->create($request->all());

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.created')
        ]);

        return redirect()->route('currencies.edit', $currency->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $currency = $this->currencyRepo->find($id);
        if (is_null($currency))
            return redirect()->route('currencies.index');

        $header = [
            'route' => ['currencies.update', $currency->id],
            'method' => 'PUT',
            'role' => 'form'
        ];

        return view('backend.utilities.currencies.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.edit_currency'))
            ->with('currency', $currency);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $currency = $this->currencyRepo->find($id);

        if (is_null($currency))
            return redirect()->route('currencies.index');

        \Validator::make($request->all(), [
            'name' => [
                'required',
                'max:30',
                Rule::unique('currencies', 'name')->ignore($currency->id)],
            'symbol' => 'required|max:5',
        ])->validate();
        $currency = $this->currencyRepo->update($currency, $request->all());

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.updated')
        ]);
        return redirect()->route('currencies.edit', $currency->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $currency = $this->currencyRepo->find($id);
            if (!is_null($currency)){
                $this->currencyRepo->delete($currency);
                return response()->json([
                    'data' => true,
                    'id' => $id,
                    'message' => trans('dashboard.messages.deleted'),
                    'undo' => trans('dashboard.buttons.undo')
                ]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }

    public function postUndelete(Request $request, $id)
    {
        if($request->ajax()){
            $currency = $this->currencyRepo->recordInTrash($id);
            if (!is_null($currency)){
                $this->currencyRepo->restore($currency);
                return response()->json(['data' => true, 'id' => $id]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }
}

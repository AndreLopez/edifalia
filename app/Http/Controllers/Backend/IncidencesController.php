<?php

namespace Edifalia\Http\Controllers\Backend;

use Edifalia\Models\IncidenceFile;
use Illuminate\Http\Request;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Http\Requests\Incidence\CreateEditRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Edifalia\Models\Incidence;
use Edifalia\Models\Office;
use Edifalia\Repositories\UploadFileRepository;
use Edifalia\Repositories\IncidenceRepository;
use Illuminate\Support\Facades\Gate;

class IncidencesController extends Controller
{
    protected $incidenceRepo;

    public function __construct(IncidenceRepository $incidenceRepo)
    {
        $this->incidenceRepo = $incidenceRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Gate::allows('admin')){
            $incidences = $this->incidenceRepo->getAll();
        } else if(Gate::allows('employee')){
            $incidences = $this->incidenceRepo->getEmployeeIncidences();
        }
        return view('backend.management.incidences.index')
            ->with('incidences', $incidences);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $header = [
            'route' => 'incidences.store',
            'method' => 'POST',
            'role' => 'form',
            'files' => 'true'
        ];
        return view('backend.management.incidences.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.add_incidence'))->with('new', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEditRequest $request, UploadFileRepository $up)
    {
        if($this->incidenceRepo->checkCommunity($request->get('community_id'))){
            $incidence = $this->incidenceRepo->saveNewWithUserAndIncidence($request);
        } else {
            return redirect()->route('incidences.create');
        }

        for ($i = 1; $i < 5; $i++){
            if ($request->hasFile('file'.$i)){
                if (!$up->uploadFile($request->file('file'.$i), 'local_incidences')) {
                    Session::flash('message', [
                        'alert' => 'danger',
                        'text' => $up->getErrorMessage(),
                    ]);
                    return redirect()->route('incidences.edit', $incidence->id);
                }
                else {
                    IncidenceFile::create([
                        'incidence_id' => $incidence->id,
                        'file' => $up->getFileName(),
                        'title' => $request->get('file_title'.$i),
                        'publish' => $request->get('publish_file'.$i),
                    ]);
                }
            }
        }

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.created')
        ]);

        return redirect()->route('incidences.edit', $incidence->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $incidence = $this->incidenceRepo->find($id);

        if (is_null($incidence) || Gate::denies('has', $incidence->user_id))
            return redirect()->route('incidences.index');

        return view('backend.management.incidences.show')->with('incidence', $incidence);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $incidence = $this->incidenceRepo->find($id);
        if (is_null($incidence) || Gate::denies('has', $incidence->user_id))
            return redirect()->route('incidences.index');

        $header = [
            'route' => ['incidences.update', $incidence->id],
            'method' => 'PUT',
            'role' => 'form',
            'files' => 'true'
        ];

        return view('backend.management.incidences.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.edit_incidence'))
            ->with('incidence', $incidence)->with('edit', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateEditRequest $request, UploadFileRepository $up, $id)
    {
        $incidence = $this->incidenceRepo->find($id);
        if (is_null($incidence) || Gate::denies('has', $incidence->user_id))
            return redirect()->route('incidences.index');

        if($this->incidenceRepo->checkCommunity($request->get('community_id'))){
            $this->incidenceRepo->updateIncidence($incidence, $request);
        } else {
            return redirect()->route('incidences.edit', $incidence->id);
        }

        for ($i = 1; $i < 5; $i++){
            if ($request->hasFile('file'.$i)){
                if (!$up->uploadFile($request->file('file'.$i), 'local_incidences')) {
                    Session::flash('message', [
                        'alert' => 'danger',
                        'text' => $up->getErrorMessage(),
                    ]);
                    return redirect()->route('incidences.edit', $incidence->id);
                }
                else {
                    IncidenceFile::create([
                        'incidence_id' => $incidence->id,
                        'file' => $up->getFileName(),
                        'title' => $request->get('file_title'.$i),
                        'publish' => $request->get('publish_file'.$i),
                    ]);
                }
            }
        }

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.updated')
        ]);
        return redirect()->route('incidences.edit', $incidence->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $incidence = $this->incidenceRepo->find($id);
            if (!is_null($incidence) && Gate::allows('has', $incidence->user_id)){
                $this->incidenceRepo->delete($incidence);
                return response()->json([
                    'data' => true,
                    'id' => $id,
                    'message' => trans('dashboard.messages.deleted'),
                    'undo' => trans('dashboard.buttons.undo')
                ]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }

    public function postUndelete(Request $request, $id)
    {
        if($request->ajax()){
            $incidence = $this->incidenceRepo->recordInTrash($id);
            if (!is_null($incidence)){
                $this->incidenceRepo->restore($incidence);
                return response()->json(['data' => true, 'id' => $id]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }

    public function getCommunities(Request $request)
    {
        if($request->ajax()){
            $office = Office::find($request->id);
            if (!is_null($office)) {
                $communities = [];
                foreach ($office->communities as $community) {
                    if (Gate::allows('employee')) {
                        $result = Auth::user()->employee->communities()
                            ->where('community_id', $community->id)->exists();
                        if ($result)
                            $communities[$community->id] = $community->name;
                    } else {
                        $communities[$community->id] = $community->name;
                    }
                }
                $value = '';
                if (!empty($request->incidence_id)) {
                    $incidence = Incidence::find($request->incidence_id);
                    if (!is_null($incidence)){
                        if($office->communities()->where('id', $incidence->community_id)->exists())
                            $value = $incidence->community_id;
                    }
                }
                return response()->json([
                    'communities' => $communities,
                    'select' => trans('dashboard.fields.select_community'),
                    'value' => $value
                ]);
            }
        }
    }
}

<?php

namespace Edifalia\Http\Controllers\Backend;

use Edifalia\Models\Continent;
use Edifalia\Models\MainUser;
use Illuminate\Http\Request;
use Edifalia\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Session;

class MainUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->mainUser()->exists()){
            return redirect()->route('main-users.edit', Auth::user()->mainUser->id);
        }
        return redirect('/');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $main_user = MainUser::find($id);
        if (is_null($main_user))
            return redirect('/');

        $header = [
            'route' => ['main-users.update', $main_user->id],
            'method' => 'PUT',
            'role' => 'form',
            'id'
        ];
        return view('backend.utilities.main_users.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.edit_main_user'))
            ->with('main_user', $main_user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mainUser = MainUser::find($id);
        if (is_null($mainUser))
            return redirect()->route('main-users.index');

        $ipRule = 'ip';
        foreach ($request->get('ip_list') as $ip){
            if ($ip == "") {
                $ipRule =  '';
                break;
            }
        }

        \Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => ['required', 'email', 'max:255', Rule::unique('users', 'email')
                ->ignore($mainUser->user->id)],
            'password' => 'min:6|confirmed',
            'company' => 'max:200',
            'first_name' => 'required|max:100',
            'last_name' => 'required|max:100',
            'dni' => ['required', 'max:10', Rule::unique('main_users', 'dni')->ignore($mainUser->id)],
            'address' => 'max:255',
            'postal_code_id' => 'required|exists:postal_codes,id',
            'phone' => 'max:20',
            'fax' => 'max:20',
            'signature' => 'max:500',
            'ip_list_option' => 'numeric|in:1,2',
            'ip_list.*' => $ipRule,
            'collegiate_number' => 'numeric',
            'aaff_program' => 'max:100',
            'aaff_email' => ['email', 'max:255', Rule::unique('main_users', 'aaff_email')
                ->ignore($mainUser->id)],
            'letterhead' => 'max:500',
            'language_id' => 'required|numeric|exists:languages,id',
            'currency_id' => 'required|numeric|exists:currencies,id',
            'number_format_id' => 'required|numeric|exists:number_formats,id',
            'date_format_id' => 'required|numeric|exists:date_formats,id',
            'continent_id' => 'required|numeric|exists:continents,id',
            'zone_id' => 'required|numeric|not_in:0|exists:currencies,id',
        ])->validate();

        $mainUser->user->fill($request->all());
        $mainUser->user->save();
        $mainUser->fill($request->all());
        $mainUser->save();

        if ($request->has('ip_list')){
            foreach ($request->get('ip_list') as $ip){
                if (!empty($ip) && !$mainUser->ipAccesses()->where('ip', $ip)->exists()) {
                    $mainUser->ipAccesses()->create([
                        'ip' => $ip,
                    ]);
                }
            }
            foreach ($mainUser->ipAccesses as $ipAccess) {
                if (!in_array($ipAccess->ip, $request->get('ip_list'))) {
                    $ipAccess->delete();
                }
            }
        }

        Session::flash('message', [
            'alert' => 'success',
            'icon' => 'check',
            'text' => trans('dashboard.messages.updated')
        ]);

        return redirect()->route('main-users.edit', $mainUser->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getZones(Request $request)
    {
        if($request->ajax()){

            $data = false;
            $result = trans('dashboard.messages.error');
            $userZoneId = (Auth::user()->mainUser()->exists()) ? Auth::user()->mainUser->zone_id: null;

            $continent = Continent::find($request->get('continent_id'));
            if(!is_null($continent)){
                if ($continent->zones()->exists()){
                    $zones[0] = trans('dashboard.fields.select_zone');
                    foreach ($continent->zones()->get() as $zone){
                        $zones[$zone->id] = $zone->name;
                    }
                    $data = true;
                    $result = $zones;
                } else {
                    $result = trans('dashboard.messages.not_zones');
                }
            }
            return response()->json([
                'data' => $data,
                'result' => $result,
                'userZoneId' => $userZoneId
            ]);
        }
    }
}

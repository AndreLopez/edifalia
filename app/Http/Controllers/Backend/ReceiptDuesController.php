<?php

namespace Edifalia\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Edifalia\Models\ReceiptDue;
use Edifalia\Models\ReceiptDueItem;
use Edifalia\Models\InvoicingConcept;
use Edifalia\Models\Backend\Community;
use Edifalia\Models\Common\Property;
use Edifalia\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Session;

class ReceiptDuesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'breadcrumb_title' => trans('receipt_due.management_title'),
            'breadcrumb_subtitle' => trans('receipt_due.listing_title'),
            'panel_title' => trans('receipt_due.list_title'),
            'url_new' => route('receiptdues.create'),
            'theads' => [
                ['align' => 'text-center', 'name' => 'common.lst_col_name'],
            ],
            'tbody' => []
        ];
        $receiptdue = ReceiptDue::receipt_due_all();

        foreach ($receiptdue as $row)
        {
            $id = $row->id;
            $cols  = "<td class='col-xs-2 col-sm-2 col-md-2 col-lg-2'>". $row->id ."</td>";
            $cols .= "<td class='col-xs-2 col-sm-2 col-md-2 col-lg-2'>". $row->name ."</td>";
            $cols .= "<td class='col-xs-2 col-sm-2 col-md-2 col-lg-2'>". $row->total_amount ."</td>";
            $cols .= "<td class='col-xs-2 col-sm-2 col-md-2 col-lg-2'>". $row->month ."</td>";
            $cols .= "<td class='col-xs-2 col-sm-2 col-md-2 col-lg-2'>". $row->year ."</td>";
            
            array_push($data['tbody'], [
                'cols' => $cols,
                'url_show' => route('receiptdues.show', $id),
                'url_edit' => route('receiptdues.edit', $id),
                'url_delete' => 'receiptdues.destroy', 'record_id' => $id
            ]);
        }

        return view('backend.receiptdue.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'breadcrumb_title' => trans('receipt_due.management_title'),
            'breadcrumb_subtitle' => trans('receipt_due.add_title'),
            'panel_title' => trans('receipt_due.new_title'),
            'btn_record_submit' => trans('common.btn_save_text'),
            'breadcrumb_link' => route('receiptdues.index'),
            'url_new' => route('receiptdues.create'),
            'form_url' => url('/receiptdues'),
            'form_method' => 'POST'
        ];

        $receiptdue = new ReceiptDue;
        $receiptdueItem = [];

        // Get communities
        if (Community::all()->count() > 0)
        {
            $communities = [];
            foreach (ReceiptDue::get_communities() as $value)
            {
                $communities[$value->id] = $value->name;
            }
        }
        // Get all invoicing concepts
        if (InvoicingConcept::all()->count() > 0) $concept = InvoicingConcept::all()->pluck('name', 'id');

        return view('backend.receiptdue.create-edit-fields', compact('data', 'receiptdue', 'receiptdueItem', 'communities', 'concept'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validation
        $this->validate($request, [
            'invoicing_concept_id' => 'required',
            'total_amount'         => 'required',
            'month'                => 'required',
            'year'                 => 'required',
            'community_id'         => 'required'
        ]);
        // Receipt Dues values
        $receiptdue = new ReceiptDue;
        $receiptdue->invoicing_concept_id = $request->invoicing_concept_id;
        $receiptdue->community_id         = $request->community_id;
        $receiptdue->total_amount         = $request->total_amount;
        $receiptdue->month                = $request->month;
        $receiptdue->year                 = $request->year;

        if ($receiptdue->save())
        {   
            // Receipt Due items values
            $receiptdue_id = DB::table('receipt_dues')->orderBy('id', 'desc')->first();
            
            $sqlData = array();
            for ($i=0; $i < count($request->property_id); $i++)
            { 
                $sqlData[] = array(
                    'property_id'    => $request->property_id[$i],
                    'amount'         => $request->amount[$i],
                    'receipt_due_id' => $receiptdue_id->id
                );                
            }
            DB::table('receipt_due_items')->insert($sqlData);

            // $request->session()->flash('flash_message', 'Success!');
            Session::flash('message', [
                'alert' => 'success',
                'text' => trans('dashboard.messages.created')
            ]);
            return redirect('receiptdues');
        }
        else
        {
            return view('backend.receiptdue.add');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [
            'breadcrumb_title' => trans('receipt_due.management_title'),
            'breadcrumb_subtitle' => trans('receipt_due.view_title'),
            'panel_title' => trans('receipt_due.view_title'),
            'breadcrumb_link' => route('receiptdues.index'),
            'url_new' => route('receiptdues.create'),
            'url_edit' => route('receiptdues.edit', $id),
            'url_delete' => 'receiptdues.destroy',
            'record_id' => $id
        ];
        // get receipt due
        $receiptdue = ReceiptDue::get_receipt_due($id);

        // get items
        $receiptdueItem = ReceiptDueItem::get_receipt_due_items(Auth::id(), $id);

        return view('backend.receiptdue.view', compact('data', 'receiptdue', 'receiptdueItem'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'breadcrumb_title' => trans('receipt_due.management_title'),
            'breadcrumb_subtitle' => trans('receipt_due.update_title'),
            'panel_title' => trans('receipt_due.edit_title'),
            'btn_record_submit' => trans('common.btn_save_text'),
            'breadcrumb_link' => route('receiptdues.index'),
            'url_new' => route('receiptdues.create'),
            'form_url' => url('/receiptdues/'.$id),
            'form_method' => 'PATCH'
        ];

        // Get communities
        if (Community::all()->count() > 0)
        {
            $communities = [];
            foreach (ReceiptDue::get_communities() as $value)
            {
                $communities[$value->id] = $value->name;
            }
        }
        // Get all invoicing concepts
        if (InvoicingConcept::all()->count() > 0) $concept = InvoicingConcept::all()->pluck('name', 'id');

        // get receipt due
        $receiptdue = ReceiptDue::get_receipt_due($id);

        // get items
        $receiptdueItem = ReceiptDueItem::get_receipt_due_items(Auth::id(), $id);

        return view('backend.receiptdue.create-edit-fields', compact('data', 'receiptdue', 'receiptdueItem', 'communities', 'concept'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validation
        $this->validate($request, [
            'invoicing_concept_id' => 'required',
            'total_amount'         => 'required',
            'month'                => 'required',
            'year'                 => 'required',
            'community_id'         => 'required'
        ]);
        // Receipt Dues values
        $receiptdue = ReceiptDue::find($id);
        $receiptdue->invoicing_concept_id = $request->invoicing_concept_id;
        $receiptdue->community_id         = $request->community_id;
        $receiptdue->total_amount         = $request->total_amount;
        $receiptdue->month                = $request->month;
        $receiptdue->year                 = $request->year;

        if ($receiptdue->save())
        {   
            // Receipt Due items values
            $receiptdueItem = ReceiptDueItem::where('receipt_due_id', $id)->get();
            $sqlData = array();
            for ($i=0; $i < count($request->property_id); $i++)
            {
                $receiptdueItem[$i]->property_id = $request->property_id[$i];
                $receiptdueItem[$i]->amount      = $request->amount[$i];
                $receiptdueItem[$i]->save();
            }

            // $request->session()->flash('flash_message', 'Success!');
            Session::flash('message', [
                'alert' => 'success',
                'text' => trans('dashboard.messages.updated')
            ]);
            return redirect('receiptdues');
        }
        else
        {
            return view('backend.receiptdue.add');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax())
        {
            $receiptdue     = ReceiptDue::find($id);
            $receiptdueItem = ReceiptDueItem::where('receipt_due_id', $id)->get();
            
            if (!is_null($receiptdue) && !empty($receiptdueItem))
            {
                $receiptdue->delete();
                foreach ($receiptdueItem as $item)
                {
                    $item->delete();   
                }

                return response()->json([
                    'data' => true,
                    'id' => $id,
                    'message' => trans('dashboard.messages.deleted'),
                    'undo' => trans('dashboard.buttons.undo')
                ]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }

    /**
     * Recover record deleted
     * 
     * @param  Request $request
     * @param  int $id
     * @return json
     */
    public function postUndelete(Request $request, $id)
    {
        if($request->ajax())
        {
            $receiptdue = ReceiptDue::onlyTrashed()->where('id', $id)->first();
            $receiptdueItem = ReceiptDueItem::onlyTrashed()->where('receipt_due_id', $id)->get();
            
            if (!is_null($receiptdue) && !empty($receiptdueItem))
            {
                $receiptdue->restore();
                foreach ($receiptdueItem as $item)
                {
                    $item->restore();
                }
                return response()->json(['data' => true, 'id' => $id]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }

    /**
     * Get all properties that belongs to a community
     * 
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function getProperties(Request $request)
    {
        if ($request->ajax())
        {
            $properties = ReceiptDue::get_properties(Auth::id(), $request->id);
                            
            if(!is_null($properties))
            {
                return response()->json([
                    'success' => true,
                    'data' => $properties
                ]);
            }
            else{
                return response()->json([
                    'success' => false,
                    'message' => 'error...!!!'
                ]);
            }
        }
    }

    public function getReceipDueLastMonth(Request $request)
    {
        if ($request->ajax())
        {
            //SELECT * FROM receipt_dues WHERE invoicing_concept_id = 2 ORDER BY month DESC LIMIT 1
            $receiptdue = DB::select(
                'SELECT * FROM receipt_dues WHERE invoicing_concept_id = ?
                ORDER BY month DESC LIMIT 1', [$request->id]);
            
            $receiptdueItem = ReceiptDueItem::get_last_month_records(Auth::id(), $receiptdue[0]->invoicing_concept_id, $receiptdue[0]->id);
            //dd($receiptdueItem);
            if(!is_null($receiptdueItem))
            {
                return response()->json([
                    'success' => true,
                    'data' => $receiptdueItem
                ]);
            }
            else{
                return response()->json([
                    'success' => false,
                    'message' => 'error...!!!'
                ]);
            }
        }
    }
}

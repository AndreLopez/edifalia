<?php

namespace Edifalia\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\Rate;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

class RatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rates = Rate::paginate(10);
        return view('backend.management.rates.index')->with('rates', $rates);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $header = [
            'route' => 'rates.store',
            'method' => 'POST',
            'role' => 'form',
        ];
        return view('backend.management.rates.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.add_rate'))->with('new', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Validator::make($request->all(), [
            'name' => 'required|max:200|unique:rates'
        ])->validate();

        $rate = new Rate();
        $rate->fill($request->all());
        $rate->save();

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.created')
        ]);

        return redirect()->route('rates.edit', $rate->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rate = Rate::find($id);
        if (is_null($rate))
            return redirect()->route('rates.index');

        $header = [
            'route' => ['rates.update', $rate->id],
            'method' => 'PUT',
            'role' => 'form'
        ];

        return view('backend.management.rates.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.edit_rate'))
            ->with('rate', $rate)->with('edit', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rate = Rate::find($id);
        if (is_null($rate))
            return redirect()->route('rates.index');

        \Validator::make($request->all(), [
            'name' => [
                'required',
                'max:200',
                Rule::unique('rates', 'name')->ignore($rate->id)]
        ])->validate();

        $rate->fill($request->all());
        $rate->save();

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.updated')
        ]);
        return redirect()->route('rates.edit', $rate->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $rate = Rate::find($id);
            if (!is_null($rate)){
                $rate->delete();
                return response()->json([
                    'data' => true,
                    'id' => $id,
                    'message' => trans('dashboard.messages.deleted'),
                    'undo' => trans('dashboard.buttons.undo')
                ]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }

    public function postUndelete(Request $request, $id)
    {
        if($request->ajax()){
            $rate = Rate::onlyTrashed()->where('id', $id)->first();
            if (!is_null($rate)){
                $rate->restore();
                return response()->json(['data' => true, 'id' => $id]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }
}

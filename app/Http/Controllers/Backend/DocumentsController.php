<?php

namespace Edifalia\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\Document;
use Edifalia\Http\Requests\Document\CreateRequest;
use Edifalia\Http\Requests\Document\EditRequest;
use Illuminate\Support\Facades\Session;
use Edifalia\Repositories\UploadFileRepository;
use Edifalia\Models\Tag;
use Illuminate\Support\Facades\Gate;
use Edifalia\Repositories\DocumentRepository;

class DocumentsController extends Controller
{
    protected $documentRepo;

    public function __construct(DocumentRepository $documentRepo)
    {
        $this->documentRepo = $documentRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Gate::allows('admin')){
            $documents = $this->documentRepo->getAll();
        } else if(Gate::allows('employee')){
            $documents = $this->documentRepo->getEmployeeDocuments();
        }
        return view('backend.management.documents.index')
            ->with('documents', $documents);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $header = [
            'route' => 'documents.store',
            'method' => 'POST',
            'role' => 'form',
            'files' => 'true'
        ];
        return view('backend.management.documents.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.add_doc'))->with('new', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $up
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request, UploadFileRepository $up)
    {
        if($this->documentRepo->checkCommunity($request->get('community_id'))) {
            if (!$up->uploadFile($request->document, 'local_documents', $this->getNewDirName($request->tag_id))) {
                Session::flash('message', [
                    'alert' => 'danger',
                    'text' => $up->getErrorMessage(),
                ]);
                return back()->withInput();
            }
            $this->documentRepo->saveNewWithUserAndDoc($request, $up->getFileName());
        } else {
            return redirect()->route('documents.create');
        }

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.created')
        ]);

        return redirect()->route('documents.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $document = $this->documentRepo->find($id);

        if (is_null($document) || Gate::denies('has', $document->user_id))
            return redirect()->route('documents.index');

        return view('backend.management.documents.show')->with('document', $document);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $document = $this->documentRepo->find($id);
        if (is_null($document) || Gate::denies('has', $document->user_id))
            return redirect()->route('documents.index');

        $header = [
            'route' => ['documents.update', $document->id],
            'method' => 'PUT',
            'role' => 'form',
            'files' => 'true'
        ];

        return view('backend.management.documents.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.edit_doc'))
            ->with('document', $document)->with('edit', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditRequest $request, UploadFileRepository $up, $id)
    {
        $document = $this->documentRepo->find($id);
        if (is_null($document) || Gate::denies('has', $document->user_id))
            return redirect()->route('documents.index');

        if ($this->documentRepo->checkCommunity($request->get('community_id'))) {
            $currentDocument = strstr($document->document, '/'); // -> with '/'
            $currentDir = strstr($document->document, '/', true); // <- without '/'
            $newDir = $this->getNewDirName($request->tag_id);

            if ($request->hasFile('document')) {
                if (!$up->uploadFile($request->document, 'local_documents', $newDir)) {
                    Session::flash('message', [
                        'alert' => 'danger',
                        'text' => $up->getErrorMessage(),
                    ]);
                    return back()->withInput();
                }
                $up->deleteFile($document->document, 'local_documents');
                $document->document = $up->getFileName();
            } elseif ($currentDir != $newDir) {
                if (\Storage::disk('local_documents')->exists($document->document)) {
                    \Storage::disk('local_documents')->move($document->document, $newDir . $currentDocument);
                    $up->deleteFile($document->document, 'local_documents');
                    $document->document = $newDir . $currentDocument;
                }
            }
            $this->documentRepo->updateDocument($document, $request);
        } else {
            return redirect()->route('documents.edit', $document->id);
        }

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.updated')
        ]);
        return redirect()->route('documents.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $document = $this->documentRepo->find($id);
            if (!is_null($document) && Gate::allows('has', $document->user_id)){
                $this->documentRepo->delete($document);
                $array = [
                    'data' => true,
                    'id' => $id,
                    'message' => trans('dashboard.messages.deleted'),
                    'undo' => trans('dashboard.buttons.undo')
                ];
            } else {
                $array = [
                    'data' => false,
                    'message' => trans('dashboard.messages.error')
                ];
            }
            return response()->json($array);
        }
    }

    public function postUndelete(Request $request, $id)
    {
        if($request->ajax()){
            $document = $this->documentRepo->recordInTrash($id);
            if (!is_null($document)){
                $this->documentRepo->restore($document);
                $array = ['data' => true, 'id' => $id];
            } else {
                $array = [
                    'data' => false,
                    'message' => trans('dashboard.messages.error')
                ];
            }
            return response()->json($array);
        }
    }

    private function getNewDirName($id)
    {
        $tag = Tag::find($id);
        $dir = str_slug($tag->name, '_');
        return $dir;
    }
}

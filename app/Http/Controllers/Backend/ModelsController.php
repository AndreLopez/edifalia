<?php

namespace Edifalia\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\ManagementModel;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

class ModelsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = ManagementModel::paginate(10);
        return view('backend.management.models.index')->with('models', $models);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $header = [
            'route' => 'models.store',
            'method' => 'POST',
            'role' => 'form',
        ];
        return view('backend.management.models.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.add_model'))->with('new', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Validator::make($request->all(), [
            'name' => 'required|max:200|unique:models'
        ])->validate();

        $model = new ManagementModel();
        $model->fill($request->all());
        $model->save();

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.created')
        ]);

        return redirect()->route('models.edit', $model->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = ManagementModel::find($id);
        if (is_null($model))
            return redirect()->route('models.index');

        $header = [
            'route' => ['models.update', $model->id],
            'method' => 'PUT',
            'role' => 'form'
        ];

        return view('backend.management.models.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.edit_model'))
            ->with('model', $model)->with('edit', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = ManagementModel::find($id);
        if (is_null($model))
            return redirect()->route('models.index');

        \Validator::make($request->all(), [
            'name' => [
                'required',
                'max:200',
                Rule::unique('models', 'name')->ignore($model->id)]
        ])->validate();

        $model->fill($request->all());
        $model->save();

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.updated')
        ]);
        return redirect()->route('models.edit', $model->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $model = ManagementModel::find($id);
            if (!is_null($model)){
                $model->delete();
                return response()->json([
                    'data' => true,
                    'id' => $id,
                    'message' => trans('dashboard.messages.deleted'),
                    'undo' => trans('dashboard.buttons.undo')
                ]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }

    public function postUndelete(Request $request, $id)
    {
        if($request->ajax()){
            $model = ManagementModel::onlyTrashed()->where('id', $id)->first();
            if (!is_null($model)){
                $model->restore();
                return response()->json(['data' => true, 'id' => $id]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }
}

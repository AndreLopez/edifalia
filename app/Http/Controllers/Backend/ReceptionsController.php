<?php

namespace Edifalia\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\Reception;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

class ReceptionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $receptions = Reception::paginate(10);
        return view('backend.management.receptions.index')->with('receptions', $receptions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $header = [
            'route' => 'receptions.store',
            'method' => 'POST',
            'role' => 'form',
        ];
        return view('backend.management.receptions.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.add_reception'))->with('new', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Validator::make($request->all(), [
            'name' => [
                'required',
                'max:200',
                'unique:receptions']
        ])->validate();

        $reception = new Reception();
        $reception->fill($request->all());
        $reception->save();

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.created')
        ]);

        return redirect()->route('receptions.edit', $reception->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reception = Reception::find($id);
        if (is_null($reception))
            return redirect()->route('receptions.index');

        $header = [
            'route' => ['receptions.update', $reception->id],
            'method' => 'PUT',
            'role' => 'form'
        ];

        return view('backend.management.receptions.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.edit_reception'))
            ->with('reception', $reception)->with('edit', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $reception = Reception::find($id);
        if (is_null($reception))
            return redirect()->route('receptions.index');

        \Validator::make($request->all(), [
            'name' => [
                'required',
                'max:200',
                Rule::unique('receptions', 'name')->ignore($reception->id)]
        ])->validate();

        $reception->fill($request->all());
        $reception->save();

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.updated')
        ]);
        return redirect()->route('receptions.edit', $reception->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $reception = Reception::find($id);
            if (!is_null($reception)){
                $reception->delete();
                return response()->json([
                    'data' => true,
                    'id' => $id,
                    'message' => trans('dashboard.messages.deleted'),
                    'undo' => trans('dashboard.buttons.undo')
                ]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }

    public function postUndelete(Request $request, $id)
    {
        if($request->ajax()){
            $reception = Reception::onlyTrashed()->where('id', $id)->first();
            if (!is_null($reception)){
                $reception->restore();
                return response()->json(['data' => true, 'id' => $id]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }
}

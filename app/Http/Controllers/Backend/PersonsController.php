<?php

namespace Edifalia\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\Backend\Person;
use Edifalia\Models\Common\PostalCode;
use Edifalia\Models\Common\PaymentMethod;
use Edifalia\Models\Backend\AccountingAccount;
use Edifalia\Models\Backend\BankAccount;
use Illuminate\Pagination\Paginator;
use Illuminate\Database\Query\Builder;
use Edifalia\User; 
use Edifalia\Http\Requests\CreatePersonRequest;

class PersonsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'breadcrumb_title' => trans('person.management_title'),
            'breadcrumb_subtitle' => trans('person.listing_title'),
            'panel_title' => trans('person.list_title'),
            'url_new' => route('person.create'),
            'theads' => [
                ['align' => 'text-center', 'first_name' => 'common.lst_first_name_title'],
            ],
            'tbody' => []
        ];
           $persons = Person::all();
        foreach ($persons as $person) {
          $id = $person->id;
              $cols  = "<td class='col-xs-2 col-sm-2 col-md-2 col-lg-2    '>" . $person->reference . "</td>";
            array_push($data['tbody'], [
                'cols' => $cols, 'url_edit' => route('person.edit', $id),
                'url_delete' => 'person.destroy', 'record_id' => $id
            ]);

        }

         return view('layouts.list-persons')->with('data', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $paymentMethods = [];
        $postalCodes = [];
        $users = [];

         $data = [
            'breadcrumb_title' => trans('person.management_title'),
            'breadcrumb_subtitle' => trans('person.listing_title'),
            'breadcrumb_link' => route('person.index'),
            'panel_title' => trans('person.new_title'),
            'btn_record_submit' => trans('common.btn_save_text'),
        ];
         $header = [
            'route' => 'person.store', 
            'method' => 'POST', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];
        
        if (PaymentMethod::all()->count() > 0)
          $paymentMethods = PaymentMethod::all()->pluck('type', 'id');

        if (PostalCode::all()->count() > 0)
          $postalCodes = PostalCode::all()->pluck('code', 'id');

        if (User::all()->count() > 0)
          $users = User::all()->pluck('name','id');

       return view ('layouts.forms')
       ->with('data', $data)
       ->with('new', true)
       ->with('header', $header)
       ->with('paymentMethods', $paymentMethods)
       ->with('postalCodes', $postalCodes)
       ->with('users', $users)
       ->with('fields_template', 'backend.person.create-edit-fields');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request, [
            'first_name' => 'required|max:75',
            'last_name' => 'required|max:75',
            'address' => 'required', 
            'dni' => 'required',
            'reference' => 'required', 
            'strength_language' => 'required', 
            'message_email_only' => 'nullable', 
            'group_properties' => 'nullable', 
            'group_invoice_concepts' => 'nullable', 
            'dont_get_email' => 'nullable', 
            'can_update_own_data' => 'nullable', 
            'exclusiveness' => 'nullable', 
            'virtual_office_access' => 'nullable', 
            'update_data_from_virtual_office' => 'nullable', 
            'notes' => 'required',
            'postal_code_id' => 'required|integer|exists:postal_codes,id',
            'payment_method_id' => 'required|integer|exists:payment_methods,id',
            'user_id' => 'required|integer|exists:users,id',
        ]);
    
        $person = new Person();
        $person->fill($request->all()); 
        $person->save(); 

        $request->session()->flash('message', ['type' => 'store']); 
        return redirect()->route('person.index');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */
    public function ApiStore(CreatePersonRequest $request)
    {

        if ($request->ajax()) 
        {
            $code = 0;
            $iban = 0;
            $accountingAccountObj = null;
            $bankAccountObj = null;
            $accounting = null;
            $bankAccount = null;

            $person = new Person();
            $person->fill($request->all()); 
            $person->save(); 

            foreach ($request->get('accounting_account_code') as $key => $value) 
            {
                if (!empty($value)) 
                {
                    $code = $request->input('accounting_account_code.' . $key);
                    $accountingAccountObj = AccountingAccount::whereCode($code)->first();
                    if(count($accountingAccountObj) == 0)
                    {
                        $accounting = new AccountingAccount();
                        $accounting->code = $request->input('accounting_account_code.' . $key);
                        $accounting->title = $request->input('accounting_account_title.' . $key);
                        $accounting->save();

                        $person->accountingAccounts()->attach($accounting->id);
                    }
                }
            }

            foreach ($request->get('bank_account_accounting_code')as $key => $value) 
            {
                if (!empty($value)) 
                {
                    $code = $request->input('bank_account_accounting_code.' . $key);
                    $accountingAccountObj = AccountingAccount::whereCode($code)->first();
                    if (count($accountingAccountObj) > 0 && !$accountingAccountObj->hasBankAccount()) 
                    {
                        $iban = $request->input('bank_account_iban.' . $key);
                        $bankAccountObj = BankAccount::whereIban($iban)->first();
                        if (count($bankAccountObj) == 0) 
                        {
                            $bankAccount = new BankAccount();
                            $bankAccount->iban = $request->input('bank_account_iban.' . $key);
                            $bankAccount->code_a = $request->input('bank_account_code_a.' . $key);
                            $bankAccount->code_b = $request->input('bank_account_code_b.' . $key);
                            $bankAccount->code_c = $request->input('bank_account_code_c.' . $key);
                            $bankAccount->code_d = $request->input('bank_account_code_d.' . $key);
                            $bankAccount->code_e = $request->input('bank_account_code_e.' . $key);
                            $bankAccount->sufix = $request->input('bank_account_sufix.' . $key);
                            $bankAccount->bic = $request->input('bank_account_bic.' . $key);
                            $bankAccount->accounting_account_id = $accountingAccountObj->id;
                            $bankAccount->save();

                            $person->bankAccounts()->attach($bankAccount->id);
                        }
                    }
                }
            }
            return response()->json(['person' => $person, 'success' => $person ? true : false]);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $person = Person::findOrfail($id);

         $data = [
            'breadcrumb_title' => trans('person.management_title'),
            'breadcrumb_subtitle' => trans('person.listing_title'),
            'breadcrumb_link' => route('person.index'),
            'panel_title' => trans('person.new_title'),
            'btn_record_submit' => trans('common.edit_record_submit'),
        ];

         $header = [
          'route' => ['person.update', $person->id], 
            'method' => 'PUT', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];

     if (PaymentMethod::all()->count() > 0)
          $paymentMethods = PaymentMethod::all()->pluck('type', 'id');

        if (PostalCode::all()->count() > 0)
          $postalCodes = PostalCode::all()->pluck('code', 'id');

        if (User::all()->count() > 0)
          $users = User::all()->pluck('name','id');
       
       return view ('layouts.forms')
       ->with('data', $data)
       ->with('model', $person)
       ->with('edit', true)
       ->with('header', $header)
       ->with('paymentMethods', $paymentMethods)
       ->with('postalCodes', $postalCodes)
       ->with('users', $users)
       ->with('fields_template', 'backend.person.create-edit-fields');


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          $this->validate($request, [
        'name' => 'required|max:100',
        'analysis' => 'required',
        'address' => 'required', 
        'total_employees' => 'nullable',
         ]);

            $departments = Department::find($id);
        if (!is_null($departments)) {
            $departments->fill($request->all());
            $departments->save();
            $request->session()->flash('message', ['type' => 'update']);
        }
        return redirect()->route('department.index');
          
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     if ($request->ajax()) {
            $persons = Person::find($id);
            if (!is_null($persons)) {
                $persons->delete();
                $request->session()->flash('message', ['type' => 'destroy']);
                return response()->json(['result' => true]);
            }
            return response()->json(['result' => false]);
        }
        return redirect()->route('person.index');
    }

    public function selectList(Request $request)
    {
        if ($request->ajax()) {
            $items = [];
            $items = Person::all();
            return response()->json(['items' => $items, 'success' => $items ? true : false]);
        }
    }
}

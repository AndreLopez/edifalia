<?php

namespace Edifalia\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Edifalia\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Edifalia\Models\Backend\Community;
use Edifalia\Models\Reading;
use Edifalia\Http\Requests\Reading\CreateEditRequest;
use Illuminate\Support\Facades\Gate;
use Edifalia\Repositories\ReadingRepository;

class ReadingsController extends Controller
{
    protected $readingRepo;

    public function __construct(ReadingRepository $readingRepo)
    {
        $this->readingRepo = $readingRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Gate::allows('admin')){
            $readings = $this->readingRepo->getAll();
        } else if(Gate::allows('employee')){
            $readings = $this->readingRepo->getEmployeeReadings();
        }
        return view('backend.management.readings.index')->with('readings', $readings);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $header = [
            'route' => 'readings.store',
            'method' => 'POST',
            'role' => 'form',
        ];
        return view('backend.management.readings.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.add_reading'))->with('new', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEditRequest $request)
    {
        if($this->readingRepo->checkCommunity($request->get('community_id'))){
            $reading = $this->readingRepo->saveNewWithUserAndReading($request);
        } else {
            return redirect()->route('readings.create');
        }

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.created')
        ]);

        return redirect()->route('readings.edit', $reading->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reading = $this->readingRepo->find($id);
        if (is_null($reading) || Gate::denies('has', $reading->user_id))
            return redirect()->route('readings.index');

        $header = [
            'route' => ['readings.update', $reading->id],
            'method' => 'PUT',
            'role' => 'form'
        ];

        return view('backend.management.readings.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.edit_reading'))
            ->with('reading', $reading)->with('edit', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateEditRequest $request, $id)
    {
        $reading = $this->readingRepo->find($id);
        if (is_null($reading) || Gate::denies('has', $reading->user_id))
            return redirect()->route('readings.index');

        if($this->readingRepo->checkCommunity($request->get('community_id'))){
            $this->readingRepo->updateReading($reading, $request);
        } else {
            return redirect()->route('readings.edit', $reading->id);
        }

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.updated')
        ]);
        return redirect()->route('readings.edit', $reading->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $reading = $this->readingRepo->find($id);
            if (!is_null($reading) && Gate::allows('has', $reading->user_id)){
                $this->readingRepo->delete($reading);
                return response()->json([
                    'data' => true,
                    'id' => $id,
                    'message' => trans('dashboard.messages.deleted'),
                    'undo' => trans('dashboard.buttons.undo')
                ]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }

    public function postUndelete(Request $request, $id)
    {
        if($request->ajax()){
            $reading = $this->readingRepo->recordInTrash($id);
            if (!is_null($reading)){
                $this->readingRepo->restore($reading);
                return response()->json(['data' => true, 'id' => $id]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }

    public function getReadingData(Request $request)
    {
        if ($request->ajax())
        {
            $reading = $this->readingRepo->find($request->get('reading_id'));
            if(!is_null($reading)){
                return response()->json([
                    'data' => true,
                    'title' => $reading->title,
                    'invoicing' => $reading->invoicingConcept->id,
                    'rate' => $reading->rate->id,
                    'periodicity' => $reading->periodicity,
                    'month' => $reading->month,
                    'year' => $reading->year,
                    'concept_receipt' => $reading->concept_receipt,
                    'date' => $reading->date,
                ]);
            }
            else{
                return response()->json([
                    'data' => false,
                    'message' => trans('dashboard.messages.error')
                ]);
            }
        }
    }

    public function getCommunityProperties(Request $request)
    {
        if($request->ajax()){
            $community = Community::find($request->community_id);
            if(!is_null($community)){
                $properties = $community->properties()->get();
                $html = '';
                if($properties->count()){
                    $previous = trans('dashboard.fields.previous');
                    $current = trans('dashboard.fields.current');
                    $consumption = trans('dashboard.fields.consumption');
                    foreach ($properties as $property){
                        $previousValue = '';
                        $currentValue = '';
                        $consumptionValue = '';
                        if($request->has('objective_id')){
                            $reading = $property->readings()
                                ->where('reading_id',$request->get('objective_id'))
                                ->first();
                            if(!is_null($reading)){
                                $previousValue = $reading->pivot->previous;
                                $currentValue = $reading->pivot->current;
                                $consumptionValue = $reading->pivot->consumption;
                            }
                        }
                        $html .= "<div class='row'>";
                        $html .= "<div class='form-group col-md-3'>";
                        $html .= "<p><strong>{$property->property_number} - {$property->building_number}</strong></p>";
                        $html .= "</div><div class='form-group col-md-3'>";
                        $html .= "<input type='text' name='previous[$property->id]' class='form-control' placeholder='{$previous}' value='{$previousValue}'>";
                        $html .= "</div><div class='form-group col-md-3'>";
                        $html .= "<input type='text' name='current[$property->id]' class='form-control' placeholder='{$current}' value='{$currentValue}'>";
                        $html .= "</div><div class='form-group col-md-3'>";
                        $html .= "<input type='text' name='consumption[$property->id]' class='form-control' placeholder='{$consumption}' value='{$consumptionValue}'>";
                        $html .= "</div></div>";
                    };
                }
                else {
                    $html .= "<p class='text-yellow'>".trans('dashboard.messages.no_properties')."</p>";
                }
                return response()->json(['data' => true, 'html' => $html]);
            }
            else {
                return response()->json([
                    'data' => false,
                    'message' => trans('dashboard.messages.error')
                ]);
            }
        }
    }
}

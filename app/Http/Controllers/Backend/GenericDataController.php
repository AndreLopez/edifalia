<?php

namespace Edifalia\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Edifalia\Http\Requests;
use Edifalia\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Illuminate\Database\Query\Builder;

/**
 * Controller for Generic Data
 * @author Máximo Sojo maxsojo13@gmail.com <maxtoan at atechnologies>
 */
class GenericDataController extends Controller
{
    public function getCommunity(Request $request){
    	// $result = \Edifalia\Models\Backend\Community::all();
        $result = \DB::table('communities')
            ->select('communities.id', 'communities.name')            
            ->get();
        return response()->json($result);
    }

    public function getPropertyForCommunity(Request $request){
    	$result = \DB::table('communities')
            ->join('properties', 'properties.community_id', '=', 'communities.id')
            ->join('persons', 'persons.id', '=', 'properties.person_id')
            ->select('properties.*', 'persons.*')
            ->where('communities.id', '=', $request->get('id'))
            ->get();
    	return response()->json($result);
    }

    public function getCommunityForId(Request $request){
    	$data = json_decode($request->getContent());
    	$result = [];
        $dni = $email = $phone = false;
        
        $dataProperty = $data->propertyAvailble;        

        foreach ($dataProperty as $key => $value) {
            $property = \Edifalia\Models\Common\Property::find($value->id);
            $person = \Edifalia\Models\Backend\Person::find($property->person_id);
            if (!$data->include->notData) {
                $dni = $email = $phone = null;
                if ($data->include->dni) {
                    $dni = $person->dni;
                }
                if ($person->user) {
                    if ($data->include->email) {
                        $email = $person->user->email;
                    }
                    if ($data->include->phone) {
                        $phone = $person->user->phone;
                    }
                }
            }

            $result[] = [
                'building_number' => $property->building_number,
                'property' => $property->property_number,
                'coefficient' => $property->coefficient,
                'person' => $person->getFullNameAttribute(),
                'dni' => $dni,
                'email' => $email,
                'phone' => $phone
            ];
    	}
        
    	return response()->json($result);    	
    }

    public function getMeetingForCommunity(Request $request){
        $result = \Edifalia\Models\Meeting::where('community_id', $request->get('id'))->get();
        return response()->json($result);
    }

    public function getBudgetForCommunity(Request $request){
        $result = \Edifalia\Models\Budget::where('community_id', $request->get('id'))->get();
        return response()->json($result);
    }

    public function getReadingForCommunity(Request $request){
        $result = \DB::table('readings')
            ->select('readings.id', 'readings.title')
            ->where('readings.community_id', '=', $request->get('id'))
            ->get();
        return response()->json($result);
    }

    public function getMeetingControl(Request $request){
        $result = \DB::table('meeting_controls')
            ->join('persons', 'persons.id', '=', 'meeting_controls.person_id')
            ->select('meeting_controls.*', 'persons.*')
            ->where('meeting_controls.deleted_at', '=', null)
            ->get();
        return response()->json($result);
    }

    public function getReadingForId(Request $request){
        $reading = \DB::table('readings')
            ->where('readings.id', '=', $request->get('id'))
            ->get();

        $propertyReading = \DB::table('property_reading')
            ->join('properties', 'properties.id', '=', 'property_reading.property_id')
            ->select('property_reading.*', 'properties.property_number')
            ->where('property_reading.reading_id', '=', $request->get('id'))
            ->get();

        $data = [
            'property_reading' => $propertyReading,
            'reading' => $reading
        ];
        return response()->json($data);
    }

    public function getMonths(Request $request){
        $dataMonths = \Edifalia\Models\Report::getMonth();
        return response()->json($dataMonths);
    }
}
<?php

namespace Edifalia\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Repositories\ChargeRepository;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Edifalia\Http\Requests\Charges\CreateEditRequest;
use Edifalia\Models\Backend\Community;
use Illuminate\Support\Facades\Request as RequestObj;
use Edifalia\Models\Charge;

class ChargesController extends Controller
{
    protected $chargeRepo;

    public function __construct(ChargeRepository $chargeRepo)
    {
        $this->chargeRepo = $chargeRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $charges = Charge::filterCharges(RequestObj::all());
        return view('backend.management.charges.index', compact('charges'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $header = [
            'route' => 'charges.store',
            'method' => 'POST',
            'role' => 'form',
        ];
        return view('backend.management.charges.create_or_edit')
            ->with('header', $header)
            ->with('title', trans('dashboard.titles.add_charge'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEditRequest $request)
    {
        if($this->chargeRepo->checkCommunity($request->get('community_id'))){
            $this->chargeRepo->saveNewWithUser($request->all());
        } else {
            return redirect()->route('charges.create');
        }

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.created')
        ]);

        return redirect()->route('charges.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $charge = $this->chargeRepo->find($id);

        if (is_null($charge) || Gate::denies('has', $charge->user_id))
            return redirect()->route('charges.index');

        $header = [
            'route' => ['charges.update', $charge->id],
            'method' => 'PUT',
            'role' => 'form'
        ];

        return view('backend.management.charges.create_or_edit')
            ->with('header', $header)
            ->with('title', trans('dashboard.titles.edit_charge'))
            ->with('charge', $charge);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateEditRequest $request, $id)
    {
        $charge = $this->chargeRepo->find($id);

        if (is_null($charge) || Gate::denies('has', $charge->user_id))
            return redirect()->route('charges.index');

        if($this->chargeRepo->checkCommunity($request->get('community_id'))){
            $this->chargeRepo->update($charge, $request->all());
        } else {
            return redirect()->route('charges.edit', $charge->id);
        }

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.updated')
        ]);
        return redirect()->route('charges.edit', $charge->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $charge = $this->chargeRepo->find($id);
            if (!is_null($charge) && Gate::allows('has', $charge->user_id)){
                $this->chargeRepo->delete($charge);
                $array = [
                    'data' => true,
                    'id' => $id,
                    'message' => trans('dashboard.messages.deleted'),
                    'undo' => trans('dashboard.buttons.undo')
                ];
            } else {
                $array = [
                    'data' => false,
                    'message' => trans('dashboard.messages.error')
                ];
            }
            return response()->json($array);
        }
    }

    public function postUndelete(Request $request, $id)
    {
        if($request->ajax()){
            $charge = $this->chargeRepo->recordInTrash($id);
            if (!is_null($charge)){
                $this->chargeRepo->restore($charge);
                $array = ['data' => true, 'id' => $id];
            } else {
                $array = [
                    'data' => false,
                    'message' => trans('dashboard.messages.error')
                ];
            }
            return response()->json($array);
        }
    }

    public function getReceiptsAndProperties(Request $request)
    {
        if ($request->ajax()) {
            $community = Community::find($request->get('community_id'));
            if (!is_null($community)) {
                if ($community->issueReceipts()->exists()) {
                    $receipts = [];
                    foreach ($community->issueReceipts as $receipt) {
                        $receipts[$receipt->id] = $receipt->id;
                    }
                } else {
                    return response()->json([
                        'data' => false,
                        'message' => trans('dashboard.messages.no_receipts'),
                    ]);
                }
                if ($community->properties()->exists()) {
                    $properties = [];
                    foreach ($community->properties as $property) {
                        $properties[$property->id] = $property->property_number;
                    }
                } else {
                    return response()->json([
                        'data' => false,
                        'message' => trans('dashboard.messages.no_properties'),
                    ]);
                }

                $issueReceiptValue = '';
                $propertyValue = '';
                if (!empty($request->charge_id)){
                    $charge = $this->chargeRepo->find($request->charge_id);
                    if (!is_null($charge)){
                        if($charge->issueReceipt()->where('id', $charge->issue_receipt_id)->exists()) {
                            $issueReceiptValue = $charge->issue_receipt_id;
                        }
                        if($charge->property()->where('id', $charge->property_id)->exists()) {
                            $propertyValue = $charge->property_id;
                        }
                    }
                }

                return response()->json([
                    'data' => true,
                    'receipts' => $receipts,
                    'properties' => $properties,
                    'select_receipts' => trans('dashboard.fields.select_receipts'),
                    'select_properties' => trans('dashboard.fields.select_properties'),
                    'issueReceiptValue' => $issueReceiptValue,
                    'propertyValue' => $propertyValue
                ]);
            }
        }
    }
}

<?php

namespace Edifalia\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Http\Requests\Template\CreateRequest;
use Edifalia\Http\Requests\Template\EditRequest;
use Illuminate\Support\Facades\Session;
use Edifalia\Repositories\UploadFileRepository;
use Edifalia\Repositories\TemplateRepository;
use Illuminate\Support\Facades\Gate;

class TemplatesController extends Controller
{
    protected $templateRepo;

    public function __construct(TemplateRepository $templateRepo)
    {
        $this->templateRepo = $templateRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Gate::allows('admin')){
            $templates = $this->templateRepo->getAll();
        } else if(Gate::allows('employee')){
            $templates = $this->templateRepo->getEmployeeTemplates();
        }
        return view('backend.management.templates.index')
            ->with('templates', $templates);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $header = [
            'route' => 'templates.store',
            'method' => 'POST',
            'role' => 'form',
            'files' => 'true'
        ];
        return view('backend.management.templates.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.add_template'))->with('new', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request, UploadFileRepository $up)
    {
        if (!$up->uploadFile($request->file, 'local_templates')) {
            Session::flash('message', [
                'alert' => 'danger',
                'text' => $up->getErrorMessage(),
            ]);
            return back()->withInput();
        }
        $template = $this->templateRepo->saveNewWithUserAndTempl($request, $up->getFileName());

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.created')
        ]);

        return redirect()->route('templates.edit', $template->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $template = $this->templateRepo->find($id);
        if (is_null($template) || Gate::denies('has', $template->user_id))
            return redirect()->route('templates.index');

        $header = [
            'route' => ['templates.update', $template->id],
            'method' => 'PUT',
            'role' => 'form',
            'files' => 'true'
        ];

        return view('backend.management.templates.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.edit_template'))
            ->with('template', $template)->with('edit', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditRequest $request, UploadFileRepository $up, $id)
    {
        $template = $this->templateRepo->find($id);
        if (is_null($template) || Gate::denies('has', $template->user_id))
            return redirect()->route('templates.index');

        if ($request->hasFile('file')) {
            if (!$up->uploadFile($request->file, 'local_templates')) {
                Session::flash('message', [
                    'alert' => 'danger',
                    'text' => $up->getErrorMessage(),
                ]);
                return back()->withInput();
            }
            $up->deleteFile($template->file, 'local_templates');
            $template->file = $up->getFileName();
        }

        $this->templateRepo->updateTemplate($template, $request);

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.updated')
        ]);
        return redirect()->route('templates.edit', $template->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $template = $this->templateRepo->find($id);
            if (!is_null($template) && Gate::allows('has', $template->user_id)){
                $this->templateRepo->delete($template);
                return response()->json([
                    'data' => true,
                    'id' => $id,
                    'message' => trans('dashboard.messages.deleted'),
                    'undo' => trans('dashboard.buttons.undo')
                ]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }

    public function postUndelete(Request $request, $id)
    {
        if($request->ajax()){
            $template = $this->templateRepo->recordInTrash($id);
            if (!is_null($template)){
                $this->templateRepo->restore($template);
                return response()->json(['data' => true, 'id' => $id]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }
}

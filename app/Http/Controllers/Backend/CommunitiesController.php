<?php

namespace Edifalia\Http\Controllers\Backend;

use Illuminate\Http\Request;

use Edifalia\Http\Requests;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Http\Requests\CommunityRequest;
use Edifalia\Models\Backend\AccountingAccount;
use Edifalia\Models\Backend\BankAccount;
use Edifalia\Models\Backend\Community;
use Edifalia\Models\Backend\CommunityBankAccount;
use Edifalia\Models\Backend\CommunityAccountingAccount;
use Edifalia\Models\Common\Status;
use Edifalia\Models\Common\Country;
use Edifalia\Models\Common\PostalCode;



class CommunitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'breadcrumb_title' => trans('community.management_title'),
            'breadcrumb_subtitle' => trans('community.listing_title'),
            'panel_title' => trans('community.list_title'),
            'url_new' => route('community.create'),
            'theads' => [
                ['align' => 'text-center', 'name' => 'common.lst_col_name'],
            ],
            'tbody' => []
        ];
        $communities = Community::all();
        foreach ($communities as $community) {
            $id = $community->id;
            $cols  = "<td class='col-xs-10 col-sm-10 col-md-10 col-lg-10'>" . $community->name . "</td>";
            array_push($data['tbody'], [
                'cols' => $cols, 'url_edit' => route('community.edit', $id),
                'url_delete' => 'community.destroy', 'record_id' => $id
            ]);
        }
        
        return view('layouts.list-records')->with('data', $data);
        /*$comunidad = Community::orderBy('code','ASC')->paginate(5);
        return view('layouts.templates.index')->with('comunidad',$comunidad);*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'breadcrumb_title' => trans('community.management_title'),
            'breadcrumb_subtitle' => trans('community.add_title'),
            'breadcrumb_link' => route('community.index'),
            'panel_title' => trans('community.new_title'),
            'btn_record_submit' => trans('common.btn_save_text'),
        ];
        $header = [
            'route' => 'community.store', 
            'method' => 'POST', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];
        $estatus = Status::all();
        $st_options = [];
        foreach ($estatus as $st) {
            $st_options[$st->id] = $st->name;
        }
        $countries = Country::all();
        $country_options = [];
        foreach ($countries as $country) {
            $country_options[$country->id] = $country->name;
        }
        $postal_codes = PostalCode::all();
        $postal_code_options = [];
        foreach ($postal_codes as $postal_code) {
            $postal_code_options[$postal_code->id] = $postal_code->code;
        }

        return view('layouts.forms')->with('data', $data)->with('new', true)->with('header', $header)
                                    ->with('status', $st_options)->with('countries', $country_options)
                                    ->with('postal_codes', $postal_code_options)
                                    ->with('fields_template', 'backend.community.create-edit-fields');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommunityRequest $request)
    {
        
        $comunidad = new  Community();
        $comunidad->code = $request->get('code');
        $comunidad->name = $request->get('name');
        $comunidad->address = $request->get('address');
        $comunidad->nif = $request->get('nif');
        $comunidad->structure_year = $request->get('structure_year');
        $comunidad->status_id = $request->get('status_id');
        $comunidad->postal_code_id = $request->get('postal_code_id');
        $comunidad->elevator_numbers = (!empty($request->get('elevator_numbers'))) ? $request->get('elevator_numbers') : 0;
        $comunidad->platform_accounting = $request->get('platform_accounting') ? $request->get('platform_accounting'): false;
        
        if (!empty($request->get('longitude')) && !empty($request->get('latitude'))) {
            $comunidad->longitude = $request->get('longitude');
            $comunidad->latitude = $request->get('latitude');
        }
        if (!empty($request->get('notes'))) {
            $comunidad->notes = $request->get('notes');
        }
        //$comunidad->fill($request->all());
        $comunidad->save();

        /** Registra lasa cuentas bancarias de la comunidad */
        foreach ($request->get('bank_account_accounting_code') as $key => $value) {
            if (!empty($value)) {
                $ac = AccountingAccount::where('code', $value)->first();
                
                $bank_account = new BankAccount();
                $bank_account->iban = $request->input('bank_account_iban.' . $key);
                $bank_account->code_a = $request->input('bank_account_code_a.' . $key);
                $bank_account->code_b = $request->input('bank_account_code_b.' . $key);
                $bank_account->code_c = $request->input('bank_account_code_c.' . $key);
                $bank_account->code_d = $request->input('bank_account_code_d.' . $key);
                $bank_account->code_e = $request->input('bank_account_code_e.' . $key);
                $bank_account->sufix = $request->input('bank_account_sufix.' . $key);
                $bank_account->bic = $request->input('bank_account_bic.' . $key);
                $bank_account->accounting_account_id = $ac->id;
                $bank_account->save();

                $comunidad->bankAccounts()->attach($bank_account->id);
            }
        }

        /** Registra las cuentas contables de la comunidad */
        foreach ($request->get('accounting_account_code') as $key => $value) {
            if (!empty($value)) {
                $accounting = new AccountingAccount();
                $accounting->code = $request->input('accounting_account_code.' . $key);
                $accounting->title = $request->input('accounting_account_title.' . $key);
                $accounting->save();

                $comunidad->accountingAccounts()->attach($accounting);
            }
        }

        $request->session()->flash('message', [
            'alert' => 'success', 'text' => trans('dashboard.messages.created')
        ]);

        return redirect()->route('community.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show ($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $community = Community::findOrfail($id);
        $data = [
            'breadcrumb_title' => trans('community.management_title'),
            'breadcrumb_subtitle' => trans('community.add_title'),
            'breadcrumb_link' => route('community.index'),
            'panel_title' => trans('community.new_title'),
            'btn_record_submit' => trans('common.btn_save_text'),
        ];
        $header = [
            'route' => ['community.update', $community->id], 
            'method' => 'PUT', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];
        $estatus = Status::all();
        $st_options = [];
        foreach ($estatus as $st) {
            $st_options[$st->id] = $st->name;
        }
        $countries = Country::all();
        $country_options = [];
        foreach ($countries as $country) {
            $country_options[$country->id] = $country->name;
        }
        $postal_codes = PostalCode::all();
        $postal_code_options = [];
        foreach ($postal_codes as $postal_code) {
            $postal_code_options[$postal_code->id] = $postal_code->code;
        }
        return view('layouts.forms')->with('data', $data)->with('edit', true)->with('header', $header)
                                    ->with('model', $community)->with('status', $st_options)->with('countries', $country_options)
                                    ->with('postal_codes', $postal_code_options)
                                    ->with('fields_template', 'backend.community.create-edit-fields');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'code' => 'required|max:4|unique:communities,code,' . $id,
            'name' => 'required|max:75',
            'address' => 'required',
            'nif' => 'required|max:15|unique:communities,nif,' . $id,
            'structure_year' => 'required|max:4|min:4',
            'elevator_numbers' => 'nullable|integer',
            'longitude' => 'nullable',
            'latitude' => 'nullable',
            'notes' => 'nullable',
        ]);

        $community = Community::($id);
        if (!is_null($community)) {
            $community->fill($request->all());
            $community->save();
            $request->session()->flash('message', [
                'alert' => 'success', 'text' => trans('dashboard.messages.updated')
            ]);
        }
        return redirect()->route('community.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
       
        $charges = Charge::findOrFail($id); 
        $charges->forceDelete(); 

        return redirect()->route('charge.index');

    }


}

<?php

namespace Edifalia\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Edifalia\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Edifalia\Repositories\ZoneRepository;
use Illuminate\Validation\Rule;

class ZonesController extends Controller
{
    protected $zoneRepo;

    public function __construct(ZoneRepository $zoneRepo)
    {
        $this->zoneRepo = $zoneRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $zones = $this->zoneRepo->getAll();
        return view('backend.utilities.zones.index', compact('zones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $header = [
            'route' => 'zones.store',
            'method' => 'POST',
            'role' => 'form',
        ];
        return view('backend.utilities.zones.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.add_zone'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Validator::make($request->all(), [
            'name' => 'required|max:100|unique:zones',
            'continent_id' => 'required|numeric|exists:continents,id',
        ])->validate();

        $zone = $this->zoneRepo->create($request->all());

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.created')
        ]);

        return redirect()->route('zones.edit', $zone->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $zone = $this->zoneRepo->find($id);
        if (is_null($zone))
            return redirect()->route('zones.index');

        $header = [
            'route' => ['zones.update', $zone->id],
            'method' => 'PUT',
            'role' => 'form'
        ];

        return view('backend.utilities.zones.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.edit_zone'))
            ->with('zone', $zone);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $zone = $this->zoneRepo->find($id);

        if (is_null($zone))
            return redirect()->route('zones.index');

        \Validator::make($request->all(), [
            'name' => [
                'required',
                'max:100',
                Rule::unique('zones', 'name')->ignore($zone->id)],
            'continent_id' => 'required|numeric|exists:continents,id',
        ])->validate();

        $zone = $this->zoneRepo->update($zone, $request->all());

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.updated')
        ]);
        return redirect()->route('zones.edit', $zone->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $zone = $this->zoneRepo->find($id);
            if (!is_null($zone)){
                $this->zoneRepo->delete($zone);
                return response()->json([
                    'data' => true,
                    'id' => $id,
                    'message' => trans('dashboard.messages.deleted'),
                    'undo' => trans('dashboard.buttons.undo')
                ]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }

    public function postUndelete(Request $request, $id)
    {
        if($request->ajax()){
            $zone = $this->zoneRepo->recordInTrash($id);
            if (!is_null($zone)){
                $this->zoneRepo->restore($zone);
                return response()->json(['data' => true, 'id' => $id]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }
}

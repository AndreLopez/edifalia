<?php
namespace Edifalia\Http\Controllers\Backend;

use Illuminate\Http\Request;

use Edifalia\Http\Requests;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\Backend\Contract;
use Edifalia\Models\Backend\Community;
use Edifalia\Models\Backend\Supplier;
use Illuminate\Pagination\Paginator;
use Illuminate\Database\Query\Builder;
use Edifalia\Models\Common\PaymentMethod;
use Edifalia\Models\Backend\AccountingAccount;
use Edifalia\Http\Requests\ContractRequest;
use Edifalia\Models\Common\BankAccount;

class ContractsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'breadcrumb_title' => trans('contract.management_title'),
            'breadcrumb_subtitle' => trans('contract.listing_title'),
            'panel_title' => trans('contract.list_title'),
            'url_new' => route('contract.create'),
            'theads' => [
                ['align' => 'text-center', 'reference' => 'common.contract_lst_reference_name'],
            ],
            'tbody' => []
        ];

        $contracts = Contract::all();
        foreach ($contracts as $contract) {
          $id = $contract->id;
              $cols  = "<td class='col-xs-2 col-sm-2 col-md-2 col-lg-2    '>" . $contract->reference . "</td>";
            array_push($data['tbody'], [
                'cols' => $cols, 'url_edit' => route('contract.edit', $id),
                'url_delete' => 'contract.destroy', 'record_id' => $id
            ]);

        }

         return view('layouts.list-records_contract')->with('data', $data);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         

        $data = [
            'breadcrumb_title' => trans('contract.management_title'),
            'breadcrumb_subtitle' => trans('contract.add_title'),
            'breadcrumb_link' => route('contract.index'),
            'panel_title' => trans('contract.new_title'),
            'btn_record_submit' => trans('common.btn_save_text'),
        ];
        $header = [
            'route' => 'contract.store', 
            'method' => 'POST', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];


        $communities = Community::all();
        $community_options = [];
        foreach ($communities as  $community) {
            $community_options[$community->id] = $community ->name;
        }

        $suppliers = Supplier::all(); 
        $supplier_options = [];
        foreach ($suppliers as  $supplier) {
            $supplier_options[$supplier->id] = $supplier->business_name;
        }

        $payments = PaymentMethod::all();
        $payment_options = [];
        foreach ($payments as $payment) {
            $payment_options[$payment->id] = $payment->type; 
        }

        $accounting_accounts = AccountingAccount::all();
        $ac_options = [];
        foreach ($accounting_accounts as $accounting_account) {
            $ac_options[$accounting_account->id] = $accounting_account->code;
        }

        return view ('layouts.forms')->with('data', $data)->with('new', true)->with('header', $header)->with('communities', $community_options)->with('suppliers', $supplier_options)->with('payments', $payment_options)->with('accounting_accounts', $ac_options)->with('fields_template', 'backend.contract.create-edit-fields');

    }
            


    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContractRequest $request)
    {
        $contract = new Contract(); 
        $contract->reference = $reference->get('reference');

           if (!empty($request->get('description'))){
        $contract->description = $request->get('description');

       }

     $contract->jan = $request->get('jan') ? $request->get('jan'):false; 

     $contract->feb = $request->get('feb') ? $request->get('feb'):false; 

    $contract->mar = $request->get('mar') ? $request->get('mar'):false; 

    $contract->apr = $request->get('apr') ? $request->get('apr'):false; 

    $contract->may = $request->get('may') ? $request->get('may'):false;  
    
    $contract->jun = $request->get('jun') ? $request->get('jun'):false; 

    $contract->jul = $request->get('jul') ? $request->get('jul'):false; 

    $contract->aug = $request->get('aug') ? $request->get('aug'):false; 

    $contract->sep = $request->get('sep') ? $request->get('sep'):false; 

    $contract->oct = $request->get('oct') ? $request->get('oct'):false; 

    $contract->nov = $request->get('nov') ? $request->get('nov'):false; 

    $contract->dec = $request->get('dec') ? $request->get('dec'):false;

    $contract->amount = $request->get('amount');
    $contract->minimum = $request->get('minimum');
    $contract->maximum = $request->get('maximum'); 

     $contract->publish_virtual_office = $request->get('publish_virtual_office') ? $request->get('dec'):false;

     $contract->contract_date = $request->get('contract_date');
     $contract->duration = $request->get('duration'); 

      if (!empty($request->get('observation'))){
        $contract->notes = $request->get('observation');
     }

    $contract->community_id =$request->get('community_id');
    $contract->supplier_id = $request->get('supplier_id');
    $contract->payment_method_id = $request->get('payment_method_id'); 

    $contract->save(); 

     /** Registra lasa cuentas bancarias de la comunidad */
        foreach ($request->get('bank_account_accounting_code') as $key => $value) {
            if (!empty($value)) {
                $ac = AccountingAccount::where('code', $value)->first();
                
                $bank_account = new BankAccount();
                $bank_account->iban = $request->input('bank_account_iban.' . $key);
                $bank_account->code_a = $request->input('bank_account_code_a.' . $key);
                $bank_account->code_b = $request->input('bank_account_code_b.' . $key);
                $bank_account->code_c = $request->input('bank_account_code_c.' . $key);
                $bank_account->code_d = $request->input('bank_account_code_d.' . $key);
                $bank_account->code_e = $request->input('bank_account_code_e.' . $key);
                $bank_account->sufix = $request->input('bank_account_sufix.' . $key);
                $bank_account->bic = $request->input('bank_account_bic.' . $key);
                $bank_account->accounting_account_id = $ac->id;
                $bank_account->save();

                $contract->bankAccounts()->attach($bank_account->id);
            }
        }

        /** Registra las cuentas contables de la comunidad */
        foreach ($request->get('accounting_account_code') as $key => $value) {
            if (!empty($value)) {
                $accounting = new AccountingAccount();
                $accounting->code = $request->input('accounting_account_code.' . $key);
                $accounting->title = $request->input('accounting_account_title.' . $key);
                $accounting->save();

                $contract->accountingAccounts()->attach($accounting);
            }
        }

        $request->session()->flash('message', [
            'alert' => 'success', 'text' => trans('dashboard.messages.created')
        ]);

        return redirect()->route('contract.index');



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $contract = Contract::findOrfail($id);
         $data = [
            'breadcrumb_title' => trans('contract.management_title'),
            'breadcrumb_subtitle' => trans('contract.add_title'),
            'breadcrumb_link' => route('contract.index'),
            'panel_title' => trans('contract.new_title'),
             'btn_record_submit' => trans('common.edit_record_submit'),
        ];
         $header = [
            'route' => ['contract.update', $contract->id], 
            'method' => 'PUT', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];

      $communities = Community::all();
        $community_options = [];
        foreach ($communities as  $community) {
            $community_options[$community->id] = $community ->name;
        }

        $suppliers = Supplier::all(); 
        $supplier_options = [];
        foreach ($suppliers as  $supplier) {
            $supplier_options[$supplier->id] = $supplier->business_name;
        }

        $payments = PaymentMethod::all();
        $payment_options = [];
        foreach ($payments as $payment) {
            $payment_options[$payment->id] = $payment->type; 
        }

        $accounting_accounts = AccountingAccount::all();
        $ac_options = [];
        foreach ($accounting_accounts as $accounting_account) {
            $ac_options[$accounting_account->id] = $accounting_account->code;
        }

 return view ('layouts.forms')->with('model', $contract)->with('data', $data)->with('edit', true)->with('header', $header)->with('communities', $community_options)->with('suppliers', $supplier_options)->with('payments', $payment_options)->with('accounting_accounts', $ac_options)->with('fields_template', 'backend.contract.create-edit-fields');




    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request, [
        'reference' => 'required|max:255',
        'description' => 'required', 
        
       
        ]);

        $contracts = Contract::find($id);
        if (!is_null($contracts)) {
            $contracts->fill($request->all());
            $contracts->save();
            $request->session()->flash('message', ['type' => 'update']);
        }
        return redirect()->route('contract.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
     if ($request->ajax()) {
            $contracts = Contract::find($id);
            if (!is_null($contracts)) {
                $contracts->delete();
                $request->session()->flash('message', ['type' => 'destroy']);
                return response()->json(['result' => true]);
            }
            return response()->json(['result' => false]);
        }
        return redirect()->route('contract.index');
    }
}

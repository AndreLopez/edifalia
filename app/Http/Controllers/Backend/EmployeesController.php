<?php

namespace Edifalia\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Edifalia\Http\Controllers\Controller;
use Edifalia\User;
use Edifalia\Models\Employee;
use Edifalia\Http\Requests\Employee\CreateRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Request as RequestObj;
Use Illuminate\Validation\Rule;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::filterEmployees(RequestObj::get('name'));
        return view('backend.utilities.employees.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $header = [
            'route' => 'employees.store',
            'method' => 'POST',
            'role' => 'form',
        ];
        return view('backend.utilities.employees.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.add_employee'))->with('new', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        if (is_null($request->communities)){
            Session::flash('message', [
                'alert' => 'danger',
                'icon' => 'ban',
                'text' => trans('dashboard.messages.empty_community')
            ]);
            return redirect()->back()->withInput();
        }
        if (is_null($request->access_data) && is_null($request->access_management)
            && is_null($request->access_mail) && is_null($request->access_incidents)
            && is_null($request->access_reports) && is_null($request->access_accounting)){
            Session::flash('message', [
                'alert' => 'danger',
                'icon' => 'ban',
                'text' => trans('dashboard.messages.empty_permissions')
            ]);
            return redirect()->back()->withInput();
        }

        $user = new User();
        $user->fill($request->all());
        $user->type = 1;
        $user->save();

        $employee = new Employee();
        $employee->fill($request->all());
        $employee->user_id = $user->id;
        $employee->save();
        $employee->communities()->attach($request->communities);

        Session::flash('message', [
            'alert' => 'success',
            'icon' => 'check',
            'text' => trans('dashboard.messages.created')
        ]);
        return redirect()->route('employees.edit', $employee->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = Employee::find($id);
        if (is_null($employee))
            return redirect()->route('employees.index');

        return view('backend.utilities.employees.show')->with('employee', $employee);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::find($id);
        if (is_null($employee))
            return redirect()->route('employees.index');

        $header = [
            'route' => ['employees.update', $employee->id],
            'method' => 'PUT',
            'role' => 'form',
            'id'
        ];
        return view('backend.utilities.employees.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.edit_employee'))
            ->with('employee', $employee)->with('edit', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employee = Employee::find($id);
        if (is_null($employee))
            return redirect()->route('employees.index');

        \Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => ['required', 'email', 'max:255', Rule::unique('users', 'email')
                ->ignore($employee->user->id)],
            'password' => 'min:6|confirmed',
            'first_name' => 'required|max:100',
            'last_name' => 'required|max:100',
            'dni' => ['required', 'max:10', Rule::unique('employees', 'dni')->ignore($employee->id)],
            'department' => 'required',
            'phone' => 'max:20',
            'extension' => 'max:20',
            'position_id' => 'required|exists:positions,id',
            'office_id' => 'required|exists:offices,id',
            'communities.*' => 'required|numeric|exists:communities,id',
            'access_data' => 'in:1',
            'access_management' => 'in:1',
            'access_mail' => 'in:1',
            'access_incidents' => 'in:1',
            'access_reports' => 'in:1',
            'access_accounting' => 'in:1',
            //'signature' => 'max:500',
        ])->validate();

        if (is_null($request->communities)){
            Session::flash('message', [
                'alert' => 'danger',
                'icon' => 'ban',
                'text' => trans('dashboard.messages.empty_community')
            ]);
            return redirect()->back()->withInput();
        }
        if (is_null($request->access_data) && is_null($request->access_management)
                && is_null($request->access_mail) && is_null($request->access_incidents)
                && is_null($request->access_reports) && is_null($request->access_accounting)){
            Session::flash('message', [
                'alert' => 'danger',
                'icon' => 'ban',
                'text' => trans('dashboard.messages.empty_permissions')
            ]);
            return redirect()->back()->withInput();
        }

        if (is_null($request->active) && !$employee->user->active)
            $employee->user->active = true;
        $employee->user->fill($request->all());
        $employee->user->save();

        $employee->synchronizePermissions($request);
        $employee->fill($request->all());
        $employee->save();
        $employee->communities()->sync($request->communities);

        Session::flash('message', [
            'alert' => 'success',
            'icon' => 'check',
            'text' => trans('dashboard.messages.updated')
        ]);
        return redirect()->route('employees.edit', $employee->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $employee = Employee::find($id);
            if (!is_null($employee)){
                $employee->user->delete();
                $employee->delete();
                return response()->json([
                    'data' => true,
                    'id' => $id,
                    'message' => trans('dashboard.messages.deleted'),
                    'undo' => trans('dashboard.buttons.undo')
                ]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }

    public function postUndelete(Request $request, $id)
    {
        if($request->ajax()){
        $employee = Employee::onlyTrashed()->where('id', $id)->first();
            if (!is_null($employee)){
                $employee->user()->restore();
                $employee->restore();
                return response()->json(['data' => true, 'id' => $id]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }

    public function postSaveStatus(Request $request, $id)
    {
        if($request->ajax()){
            $employee = Employee::find($id);
            if (!is_null($employee)){
                $employee->user->active = $request->status;
                $employee->user->save();
                return response()->json([
                    'data' => true,
                    'message' => trans('dashboard.messages.saved')
                ]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }
}

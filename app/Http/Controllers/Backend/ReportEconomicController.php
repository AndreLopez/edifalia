<?php

namespace Edifalia\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Edifalia\Http\Requests;
use Edifalia\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Illuminate\Database\Query\Builder;
// use PDF;
use Edifalia\Models\Pdf\EdifaliaPDF;

/**
 * Controller for reports economics
 * @author Máximo Sojo maxsojo13@gmail.com <maxtoan at atechnologies>
 */
class ReportEconomicController extends Controller
{
    /**
     * 
     *
     * @return \Illuminate\Http\Response
     */
    public function forAccount()
    {
    	return view('backend.report.economic.account.index');
	}

    public function forAccountExcel(Request $request){
        
        $title = 'Ingresos y gastos agrupados por cuentas';
        
        $data = [
            'title' => $title            
        ];

        \Maatwebsite\Excel\Facades\Excel::create(trans('dashboard.mnu_dossier_sign'), function($excel) use ($data) {
            $excel->sheet(trans('dashboard.mnu_dossier_sign'), function($sheet) use ($data) {
                $sheet->setPageMargin(0.35);
                // $sheet->setOrientation('landscape');
                $sheet->loadView('backend.report.economic.account.excel', $data);
            });
        })->export('xls');
    }

	/**
	 * 
	 */
	public function forReceipt(Request $request){
		return view('backend.report.economic.receipt.index');
	}

	/**
	 * 
	 */
	public function forReceiptExportPdf(Request $request){
		$data = json_decode($request->get('data'));
		$title = trans('report.economic.receipt');
    	$community = \Edifalia\Models\Backend\Community::find($data->idCommunity);
        
    	$receipt = \DB::table('issue_receipts as ir')    	
            ->join('community_issue_receipt as ci', 'ci.issue_receipt_id', '=', 'ir.id')
            ->join('receipt_dues as rd', 'rd.issue_receipt_id', '=', 'ir.id')
            ->join('receipt_due_items as rdi', 'rdi.receipt_due_id', '=', 'rd.id')
            ->join('properties as p', 'p.id', '=', 'rdi.property_id')
            ->select('ir.*', 'rdi.*', 'p.*')
            ->where('ci.community_id', '=', $data->idCommunity)
            ->get();
        
		EdifaliaPDF::SetTitle($title);
        EdifaliaPDF::Header();
        EdifaliaPDF::Footer();
        EdifaliaPDF::AddPage();
        // EdifaliaPDF::Write(0, trans('dashboard.mnu_dossier_sign'));
        EdifaliaPDF::SetMargins(7, 18, 7);
        EdifaliaPDF::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
        // $image = public_path().'/static/images/logo-horizontal.png';
        // EdifaliaPDF::Image($image,
        //     $x = '10',
        //     $y = '7',
        //     $w = 30,
        //     $h = 0,
        //     $type = 'PNG',
        //     $link = '',
        //     $align = 'N',
        //     $resize = false,
        //     $dpi = 300,
        //     $palign = 'L',
        //     $ismask = false,
        //     $imgmask = false,
        //     $border = 0,
        //     $fitbox = false,
        //     $hidden = false,
        //     $fitonpage = false,
        //     $alt = false,
        //     $altimgs = array());
        $html = view('backend.report.economic.receipt.pdf')
                ->with(['data' => $data])
                ->with(['community' => $community])
                ->with(['receipt' => $receipt]);
        EdifaliaPDF::writeHTMLCell(120, 0, '', '', $html, '', 1, 0, true, 'R', true);
        EdifaliaPDF::Output('receipts.pdf', 'I');
	}

    public function forReceiptExportExcel(Request $request){
        
        $data = json_decode($request->get('data'));
        $title = 'Recibos emitidos del '.$data->dateStart.' al '.$data->dateEnd;
        $community = \Edifalia\Models\Backend\Community::find($data->idCommunity);
        
        $receipt = \DB::table('issue_receipts as ir')       
            ->join('community_issue_receipt as ci', 'ci.issue_receipt_id', '=', 'ir.id')
            ->join('receipt_dues as rd', 'rd.issue_receipt_id', '=', 'ir.id')
            ->join('receipt_due_items as rdi', 'rdi.receipt_due_id', '=', 'rd.id')
            ->join('properties as p', 'p.id', '=', 'rdi.property_id')
            ->select('ir.*', 'rdi.*', 'p.*')
            ->where('ci.community_id', '=', $data->idCommunity)
            ->get();
        
        $data = [
            'title' => $title,
            'community' => $community,
            'receipt' => $receipt
        ];

        \Maatwebsite\Excel\Facades\Excel::create(trans('dashboard.mnu_dossier_sign'), function($excel) use ($data) {
            $excel->sheet(trans('dashboard.mnu_dossier_sign'), function($sheet) use ($data) {
                $sheet->setPageMargin(0.35);
                $sheet->setOrientation('landscape');
                $sheet->loadView('backend.report.economic.receipt.excel', $data);
            });
        })->export('xls');
    }

	public function forMovement(Request $request){
        return view('backend.report.economic.movement.index');
	}

    public function forMovementExcel(Request $request){
        $data = json_decode($request->get('_dc'));
        
        $budgetProperty = $dataBudget = [];
        $amount = $totalIn = $totalOut = $total = $sald = 0;
        $community = \Edifalia\Models\Backend\Community::find($data->idCommunity);
        $dataMonths = \Edifalia\Models\Report::getMonth();
        
        foreach ($dataMonths as $key => $month) {
            $amount = 0;
            $budgetProperty = \DB::table('budget_property')
                ->join('budgets', 'budget_property.budget_id', '=', 'budgets.id')
                ->select('budget_property.amount', 'budgets.month', 'budgets.title')
                ->where('budgets.community_id', '=', $community->id)
                ->where('budgets.month', '=', $month['id'])
                ->get();
                
                        
            foreach ($budgetProperty as $key => $value) {
                $amount = $amount + $value->amount;                
            }
            $dataBudget[] = ['title' => 'Mes de '.$month['name'], 
                    'data' => [
                        'title'  => 'Pago de servicios',
                        'amount' => number_format($amount, 2, ',', ' '),
                        'in'     => 1
                    ]
                ];
            if ($data->includeAll) {
                if (date('m') == $month['id']) {              
                    break;
                }
            }else{
                if ($data->month->id == $month['id']) {              
                    break;
                }
            }
        }
        // Totales
        foreach ($dataBudget as $key => $value) {
            if ($value['data']['in'] == 1) {
                $totalIn = $totalIn + $value['data']['amount'];
            }else{
                $totalOut = $totalOut + $value['data']['amount'];
            }
        }

        $title = 'Detalle de ingresos y gastos de '.$community->name.' - Período desde 01/01/2017 hasta 31/12/2017';

        $data = [
            'title' => $title,
            'dataBudget' => $dataBudget,
            'total' => [
                'totalIn'  => number_format($totalIn, 2, ',', ' '),
                'totalOut' => number_format($totalOut, 2, ',', ' '),
                'sald' => number_format($sald, 2, ',', ' ')
            ]
        ];

        \Maatwebsite\Excel\Facades\Excel::create('movement', function($excel) use ($data) {
            $excel->sheet('movement', function($sheet) use ($data) {
                $sheet->setPageMargin(0.35);
                // $sheet->setOrientation('landscape');
                $sheet->loadView('backend.report.economic.movement.excel', $data);
            });
        })->export('xls');
    }
	
    public function forClasification(Request $request){
		return view('backend.report.economic.clasification.index');
	}

    public function forClasificationExcel(Request $request){
        
        $title = 'Clasificación';
        
        $data = [
            'title' => $title
        ];

        \Maatwebsite\Excel\Facades\Excel::create(trans('dashboard.mnu_dossier_sign'), function($excel) use ($data) {
            $excel->sheet(trans('dashboard.mnu_dossier_sign'), function($sheet) use ($data) {
                $sheet->setPageMargin(0.35);
                // $sheet->setOrientation('landscape');
                $sheet->loadView('backend.report.economic.clasification.excel', $data);
            });
        })->export('xls');
    }

    public function forMonth(Request $request){
        return view('backend.report.economic.month.index');
    }

    public function forMonthPdf(Request $request){
        
        $title = trans('report.economic.receipt');
        
        EdifaliaPDF::SetTitle($title);
        EdifaliaPDF::Header();
        EdifaliaPDF::Footer();
        EdifaliaPDF::AddPage('L');
        // EdifaliaPDF::Write(0, trans('dashboard.mnu_dossier_sign'));
        EdifaliaPDF::SetMargins(7, 18, 7);
        EdifaliaPDF::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
        // $image = public_path().'/static/images/logo-horizontal.png';
        // EdifaliaPDF::Image($image,
        //     $x = '10',
        //     $y = '7',
        //     $w = 30,
        //     $h = 0,
        //     $type = 'PNG',
        //     $link = '',
        //     $align = 'N',
        //     $resize = false,
        //     $dpi = 300,
        //     $palign = 'L',
        //     $ismask = false,
        //     $imgmask = false,
        //     $border = 0,
        //     $fitbox = false,
        //     $hidden = false,
        //     $fitonpage = false,
        //     $alt = false,
        //     $altimgs = array());
        $html = view('backend.report.economic.month.pdf');
        EdifaliaPDF::writeHTMLCell(120, 0, '', '', $html, '', 1, 0, true, 'R', true);
        EdifaliaPDF::Output('reporr-fot-month.pdf', 'I');
    }

    public function forMonthExcel(Request $request){
        
        $title = 'Month';
        
        $data = [
            'title' => $title
        ];

        \Maatwebsite\Excel\Facades\Excel::create(trans('dashboard.mnu_dossier_sign'), function($excel) use ($data) {
            $excel->sheet(trans('dashboard.mnu_dossier_sign'), function($sheet) use ($data) {
                $sheet->setPageMargin(0.35);
                // $sheet->setOrientation('landscape');
                $sheet->loadView('backend.report.economic.month.excel', $data);
            });
        })->export('xls');
    }

	public function forProperty(Request $request){
		return view('backend.report.economic.property.index');
	}

    public function forPropertyExcel(Request $request){
        $idCommunity = $request->query->get('idCommunity');
        $title = 'Ingresos realizados por propiedades';
        
        $budgetProperty = $dataBudget = $data = [];
        $amount = $totalIn = $totalOut = $total = $sald = 0;

        $community = \Edifalia\Models\Backend\Community::find($idCommunity);
        $dataMonths = \Edifalia\Models\Report::getMonth();
        $dataYear = \Edifalia\Models\Report::getYear();

        // 
        $dat = [];
        $cont = 0;
        foreach ($dataMonths as $key => $month) {
            $datProperty = [];
            $properties = \DB::table('properties')
                ->select('properties.*')
                ->where('properties.community_id', '=', $community->id)                
                ->get();

            if ($cont == 0) {
                $dat[] = [
                    'month' => $month['id'],
                    'data' => []
                ];                
            }

            foreach ($properties as $key => $valueProperty) {
                $amount = 0;
                $budgetProperty = \DB::table('budget_property')
                    ->join('budgets', 'budget_property.budget_id', '=', 'budgets.id')
                    ->select('budget_property.amount', 'budgets.month', 'budgets.title')
                    ->where('budget_property.property_id', '=', $valueProperty->id)
                    ->where('budgets.community_id', '=', $community->id)
                    ->where('budgets.month', '=', $month['id'])
                    ->get();

                foreach ($budgetProperty as $key => $value) {
                    $amount = $amount + $value->amount;                
                }
                $datProperty[] = [
                    'property' => $valueProperty->property_number,
                    'amount' => $amount                    
                ];                
            }

            $dat[] = [
                'month' => $month['id'],
                'data' => $datProperty
            ];

            $cont = $cont + 1;          
        }
        
        // Totales
        // foreach ($dataBudget as $key => $value) {
        //     if ($value['data']['in'] == 1) {
        //         $totalIn = $totalIn + $value['data']['amount'];
        //     }else{
        //         $totalOut = $totalOut + $value['data']['amount'];
        //     }
        // }

        $title = 'Detalle de ingresos y gastos de '.$community->name.' - Período desde 01/01/2017 hasta 31/12/2017';

        // $data = [
        //     'title' => $title,
        //     'dataBudget' => $dataBudget,
        //     'total' => [
        //         'totalIn'  => number_format($totalIn, 2, ',', ' '),
        //         'totalOut' => number_format($totalOut, 2, ',', ' '),
        //         'sald' => number_format($sald, 2, ',', ' ')
        //     ]
        // ];
        // var_dump($data['dataBudget']);
        // die();
        // 
        foreach ($dataMonths as $key => $value) {
            $preData[] = ['id' => $value['id'], 'name' => $value['name'].'/'.$dataYear[0]['name']];
        }
        $data = [
            'title' => $title,
            'preData' => $preData,
            'data' => $dat
        ];
        // var_dump($data);
        // die();

        \Maatwebsite\Excel\Facades\Excel::create('property', function($excel) use ($data) {
            $excel->sheet('property', function($sheet) use ($data) {
                $sheet->setPageMargin(0.35);
                $sheet->setOrientation('landscape');
                $sheet->loadView('backend.report.economic.property.excel', $data);
            });
        })->export('xls');
    }

	public function forTotal(Request $request){
		return view('backend.report.economic.total.index');
	}

    public function forTotalExcel(Request $request){
        
        $title = 'Total';
        
        $data = [
            'title' => $title
        ];

        \Maatwebsite\Excel\Facades\Excel::create(trans('dashboard.mnu_dossier_sign'), function($excel) use ($data) {
            $excel->sheet(trans('dashboard.mnu_dossier_sign'), function($sheet) use ($data) {
                $sheet->setPageMargin(0.35);
                // $sheet->setOrientation('landscape');
                $sheet->loadView('backend.report.economic.total.excel', $data);
            });
        })->export('xls');
    }

	public function forSupplierPayment(Request $request){
		return view('backend.report.economic.supplier.payment.index');
	}

    public function forSupplierPaymentExcel(Request $request){
        
        $title = 'supplier payment';
        
        $data = [
            'title' => $title
        ];

        \Maatwebsite\Excel\Facades\Excel::create(trans('dashboard.mnu_dossier_sign'), function($excel) use ($data) {
            $excel->sheet(trans('dashboard.mnu_dossier_sign'), function($sheet) use ($data) {
                $sheet->setPageMargin(0.35);
                // $sheet->setOrientation('landscape');
                $sheet->loadView('backend.report.economic.supplier.payment.excel', $data);
            });
        })->export('xls');
    }

	public function forSupplierPending(Request $request){
		return view('backend.report.economic.supplier.pending.index');
	}

    public function forSupplierPendingExcel(Request $request){
        
        $title = 'supplier pending';
        
        $data = [
            'title' => $title
        ];

        \Maatwebsite\Excel\Facades\Excel::create(trans('dashboard.mnu_dossier_sign'), function($excel) use ($data) {
            $excel->sheet(trans('dashboard.mnu_dossier_sign'), function($sheet) use ($data) {
                $sheet->setPageMargin(0.35);
                // $sheet->setOrientation('landscape');
                $sheet->loadView('backend.report.economic.supplier.pending.excel', $data);
            });
        })->export('xls');
    }
}
<?php

namespace Edifalia\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\Position;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

class PositionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $positions = Position::paginate(10);
        return view('backend.utilities.positions.index')->with('positions', $positions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $header = [
            'route' => 'positions.store',
            'method' => 'POST',
            'role' => 'form',
        ];
        return view('backend.utilities.positions.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.add_position'))->with('new', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Validator::make($request->all(), ['name' => 'required|max:100|unique:positions'])->validate();

        $position = new Position();
        $position->fill($request->all());
        $position->save();

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.created')
        ]);

        return redirect()->route('positions.edit', $position->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $position = Position::find($id);
        if (is_null($position))
            return redirect()->route('positions.index');

        $header = [
            'route' => ['positions.update', $position->id],
            'method' => 'PUT',
            'role' => 'form'
        ];

        return view('backend.utilities.positions.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.edit_position'))
            ->with('position', $position)->with('edit', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $position = Position::find($id);
        if (is_null($position))
            return redirect()->route('positions.index');

        \Validator::make($request->all(), [
            'name' => [
                'required',
                'max:100',
                Rule::unique('positions', 'name')->ignore($position->id)]
        ])->validate();

        $position->fill($request->all());
        $position->save();

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.updated')
        ]);
        return redirect()->route('positions.edit', $position->id);
    }

}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $position = Position::find($id);
            if (!is_null($position)){
                $position->delete();
                return response()->json([
                    'data' => true,
                    'id' => $id,
                    'message' => trans('dashboard.messages.deleted'),
                    'undo' => trans('dashboard.buttons.undo')
                ]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }

    public function postUndelete(Request $request, $id)
    {
        if($request->ajax()){
            $position = Position::onlyTrashed()->where('id', $id)->first();
            if (!is_null($position)){
                $position->restore();
                return response()->json(['data' => true, 'id' => $id]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }
}

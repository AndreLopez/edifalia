<?php

namespace Edifalia\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\Sector;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

class SectorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sectors = Sector::paginate(10);
        return view('backend.management.sectors.index')->with('sectors', $sectors);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $header = [
            'route' => 'sectors.store',
            'method' => 'POST',
            'role' => 'form'
        ];

        return view('backend.management.sectors.create_or_edit')
            ->with('header', $header)->with('new', true)
            ->with('title', trans('dashboard.titles.add_sector'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Validator::make($request->all(), [
            'name' => 'required|max:200|unique:sectors'
        ])->validate();

        $sector = new Sector();
        $sector->fill($request->all());
        $sector->save();

        Session::flash('message', [
            'alert' => 'succes',
            'text' => trans('dashboard.messages.created')
        ]);

        return redirect()->route('sectors.edit', $sector->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sector = Sector::find($id);
        if(is_null($sector))
            return redirect()->route('sectors.index');

        $header = [
            'route' => ['sectors.update', $sector->id],
            'method' => 'PUT',
            'role' => 'form'
        ];

        return view('backend.management.sectors.create_or_edit')
            ->with('header', $header)->with('sector', $sector)->with('edit', true)
            ->with('title', trans('dashboard.titles.edit_sector'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sector = Sector::find($id);
        if(is_null($sector))
            return redirect()->route('sectors.index');

        \Validator::make($request->all(), [
            'name' => [
                'required',
                'max:200',
                Rule::unique('sectors', 'name')->ignore($sector->id)]
        ])->validate();

        $sector->fill($request->all());
        $sector->save();

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.edited')
        ]);

        return redirect()->route('sectors.edit', $sector->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->ajax()){
            $sector = Sector::find($id);
            if (!is_null($sector)){
                $sector->delete();
                return response()->json([
                    'data' => true,
                    'id' => $id,
                    'message' => trans('dashboard.messages.deleted'),
                    'undo' => trans('dashboard.buttons.undo')
                ]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }

    public function postUndelete(Request $request, $id)
    {
        if($request->ajax()){
            $sector = Sector::onlyTrashed()->where('id', $id)->first();
            if (!is_null($sector)){
                $sector->restore();
                return response()->json(['data' => true, 'id' => $id]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }
}

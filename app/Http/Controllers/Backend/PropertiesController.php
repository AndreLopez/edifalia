<?php

namespace Edifalia\Http\Controllers\Backend;

use Illuminate\Http\Request;

use Edifalia\Http\Requests;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\Common\Property;
use Edifalia\Models\Common\PropertyType;
use Edifalia\Models\Backend\Community;
use Edifalia\Models\Backend\AccountingAccount;
use Edifalia\Models\Backend\BankAccount;
use Edifalia\Models\Common\PaymentMethod;
use Edifalia\Models\Common\PostalCode;
use Edifalia\Models\Common\Language;
use Edifalia\User; 
use Edifalia\Http\Requests\CreatePropertyRequest;

class PropertiesController extends Controller
{
  
  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  public function index()
  {
      $data = [
        'breadcrumb_title' => trans('common.property_management_title'),
        'breadcrumb_subtitle' => trans('common.property_list_title'),
        'panel_title' => trans('common.lst_property_title'),
        'url_new' => route('property.create'),
        'theads' => [
          [ 'align' => 'text-center', 'name' => 'properties.lst_col_id'],
          [ 'align' => 'text-center', 'name' => 'properties.lst_col_building_number'],
          [ 'align' => 'text-center', 'name' => 'properties.lst_col_property_number'],
          [ 'align' => 'text-center', 'name' => 'properties.lst_col_registered_property'],
          [ 'align' => 'text-center', 'name' => 'common.lst_col_type'],
          [ 'align' => 'text-center', 'name' => 'properties.lst_col_status',],
          [ 'align' => 'text-center', 'name' => 'properties.lst_col_coefficient',],
          [ 'align' => 'text-center', 'name' => 'common.lst_col_group_properties'],
          [ 'align' => 'text-center', 'name' => 'common.lst_col_group_concepts'],
          [ 'align' => 'text-center', 'name' => 'properties.lst_col_person_relation'],
          [ 'align' => 'text-center', 'name' => 'properties.lst_col_payment_method'],
          [ 'align' => 'text-center', 'name' => 'properties.lst_col_community']
        ],
        'tbody' => []
      ];

      $properties = Property::all();
      foreach ($properties as $property)
      {
        $id = $property->id;
        $cols  = "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . $id . "</td>";
        $cols .= "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . $property->building_number . "</td>";
        $cols .= "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . $property->property_number . "</td>";
        $cols .= "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . $property->registered_property . "</td>";
        $cols .= "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . $property->type->name . "</td>";
        $cols .= "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . $property->status_format . "</td>";
        $cols .= "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . $property->coefficient . "</td>";
        $cols .= "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . $property->group_properties_format . "</td>";
        $cols .= "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . $property->group_concepts_format . "</td>";
        $cols .= "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . $property->person->full_name . "</td>";
        $cols .= "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . $property->paymentMethod->type . "</td>";
        $cols .= "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . $property->community->code_name . "</td>";


        array_push($data['tbody'], 
         [
           'cols' => $cols, 
           'url_edit' => route('property.edit', $id),
           'url_delete' => 'property.destroy', 
           'record_id' => $id
         ]);
      }

      return view('layouts.list-records')->with('data', $data);
  }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $propertyTypes = [];
        $communities = [];
        $accountingAccounts = [];
        $paymentMethods = [];
        $postalCodes = [];
        $users = [];
        $languages = [];

        $data = [
            'breadcrumb_title' => trans('common.property_management_title'),
            'breadcrumb_subtitle' => trans('common.property_add_title'),
            'breadcrumb_link' => route('property.index'),
            'panel_title' => trans('common.new_property_title'),
            'btn_record_submit' => trans('common.btn_save_text'),
        ];
        $header = [
            'route' => 'property.store', 
            'method' => 'POST', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];

        if (PropertyType::all()->count() > 0)
          $propertyTypes =  PropertyType::all()->pluck('name', 'id');

        if (Community::all()->count() > 0)
          $communities = Community::all()->pluck('name', 'id');

        if (AccountingAccount::all()->count() > 0)
          $accountingAccounts = AccountingAccount::all()->pluck('code', 'id');

         if (PaymentMethod::all()->count() > 0)
          $paymentMethods = PaymentMethod::all()->pluck('type', 'id');

        if (PostalCode::all()->count() > 0)
          $postalCodes = PostalCode::all()->pluck('code', 'id');

        if (User::all()->count() > 0)
          $users = User::all()->pluck('name','id');

        if(Language::all()->count() > 0)
          $languages = Language::all()->pluck('name', 'id');

        return view('layouts.forms')
        ->with('data', $data)
        ->with('propertyTypes', $propertyTypes)
        ->with('communities', $communities)
        ->with('accountingAccounts', $accountingAccounts)
        ->with('paymentMethods', $paymentMethods)
        ->with('postalCodes', $postalCodes)
        ->with('users', $users)
        ->with('languages', $languages)
        ->with('person_id', 0)
        ->with('new', true)
        ->with('header', $header)
        ->with('fields_template', 'common.property-fields');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePropertyRequest $request)
    {

      $code = 0;
      $iban = 0;
      $accountingAccountObj = null;
      $bankAccountObj = null;
      $accounting = null;
      $bankAccount = null;


      /*$this->validate($request, [
        'building_number' => 'required|min:1|max:4', 
        'property_number' => 'required|min:1|max:6', 
        'registered_property' => 'min:1|max:45', 
        'property_type_id' => 'required|exists:property_types,id',
        'status' => 'required|boolean',
        'coefficient' => 'numeric',
        'group_properties' => 'required|boolean',
        'group_concepts' => 'required|boolean',
        'person_id' => 'required|integer|exists:persons,id',
        'payment_method_id' => 'required|integer|exists:payment_methods,id',
        'community_id' => 'required|integer|exists:communities,id'
        ]);*/

      $property = new Property();
      $input = $request->all();
      $input['coefficient'] = (isset($input['coefficient']) ? $input['coefficient'] : 0);
      $property->fill($input);
      $property->save();

      foreach ($request->get('accounting_account_code') as $key => $value) 
      {
        if (!empty($value)) 
        {
          $code = $request->input('accounting_account_code.' . $key);
          $accountingAccountObj = AccountingAccount::whereCode($code)->first();
          if(count($accountingAccountObj) == 0)
          {
            $accounting = new AccountingAccount();
            $accounting->code = $request->input('accounting_account_code.' . $key);
            $accounting->title = $request->input('accounting_account_title.' . $key);
            $accounting->save();

            $property->accountingAccounts()->attach($accounting->id);
          }
        }
      }

      foreach ($request->get('bank_account_accounting_code')as $key => $value) 
      {
        if (!empty($value)) 
        {
          $code = $request->input('bank_account_accounting_code.' . $key);
          $accountingAccountObj = AccountingAccount::whereCode($code)->first();
          if (count($accountingAccountObj) > 0 && !$accountingAccountObj->hasBankAccount()) 
          {
            $iban = $request->input('bank_account_iban.' . $key);
            $bankAccountObj = BankAccount::whereIban($iban)->first();
            if (count($bankAccountObj) == 0) 
            {
              $bankAccount = new BankAccount();
              $bankAccount->iban = $request->input('bank_account_iban.' . $key);
              $bankAccount->code_a = $request->input('bank_account_code_a.' . $key);
              $bankAccount->code_b = $request->input('bank_account_code_b.' . $key);
              $bankAccount->code_c = $request->input('bank_account_code_c.' . $key);
              $bankAccount->code_d = $request->input('bank_account_code_d.' . $key);
              $bankAccount->code_e = $request->input('bank_account_code_e.' . $key);
              $bankAccount->sufix = $request->input('bank_account_sufix.' . $key);
              $bankAccount->bic = $request->input('bank_account_bic.' . $key);
              $bankAccount->accounting_account_id = $accountingAccountObj->id;
              $bankAccount->save();

              $property->bankAccounts()->attach($bankAccount->id);
            }
          }
        }
      }

      $request->session()->flash('message', [
        'type' => 'store',
        'alert' => 'success',
        'text' => trans('dashboard.messages.created')
        ]);

      return view('common.panel-info');
    }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
     {
        $propertyTypes = [];
        $communities = [];
        $accountingAccounts = [];
        $paymentMethods = [];
        $postalCodes = [];
        $users = [];
        $languages = [];

        $property = Property::findOrfail($id);
        $data = [
          'breadcrumb_title' => trans('common.property_management_title'),
          'breadcrumb_subtitle' => trans('common.property_upd_title'),
          'breadcrumb_link' => route('property.index'),
          'panel_title' => trans('common.edit_property_title'),
          'btn_record_submit' => trans('common.edit_record_submit'),
        ];
        $header = [
          'route' => ['property.update', $property->id], 
          'method' => 'PUT', 
          'role' => 'form',
          'class' => 'form-horizontal',
        ];

        if (PropertyType::all()->count() > 0)
          $propertyTypes =  PropertyType::all()->pluck('name', 'id');

        if (Community::all()->count() > 0)
          $communities = Community::all()->pluck('name', 'id');

        if (AccountingAccount::all()->count() > 0)
          $accountingAccounts = AccountingAccount::all()->pluck('code', 'id');

         if (PaymentMethod::all()->count() > 0)
          $paymentMethods = PaymentMethod::all()->pluck('type', 'id');

        if (PostalCode::all()->count() > 0)
          $postalCodes = PostalCode::all()->pluck('code', 'id');

        if (User::all()->count() > 0)
          $users = User::all()->pluck('name','id');

        if(Language::all()->count() > 0)
          $languages = Language::all()->pluck('name', 'id');

        return view('layouts.forms')
        ->with('data', $data)
        ->with('propertyTypes', $propertyTypes)
        ->with('communities', $communities)
        ->with('accountingAccounts', $accountingAccounts)
        ->with('paymentMethods', $paymentMethods)
        ->with('postalCodes', $postalCodes)
        ->with('users', $users)
        ->with('languages', $languages)
        ->with('person_id', $property->person_id)
        ->with('edit', true)
        ->with('header', $header)
        ->with('model', $property)
        ->with('fields_template', 'common.property-fields');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
          'building_number' => 'required|min:1|max:4', 
          'property_number' => 'required|min:1|max:6', 
          'registered_property' => 'min:1|max:45', 
          'property_type_id' => 'required|exists:property_types,id',
          'status' => 'required|boolean',
          'coefficient' => 'numeric',
          'group_properties' => 'required|boolean',
          'group_concepts' => 'required|boolean',
          'person_id' => 'required|integer|exists:persons,id',
          'payment_method_id' => 'required|integer|exists:payment_methods,id',
          'community_id' => 'required|integer|exists:communities,id'
        ]);

        $property = Property::find($id);
        if (!is_null($property)) {
            $property->fill($request->all());
            $property->save();
            $request->session()->flash('message', [
              'type' => 'update',
              'alert' => 'success',
              'text' => trans('dashboard.messages.updated')
            ]);
        }
        return redirect()->route('property.index');
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) 
        {
            $property = Property::find($id);
            if (!is_null($property)) 
            {
                $property->delete();
                $request->session()->flash('message', [
                  'type' => 'destroy',
                  'alert' => 'success',
                  'text' => trans('dashboard.messages.deleted')
                ]);
                return response()->json(['result' => true]);
            }
            return response()->json(['result' => false]);
        }
        return redirect()->route('property.index');
    }

}

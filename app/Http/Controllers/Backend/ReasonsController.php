<?php

namespace Edifalia\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\Reason;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

class ReasonsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reasons = Reason::paginate(10);
        return view('backend.management.reasons.index')->with('reasons', $reasons);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $header = [
            'route' => 'reasons.store',
            'method' => 'POST',
            'role' => 'form',
        ];
        return view('backend.management.reasons.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.add_reason'))->with('new', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Validator::make($request->all(), [
            'name' => [
                'required',
                'max:200',
                'unique:reasons']
        ])->validate();

        $reason = new Reason();
        $reason->fill($request->all());
        $reason->save();

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.created')
        ]);

        return redirect()->route('reasons.edit', $reason->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reason = Reason::find($id);
        if (is_null($reason))
            return redirect()->route('reasons.index');

        $header = [
            'route' => ['reasons.update', $reason->id],
            'method' => 'PUT',
            'role' => 'form'
        ];

        return view('backend.management.reasons.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.edit_reason'))
            ->with('reason', $reason)->with('edit', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $reason = Reason::find($id);
        if (is_null($reason))
            return redirect()->route('receptions.index');

        \Validator::make($request->all(), [
            'name' => [
                'required',
                'max:200',
                Rule::unique('reasons', 'name')->ignore($reason->id)]
        ])->validate();

        $reason->fill($request->all());
        $reason->save();

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.updated')
        ]);
        return redirect()->route('reasons.edit', $reason->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $reason = Reason::find($id);
            if (!is_null($reason)){
                $reason->delete();
                return response()->json([
                    'data' => true,
                    'id' => $id,
                    'message' => trans('dashboard.messages.deleted'),
                    'undo' => trans('dashboard.buttons.undo')
                ]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }

    public function postUndelete(Request $request, $id)
    {
        if($request->ajax()){
            $reason = Reason::onlyTrashed()->where('id', $id)->first();
            if (!is_null($reason)){
                $reason->restore();
                return response()->json(['data' => true, 'id' => $id]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }
}

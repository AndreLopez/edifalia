<?php

namespace Edifalia\Http\Controllers\Backend;

use Illuminate\Http\Request;

use Edifalia\Http\Requests;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\Backend\Supplier;
use Edifalia\Models\Common\Status;
use Edifalia\Models\Common\PostalCode;
use Illuminate\Pagination\Paginator;
use Illuminate\Database\Query\Builder;
use Edifalia\Models\Common\Country;
use Edifalia\Models\Common\BankAccount;
use Edifalia\Http\Requests\SupplierRequest;
use Edifalia\Models\Backend\AccountingAccount;
use Edifalia\User; 
use Edifalia\Models\Common\SupplierActivitie;

class SuppliersController extends Controller
{
 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
      
        $data = [
            'breadcrumb_title' => trans('supplier.management_title'),
            'breadcrumb_subtitle' => trans('supplier.listing_title'),
            'panel_title' => trans('supplier.list_title'),
            'url_new' => route('supplier.create'),
            'theads' => [
                ['align' => 'text-center', 'business_name' => 'common.lst_col_business_name'],
            ],
            'tbody' => []
        ];
         $suppliers = Supplier::all();
        foreach ($suppliers as $supplier) {
            $id = $supplier->id;
            $cols  = "<td class='col-xs-2 col-sm-2 col-md-2 col-lg-2    '>" . $supplier->business_name . "</td>";
            array_push($data['tbody'], [
                'cols' => $cols, 'url_edit' => route('supplier.edit', $id),
                'url_delete' => 'supplier.destroy', 'record_id' => $id
            ]);
        
           }
          return view('layouts.list-records_supplier')->with('data', $data);
 
  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     
        $data = [
            'breadcrumb_title' => trans('supplier.management_title'),
            'breadcrumb_subtitle' => trans('supplier.add_title'),
            'breadcrumb_link' => route('supplier.index'),
            'panel_title' => trans('supplier.new_title'),
            'btn_record_submit' => trans('common.btn_save_text'),
        ];
        $header = [
            'route' => 'supplier.store', 
            'method' => 'POST', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];
        $estatus = Status::all();
        $st_options = [];
        foreach ($estatus as $st) {
            $st_options[$st->id] = $st->name;
        }

        $countries = Country::all();
        $country_options = [];
        foreach ($countries as $country) {
            $country_options[$country->id] = $country->name;
        }
       
       $postal_codes = PostalCode::all(); 
       $postal_code_options = []; 
       foreach ($postal_codes as $postal_code) {
           $postal_code_options[$postal_code->id] = $postal_code->code; 
       }

       $users = User::all();
       $user_options = []; 
       foreach ($users as  $user) {
           $user_options[$user->id] = $user->name; 
       }

     return view('layouts.forms')->with('data', $data)->with('new', true)->with('header', $header)->with('countries', $country_options)->with('status', $st_options)->with('postal_codes', $postal_code_options)->with('users', $user_options)->with('fields_template', 'backend.supplier.create-edit-fields');

 }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SupplierRequest $request )
    {

       $supplier = new Supplier(); 
       $supplier->contact_name = $request->get('contact_name');
       $supplier->cif = $request->get('cif'); 
       $supplier->business_name = $request->get('business_name'); 
       $supplier->tradename = $request->get('tradename'); 
       $supplier->address = $request->get('address');

       $supplier->website = $request->get('website');

       $supplier->update_data_from_virtual_office = $request->get('update_data_from_virtual_office') ? $request->get('update_data_from_virtual_office'):false; 
      
       $supplier->virtual_office_access = $request ->get('virtual_office_access') ? $request->get('virtual_office_access'): false; 

        $supplier->postal_code_id = $request->get('postal_code_id');

         $supplier->user_id = $request->get('user_id');


       if (!empty($request->get('notes'))){
        $supplier->notes = $request->get('notes');

       }

       
        $supplier->save(); ;

       /** Registro de cuentas bancarias de proveedores */

        foreach ($request->get('bank_account_accounting_code') as $key => $value) {
            if (!empty($value)) {
                $ac = AccountingAccount::where('code', $value)->first();
                
                $bank_account = new BankAccount();
                $bank_account->iban = $request->input('bank_account_iban.' . $key);
                $bank_account->code_a = $request->input('bank_account_code_a.' . $key);
                $bank_account->code_b = $request->input('bank_account_code_b.' . $key);
                $bank_account->code_c = $request->input('bank_account_code_c.' . $key);
                $bank_account->code_d = $request->input('bank_account_code_d.' . $key);
                $bank_account->code_e = $request->input('bank_account_code_e.' . $key);
                $bank_account->sufix = $request->input('bank_account_sufix.' . $key);
                $bank_account->bic = $request->input('bank_account_bic.' . $key);

                $bank_account->save();

                $supplier->bankAccounts()->attach($bank_account->id);
            }
        }

    
     
        $request->session()->flash('message', ['alert' => 'success', 'text' => trans('dashboard.messages.created')]); 

        return redirect()->route('supplier.index'); 

      }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show ($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     
     $supplier = Supplier::findOrfail($id);
     $data = [
        'breadcrumb_title' => trans('supplier.management_title'),
            'breadcrumb_subtitle' => trans('supplier.add_title'),
            'breadcrumb_link' => route('supplier.index'),
            'panel_title' => trans('supplier.new_title'),
            'btn_record_submit' => trans('common.btn_save_text'),
    ];

    $header = [
            'route' => ['supplier.update', $supplier->id], 
            'method' => 'PUT', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ]; 
    
      $estatus = Status::all();
        $st_options = [];
        foreach ($estatus as $st) {
            $st_options[$st->id] = $st->name;
        }

        $countries = Country::all();
        $country_options = [];
        foreach ($countries as $country) {
            $country_options[$country->id] = $country->name;
        }
       
       $postal_codes = PostalCode::all(); 
       $postal_code_options = []; 
       foreach ($postal_codes as $postal_code) {
           $postal_code_options[$postal_code->id] = $postal_code->code; 
       }


       $users = User::all();
       $user_options = []; 
       foreach ($users as  $user) {
           $user_options[$user->id] = $user->name; 
       }

    return view('layouts.forms')->with('data', $data)->with('edit', true)->with('header', $header) ->with('model', $supplier)->with('countries', $country_options)->with('status', $st_options)->with('postal_codes', $postal_code_options)->with('users', $user_options)->with('fields_template', 'backend.supplier.create-edit-fields');




    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $this->validate($request, [
         'cif' => 'required|max:15',
            'business_name' => 'required|max:45',
            'tradename' => 'required|max:45', 
            'address' => 'required',
            'notes' => 'nullable',
            'contact_name' => 'required',
            'virtual_office_access' => 'nullable', 
        'update_data_from_virtual_office' => 'nullable', 
        'postal_code_id' => 'required|integer|exists:postal_codes,id',
          'user_id' => 'required|integer|exists:users,id',
        ]);

       $supplier = Supplier::find($id); 
       if (!is_null($supplier)) {
        $supplier->fill($request->all()); 
        $supplier->save(); 
         $request->session()->flash('message', [
                'alert' => 'success', 'text' => trans('dashboard.messages.updated')
            ]);
       } 
          return redirect()->route('supplier.index');
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
      if ($request->ajax()) {
            $suppliers = Supplier::find($id);
            if (!is_null($suppliers)) {
                $suppliers->delete();
                $request->session()->flash('message', ['type' => 'destroy']);
                return response()->json(['result' => true]);
            }
            return response()->json(['result' => false]);
        }
        return redirect()->route('supplier.index');
    }

    public function selectList(Request $request)
    {
        if ($request->ajax()) {
            $items = [];
            $items = Supplier::all();
            return response()->json(['items' => $items, 'success' => $items ? true : false]);
        }
    
    }
}



<?php

namespace Edifalia\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\Office;
use Edifalia\Models\Backend\Community;
use Edifalia\Models\Employee;
use Edifalia\Http\Requests\Office\CreateRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Request as RequestObj;
Use Illuminate\Validation\Rule;

class OfficesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offices = Office::filterOffices(RequestObj::get('name'));
        return view('backend.utilities.offices.index', compact('offices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $header = [
            'route' => 'offices.store',
            'method' => 'POST',
            'role' => 'form',
        ];
        return view('backend.utilities.offices.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.add_office'))->with('new', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        if (is_null($request->communities)){
            Session::flash('message', [
                'alert' => 'danger',
                'icon' => 'ban',
                'text' => trans('dashboard.messages.empty_community')
            ]);
            return redirect()->back()->withInput();
        }

        $office = new Office();
        $office->fill($request->all());
        $office->save();

        foreach ($request->communities as $id){
            $community = Community::findOrfail($id);
            $community->office_id = $office->id;
            $community->save();
        }

        Session::flash('message', [
            'alert' => 'success',
            'icon' => 'check',
            'text' => trans('dashboard.messages.created')
        ]);
        return redirect()->route('offices.edit', $office->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $office = Office::find($id);
        if (is_null($office))
            return redirect()->route('offices.index');

        return view('backend.utilities.offices.show')->with('office', $office);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $office = Office::find($id);
        if (is_null($office))
            return redirect()->route('offices.index');

        $header = [
            'route' => ['offices.update', $office->id],
            'method' => 'PUT',
            'role' => 'form',
        ];
        return view('backend.utilities.offices.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.edit_office'))
            ->with('office', $office)->with('edit', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $office = Office::find($id);
        if (is_null($office))
            return redirect()->route('offices.index');

        \Validator::make($request->all(), [
            'identifier' => ['required', Rule::unique('offices', 'identifier')->ignore($office->id)],
            'name' => 'required|max:200',
            'address' => 'required',
            'postal_code_id' => 'required|exists:postal_codes,id',
            'phone1' => 'required|max:20',
            'phone2' => 'max:20',
            'fax' => 'max:20',
            'email' => 'email',
            'communities.*' => 'required|numeric|exists:communities,id',
        ])->validate();


        if (is_null($request->communities)){
            Session::flash('message', [
                'alert' => 'danger',
                'icon' => 'ban',
                'text' => trans('dashboard.messages.empty_community')
            ]);
            return redirect()->back()->withInput();
        }

        $office->fill($request->all());
        $office->save();

        foreach($office->communities as $myCommunity){
            if (!in_array($myCommunity->id, $request->communities)){
                $myCommunity->office_id = null;
                $myCommunity->save();
            }
        }

        foreach ($request->communities as $id){
            $community = Community::findOrfail($id);
            $community->office_id = $office->id;
            $community->save();
        }

        Session::flash('message', [
            'alert' => 'success',
            'icon' => 'check',
            'text' => trans('dashboard.messages.updated')
        ]);
        return redirect()->route('offices.edit', $office->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $office = Office::find($id);
            if (!is_null($office)){
                $office->delete();
                return response()->json([
                    'data' => true,
                    'id' => $id,
                    'message' => trans('dashboard.messages.deleted'),
                    'undo' => trans('dashboard.buttons.undo')
                ]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }

    public function postUndelete(Request $request, $id)
    {
        if($request->ajax()){
            $office = Office::onlyTrashed()->where('id', $id)->first();
            if (!is_null($office)){
                $office->restore();
                return response()->json(['data' => true, 'id' => $id]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }

    public function getCommunities(Request $request)
    {
        if($request->ajax()){
            $office = Office::find($request->office_id);
            if (!is_null($office)){
                if (!is_null($request->employee_id)){
                    $employee = Employee::find($request->employee_id);
                    if(!is_null($employee)){
                        $communitiesEmployee = $employee->communities()->get();
                        foreach ($communitiesEmployee as $commu){
                            $ids[] = $commu->id;
                        }
                    }
                    else{
                        return response()->json([
                            'data' => false,
                            'message' => trans('dashboard.messages.error')
                        ]);
                    }
                }
                $html = '';
                foreach ($office->communities as $community){
                    $html .= "<label>";
                    if (!is_null($request->employee_id) && in_array($community->id,$ids))
                        $html .= "<input id='check-commu' type='checkbox' name='communities[]' value='{$community->id}' checked>";
                    else
                        $html .= "<input id='check-commu' type='checkbox' name='communities[]' value='{$community->id}'>";
                    $html .= "{$community->name}</label>";
                }
                return response()->json(['data' => true, 'html' => $html]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }
}

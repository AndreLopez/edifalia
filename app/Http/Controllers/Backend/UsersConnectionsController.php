<?php

namespace Edifalia\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Edifalia\Http\Requests;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\Connection;
use Edifalia\Models\Access;

class UsersConnectionsController extends Controller
{
    public function getUsersOnline()
    {
        $connections = Connection::paginate(10);
        return view('backend.utilities.users_online.index')
            ->with('connections', $connections);
    }

    public function getIntranetAccess()
    {
        $accesses = Access::paginate(10);
        return view('backend.utilities.intranet_access.index')
            ->with('accesses', $accesses);
    }
}

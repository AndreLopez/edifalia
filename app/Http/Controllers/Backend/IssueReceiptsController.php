<?php

namespace Edifalia\Http\Controllers\Backend;

use Edifalia\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Edifalia\Repositories\IssueReceiptRepository;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Edifalia\Http\Requests\IssueReceipt\CreateEditRequest;

class IssueReceiptsController extends Controller
{
    protected $issueReceiptRepo;

    public function __construct(IssueReceiptRepository $issueReceiptRepo)
    {
        $this->issueReceiptRepo = $issueReceiptRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Gate::allows('admin')){
            $issueReceipts = $this->issueReceiptRepo->getAll();
        } else if(Gate::allows('employee')){
            $issueReceipts = $this->issueReceiptRepo->getEmployeeIssuedReceipts();
        }
        return view('backend.management.issue_receipts.index', compact('issueReceipts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $header = [
            'route' => 'issue-receipts.store',
            'method' => 'POST',
            'role' => 'form',
        ];
        return view('backend.management.issue_receipts.create_or_edit')
            ->with('header', $header)
            ->with('title', trans('dashboard.titles.add_issue_receipt'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEditRequest $request)
    {
        if($this->issueReceiptRepo->checkCommunities($request->get('communities'))){
            if ($this->issueReceiptRepo->existsReceiptDues($request->get('communities'))) {
                $issueReceipt = $this->issueReceiptRepo->saveNewWithUser($request->all());
                $this->issueReceiptRepo->saveCommunities($issueReceipt,
                    $request->get('communities'));
            } else {
                Session::flash('message', [
                    'alert' => 'warning',
                    'text' => trans('dashboard.messages.no_recent_receipt')
                ]);
                return redirect()->route('issue-receipts.index');
            }
        } else {
            return redirect()->route('issue-receipts.create');
        }

        $invoicingConcepts = array_filter($request->get('invoicingConcepts'));
        if(!empty($invoicingConcepts)){
            $this->issueReceiptRepo->saveInvoicingConcepts($issueReceipt,
                $invoicingConcepts);
        }

        $this->issueReceiptRepo->sendEmailsToOwners($issueReceipt);

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.created')
        ]);

        return redirect()->route('issue-receipts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $issueReceipt = $this->issueReceiptRepo->find($id);

        if (is_null($issueReceipt) || Gate::denies('has', $issueReceipt->user_id))
            return redirect()->route('issue-receipts.index');

        $idsToExclude = $this->issueReceiptRepo->invoicingConceptsToExclude($issueReceipt);

        return view('backend.management.issue_receipts.show')
            ->with('issueReceipt', $issueReceipt)
            ->with('idsToExclude', $idsToExclude);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /*$issueReceipt = $this->issueReceiptRepo->find($id);
        if (is_null($issueReceipt))
            return redirect()->route('issue-receipts.index');

        $header = [
            'route' => ['issue-receipts.update', $issueReceipt->id],
            'method' => 'PUT',
            'role' => 'form'
        ];

        return view('backend.management.issue_receipts.create_or_edit')
            ->with('header', $header)
            ->with('title', trans('dashboard.titles.edit_issue_receipt'))
            ->with('issueReceipt', $issueReceipt);*/
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateEditRequest $request, $id)
    {
        /*$issueReceipt = $this->issueReceiptRepo->find($id);
        if (is_null($issueReceipt))
            return redirect()->route('issue-receipts.index');

        if(Gate::allows('has', $issueReceipt->user_id)){
            if($this->issueReceiptRepo->checkCommunities($request->get('communities'))){
                $this->issueReceiptRepo->update($issueReceipt, $request->all());
                $this->issueReceiptRepo->updateCommunities($issueReceipt,
                    $request->get('communities'));
            } else {
                return redirect()->route('issue-receipts.edit', $issueReceipt->id);
            }

            if($request->has('invoicingConcepts')){
                $invoicingConcepts = array_filter($request->get('invoicingConcepts'));
                if(!empty($invoicingConcepts)){
                    $this->issueReceiptRepo->updateInvoicingConcepts($issueReceipt,
                        $invoicingConcepts);
                } else {
                    $this->issueReceiptRepo->detachInvoicingConcepts($issueReceipt);
                }
            }

            //$this->issueReceiptRepo->sendEmailsToOwners($issueReceipt);
        } else {
            Session::flash('message', [
                'alert' => 'warning',
                'text' => trans('dashboard.messages.not_edit_record')
            ]);
            return redirect()->route('issue-receipts.index');
        }

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.updated')
        ]);
        return redirect()->route('issue-receipts.edit', $issueReceipt->id);*/
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $issueReceipt = $this->issueReceiptRepo->find($id);
            if (!is_null($issueReceipt) && Gate::allows('has', $issueReceipt->user_id)){
                $this->issueReceiptRepo->delete($issueReceipt);
                $array = [
                    'data' => true,
                    'id' => $id,
                    'message' => trans('dashboard.messages.deleted'),
                    'undo' => trans('dashboard.buttons.undo')
                ];
            } else {
                $array = [
                    'data' => false,
                    'message' => trans('dashboard.messages.error')
                ];
            }
            return response()->json($array);
        }
    }

    public function postUndelete(Request $request, $id)
    {
        if($request->ajax()){
            $issueReceipt = $this->issueReceiptRepo->recordInTrash($id);
            if (!is_null($issueReceipt)){
                $this->issueReceiptRepo->restore($issueReceipt);
                $array = ['data' => true, 'id' => $id];
            } else {
               $array = [
                    'data' => false,
                    'message' => trans('dashboard.messages.error')
               ];
            }
            return response()->json($array);
        }
    }
}

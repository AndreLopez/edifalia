<?php

namespace Edifalia\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\Tag;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

class TagsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::paginate(10);
        return view('backend.management.tags.index')->with('tags', $tags);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $header = [
            'route' => 'tags.store',
            'method' => 'POST',
            'role' => 'form',
        ];
        return view('backend.management.tags.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.add_tag'))->with('new', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Validator::make($request->all(), [
            'name' => [
                'required',
                'max:200',
                'regex:/^[a-zñA-ZÑ0-9\s]+$/',
                'unique:tags']
        ])->validate();

        $tag = new Tag();
        $tag->fill($request->all());
        $tag->save();

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.created')
        ]);

        return redirect()->route('tags.edit', $tag->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tag = Tag::find($id);
        if (is_null($tag))
            return redirect()->route('tags.index');

        $header = [
            'route' => ['tags.update', $tag->id],
            'method' => 'PUT',
            'role' => 'form'
        ];

        return view('backend.management.tags.create_or_edit')->with('header', $header)
            ->with('title', trans('dashboard.titles.edit_tag'))
            ->with('tag', $tag)->with('edit', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tag = Tag::find($id);
        if (is_null($tag))
            return redirect()->route('tags.index');

        \Validator::make($request->all(), [
            'name' => [
                'required',
                'max:200',
                'regex:/^[a-zñA-ZÑ0-9\s]+$/',
                Rule::unique('tags', 'name')->ignore($tag->id)]
        ])->validate();

        $tag->fill($request->all());
        $tag->save();

        Session::flash('message', [
            'alert' => 'success',
            'text' => trans('dashboard.messages.updated')
        ]);
        return redirect()->route('tags.edit', $tag->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $tag = Tag::find($id);
            if (!is_null($tag)){
                $tag->delete();
                return response()->json([
                    'data' => true,
                    'id' => $id,
                    'message' => trans('dashboard.messages.deleted'),
                    'undo' => trans('dashboard.buttons.undo')
                ]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }

    public function postUndelete(Request $request, $id)
    {
        if($request->ajax()){
            $tag = Tag::onlyTrashed()->where('id', $id)->first();
            if (!is_null($tag)){
                $tag->restore();
                return response()->json(['data' => true, 'id' => $id]);
            }
            return response()->json([
                'data' => false,
                'message' => trans('dashboard.messages.error')
            ]);
        }
    }
}

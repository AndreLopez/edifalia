<?php

namespace Edifalia\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Edifalia\Http\Requests;
use Edifalia\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Illuminate\Database\Query\Builder;
// use PDF;
use Edifalia\Models\Pdf\EdifaliaPDF;

/**
 * Controller for reports
 * @author Máximo Sojo maxsojo13@gmail.com <maxtoan at atechnologies>
 */
class ReportController extends Controller
{
    /**
     * 
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	return view('backend.report.dossier.index');
	}

    /**
     * Dossier de Firmas
     */
	public function dossier()
    {
        return view('backend.report.dossier.index');
	}

    /**
     * Presentación de Reportes Dossier de Firmas
     */
	public function dossierViewReport(Request $request)
	{
		return view('backend.report.dossier.viewReport');
	}

    /**
     * 
     */
    public function dossierExportPdf(Request $request){
        $title = trans('dashboard.mnu_dossier_sign');
        $data = json_decode($request->get('data'));

        if (isset($data->title)) {
            $title = $data->title;
        }

        EdifaliaPDF::SetTitle($title);
        EdifaliaPDF::Header();
        EdifaliaPDF::Footer();
        EdifaliaPDF::AddPage();
        // EdifaliaPDF::Write(0, trans('dashboard.mnu_dossier_sign'));
        EdifaliaPDF::SetMargins(7, 18, 7);
        EdifaliaPDF::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
        $image = public_path().'/static/images/logo-horizontal.png';
        EdifaliaPDF::Image($image,
            $x = '10',
            $y = '7',
            $w = 30,
            $h = 0,
            $type = 'PNG',
            $link = '',
            $align = 'N',
            $resize = false,
            $dpi = 300,
            $palign = 'L',
            $ismask = false,
            $imgmask = false,
            $border = 0,
            $fitbox = false,
            $hidden = false,
            $fitonpage = false,
            $alt = false,
            $altimgs = array());
        $html = view('backend.report.dossier.pdf')
                ->with(['data' => $data]) ;
        EdifaliaPDF::writeHTMLCell(120, 0, '', '', $html, '', 1, 0, true, 'R', true);
        EdifaliaPDF::Output('dossier.pdf', 'D');
    }

    /**
     * 
     */
    public function dossierExportExcel(Request $request){
        $title = trans('dashboard.mnu_dossier_sign');
        $json = json_decode($request->get('data'));
        if (isset($json->title)) {
            $title = $json->title;
        }

        $data = [
            'title' => $title,
            'data' => $json
        ];

        \Maatwebsite\Excel\Facades\Excel::create(trans('dashboard.mnu_dossier_sign'), function($excel) use ($data) {
            $excel->sheet(trans('dashboard.mnu_dossier_sign'), function($sheet) use ($data) {
                $sheet->setPageMargin(0.35);                
                $sheet->loadView('backend.report.dossier.excel', $data);                
            });
        })->export('xls');
    }

    /**
     * Juntas
     */
    public function meeting($format = null)
    {
        if($format == 'json') {
            $result = \DB::table('meetings')
                ->select('id', 'title', 'date_in', 'place')
                ->where('meetings.deleted_at', '=', null)
                ->get();
            return response()->json($result);
        }
        return view('backend.report.meeting.index');
    }

    /**
     * 
     */
    public function meetingCreate(Request $request)
    {
        $typeMeeting = \Edifalia\Models\Meeting::getType();
        return view('backend.report.meeting.create')
                ->with(['meetingType' => $typeMeeting]);
    }

    public function meetingRemove(Request $request){
        $meeting = \Edifalia\Models\Meeting::find($request->get('id'));
        if ($meeting) {
            $meeting->delete();
            return response()->json([
                'data' => true,
                'message' => trans('dashboard.messages.deleted')                
            ]);
        }
        return response()->json([
            'data' => false,
            'message' => trans('dashboard.messages.error')
        ]);
    }

    /**
     * 
     */
    public function meetingLoad(Request $request)
    {
        $data = json_decode($request->getContent());
        
        if ($data) {
            $meeting = new \Edifalia\Models\Meeting();
            $meeting->type = $data->meeting->typeMeeting;
            $meeting->title = $data->meeting->title;
            $meeting->date_in = $data->meeting->date_in;
            $meeting->place = $data->meeting->place;
            $meeting->order_day = $data->meeting->order_day;
            $meeting->minute = $data->meeting->minute;
            $meeting->time_one = $data->meeting->time_one;
            $meeting->time_two = $data->meeting->time_two;
            $meeting->time_end = $data->meeting->time_end;
            $meeting->community_id = $data->community->id;
            $meeting->save();

            foreach ($data->assistant as $key => $value) {
                $meetingAssistant = new \Edifalia\Models\MeetingAssistant();
                $meetingAssistant->presented = $value->action;
                $meetingAssistant->meeting_id = $meeting->id;
                $meetingAssistant->property_id = $value->id;
                $meetingAssistant->save();                
            }
            
            foreach ($data->voting->title as $key => $value) {            
                $meetingVoting = new \Edifalia\Models\MeetingVoting();
                $meetingVoting->description = $data->meeting->voting;
                $meetingVoting->agreement = $value;                
                $meetingVoting->meeting_id = $meeting->id;
                $meetingVoting->save();
                foreach ($data->voting->user->$key as $ky => $valUsers) {
                     $idUser = explode('_', $ky);
                     $meetingVotingDetail = new \Edifalia\Models\MeetingVotingDetail();
                     $meetingVotingDetail->voting = $valUsers;
                     $meetingVotingDetail->voting_id = $meetingVoting->id;
                     $meetingVotingDetail->property_id = $idUser[1];
                     $meetingVotingDetail->save();
                }
            }
            
            $url = route('meeting.export-pdf', ['id' => $meeting->id]);
            $message = ['message' => 'Junta Creada Exitosamente, puede exportarla a través del siguiente enlace. ', 'url' => $url];
        }else{
            $message = ['message' => "Error en Carga de Data."];
        }
        return response()->json($message);
    }

    /**
     * 
     */
    public function meetingShow($id)
    {   
        $votingDescription = "";
        $votingId = 0;
        $array = [];

        $typeMeeting = \Edifalia\Models\Meeting::getType();
        $meeting = \Edifalia\Models\Meeting::find($id);
        $meetingAssistant = \Edifalia\Models\MeetingAssistant::where('meeting_id', $meeting->id)->get();        
        $meetingVoting = \Edifalia\Models\MeetingVoting::where('meeting_id', $meeting->id)->get();        
        
        foreach ($meetingVoting as $key => $dataVoting) {
            $votingDescription = $dataVoting->description;
            $votingId = $dataVoting->id;
            $array[$dataVoting->id]['agreement'] = $dataVoting->agreement;
            $meetingVotingDetail = \Edifalia\Models\MeetingVotingDetail::where('voting_id', $votingId)->get();        
            foreach ($meetingVotingDetail as $key => $dataVotingDetail) {
                $array[$dataVoting->id]['data'][] = [
                    'voting' => $dataVotingDetail->voting,
                    'person' => $dataVotingDetail->property->person->FullName
                ];
            }            
        }
        
        return view('backend.report.meeting.show')
            ->with('meeting', $meeting)
            ->with('meetingAssistant', $meetingAssistant)
            ->with('typeMeeting', $typeMeeting)
            ->with('voting', $meetingVoting)
            ->with('votingDescription', $votingDescription)
            ->with('meetingVotingDetail', $array);
    }

    /**
     * 
     */
    public function meetingExportPdf($id){
        $typeMeeting = \Edifalia\Models\Meeting::getType();
        $meeting = \Edifalia\Models\Meeting::find($id);
        $meetingAssistant = \Edifalia\Models\MeetingAssistant::where('meeting_id', $meeting->id)->get();
        $meetingVoting = \Edifalia\Models\MeetingVoting::where('meeting_id', $meeting->id)->get();
        foreach ($meetingVoting as $key => $dataVoting) {
            $votingDescription = $dataVoting->description;
            $votingId = $dataVoting->id;
            $array[$dataVoting->id]['agreement'] = $dataVoting->agreement;
            $meetingVotingDetail = \Edifalia\Models\MeetingVotingDetail::where('voting_id', $votingId)->get();        
            foreach ($meetingVotingDetail as $key => $dataVotingDetail) {
                $array[$dataVoting->id]['data'][] = [
                    'voting' => $dataVotingDetail->voting,
                    'person' => $dataVotingDetail->property->person->FullName
                ];
            }            
        }

        EdifaliaPDF::SetTitle(trans('dashboard.mnu_meetings'));
        EdifaliaPDF::Header();
        EdifaliaPDF::Footer();
        EdifaliaPDF::AddPage();
        // EdifaliaPDF::Write(0, trans('dashboard.mnu_dossier_sign'));
        EdifaliaPDF::SetMargins(18, 24, 18);
        EdifaliaPDF::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
        $image = public_path().'/static/images/logo-horizontal.png';
        EdifaliaPDF::Image($image,
            $x = '10',
            $y = '7',
            $w = 30,
            $h = 0,
            $type = 'PNG',
            $link = '',
            $align = 'N',
            $resize = false,
            $dpi = 300,
            $palign = 'L',
            $ismask = false,
            $imgmask = false,
            $border = 0,
            $fitbox = false,
            $hidden = false,
            $fitonpage = false,
            $alt = false,
            $altimgs = array());
        $html = view('backend.report.meeting.pdf')
                ->with('meeting', $meeting)
                ->with('meetingAssistant', $meetingAssistant)
                ->with('typeMeeting', $typeMeeting)
                ->with('voting', $meetingVoting)
                ->with('votingDescription', $votingDescription)
                ->with('meetingVotingDetail', $array);
        EdifaliaPDF::writeHTMLCell(120, 0, '', '', $html, '', 1, 0, true, 'R', true);
        EdifaliaPDF::Output(trans('dashboard.mnu_meetings').'.pdf', 'D');
    }
    
    /**
     * 
     */
    public function meetingExportExcel($id){
        $typeMeeting = \Edifalia\Models\Meeting::getType();
        $meeting = \Edifalia\Models\Meeting::find($id);
        $meetingAssistant = \Edifalia\Models\MeetingAssistant::where('meeting_id', $meeting->id)->get();
        $meetingVoting = \Edifalia\Models\MeetingVoting::where('meeting_id', $meeting->id)->get();
        foreach ($meetingVoting as $key => $dataVoting) {
            $votingDescription = $dataVoting->description;
            $votingId = $dataVoting->id;
            $array[$dataVoting->id]['agreement'] = $dataVoting->agreement;
            $meetingVotingDetail = \Edifalia\Models\MeetingVotingDetail::where('voting_id', $votingId)->get();        
            foreach ($meetingVotingDetail as $key => $dataVotingDetail) {
                $array[$dataVoting->id]['data'][] = [
                    'voting' => $dataVotingDetail->voting,
                    'person' => $dataVotingDetail->property->person->FullName
                ];
            }            
        }

        $data = [
            'typeMeeting' => $typeMeeting,
            'meeting' => $meeting,
            'meetingAssistant' => $meetingAssistant,
            'voting' => $meetingVoting,
            'votingDescription' => $votingDescription,
            'meetingVotingDetail' => $array
        ];

        \Maatwebsite\Excel\Facades\Excel::create(trans('dashboard.mnu_meetings'), function($excel) use ($data) {
            $excel->sheet(trans('dashboard.mnu_meetings'), function($sheet) use ($data) {
                $sheet->setPageMargin(0.35);                
                $sheet->loadView('backend.report.meeting.excel', $data);                
            });
        })->export('xls');
    }

    /**
     * 
     */
    public function meetingAssistant()
    {
        return view('backend.report.meeting.assistant');        
    }

    /**
     * Liquidación de Consumos
     */
    public function liquidate()
    {
        return view('backend.report.liquidate.index');
    }

    /**
     * 
     */
    public function liquidateExportPdf(Request $request){
        $data = json_decode($request->get('data'));
        $title = trans('dashboard.mnu_liquidate_cons');
        $totalPrevious = $totalCurrent = $totalConsumption = 0;
        $arrayTotales = [];

        if (isset($data->reading->title)) {
            $title = $data->reading->title;
        }
        
        foreach ($data->readings as $key => $value) {
            $totalPrevious = $totalPrevious + $value->previous;
            $totalCurrent = $totalCurrent + $value->current;
            $totalConsumption = $totalConsumption + $value->consumption;
        }
        
        $arrayTotales = [
            'totalPrevious' => number_format($totalPrevious, 2, ',', ' '),
            'totalCurrent' => number_format($totalCurrent, 2, ',', ' '),
            'totalConsumption' => number_format($totalConsumption, 2, ',', ' ')
        ];

        EdifaliaPDF::SetTitle($title);
        EdifaliaPDF::Header();
        EdifaliaPDF::Footer();
        EdifaliaPDF::AddPage();
        // EdifaliaPDF::Write(0, trans('dashboard.mnu_dossier_sign'));
        EdifaliaPDF::SetMargins(18, 24, 18);
        EdifaliaPDF::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
        $image = public_path().'/static/images/logo-horizontal.png';
        EdifaliaPDF::Image($image,
            $x = '10',
            $y = '7',
            $w = 30,
            $h = 0,
            $type = 'PNG',
            $link = '',
            $align = 'N',
            $resize = false,
            $dpi = 300,
            $palign = 'L',
            $ismask = false,
            $imgmask = false,
            $border = 0,
            $fitbox = false,
            $hidden = false,
            $fitonpage = false,
            $alt = false,
            $altimgs = array());
        $html = view('backend.report.liquidate.pdf')
                ->with('community', $data->community)
                ->with('reading', $data->reading)
                ->with('dataReading', $data->dataReading)
                ->with('readings', $data->readings)
                ->with('consumption', $data->consumption)
                ->with('arrayTotales', $arrayTotales);
        EdifaliaPDF::writeHTMLCell(120, 0, '', '', $html, '', 1, 0, true, 'R', true);
        EdifaliaPDF::Output(trans('dashboard.mnu_liquidate_cons').'.pdf', 'D');
    }

    /**
     *     
     */
    public function liquidateExportExcel(Request $request){
        $array = [];

        $data = json_decode($request->get('data'));
        $title = trans('dashboard.mnu_liquidate_cons');
        $totalPrevious = $totalCurrent = $totalConsumption = 0;
        $arrayTotales = [];

        if (isset($data->reading->title)) {
            $title = $data->reading->title;
        }
        
        foreach ($data->readings as $key => $value) {
            $totalPrevious = $totalPrevious + $value->previous;
            $totalCurrent = $totalCurrent + $value->current;
            $totalConsumption = $totalConsumption + $value->consumption;
        }
        
        $array = [
            'title' => $title,
            'dataReading' => $data->dataReading,
            'readings' => $data->readings,
            'arrayTotales' => [
                'totalPrevious' => number_format($totalPrevious, 2, ',', ' '),
                'totalCurrent' => number_format($totalCurrent, 2, ',', ' '),
                'totalConsumption' => number_format($totalConsumption, 2, ',', ' ')
            ]
        ];

        \Maatwebsite\Excel\Facades\Excel::create(trans('dashboard.mnu_liquidate_cons'), function($excel) use ($array) {
            $excel->sheet(trans('dashboard.mnu_liquidate_cons'), function($sheet) use ($array) {
                $sheet->setPageMargin(0.35);                
                $sheet->loadView('backend.report.liquidate.excel', $array);                
            });
        })->export('xls');
    }

    /**
     * Liquidaciones
     */
    public function liquidation()
    {
        return view('backend.report.liquidation.index');
    }

    /**
     * 
     */
    public function liquidationExportPdf($id)
    {
        $totalGeneral = $totalAmount = 0;
        $budgetProperty = \DB::table('budget_property')
            ->join('budgets', 'budget_property.budget_id', '=', 'budgets.id')
            ->join('properties', 'properties.id', '=', 'budget_property.property_id')
            ->select('budget_property.*', 'properties.*', 'budgets.*')
            ->where('budgets.community_id', '=', $id)
            ->get();

        foreach ($budgetProperty as $key => $value) {
            $totalGeneral = $totalGeneral + $value->general;
            $totalAmount = $totalAmount + $value->amount;
        }
        $totalGeneral = number_format($totalGeneral, 2, ',', ' ');
        $totalAmount = number_format($totalAmount, 2, ',', ' ');
        
        EdifaliaPDF::SetTitle(trans('dashboard.mnu_liquidations'));
        EdifaliaPDF::Header();
        EdifaliaPDF::Footer();
        EdifaliaPDF::AddPage();
        EdifaliaPDF::SetMargins(18, 24, 18);
        EdifaliaPDF::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
        $image = public_path().'/static/images/logo-horizontal.png';
        EdifaliaPDF::Image($image,
            $x = '10',
            $y = '7',
            $w = 30,
            $h = 0,
            $type = 'PNG',
            $link = '',
            $align = 'N',
            $resize = false,
            $dpi = 300,
            $palign = 'L',
            $ismask = false,
            $imgmask = false,
            $border = 0,
            $fitbox = false,
            $hidden = false,
            $fitonpage = false,
            $alt = false,
            $altimgs = array());
        $html = view('backend.report.liquidation.pdf')
            ->with('properties', $budgetProperty)
            ->with('totalGeneral', $totalGeneral)
            ->with('totalAmount', $totalAmount);
        EdifaliaPDF::writeHTMLCell(120, 0, '', '', $html, '', 1, 0, true, 'R', true);
        EdifaliaPDF::Output('liquidation.pdf', 'D');
    }
    
    /**
     * Comparación de Presupuestos y Gastos
     */
    public function spending()
    {
        return view('backend.report.spending.index');
    }

    /**
     * 
     */
    public function spendingExportPdf(Request $request)
    {
        $amountTotal = $totalGastos = $dif = 0;
        $json = json_decode($request->get('data'));
        
        $budgetProperty = \DB::table('budget_property')
            ->select('budget_property.*')
            ->where('budget_property.budget_id', '=', $json->budget->id)
            ->get();

        $budget = \DB::table('budgets')
            ->select('budgets.*')
            ->where('budgets.id', '=', $json->budget->id)
            ->get();

        $receipt = \DB::table('receipt_dues')
            ->select('receipt_dues.*')
            ->where('receipt_dues.invoicing_concept_id', '=', $budget[0]->invoicing_concept_id)
            ->where('receipt_dues.community_id', '=', $budget[0]->community_id)
            ->get();

        foreach ($receipt as $key => $value) {
            $totalGastos = $value->total_amount;
        }
        
        foreach ($budgetProperty as $key => $value) {
            $amountTotal = $amountTotal + $value->general;
        }

        $dif = $amountTotal - $totalGastos;

        EdifaliaPDF::SetTitle(trans('dashboard.mnu_compare_budget_spending'));
        EdifaliaPDF::Header();
        EdifaliaPDF::Footer();
        EdifaliaPDF::AddPage();
        EdifaliaPDF::SetMargins(18, 24, 18);
        EdifaliaPDF::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
        $image = public_path().'/static/images/logo-horizontal.png';
        EdifaliaPDF::Image($image,
            $x = '10',
            $y = '7',
            $w = 30,
            $h = 0,
            $type = 'PNG',
            $link = '',
            $align = 'N',
            $resize = false,
            $dpi = 300,
            $palign = 'L',
            $ismask = false,
            $imgmask = false,
            $border = 0,
            $fitbox = false,
            $hidden = false,
            $fitonpage = false,
            $alt = false,
            $altimgs = array());
        $html = view('backend.report.spending.pdf')
                    ->with('community', $json->community)
                    ->with('budget', $json->budget)
                    ->with('amountTotal', $amountTotal)
                    ->with('receipt', $receipt)
                    ->with('dif', $dif)
                    ->with('totalGastos', $totalGastos);
        EdifaliaPDF::writeHTMLCell(120, 0, '', '', $html, '', 1, 0, true, 'R', true);
        EdifaliaPDF::Output('spending.pdf', 'D');
    }

    public function controlIndex($format = null){
        if($format == 'json') {
            $result = \DB::table('meeting_controls')
                ->join('persons', 'persons.id', '=', 'meeting_controls.person_id')
                ->select('meeting_controls.*', 'persons.*')            
                ->where('meeting_controls.deleted_at', '=', null)
                ->get();                
            return response()->json($result);
        }
        return view('backend.report.meeting.control.index');
    }

    public function controlCreate(Request $request){
         $meetingControl = new \Edifalia\Models\MeetingControl();

        if ($request->isMethod('POST')) {
            $data = json_decode($request->getContent());
            $meetingControl->title = $data->title;
            $meetingControl->person_id = $data->person;
            $meetingControl->save();
            return response()->json(['message' => 'Responsable cargado exitosamente.']);
        }

        $person = \Edifalia\Models\Backend\Person::all();
        return view('backend.report.meeting.control.form')->with('person', $person);
    }

    public function controlRemove(Request $request){
        $meetingControl = \Edifalia\Models\MeetingControl::find($request->get('id'));
        if ($meetingControl) {
            $meetingControl->delete();
            return response()->json([
                'data' => true,
                'message' => trans('dashboard.messages.deleted')                
            ]);
        }
        return response()->json([
            'data' => false,
            'message' => trans('dashboard.messages.error')
        ]);
    }
}
<?php

namespace Edifalia\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\Backend\Community;
use Edifalia\Models\Common\Property;

class CoefficientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'breadcrumb_title' => trans('properties.coefficient_management_title'),
            'breadcrumb_subtitle' => trans('properties.lst_coefficient_title'),
            'panel_title' => trans('properties.lst_coefficient_title'),
            'theads' => [
                ['align' => 'text-center', 'name' => 'forms.properties_label'],
                ['align' => 'text-center', 'name' => 'forms.community_select_label'],
            ],
            'tbody' => []
        ];
        $communities = Community::all();
        foreach ($communities as $community) {
            $id = $community->id;
            $cols = "<td class='col-xs-1 col-sm-1 col-md-1 col-lg-1 details-control'></td>";
            $cols .= "<td class='col-xs-9 col-sm-9 col-md-9 col-lg-9'>" . $community->name . "</td>";
            $body = [
                'cols' => $cols, 'url_edit' => route('coefficient.edit', $id),
                'url_delete' => 'coefficient.index', 'record_id' => $id, 'no_delete' => true
            ];
            $properties = Property::where('community_id', $id)->get();
            if ($properties) {
                $p = [];
                foreach ($properties as $property) {
                    $p[$property->property_number] = $property->coefficient . " %";
                }
                $body = array_merge($body, ['data_child' => json_encode($p)]);
            }
            array_push($data['tbody'], $body);
        }
        return view('layouts.list-records')->with('data', $data);
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $community = Community::find($id);
        $data = [
            'breadcrumb_title' => trans('properties.coefficient_management_title'),
            'breadcrumb_subtitle' => trans('properties.coefficient_add_title'),
            'breadcrumb_link' => route('coefficient.index'),
            'panel_title' => trans('properties.new_coefficient_title'),
            'btn_record_submit' => trans('common.btn_save_text'),
        ];
        $header = [
            'route' => ['coefficient.update', $id], 
            'method' => 'PUT', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];
        $co_total = 0;
        $properties = Property::where('community_id', $id)->get();
        foreach ($properties as $property) {
            $co_total += $property->coefficient;
        }
        return view('layouts.forms')->with('data', $data)->with('new', true)->with('header', $header)
                                    ->with('community', $community)->with('coefficient_total', $co_total)
                                    ->with('properties', $properties)
                                    ->with('fields_template', 'common.coefficient-fields');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = ['coefficent_total' => 'required|numeric|min:100|max:100'];
        foreach ($request->get('coefficient_propiedad') as $key => $value) {
            if (!empty($value)) {
                $rules['coefficient_propiedad.' . $key] = 'required|numeric|min:0|max:100';
            }
        }
        $this->validate($request, $rules);

        foreach ($request->get('coefficient_propiedad') as $key => $value) {
            $property = Property::find($key);
            $property->coefficient = $request->input('coefficient_propiedad.' . $key);
            $property->save();
        }
        $request->session()->flash('message', [
            'alert' => 'success', 'text' => trans('dashboard.messages.updated')
        ]);
        return redirect()->route('coefficient.index');
    }

}

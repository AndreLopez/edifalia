<?php

namespace Edifalia\Http\Controllers\Backend;

use Illuminate\Http\Request;

use Edifalia\Http\Requests;
use Edifalia\Http\Controllers\Controller;
use Edifalia\Models\Common\PostalCode;
use Edifalia\Models\Common\Country;

class PostalCodesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'breadcrumb_title' => trans('common.postal_code_management_title'),
            'breadcrumb_subtitle' => trans('common.postal_code_list_title'),
            'panel_title' => trans('common.lst_postal_code_title'),
            'url_new' => route('postal-code.create'),
            'theads' => [
                ['align' => 'text-center', 'name' => 'common.lst_col_code'],
                ['align' => 'text-center', 'name' => 'common.lst_col_country'],
                ['align' => 'text-center', 'name' => 'common.lst_col_province'],
            ],
            'tbody' => []
        ];
        $postal_codes = PostalCode::all();
        foreach ($postal_codes as $postal_code) {
            $id = $postal_code->id;
            $cols  = "<td class='col-xs-2 col-sm-2 col-md-2 col-lg-2'>" . $postal_code->code . "</td>";
            $cols .= "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . $postal_code->country->name . "</td>";
            $cols .= "<td class='col-xs-4 col-sm-4 col-md-4 col-lg-4'>" . $postal_code->province . "</td>";
            array_push($data['tbody'], [
                'cols' => $cols, 'url_edit' => route('postal-code.edit', $id), 'url_show' => route('postal-code.show', $id),
                'url_delete' => 'postal-code.destroy', 'record_id' => $id
            ]);
        }
        
        return view('layouts.list-records')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'breadcrumb_title' => trans('common.postal_code_management_title'),
            'breadcrumb_subtitle' => trans('common.postal_code_add_title'),
            'breadcrumb_link' => route('postal-code.index'),
            'panel_title' => trans('common.new_postal_code_title'),
            'btn_record_submit' => trans('common.btn_save_text'),
        ];
        $header = [
            'route' => 'postal-code.store', 
            'method' => 'POST', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];
        $countries = Country::all();
        $country_options = [];
        foreach ($countries as $country) {
            $country_options[$country->id] = $country->name;
        }
        return view('layouts.forms')->with('data', $data)->with('new', true)->with('header', $header)
                                    ->with('countries', $country_options)
                                    ->with('fields_template', 'common.postal-code-fields');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|unique:postal_codes,code|max:45',
            'country_id' => 'required',
            'town' => 'required|max:45',
            'province' => 'required|max:45',
            'region' => 'required|max:45',
        ]);

        $postal_code = new PostalCode();
        $postal_code->fill($request->all());
        $postal_code->save();

        $request->session()->flash('message', [
            'alert' => 'success', 'text' => trans('dashboard.messages.created')
        ]);

        return view('common.panel-info');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $postal_code = PostalCode::findOrfail($id);
        $data = [
            'breadcrumb_title' => trans('common.postal_code_management_title'),
            'breadcrumb_subtitle' => trans('common.postal_code_show_title'),
            'breadcrumb_link' => route('postal-code.index'),
            'panel_title' => trans('common.show_postal_code_title'),
            'btn_record_submit' => trans('common.btn_save_text'),
        ];
        $header = [
            'route' => 'postal-code.store', 
            'method' => 'POST', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];
        return view('layouts.show')->with('data', $data)->with('header', $header)->with('postal_code', $postal_code)
                                   ->with('show_template', 'common.postal-code-show');;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $postal_code = PostalCode::findOrfail($id);
        $data = [
            'breadcrumb_title' => trans('common.postal_code_management_title'),
            'breadcrumb_subtitle' => trans('common.postal_code_upd_title'),
            'breadcrumb_link' => route('postal-code.index'),
            'panel_title' => trans('common.edit_postal_code_title'),
            'btn_record_submit' => trans('common.edit_record_submit'),
        ];
        $header = [
            'route' => ['postal-code.update', $postal_code->id], 
            'method' => 'PUT', 
            'role' => 'form',
            'class' => 'form-horizontal',
        ];
        $countries = Country::all();
        $country_options = [];
        foreach ($countries as $country) {
            $country_options[$country->id] = $country->name;
        }
        return view('layouts.forms')->with('data', $data)->with('edit', true)->with('header', $header)
                                    ->with('model', $postal_code)->with('countries', $country_options)
                                    ->with('fields_template', 'common.postal-code-fields');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'code' => 'required|max:45|unique:postal_codes,code,' . $id,
            'country_id' => 'required',
            'town' => 'required|max:45',
            'province' => 'required|max:45',
            'region' => 'required|max:45',
        ]);

        $postal_code = PostalCode::find($id);
        if (!is_null($postal_code)) {
            $postal_code->fill($request->all());
            $postal_code->save();
            $request->session()->flash('message', [
                'alert' => 'success', 'text' => trans('dashboard.messages.updated')
            ]);
        }
        return redirect()->route('postal-code.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $postal_code = PostalCode::find($id);
            if (!is_null($postal_code)) {
                $postal_code->delete();
                $request->session()->flash('message', [
                    'alert' => 'success', 'text' => trans('dashboard.messages.deleted')
                ]);
                return response()->json(['result' => true]);
            }
            return response()->json(['result' => false]);
        }
        return redirect()->route('postal-code.index');
    }


    public function getData(Request $request, $id)
    {
        $postal_code = PostalCode::find($id);
        if (!is_null($postal_code)) {
            return response()->json([
                'result' => true, 'town' => $postal_code->town, 'province' => $postal_code->province,
                'region' => $postal_code->region
            ]);
        }
        else {
            return response()->json(['result' => false, 'message' => trans('')]);
        }
    }
}

<?php

namespace Edifalia\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContractRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [

        'reference' => 'required|max:255', 
        'description' => 'requiered', 
        'jan' => 'nullable', 
        'feb' => 'nullable',
        'mar' => 'nullable', 
        'apr' => 'nullable', 
        'may' => 'nullable', 
        'jun' => 'nullable', 
        'jul' => 'nullable',
        'aug' => 'nullable', 
        'sep' => 'nullable',
        'oct' => 'nullable', 
        'nov' => 'nullable', 
        'dec' => 'nullable', 
        'amount' => 'required',
        'minimum' => 'required', 
        'maximum' => 'required', 
        'contract_file' => 'nullable', 
        'duration' => 'required|max:10',
        'duration_type' => 'required|max:1',
        'observations' => 'required',
        'community_id' => 'required|integer|exists:communities,id', 
        'supplier_id' => 'required|integer|exists:suppliers,id', 
        'payment_method_id' => 'required|integer|exists:payment_methods,id',

        ];

        /** Reglas de validación para los campos de las cuentas bancarias agregadas dinámicamente */
        foreach ($this->request->get('bank_account_accounting_code') as $key => $value) {
            if (!empty($value)) {
                $rules['bank_account_accounting_code.' . $key] = 'required|max:15';
                $rules['bank_account_iban.' . $key] = 'required|size:4';
                $rules['bank_account_code_a.' . $key] = 'required|size:4';
                $rules['bank_account_code_b.' . $key] = 'required|size:4';
                $rules['bank_account_code_c.' . $key] = 'required|size:4';
                $rules['bank_account_code_d.' . $key] = 'required|size:4';
                $rules['bank_account_code_e.' . $key] = 'required|size:4';
                $rules['bank_account_sufix.' . $key] = 'required|size:4';
                $rules['bank_account_bic.' . $key] = 'required|size:4';
            }
        }


   /** Reglas de validación para los campos de las cuentas contables agregadas dinámicamente */
        foreach ($this->request->get('accounting_account_code') as $key => $value) {
            if (!empty($value)) {
                $rules['accounting_account_code.' . $key] = 'required|max:15|unique:accounting_accounts,code';
                $rules['accounting_account_title.' . $key] = 'required|max:45';
            }
        }

        return $rules;
    }
}

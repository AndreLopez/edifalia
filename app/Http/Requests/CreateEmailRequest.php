<?php

namespace Edifalia\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateEmailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
          'priority' => 'required',
          'body' => 'required',
          'subject' => 'required|min:1|max:50',
          'from' => 'integer|exists:users,id'
        ];

        /*foreach ($this->request->get('community_ids') as $key => $value) {
            $rules['community_ids.' . $key] = 'integer|exists:communities,id';
        }

        foreach ($this->request->get('users_id') as $key => $value) {
            $rules['users_id.' . $key] = 'integer|exists:users,id';
        }*/

        return $rules;
    }
}

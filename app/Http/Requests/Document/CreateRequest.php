<?php

namespace Edifalia\Http\Requests\Document;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'community_id' => 'required|numeric|exists:communities,id',
            'document' => 'required|mimes:pdf,doc,docx,xls,xlsx,jpg,jpeg,png|max:2048',
            'title' => 'required|max:200',
            'publication_date' => 'required|date',
            'page_id' => 'required|numeric|exists:pages,id',
            'tag_id' => 'required|numeric|exists:tags,id',
            'owners.*' => 'numeric|exists:persons,id',
        ];
    }
}

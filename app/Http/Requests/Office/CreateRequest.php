<?php

namespace Edifalia\Http\Requests\Office;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'identifier' => 'required|unique:offices',
            'name' => 'required|max:200',
            'address' => 'required',
            'postal_code_id' => 'required|exists:postal_codes,id',
            'phone1' => 'required|max:20',
            'phone2' => 'max:20',
            'fax' => 'max:20',
            'email' => 'email',
            'communities.*' => 'required|numeric|exists:communities,id',
        ];
    }
}

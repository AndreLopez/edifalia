<?php

namespace Edifalia\Http\Requests\Charges;

use Illuminate\Foundation\Http\FormRequest;

class CreateEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'community_id' => 'required|numeric|exists:communities,id',
            'issue_receipt_id' => 'required|numeric|exists:issue_receipts,id',
            'property_id' => 'required|numeric|exists:properties,id',
            'pending' => 'required|numeric',
            'charge_type' => 'required|integer',
        ];
    }
}

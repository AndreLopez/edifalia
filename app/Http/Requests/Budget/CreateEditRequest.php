<?php

namespace Edifalia\Http\Requests\Budget;

use Illuminate\Foundation\Http\FormRequest;

class CreateEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'community_id' => 'required|exists:communities,id',
            'invoicing_concept_id' => 'required|exists:invoicing_concepts,id',
            'title' => 'required|max:200',
            'periodicity' => 'required|numeric|in:1,2,3,4',
            'duration' => 'required|numeric',
            'rounding' => 'required|numeric|digits_between:1,7',
            'month' => 'required|numeric|digits_between:1,12',
            'year' => 'required|numeric|min:2017',
            'general.*' => 'numeric',
        ];
    }
}

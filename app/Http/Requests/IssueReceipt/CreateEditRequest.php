<?php

namespace Edifalia\Http\Requests\IssueReceipt;

use Illuminate\Foundation\Http\FormRequest;

class CreateEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required|date',
            'message' => 'required|max:200',
            'issued' => 'boolean',
            'communities.*' => 'required|numeric|exists:communities,id',
            'invoicingConcepts.*' => 'numeric|exists:invoicing_concepts,id',
        ];
    }
}

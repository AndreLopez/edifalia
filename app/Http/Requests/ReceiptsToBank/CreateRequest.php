<?php

namespace Edifalia\Http\Requests\ReceiptsToBank;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file_format' => 'required|numeric|in:1,2,3',
            'state' => 'required|numeric|in:1,2',
            'date_sent_to_bank' => 'required|date',
            'charge_date' => 'required|date',
            'bank_request' => 'boolean',
            'payment_date' => 'date',
            'communities.*' => 'required|numeric|exists:communities,id',
            'presenters.*' => '',
        ];
    }
}

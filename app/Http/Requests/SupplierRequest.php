<?php

namespace Edifalia\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SupplierRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'cif' => 'required|max:15|unique:suppliers,cif',
            'business_name' => 'required|max:45',
            'tradename' => 'required|max:45', 
            'address' => 'required',
            'notes' => 'nullable',
            'contact_name' => 'required',
            'virtual_office_access' => 'nullable', 
        'update_data_from_virtual_office' => 'nullable', 
        'postal_code_id' => 'required|integer|exists:postal_codes,id',
        ];

        /** Reglas de validación para los campos de las cuentas bancarias agregadas dinámicamente */
        foreach ($this->request->get('bank_account_accounting_code') as $key => $value) {
            if (!empty($value)) {
                $rules['bank_account_accounting_code.' . $key] = 'required|max:15';
                $rules['bank_account_iban.' . $key] = 'required|size:4';
                $rules['bank_account_code_a.' . $key] = 'required|size:4';
                $rules['bank_account_code_b.' . $key] = 'required|size:4';
                $rules['bank_account_code_c.' . $key] = 'required|size:4';
                $rules['bank_account_code_d.' . $key] = 'required|size:4';
                $rules['bank_account_code_e.' . $key] = 'required|size:4';
                $rules['bank_account_sufix.' . $key] = 'required|size:4';
                $rules['bank_account_bic.' . $key] = 'required|size:4';
            }
        }

       

   

        return $rules;
    }
}

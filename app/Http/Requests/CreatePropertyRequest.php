<?php

namespace Edifalia\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreatePropertyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'building_number' => 'required|min:1|max:4', 
            'property_number' => 'required|min:1|max:6', 
            'registered_property' => 'min:1|max:45', 
            'property_type_id' => 'required|exists:property_types,id',
            'status' => 'required|boolean',
            'coefficient' => 'numeric',
            'group_properties' => 'required|boolean',
            'group_concepts' => 'required|boolean',
            'person_id' => 'required|integer|exists:persons,id',
            'payment_method_id' => 'required|integer|exists:payment_methods,id',
            'community_id' => 'required|integer|exists:communities,id'
        ];

        
        foreach ($this->request->get('accounting_account_code') as $key => $value) {
            $rules['accounting_account_code.' . $key] = 'required|max:15|unique:accounting_accounts,code';
            $rules['accounting_account_title.' . $key] = 'required|max:45';
        }
        
        foreach ($this->request->get('bank_account_accounting_code') as $key => $value) {
            $rules['bank_account_accounting_code.' . $key] = 'required|max:15';
            $rules['bank_account_iban.' . $key] = '
                    required|size:4|unique:bank_accounts,iban';
            $rules['bank_account_code_a.' . $key] = 'required|size:4';
            $rules['bank_account_code_b.' . $key] = 'required|size:4';
            $rules['bank_account_code_c.' . $key] = 'required|size:4';
            $rules['bank_account_code_d.' . $key] = 'required|size:4';
            $rules['bank_account_code_e.' . $key] = 'required|size:4';
            $rules['bank_account_sufix.' . $key] = 'required|size:4';
            $rules['bank_account_bic.' . $key] = 'required|size:4';
        }

        return $rules;
    }

    public function messages()
    {
        $messages = [];

        foreach ($this->request->get('accounting_account_code') as $key => $value) {
            $messages['accounting_account_code.'.$key.'.required'] = 
            'El campo "Cuenta Contable '.$key.'" es obligatorio.';
            $messages['accounting_account_code.'.$key.'.max'] = 
            'El campo "Cuenta Contable '.$key.'" debe ser menor a :max caracteres .';
        }

        return $messages;
    }
}

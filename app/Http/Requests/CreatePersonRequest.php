<?php

namespace Edifalia\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreatePersonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       $rules = [
           'first_name' => 'required|max:75',
           'last_name' => 'required|max:75',
           'address' => 'required', 
           'dni' => 'required|max:10|unique:persons,dni',
           'reference' => 'required|max:12', 
           'strength_language' => 'required|max:1', 
           'message_email_only' => 'nullable', 
           'group_properties' => 'nullable', 
           'group_invoice_concepts' => 'nullable', 
           'dont_get_email' => 'nullable', 
           'can_update_own_data' => 'nullable', 
           'exclusiveness' => 'nullable', 
           'virtual_office_access' => 'nullable', 
           'update_data_from_virtual_office' => 'nullable', 
           'notes' => 'nullable',
           'postal_code_id' => 'required|integer|exists:postal_codes,id',
           'language_id' => 'required|integer|exists:languages,id',
           'payment_method_id' => 'required|integer|exists:payment_methods,id',
           'user_id' => 'required|integer|exists:users,id'
       ];

        
        foreach ($this->request->get('accounting_account_code') as $key => $value) {
            $rules['accounting_account_code.' . $key] = 'required|max:15|unique:accounting_accounts,code';
            $rules['accounting_account_title.' . $key] = 'required|max:45';
        }
        
        foreach ($this->request->get('bank_account_accounting_code') as $key => $value) {
            $rules['bank_account_accounting_code.' . $key] = 'required|max:15';
            $rules['bank_account_iban.' . $key] = '
                    required|size:4|unique:bank_accounts,iban';
            $rules['bank_account_code_a.' . $key] = 'required|size:4';
            $rules['bank_account_code_b.' . $key] = 'required|size:4';
            $rules['bank_account_code_c.' . $key] = 'required|size:4';
            $rules['bank_account_code_d.' . $key] = 'required|size:4';
            $rules['bank_account_code_e.' . $key] = 'required|size:4';
            $rules['bank_account_sufix.' . $key] = 'required|size:4';
            $rules['bank_account_bic.' . $key] = 'required|size:4';
        }

        return $rules;
    }
}

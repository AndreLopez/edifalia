<?php

namespace Edifalia\Http\Requests\Employee;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'first_name' => 'required|max:100',
            'last_name' => 'required|max:100',
            'dni' => 'required|max:10|unique:employees',
            'department' => 'required',
            'phone' => 'max:20',
            'extension' => 'max:20',
            'position_id' => 'required|exists:positions,id',
            'office_id' => 'required|exists:offices,id',
            'communities.*' => 'required|numeric|exists:communities,id',
            'access_data' => 'in:1',
            'access_management' => 'in:1',
            'access_mail' => 'in:1',
            'access_incidents' => 'in:1',
            'access_reports' => 'in:1',
            'access_accounting' => 'in:1',
            //'signature' => 'max:500',
        ];
    }
}

<?php

namespace Edifalia\Http\Requests\Incidence;

use Illuminate\Foundation\Http\FormRequest;

class CreateEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'office_id' => 'required|numeric|exists:offices,id',
            'community_id' => 'required|numeric|exists:communities,id',
            'priority' => 'required|numeric',
            'reception_id' => 'required|numeric',
            'reason_id' => 'required|numeric',
            'controls' => 'required|numeric',
            'publish' => 'numeric|in:1',
            'conclude' => 'numeric|in:1',
            'title' => 'required|max:200',
            'details' => 'max:600',
            'sector_id' => 'required|numeric',
            'supplier_id' => 'required|numeric|exists:suppliers,id',
            'send' => 'numeric|in:0,1,2',
            'other_annotations' => 'max:600',
            //communicator
            'type_id' => 'numeric|exists:communicator_types,id',
            'name' => 'max:100',
            'last_name' => 'max:100',
            'address' => 'alpha_num',
            'email' => 'email',
            'phone' => 'max:20',
            'other_phone' => 'max:20',
            //files
            'file1' => 'mimes:pdf,doc,docx,xls,xlsx|max:2048',
            'file_title1' => 'required_with:file1|max:200',
            'publish_file1' => 'numeric|in:1',
            'file2' => 'mimes:pdf,doc,docx,xls,xlsx|max:2048',
            'file_title2' => 'required_with:file2|max:200',
            'publish_file2' => 'numeric|in:1',
            'file3' => 'mimes:pdf,doc,docx,xls,xlsx|max:2048',
            'file_title3' => 'required_with:file3|max:200',
            'publish_file3' => 'numeric|in:1',
            'file4' => 'mimes:pdf,doc,docx,xls,xlsx|max:2048',
            'file_title4' => 'required_with:file4|max:200',
            'publish_file4' => 'numeric|in:1',
        ];
    }
}

<?php

namespace Edifalia\Http\Requests\Template;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => 'required|mimes:doc,docx,odt|max:2048',
            'title' => 'required|max:200',
            'model_id' => 'required|numeric|exists:models,id',
            'office_id' => 'numeric|exists:offices,id',
        ];
    }
}

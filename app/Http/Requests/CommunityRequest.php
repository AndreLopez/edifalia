<?php

namespace Edifalia\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommunityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'code' => 'required|max:4|unique:communities,code',
            'name' => 'required|max:75',
            'address' => 'required',
            'nif' => 'required|max:15|unique:communities,nif',
            'structure_year' => 'required|max:4|min:4',
            'elevator_numbers' => 'nullable|integer',
            'longitude' => 'nullable',
            'latitude' => 'nullable',
            'notes' => 'nullable'
        ];

        /** Reglas de validación para los campos de las cuentas bancarias agregadas dinámicamente */
        foreach ($this->request->get('bank_account_accounting_code') as $key => $value) {
            if (!empty($value)) {
                $rules['bank_account_accounting_code.' . $key] = 'required|max:15';
                $rules['bank_account_iban.' . $key] = 'required|size:4';
                $rules['bank_account_code_a.' . $key] = 'required|size:4';
                $rules['bank_account_code_b.' . $key] = 'required|size:4';
                $rules['bank_account_code_c.' . $key] = 'required|size:4';
                $rules['bank_account_code_d.' . $key] = 'required|size:4';
                $rules['bank_account_code_e.' . $key] = 'required|size:4';
                $rules['bank_account_sufix.' . $key] = 'required|size:4';
                $rules['bank_account_bic.' . $key] = 'required|size:4';
            }
        }

        /** Reglas de validación para los campos de las cuentas contables agregadas dinámicamente */
        foreach ($this->request->get('accounting_account_code') as $key => $value) {
            if (!empty($value)) {
                $rules['accounting_account_code.' . $key] = 'required|max:15|unique:accounting_accounts,code';
                $rules['accounting_account_title.' . $key] = 'required|max:45';
            }
        }

        return $rules;
    }
}

<?php

namespace Edifalia\Http\Requests\Reading;

use Illuminate\Foundation\Http\FormRequest;

class CreateEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'community_id' => 'required|numeric|exists:communities,id',
            'invoicing_concept_id' => 'required|numeric|exists:invoicing_concepts,id',
            'title' => 'required|max:200',
            'periodicity' => 'required|numeric|in:1,2,3,4',
            'rate_id' => 'required|numeric|exists:rates,id',
            'month' => 'required|numeric|digits_between:1,12',
            'year' => 'required|numeric|min:2017',
            'concept_receipt' => 'required',
            'date' => 'required|date',
            'previous.*' => 'numeric',
            'current.*' => 'numeric',
            'consumption.*' => 'numeric',
        ];
    }
}

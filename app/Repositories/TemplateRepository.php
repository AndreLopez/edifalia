<?php

namespace Edifalia\Repositories;

use Edifalia\Models\Template;
use Illuminate\Support\Facades\Auth;

class TemplateRepository extends BaseRepository
{
    public function getModel()
    {
        return new Template();
    }

    public function getEmployeeTemplates($paginate = true, $pagesNumber = 10)
    {
        return ($paginate) ? Auth::user()->templates()->paginate($pagesNumber) :
            Auth::user()->templates()->get();
    }

    public function saveNewWithUserAndTempl($request, $documentName)
    {
        $template = $this->getModel();
        $template->user_id = Auth::user()->id;
        if ($request->has('office_id'))
            $template->office_id = $request->get('office_id');
        $template->file = $documentName;
        $template->fill($request->all());
        $template->save();
        return $template;
    }

    public function updateTemplate($template, $request)
    {
        if ($request->has('office_id'))
            $template->office_id = $request->get('office_id');
        else{
            if (!is_null($template->office_id))
                $template->office_id = null;
        }

        $template->fill($request->all());
        $template->save();

        return $template;
    }
}
<?php

namespace Edifalia\Repositories;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

abstract class BaseRepository
{
    abstract public function getModel();

    public function find($id)
    {
        return $this->getModel()->find($id);
    }

    public function recordInTrash($id)
    {
        return $this->getModel()->onlyTrashed()->where('id', $id)->first();
    }

    public function getAll($paginate = true, $pagesNumber = 10){
        return ($paginate) ? $this->getModel()->paginate($pagesNumber) : $this->getModel()->get();
    }

    public function create($data)
    {
        return $this->getModel()->create($data);
    }

    public function update($object, $data)
    {
        $object->fill($data);
        $object->save();
        return $object;
    }

    public function delete($object)
    {
        $object->delete();
    }

    public function restore($object)
    {
        $object->restore();
    }

    public function saveNewWithUser($data)
    {
        $object = $this->getModel();
        $object->user_id = Auth::user()->id;
        $object->fill($data);
        $object->save();
        return $object;
    }

    /// Methods for users type employe ///

    public function checkCommunities($data)
    {
        if(Gate::allows('employee')){
            foreach ($data as $community_id){
                $result = Auth::user()->employee->communities()
                    ->where('community_id', $community_id)->exists();
                if(!$result)
                    return false;
            }
        }
        return true;
    }

    public function saveCommunities($object, $data)
    {
        $object->communities()->attach($data);
    }

    public function checkCommunity($id)
    {
        $result = true;
        if(Gate::allows('employee')){
            $result = Auth::user()->employee->communities()
                ->where('community_id', $id)->exists();
        }
        return $result;
    }
}
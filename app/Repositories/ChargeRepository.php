<?php

namespace Edifalia\Repositories;

use Edifalia\Models\Charge;

class ChargeRepository extends BaseRepository
{
    public function getModel()
    {
        return new Charge();
    }
}
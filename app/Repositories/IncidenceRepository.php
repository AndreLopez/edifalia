<?php

namespace Edifalia\Repositories;

use Edifalia\Models\Incidence;
use Illuminate\Support\Facades\Auth;
use Edifalia\Models\Communicator;

class IncidenceRepository extends BaseRepository
{
    public function getModel()
    {
        return new Incidence();
    }

    public function getEmployeeIncidences($paginate = true, $pagesNumber = 10)
    {
        return ($paginate) ? Auth::user()->incidences()->paginate($pagesNumber) :
            Auth::user()->incidences()->get();
    }

    public function saveNewWithUserAndIncidence($request)
    {
        $incidence = $this->getModel();
        $incidence->user_id = Auth::user()->id;

        if ($request->has('type_id')){
            $communicator = Communicator::create([
                'type_id' => $request->get('type_id'),
                'name' => $request->get('name'),
                'last_name' => $request->get('last_name'),
                'address' => $request->get('address'),
                'email' => $request->get('email'),
                'phone' => $request->get('phone'),
                'other_phone' => $request->get('other_phone'),
            ]);
            $incidence->communicator_id = $communicator->id;
        }

        $incidence->fill($request->all());
        $incidence->save();
        return $incidence;
    }

    public function updateIncidence($incidence, $request)
    {
        if ($request->has('type_id')){
            $array = [
                'type_id' => $request->get('type_id'),
                'name' => $request->get('name'),
                'last_name' => $request->get('last_name'),
                'address' => $request->get('address'),
                'email' => $request->get('email'),
                'phone' => $request->get('phone'),
                'other_phone' => $request->get('other_phone'),
            ];

            if($incidence->communicator()->exists())
                $communicator = $incidence->communicator()->first();
            else
                $communicator = new Communicator();

            $communicator->fill($array);
            $communicator->save();
            $incidence->communicator_id = $communicator->id;
        }

        $incidence->fill($request->all());
        $incidence->save();
        return $incidence;
    }
}
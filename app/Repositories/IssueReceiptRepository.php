<?php

namespace Edifalia\Repositories;

use Edifalia\Models\IssueReceipt;
use Illuminate\Support\Facades\Auth;
use Edifalia\Mail\IssueReceipt as IssueReceiptMail;
use Illuminate\Support\Facades\Mail;
use Edifalia\Models\Backend\Community;
use PDF;

class IssueReceiptRepository extends BaseRepository
{
    public function getModel()
    {
        return new IssueReceipt();
    }

    public function getEmployeeIssuedReceipts($paginate = true, $pagesNumber = 10)
    {
        return ($paginate) ? Auth::user()->issueReceipts()->paginate($pagesNumber) :
            Auth::user()->issueReceipts()->get();
    }

    public function saveInvoicingConcepts(IssueReceipt $issueReceipt, $data)
    {
        $issueReceipt->invoicingConcepts()->attach($data);
    }

    public function sendEmailsToOwners(IssueReceipt $issueReceipt)
    {
        $idsToExclude = $this->invoicingConceptsToExclude($issueReceipt);
        foreach ($issueReceipt->communities as $community) {
            if($community->properties()->exists()){
                foreach ($community->properties as $property){
                    $receiptDue = $property->receiptDues()->where('issue_receipt_id', null)->first();
                    if (!is_null($receiptDue)) {
                        $receiptDues = $property->receiptDues()
                            ->where('issue_receipt_id', null)
                            ->where('month', '=', $receiptDue->month)
                            ->where('year', '=', $receiptDue->year)->get();

                        $html = view('backend.management.issue_receipts.pdf')
                            ->with(compact('issueReceipt', 'community', 'property', 'receiptDues', 'idsToExclude'))
                            ->render();

                        PDF::AddPage();
                        PDF::writeHTML($html, true, 0, true, 0);
                        $pdf_name = uniqid('', true).'.pdf';
                        $pdf_route = storage_path('app/public/pdf_issue_receipts/').$pdf_name;
                        PDF::Output($pdf_route, 'F');
                        PDF::reset();

                        $issueReceipt->issueReceiptPdfs()->create([
                            'pdf_name' => $pdf_name
                        ]);

                        Mail::to($property->person->user->email)
                            ->send(new IssueReceiptMail($issueReceipt->id, $pdf_route));
                    }
                }
                if($receiptDues->count()){
                    foreach($receiptDues as $receiptDue){
                        if(!in_array($receiptDue->invoicing_concept_id, $idsToExclude)){
                            $receiptDue->issue_receipt_id = $issueReceipt->id;
                            $receiptDue->save();
                        }
                    }
                }
            }
        }
    }

    public function invoicingConceptsToExclude(IssueReceipt $issueReceipt)
    {
        return $issueReceipt->invoicingConcepts()->pluck('invoicing_concept_id')
            ->toArray();
    }

    public function existsReceiptDues($communitiesIds)
    {
        foreach ($communitiesIds as $id){
            $community = Community::find($id);
            foreach ($community->properties as $property) {
                if ($property->receiptDues()->where('issue_receipt_id', null)->exists()) {
                    return true;
                }
            }
        }
        return false;
    }

    /*public function updateCommunities(IssueReceipt $issueReceipt, $data)
    {
        $issueReceipt->communities()->sync($data);
    }*/

    /*public function updateInvoicingConcepts(IssueReceipt $issueReceipt, $data)
    {
        $issueReceipt->invoicingConcepts()->sync($data);
    }*/

    /*public function detachInvoicingConcepts(IssueReceipt $issueReceipt)
    {
        $issueReceipt->invoicingConcepts()->detach();
    }*/
}
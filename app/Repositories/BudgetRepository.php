<?php

namespace Edifalia\Repositories;

use Edifalia\Models\Budget;
use Illuminate\Support\Facades\Auth;
use Edifalia\Models\Common\Property;
use Carbon\Carbon;

class BudgetRepository extends BaseRepository
{
    public function getModel()
    {
        return new Budget();
    }

    public function getEmployeeBudgets($paginate = true, $pagesNumber = 10)
    {
        return ($paginate) ? Auth::user()->budgets()->paginate($pagesNumber) :
            Auth::user()->budgets()->get();
    }

    public function saveNewWithUserAndBudget($request)
    {
        $budget = $this->getModel();
        $budget->user_id = Auth::user()->id;
        $budget->fill($request->all());
        $budget->save();
        if($request->has('general')){
            foreach ($request->get('general') as $id => $general){
                if(!empty($general)){
                    $property = Property::find($id);
                    if(!is_null($property)){
                        if ($property->community->id == $request->get('community_id')){
                            $amount = ($general * $property->coefficient) / 100;
                            $budget->properties()->attach($property->id, [
                                'general' => $general,
                                'amount' => $amount,
                                'coefficient' => $property->coefficient,
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now(),
                            ]);
                        }
                    }
                }
            }
        }
        return $budget;
    }

    public function updateBudget($budget, $request)
    {
        $budget->fill($request->all());
        $budget->save();
        if($request->has('general')){
            foreach ($request->get('general') as $id => $general){
                $property = Property::find($id);
                if(!empty($general)){
                    if(!is_null($property)){
                        if ($property->community->id == $request->get('community_id')){
                            $propertyInPivot = $budget->properties()->where('property_id', $property->id)->first();
                            if(!is_null($propertyInPivot)){
                                $amount = ($general * $propertyInPivot->pivot->coefficient) / 100;
                                $budget->properties()->updateExistingPivot($property->id, [
                                    'general' => $general,
                                    'amount' => $amount,
                                    'updated_at' => Carbon::now(),
                                ]);
                            }
                            else {
                                $amount = ($general * $property->coefficient) / 100;
                                $budget->properties()->attach($property->id, [
                                    'general' => $general,
                                    'amount' => $amount,
                                    'coefficient' => $property->coefficient,
                                    'created_at' => Carbon::now(),
                                    'updated_at' => Carbon::now(),
                                ]);
                            }
                        }
                    }
                }
                else {
                    $budget->properties()->detach($property->id);
                }
            }
        }
        return $budget;
    }
}
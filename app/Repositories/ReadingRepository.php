<?php

namespace Edifalia\Repositories;

use Illuminate\Support\Facades\Auth;
use Edifalia\Models\Reading;
use Carbon\Carbon;
use Edifalia\Models\Common\Property;

class ReadingRepository extends BaseRepository
{
    public function getModel()
    {
        return new Reading();
    }

    public function getEmployeeReadings($paginate = true, $pagesNumber = 10)
    {
        return ($paginate) ? Auth::user()->readings()->paginate($pagesNumber) :
            Auth::user()->readings()->get();
    }

    public function saveNewWithUserAndReading($request)
    {
        $reading = $this->getModel();
        $reading->user_id = Auth::user()->id;
        $reading->fill($request->all());
        $reading->save();

        if($request->has('previous') && $request->has('current') && $request->has('consumption')){
            foreach ($request->get('previous') as $id => $previous){
                if(!empty($request->previous[$id]) && !empty($request->current[$id]) && !empty($request->consumption[$id])){
                    $property = Property::find($id);
                    if(!is_null($property)){
                        if ($property->community->id == $request->get('community_id')){
                            $reading->properties()->attach($property->id, [
                                'previous' => $previous,
                                'current' => $request->current[$id],
                                'consumption' => $request->consumption[$id],
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now(),
                            ]);
                        }
                    }
                }
            }
        }

        return $reading;
    }

    public function updateReading($reading, $request)
    {
        $reading->fill($request->all());
        $reading->save();

        if($request->has('previous') && $request->has('current') && $request->has('consumption')){
            foreach ($request->get('previous') as $id => $previous){
                $property = Property::find($id);
                if(!empty($request->previous[$id]) && !empty($request->current[$id]) && !empty($request->consumption[$id])){
                    if(!is_null($property)){
                        if ($property->community->id == $request->get('community_id')){
                            $propertyInPivot = $reading->properties()->where('property_id', $property->id)->first();
                            if(!is_null($propertyInPivot)){
                                $reading->properties()->updateExistingPivot($property->id, [
                                    'previous' => $previous,
                                    'current' => $request->current[$id],
                                    'consumption' => $request->consumption[$id],
                                    'updated_at' => Carbon::now(),
                                ]);
                            }
                            else {
                                $reading->properties()->attach($property->id, [
                                    'previous' => $previous,
                                    'current' => $request->current[$id],
                                    'consumption' => $request->consumption[$id],
                                    'created_at' => Carbon::now(),
                                    'updated_at' => Carbon::now(),
                                ]);
                            }
                        }
                    }
                }
                elseif (empty($request->previous[$id]) && empty($request->current[$id]) && empty($request->consumption[$id])){
                    $reading->properties()->detach($property->id);
                }
            }
        }

        return $reading;
    }
}
<?php

namespace Edifalia\Repositories;

use Edifalia\Models\Zone;

class ZoneRepository extends BaseRepository
{
    public function getModel()
    {
        return new Zone();
    }
}
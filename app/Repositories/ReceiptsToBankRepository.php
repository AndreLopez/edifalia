<?php

namespace Edifalia\Repositories;

use Edifalia\Models\ReceiptsToBank;
use Illuminate\Support\Facades\Auth;

class ReceiptsToBankRepository extends BaseRepository
{
    public function getModel()
    {
        return new ReceiptsToBank();
    }

    public function getEmployeeReceiptsToBanks($paginate = true, $pagesNumber = 10)
    {
        return ($paginate) ? Auth::user()->receiptsToBanks()->paginate($pagesNumber) :
            Auth::user()->receiptsToBanks()->get();
    }
}
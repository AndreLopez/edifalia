<?php

namespace Edifalia\Repositories;

use Edifalia\Models\Currency;

class CurrencyRepository extends BaseRepository
{
    public function getModel()
    {
        return new Currency();
    }
}
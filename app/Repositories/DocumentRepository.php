<?php

namespace Edifalia\Repositories;

use Edifalia\Models\Document;
use Illuminate\Support\Facades\Auth;

class DocumentRepository extends BaseRepository
{
    public function getModel()
    {
        return new Document();
    }

    public function getEmployeeDocuments($paginate = true, $pagesNumber = 10)
    {
        return ($paginate) ? Auth::user()->documents()->paginate($pagesNumber) :
            Auth::user()->documents()->get();
    }

    public function saveNewWithUserAndDoc($request, $documentName)
    {
        $document = $this->getModel();
        $document->user_id = Auth::user()->id;
        $document->document = $documentName;
        $document->fill($request->all());
        $document->save();
        if ($request->has('owners')){
            $document->persons()->attach($request->get('owners'));
        }
        return $document;
    }

    public function updateDocument($document, $request)
    {
        $document->fill($request->all());
        $document->save();

        if ($request->has('owners'))
            $document->persons()->sync($request->get('owners'));
        else
            $document->persons()->detach();

        return $document;
    }
}
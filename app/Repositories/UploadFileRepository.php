<?php

namespace Edifalia\Repositories;

class UploadFileRepository
{
    private $file_name;
    private $error_message = '';

    public function uploadFile($file, $store, $dir = '/')
    {
        if ($file->isValid()) {
            $new_name = uniqid('', true).'.'.$file->extension();
            $this->file_name = $file->storeAs($dir, $new_name, $store);
            return true;
        }

        $this->error_message = trans('dashboard.messages.file_error').
            $file->getClientOriginalName().
            trans('dashboard.messages.try_again');

        return false;
    }

    public function getFileName()
    {
        return $this->file_name;
    }

    public function getErrorMessage()
    {
        return $this->error_message;
    }

    public function deleteFile($file, $store)
    {
        if(\Storage::disk($store)->exists($file))
            \Storage::disk($store)->delete($file);
    }
}
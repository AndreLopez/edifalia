<?php

namespace Edifalia\Policies;

use Edifalia\User;
use Edifalia\Models\IssueReceipt;
use Illuminate\Auth\Access\HandlesAuthorization;

class IssueReceiptPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the issueReceipt.
     *
     * @param  \Edifalia\User  $user
     * @param  \Edifalia\IssueReceipt  $issueReceipt
     * @return mixed
     */
    public function view(User $user, IssueReceipt $issueReceipt)
    {
        return $user->id == $issueReceipt->user_id;
    }

    /**
     * Determine whether the user can create issueReceipts.
     *
     * @param  \Edifalia\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the issueReceipt.
     *
     * @param  \Edifalia\User  $user
     * @param  \Edifalia\IssueReceipt  $issueReceipt
     * @return mixed
     */
    public function update(User $user, IssueReceipt $issueReceipt)
    {
        return $user->id == $issueReceipt->user_id;
    }

    /**
     * Determine whether the user can delete the issueReceipt.
     *
     * @param  \Edifalia\User  $user
     * @param  \Edifalia\IssueReceipt  $issueReceipt
     * @return mixed
     */
    public function delete(User $user, IssueReceipt $issueReceipt)
    {
        return $user->id == $issueReceipt->user_id;
    }
}

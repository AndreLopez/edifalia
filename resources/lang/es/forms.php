<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Forms Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during forms for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'select_yes' => 'Si',
    'select_not' => 'No',
    'select_null_option' => 'Seleccione',
    'name_label' => 'Nombre',
    'name_placeholder' => 'Indique el nombre',
    'acronym_label' => 'Acrónimo',
    'acronym_placeholder' => 'Indique el acrónimo',
    'type_label' => 'Tipo',
    'type_placeholder' => 'Indique el tipo',
    'code_label' => 'Código',
    'code_placeholder' => 'Indique el código',
    'country_label' => 'Pais',
    'country_placeholder' => 'Indique el Pais',
    'town_label' => 'Población',
    'town_placeholder' => 'Indique la población',
    'province_label' => 'Provincia',
    'province_placeholder' => 'Indique la provincia',
    'region_label' => 'Región',
    'region_placeholder' => 'Indique la región',
    'address_label' => 'Dirección',
    'address_placeholder' => 'Indique la dirección',
    'nif_label' => 'N.I.F.',
    'nif_placeholder' => 'Número de Identificación Fiscal',
    'structure_year_label' => 'Año de Construcción',
    'structure_year_placeholder' => '0',
    'elevator_number_label' => 'Número de Ascensores',
    'elevator_number_placeholder' => '0',
    'platform_accounting_label' => 'Contabilidad con Plataforma',
    'longitude_label' => 'Longitud',
    'longitude_placeholder' => 'Coordenada de longitud',
    'latitude_label' => 'Latitud',
    'latitude_placeholder' => 'Coordenada de latitud',
    'note_label' => 'Observaciones',
    'note_placeholder' => 'Indique las notas u observaciones',
    'status_label' => 'Estatus',
    'postal_code_label' => 'Código Postal',
    'active' => 'Activo',
    'active_yes' => 'SI',
    'active_no' => 'NO',
    'president_name_label' => 'Presidente',
    'president_name_placeholder' => 'Agregue el nombre del presidente',
    'bank_accounts_label' => 'Cuenta(s) Bancaria(s)',
    'iban_placeholder' => 'IBAN',
    'bank_account_sepa_label' => 'Cuenta Bancaria (SEPA)',
    'sufix_label' => 'Suf.',
    'accounting_account_label' => 'Cuenta Contable',
    'accounting_account_placeholder' => 'Indique cuenta contable',
    'account_title_label' => 'Título de Cuenta',
    'account_title_placeholder' => 'Indique título de la cuenta',
    'bic_label' => 'BIC',
    'other_treasury_accounts' => 'Otras Cuentas de Tesorería',
    'billing_concept_title_1_label' => 'Título reducido 1',
    'billing_concept_title_1_placeholder' => 'Indique título reducido 1',
    'billing_concept_title_2_label' => 'Título reducido 2',
    'billing_concept_title_2_placeholder' => 'Indique título reducido 2',

    'accounting_account' => 'Cuenta Contable',
    'bank_accounts' => 'Cuenta Bancaria',
    
    /**
     *  Form Property
     */
    'building_number_label' => 'Nro de edificio',
    'building_number_placeholder' => 'Indique el número de edificio',
    'property_number_label' => 'Nro de propiedad',
    'property_number_placeholder' => 'Indique el número de propiedad',
    'registered_property_label' => 'Registro de la propiedad',
    'registered_property_placeholder' => 'Indique el registro de la propiedad',
    'status_label' => 'Estatus',
    'coefficient_label' => 'Coeficiente',
    'coefficient_placeholder' => 'Indique el coeficiente',
    'coefficient_total_label' => 'Coeficiente Total',
    'coefficient_individual' => 'Coeficiente Individual',
    'coefficient_calc_tooltip' => 'Calcular el coeficiente individual equitativamente entre todas las propiedades',
    'group_properties_label' => 'Agrupar propiedades',
    'group_concepts_label' => 'Agrupar conceptos de facturación',
    'person_relation_label' => 'Persona asociada',
    'person_relation_placeholder' => 'Indique personsa relaciondada',
    'community_label' => 'Comunidad asociada',
    'community_placeholder' => 'Indique la comunidad relacionda',
    'accounting_account_label' => 'Nro de cuenta asociada',
    'accounting_account_placeholder' => 'Indique el nro de  cuenta relacionda',
    'payment_method_label' => 'Forma de cobro asociada',
    'payment_method_placeholder' => 'Indique la forma de cobro relacionda',
    'community_select_label' => 'Comunidad',
    'property_label' => 'Propiedad',
    'properties_label' => 'Propiedades',
    'invoicing_concept_placeholder' => 'Indique el concepto relacionado',
];

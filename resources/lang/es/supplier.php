<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Supplier Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during community for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'management_title' => 'Gestión de Proveedores',
    'listing_title' => 'Listando Proveedores',
    'list_title' => 'Lista de Proveedores',
    'add_title' => 'Agregando proveedor',
    'new_title' => 'Nuevo proveedor',

];

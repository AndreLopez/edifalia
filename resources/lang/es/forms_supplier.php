<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Forms Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during forms for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    
    'address_label' => 'Dirección',
    'address_placeholder' => 'Indique la dirección',
    'cif_label' => 'CIF',
    'cif_placeholder' => 'Indique CIF',
    'contactn_label' => 'Nombre Contacto',
    'contactn_placeholder' => 'Indique nombre del contacto',
    'businessn_label' => 'Nombre del Negocio',
    'businessn_placeholder' => 'Indique el nombre del negocio',
    'notes_label' => 'Notas',
    'notes_placeholder' => 'Ingrese alguna nota u observación', 
    'postal_code_label' => 'Código Postal', 
    'tradename_label' => 'Nombre Compañia',
    'tradename_placeholder' => 'Indique el nombre de la compañia',
    'select_null_option' => 'Seleccione',
    'website_label' => 'Website',
    'website_placeholder' => 'Ingrese el website',
    'virtual_office_access_label' => 'Acceso a oficina virtual',
    'virtual_office_access_placeholder' => 'Ingrese el acceso a oficina virtual' , 
    'update_data_from_virtual_office_label' => 'Actualizar datos desde oficina virtual',
    'update_data_from_virtual_office_placeholder' => 'Indique los datos a actualizar desde oficina virtual',
    'bank_account_id_label' => 'Cta. Banco',
    'bank_account_id_placeholder' => 'Cta. Banco',
    'iban_label' => 'Iban', 
    'iban_placeholder' => 'Ingrese código iban', 
    'number_label' => 'Número de cuenta', 
    'number_placeholder' => 'Ingrese número de cuenta', 
    'sufix_label' => 'Sufix',
    'sufix_placeholder' => 'Ingrese cuenta sufix', 
    'bic_label' => 'BIC', 
    'bic_placeholder' => 'Ingrese código de banco internacional',

    'user_id_label' => 'Usuario', 
    'supplier_activie' => 'Actividad de Proveedor', 
 



    ];

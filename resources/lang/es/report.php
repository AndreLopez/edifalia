<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Reports Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during reports for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'report' => [
        'property' => 'Propiedad',
        'coefficient' => 'Coeficiente',
        'owner' => 'Propietario',
        'dni' => 'DNI',
        'email' => 'E-Mail',
        'phone' => 'Telefono',
        'assistant-fail' => 'Sin Asistentes Cargados',
        'number' => 'Número',
        'date' => 'Fecha',
        'type-document' => 'Tipo de Documento',
        'number-document' => 'Número de Documento',
        'supplier' => 'Proveedor / Cliente',
        'bill' => 'Compras',
        'sale' => 'Ventas',
        'community' => 'Comunidad',
        'address' => 'Dirección',
        'nif' => 'NIF',
        'budget' => 'Presupuesto',
        'select-budget' => 'Seleccione Presupuesto',
        'actions' => 'Acciones'
    ],
    'dossier' => [
        'form' => [
            'communities' => 'Comunidades',
            'select-community' => 'Seleccione Comunidad...',
            'coefficients' => 'Coeficientes',
            'select-coefficients' => 'Seleccione Coeficientes...',
            'available-properties' => 'Propiedades Disponibles',
            'exclude-properties' => 'Propiedades Excluidas',
            'include' => 'Incluir',
            'dni' => 'DNI',
                'email' => 'E-Mail',
                'phone' => 'Telefono',
                'do-not-show-data' => 'No Mostrar Datos Personales',
            'order' => 'Orden',
                'fullname' => 'Nombre de Propietario',
                'property' => 'Propiedad',
                'n-property' => 'N° de Propiedad',
            'include-owner' => 'Incluir Propietario y',
                'payer' => 'Pagador',
                'spouse' => 'Conyugue',
                'attorney' => 'Apoderador',
                'joint-owners' => 'Cotitulares',
            'title' => 'Título',
        ]
    ],
    'meeting' => [
        'title' => 'Título',
        'date' => 'Fecha',
        'person' => 'Persona',
        'place' => 'Lugar de Celebración',
        'voting' => [
            'title' => 'Votaciones',
            'details' => 'Detalles de Votación',
            'description'  => 'Descripción',
            'selectVotingYes' => 'Si',
            'selectVotingNo' => 'No',
        ],
        'controlled' => [
            'title' => 'Controlada',
            'select' => 'Asistira en representación del despacho'
        ],
        'celebration' => 'Celebración',
        'conv_1' => '1° Conv.',
        'conv_2' => '2° Conv.',
        'ending' => 'Finalización',
        'type'  => [
            'title' => 'Tipo de Junta',
            'ordinary' => 'Ordinaria',
            'extra_ordinary' => 'Extraordinaria',
            'directive' => 'Directiva',
            'others' => 'Otros'
        ],
        'characteristics' => 'Caracteristicas',
        'order_day' => 'Orden del Día',
        'minute' => 'Acta',
        'assistant' => 'Asistentes',
        'minute' => 'Acta',
        'form' => [
            'title' => 'Título de la Junta',
            'select' => 'Ninguna junta seleccionada...',
            'message_select' => 'Seleccione una Comunidadad para continuar'            
        ]
    ],
    'liquidation' => [
        'exercise' => 'Ejercicio Contable',
        'exercise-select' => 'Seleccione Ejercicio Contable'
    ],
    'liquidation_cons' => [
        'liquidate' => 'Liquidar consumo de',
        'exercise-select' => 'Seleccione Ejercicio Contable',
        'liquidation-select' => 'Nunguna Liquidación de Consumos Seleccionada'
    ],
    'buttons' => [
        'to_accept' => 'Aceptar',
        'cancel' => 'Cancelar',
        'generate' => 'Generar Reporte',
        'generate_pdf' => 'Generar Reporte PDF',
        'generate_excel' => 'Generar Reporte Excel',
        'exit' => 'Salir',
        'return' => 'Volver',
        'pdf'  => 'PDF',
        'excel' => 'Excel',
        'load' => 'Cargar',
        'delete' => 'Eliminar',
        'reset' => 'Reiniciar',
        'create' => 'Crear'
    ],
    'economic' => [
        'account-groups' => 'Ingresos y Gastos Agrupados por Cuentas',
        'receipt' => 'Recibos Emitidos'
    ]

];

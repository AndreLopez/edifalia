<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Common Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during common for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'new_record_title'   => 'Nuevo Registro',
    'new_record_submit' => 'Guardar',
    'edit_record_title' => 'Actualizar Registro',
    'edit_record_submit' => 'Actualizar',
    'registers_label' => 'Registros',
    'total_label' => 'Total',
    'lang_management_title' => 'Gestión de Idiomas',
    'lang_list_title' => 'Listado de idiomas',
    'lang_add_title' => 'Agregando idioma',
    'lang_upd_title' => 'Actualizando idioma',
    'billing_concept_management_title' => 'Gestión para Conceptos de Facturación', 
    'billing_concept_list_title' => 'Listado de conceptos de facturación',
    'contact_type_management_title' => 'Gestión de Tipos de Contacto',
    'contact_type_list_title' => 'Listado de tipos de contacto',
    'contact_type_add_title' => 'Agregando tipos de contacto',
    'contact_type_upd_title' => 'Actualizando tipos de contacto',
    'country_management_title' => 'Gestión de Países',
    'country_list_title' => 'Listado de Países',
    'country_add_title' => 'Agregando Pais',
    'country_upd_title' => 'Actualizando Pais',
    'payment_method_management_title' => 'Gestión de métodos de pago',
    'payment_method_list_title' => 'Listado de métodos de pago',
    'payment_method_add_title' => 'Agregando método de pago',
    'payment_method_upd_title' => 'Actualizando método de pago',
    'position_management_title' => 'Gestión de cargos',
    'position_list_title' => 'Listado de Cargos',
    'position_add_title' => 'Agregando cargo',
    'position_upd_title' => 'Actualizando cargo',
    'postal_code_management_title' => 'Gestión de códigos postales',
    'postal_code_list_title' => 'Listado de códigos postales',
    'postal_code_add_title' => 'Agregando código postal',
    'postal_code_show_title' => 'Mostrando código postal',
    'postal_code_upd_title' => 'Actualizando código postal',

    'status_management_title' => 'Gestión de estatus',
    'status_list_title' => 'Listado de estatus',
    'status_add_title' => 'Agregando estatus',
    'status_upd_title' => 'Actualizando estatus',
    'billing_concept_add_title' => 'Agregando Concepto de Facturación',
    'billing_concept_upd_title' => 'Actualizando concepto de facturación',

    /**
    * Supplier Contact
    */

    'supplier_contact_management_title' => 'Gestión de Contacto de Proveedores', 
    'supplier_contact_list_title' => 'Listando Contacto de Proveedores',
    'lst_supplier_contact_title' => 'Lista de Contactos Proveedores',
    'supplier_contact_create' => 'Nuevo Contacto ', 
    'lst_supplier_contact_name' => 'Nombre del Contacto',

    'suppliers_contacts_add_title' => 'Agregando nuevo Contacto',
    'suppliers_contacts_new_title' => 'Nuevo Contacto',
    'contact_name_label' => 'Nombre de Contacto',
    'contact_name_placeholder' => 'Ingrese nombre de Contacto',
    'contact_type_label' => 'Tipo de Contacto',
    'supplier_id_label' => 'Proveedor', 
    'notes_label' => 'Notas', 
    'notes_placeholder' => 'Ingrese observaciones',


    /***
    * Supplier Activities
    */

    'supplier_activitie_title' => 'Gestión de Actividades de Proveedores', 
    'supplier_activitie_list_title' => 'Listando Actividades de Proveedores', 
    'lst_supplier_activitie_title' => 'Lista de Actividades de Proveedores', 
    'lst_supplier_activitie_name' => 'Actividad',
    'supplier_activitie_name_label' => 'Nombre',
    'supplier_activitie_name_placeholder' => ' Ingrese Nombre de la Actividad',
    'supplier_activitie_new_title' => 'Nueva Actividad',


     /**
     * Suppliers Sectors
     */

     'supplier_sector_title' => 'Gestión de Sectores de Proveedores', 
     'supplier_sector_list_title' => 'Listando Sectores de Proveedores', 
     'lst_supplier_sector_title' => 'Lista de Sectores de Proveedores', 
     'lst_supplier_id_name' => 'ID del Proveedor', 
     'supplier_sector_new_title' => 'Nuevo Sector',
     'suppliers_contacts_add_title' => 'Agregando nuevo Sector',
     'supplier_sector_id' => 'ID Proveedor', 
     'supplier_sector_sector_id' => 'Actividad',
      'suppliers_sector_edit_title' => 'Editando Sector',

     /**
     *  Contracts
     */

     'contract_lst_reference_name' => 'Referencia del Contrato',





    'property_management_title' => 'Gestión de Propiedades',
    'property_list_title' => 'Listado de Propiedades',
    'property_add_title' => 'Agregando Propiedad', 
    'property_upd_title' => 'Actualizando propiedad', 

    'property_type_management_title' => 'Gestión Tipo de Propiedades',
    'property_type_list_title' => 'Listado Tipo de Propiedades',
    'property_type_add_title' => 'Agregando Tipo de propiedad', 
    'property_type_upd_title' => 'Actualizando tipo de propiedad',

    /**
     * Buttons
     */
    'btn_cancel_text' => 'Cancelar',
    'btn_cancel_tooltip' => 'Presione para cancelar',
    'btn_clear_text' => 'Limpiar',
    'btn_clear_tooltip' => 'Presione para limpiar el formulario',
    'btn_save_text' => 'Guardar',
    'btn_save_tooltip' => 'Presione para registrar la información',
    'btn_back' => 'Ir atrás',

    /**
     * Lists
     */
    'lst_new_record' => 'Nuevo',
    'lst_action_title' => 'Acción',
    'lst_edit_record' => 'Editar registro',
    'lst_delete_record' => 'Eliminar registro',
    'lst_language_title' => 'Lista de Idiomas',
    'lst_country_title' => 'Lista de Países',
    'lst_property_title' => 'Lista de Propiedades',
    'lst_property_type_title' => 'Lista para tipo de propiedades',
    'lst_status_title' => 'Lista de Estatus',
    'lst_payment_method_title' => 'Lista de Métodos de Pago',
    'lst_contact_type_title' => 'Lista de Tipos de Contacto',
    'lst_postal_code_title' => 'Lista de Códigos Postales',
    'lst_position_title' => 'Lista de Cargos',
    'lst_col_name' => 'Nombre',
    'lst_col_acronym' => 'Acrónimo',
    'lst_col_type' => 'Tipo',
    'lst_col_code' => 'Código',
    'lst_col_country' => 'Pais',
    'lst_col_province' => 'Provincia',
    'lst_col_group_properties' => 'Agrupar propiedades',
    'lst_col_group_concepts' => 'Agrupar conceptos de facturación',
    'lst_act_edit' => 'Editar',
    'lst_act_delete' => 'Eliminar',
    'lst_act_show' => 'Más información',
    'lst_col_business_name' => 'Nombre de la Compañia',
    'lst_billing_concept_title' => 'Lista de Conceptos de Facturación', 

    /**
     * Forms
     */
    'new_language_title' => 'Nuevo Idioma',
    'edit_language_title' => 'Actualizar Idioma',
    'new_country_title' => 'Nuevo Pais',
    'edit_country_title' => 'Actualizar Pais',
    'new_status_title' => 'Nuevo Estatus',
    'edit_status_title' => 'Actualizar Estatus',
    'new_payment_method_title' => 'Nuevo Método de Pago',
    'edit_payment_method_title' => 'Actualizar Método de Pago',
    'new_contact_type_title' => 'Nuevo Tipo de Contacto',
    'edit_contact_type_title' => 'Actualizar Tipo de Contacto',
    'new_postal_code_title' => 'Nuevo Código Postal',
    'edit_postal_code_title' => 'Actualizar Código Postal',
    'show_postal_code_title' => 'Información del Código Postal',
    'new_position_title' => 'Nuevo Cargo',
    'edit_position_title' => 'Actualizar Cargo',
    'new_billing_concept_type_title' => 'Nuevo Concepto de Facturación',
    'edit_billing_concept_type_title' => 'Actualizar Concepto de Facturación',

    /**
    * Accounting Accounts
    */
    'new_property_title' => 'Nueva Propiedad',
    'new_property_type_title' => 'Nuevo Tipo de propiedad',
    'edit_property_title' => 'Actualizando propiedad',
    'edit_property_type_title' => 'Actualizando Tipo de propiedad',

    /**
     * Accessors Models
     */

    'affirmative_answer' => 'Si',
    'negative_answer' => 'No',


    'accounting_accounts_management_title' => 'Gestión de Cuenta Contables',
    'accounting_account_list_title' => 'Listando Cuentas Contables', 
    'lst_accounting_accounts_title' => 'Lista de Cuentas Contables',
    'accounting_account_code_title' => 'Código',
    'accounting_account_code_placeholder' => ' Ingrese el Código',
    'accounting_account_title_title' => 'Título', 
    'accounting_account_title_placeholder' => 'Ingrese el Título', 
    'edit_accounting_account_title' => 'Editar Cuenta Contables',

/**
* Persons
*/
'lst_first_name_title' => 'Primer Nombre',

/**
* Person Contacts  
*/

'person_contact_management_title' => 'Gestión de Contactos', 
'person_contact_list_title' => 'Listando Contactos', 
'lst_person_contact_title' => 'Lsta de Contactos', 
 'person_contact_management_title' => 'Gestión de Contactos',
'person_contact_listing_title' => 'Listando Contactos',
'person_contact_list_title' => 'Lista de Contactos',
'person_contact_contact_title' => 'Contacto',    
'person_contact_contact_placeholder' => 'Ingrese Contacto',
'person_contact_notes_title' => 'Notas',
'person_contact_notes_placeholder' => 'Ingrese notas', 
 'contact_type_title' => 'Tipo de Contacto', 
 'person_title' => 'Nombre de la Persona',
 
];


<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Panel Info Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during panel info for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'box_languages_title' => 'IDIOMAS',
    'box_countries_title' => 'PAISES',
    'box_status_title' => 'ESTATUS',
    'box_payment_methods_title' => 'METODOS DE PAGO',
    'box_contact_types_title' => 'TIPOS DE CONTACTO',
    'box_postal_codes_title' => 'CODIGOS POSTALES',
    'box_position_title' => 'CARGOS',
    'box_suppliers_contacts_title' => 'CONTACTO PROVEEDORES',
    'box_supplier_activitie_title' => 'ACTIVIDADES PROVEEDORES',
    'box_supplier_sector_title' => 'SECTORES PROVEEDORES',
    'box_accounting_account_title' => 'CUENTAS CONTABLES',
    'box_person_contact_title' => 'CONTACTOS',
    'box_currency_title' => 'MONEDAS',
    'box_zone_title' => 'ZONAS',
];

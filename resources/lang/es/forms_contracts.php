<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Forms Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during forms for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    
    'reference_title' => 'Referencia',
    'reference_placeholder' => 'Inserte Referencia',
    'payments_title' => 'Pagos', 
    'jan_title' => 'Enero', 
    'contract_date_title' => 'Fecha de Contrato',
    'feb_title' => 'Febrero',
    'duration_title' => 'Duración',
    'duration_placeholder' => 'Ingrese duración del Contrato',
    'duration_type_title' => 'Tipo de Duración',
    'duration_type_placeholder' => 'Ingrese el tipo de duración',
    'amount_title' => 'Cantidad',
    'amount_placeholder' => 'Ingrese la cantidad',
    'minimum_title'  => 'Mínimo', 
    'minimum_placeholder' => 'Ingrese el Mínimo',
    'maximum_title' => 'Máximo', 
    'maximum_placeholder' => 'Ingrese el Máximo',
    'publish_virtual_office_title' => 'Publicar oficina virtual',
    'community_id_title' => ' Comunidad',
    'supplier_id_title' => ' Proveedor',
    'mar_title' => 'Marzo', 
    'apr_title' => 'Abril',
    'may_title' => 'Mayo',
    'jun_title' => 'Junio', 
    'jul_title' => 'Julio', 
    'aug_title' => 'Agosto', 
    'sep_title' => 'Septiembre', 
    'oct_title' => 'Octubre', 
    'nov_title' => 'Noviembre', 
    'dec_title' => 'Diciembre', 
    'description_title' => 'Descripción', 
    'description_placeholder' => 'Ingrese la descripción', 
    'observations_title' => 'Observaciones', 
    'observations_placeholder' => 'Ingrese  observación', 
    'payment_method_title' => 'Método de Pago',
    'accounting_account' => ' Cuenta Contable', 
    'months_title' => 'Meses:', 
  
    

    ];

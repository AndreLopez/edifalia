<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Receipt Dues Language Lines
    |--------------------------------------------------------------------------
    */

    'management_title' => 'Gestión de Cuota Recibos',
    'listing_title' => 'Listando Cuota Recibos',
    'list_title' => 'Lista de Cuota Recibos',
    'add_title' => 'Agregando Cuota Recibo',
    'new_title' => 'Nueva Cuota Recibo',
    'update_title' => 'Actualizando Cuota Recibo',
    'edit_title' => 'Editar Cuota Recibo',
    'view_title' => 'Detalle Cuota de Recibo',
    'table_id' => 'Id',
    'table_bloq' => 'Bloq.',
    'table_property' => 'Propiedad',
    'table_payer' => 'Pagador',
    'table_concept' => 'Concepto',
    'table_amount' => 'Importe',
    'table_month' => 'Mes',
    'table_year' => 'Año',

];

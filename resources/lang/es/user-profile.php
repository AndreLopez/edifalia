<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Common Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during common for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

'name_title' => 'Nombre', 
'user_profile_management_title' => 'Perfil de Usuario',
'position_list_title' => 'Perfil',
'email_title' => 'Correo', 
'created_at_title' => 'Creado el',
'active_title' => 'Activo',  
 'user_information_title' => 'Información del Usuario',
 'lst_edit_user' => 'Editar', 
 'email_insert' => 'Ingrese correo', 
 'password_title' => 'Nueva Contraseña', 
 'password_insert' => 'Inserte nueva Contraseña', 
 'confirm_password' => 'Confirme Contraseña', 

'user_administrator' => 'Administrador', 
'user_employee' => 'Empleado', 
'user_owner' => 'Propietario', 
'user_supplier' => 'Proveedor', 
'user_active_yes' => 'SI', 
'user_active_no' => 'NO', 

];


<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Tooltip Text Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during tooltip text for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'see_listing'   => 'Ver listado',
    'add_record' => 'Agregar registro',
    'add_bank_account' => 'Agregar cuenta bancaria',
    'add_accounting_account' => 'Agregar cuenta contable',
    'add_supplier_activitie' => 'Agregar actividad',

];

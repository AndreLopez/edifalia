<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Community Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during community for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'management_title' => 'Gestión de Comunidades',
    'listing_title' => 'Listando Comunidades',
    'list_title' => 'Lista de Comunidades',
    'add_title' => 'Agregando comunidad',
    'new_title' => 'Nueva Comunidad',

];

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Messages Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during messages for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'msg_info_title' => 'Información',
    'msg_delete_confirm' => 'Estas seguro de eliminar el registro?',
    'msg_record_success' => 'Registro almacenado con éxito',
    'msg_updated_success' => 'Registro actualizado con éxito',
    'msg_deleted_success' => 'Registro eliminado con éxito',
    'msg_ajax_error' => 'Petición fallida',
    'msg_error_title' => 'Error',
    'msg_error_text' => 'Error al eliminar el registro',
    'msg_dont_have_records' => 'No existen registros',

];

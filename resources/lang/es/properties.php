<?php 

return [
	/**
 	* Lists
 	*/
 	'lst_col_id' => 'Id',
 	'lst_col_building_number' => 'Nro de edificio',
 	'lst_col_property_number' => 'Nro de Propiedad', 
 	'lst_col_registered_property' => 'Registro de propiedad',
 	'lst_col_status' => 'Estatus',
 	'lst_col_coefficient' => 'Coeficiente',
 	'lst_col_person_relation' => 'Persona asociada',
 	'lst_col_accounting_account' => 'Nro cuenta contable',
 	'lst_col_payment_method' => 'Forma de cobro',
 	'lst_col_community' => 'Comunidad',
 	'lst_coefficient_title' => 'Listado de Coeficientes',
 	'coefficient_management_title' => 'Gestión de Coeficientes',
 	'coefficient_add_title' => 'Agregando Coeficiente',
 	'new_coefficient_title' => 'Nuevo Coeficiente',
];
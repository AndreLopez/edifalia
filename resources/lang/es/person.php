<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Person Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during community for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'management_title' => 'Gestión de Personas',
    'listing_title' => 'Listando Personas',
    'list_title' => 'Lista de Personas',
    'add_title' => 'Agregando Personas',
    'new_title' => 'Nueva Persona',

];

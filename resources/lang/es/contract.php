<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Contract Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during community for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'management_title' => 'Gestión de Contratos',
    'listing_title' => 'Listando Contratos',
    'list_title' => 'Lista de Contratos',
    'add_title' => 'Agregando   Contrato',
    'new_title' => 'Nuevo Contrato',

];

<?php

return [
    'subject' => 'Reinicio de contraseña',
    'greeting' => 'Hola',
    'receive_request' => 'Te enviamos éste correo porque recibimos una solicitud de reseteo de contraseña para su cuenta.',
    'action' => 'Restablecer contraseña',
    'delete_email' => 'Si usted no realizó ninguna solicitud, simplemente elimine éste correo.',
    'regards' => 'Saludos',
    'trouble' => 'Si tienes problemas al hacer clic en el botón ":button", copia y pega la URL a continuación en tu navegador web:',
    'attachment' => 'Debes revisar el archivo adjunto',
    'kind_regards' => 'Saludos cordiales',
    'issue_receipt' => [
        'name' => 'Edifalia',
        'receipt' => 'Recibo',
    ],
];
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Receipt Dues Language Lines
    |--------------------------------------------------------------------------
    */

    'management_title' => 'Receipt Due Management',
    'listing_title' => 'Listing Receipt Dues',
    'list_title' => 'Receipt Dues List',
    'add_title' => 'Adding Receipt Due',
    'new_title' => 'New Receipt Due',
    'update_title' => 'Updating Cuota Recibo',
    'edit_title' => 'Edit Cuota Recibo',
    'view_title' => 'Receipt Due Detail',
    'table_id' => 'Id',
    'table_bloq' => 'Bloq.',
    'table_property' => 'Propertiy',
    'table_payer' => 'Payer',
    'table_concept' => 'Concept',
    'table_amount' => 'Amount',
    'table_month' => 'Month',
    'table_year' => 'Year',

];

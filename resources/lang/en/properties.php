<?php 

return [
	/**
 	* Lists
 	*/
 	'lst_col_id' => 'Id',
 	'lst_col_building_number' => 'Building number',
 	'lst_col_property_number' => 'Property number', 
 	'lst_col_registered_property' => 'Property registration',
 	'lst_col_status' => 'Status',
 	'lst_col_coefficient' => 'Coefficient',
 	'lst_col_person_relation' => 'Person relation',
 	'lst_col_accounting_account' => 'Accounting Account',
 	'lst_col_payment_method' => 'Payment method',
 	'lst_col_community' => 'Community',
 	'lst_coefficient_title' => 'Coefficients List',
 	'coefficient_management_title' => 'Coefficient Management',
 	'coefficient_add_title' => 'Adding Coefficient',
 	'new_coefficient_title' => 'New Coefficient',
];
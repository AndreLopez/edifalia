<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Panel Info Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during panel info for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'box_languages_title' => 'LANGUAGES',
    'box_countries_title' => 'COUNTRIES',
    'box_status_title' => 'STATUS',
    'box_payment_methods_title' => 'PAYMENT METHODS',
    'box_contact_types_title' => 'CONTACT TYPES',
    'box_postal_codes_title' => 'POSTAL CODES',
    'box_position_title' => 'POSITIONS',
    'box_currency_title' => 'CURRENCIES',
    'box_zone_title' => 'ZONES',

];

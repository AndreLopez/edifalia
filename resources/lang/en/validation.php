<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => 'The :attribute is not a valid URL.',
    'after'                => 'The :attribute must be a date after :date.',
    'alpha'                => 'The :attribute may only contain letters.',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'between'              => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => 'The :attribute confirmation does not match.',
    'date'                 => 'The :attribute is not a valid date.',
    'date_format'          => 'The :attribute does not match the format :format.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'dimensions'           => 'The :attribute has invalid image dimensions.',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => 'The :attribute must be a valid email address.',
    'exists'               => 'The selected :attribute is invalid.',
    'file'                 => 'The :attribute must be a file.',
    'filled'               => 'The :attribute field is required.',
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'mimetypes'            => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'The :attribute must be at least :min characters.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => 'The :attribute must be a number.',
    'present'              => 'The :attribute field must be present.',
    'regex'                => 'The :attribute format is invalid.',
    'required'             => 'The :attribute field is required.',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'The :attribute must be a string.',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => 'The :attribute has already been taken.',
    'uploaded'             => 'The :attribute failed to upload.',
    'url'                  => 'The :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'communities.*' => [
            'required' => 'You must select at least one community.',
            'numeric' => 'The selected communities are invalid.',
            'exists' => 'The selected communities are invalid.',
        ],

        'tables.*' => [
            'required' => 'The "select table(s)" option is not correct. You should only mark the table(s).',
        ],

        'owners.*' => [
            'numeric' => 'The selected permissions are invalid.',
            'exists' => 'The selected permissions are invalid.',
        ],

        'general.*' => [
            'numeric' => 'The general amount is invalid.',
        ],

        'previous.*' => [
            'numeric' => 'The previous amount is invalid.',
        ],

        'current.*' => [
            'numeric' => 'The current amount is invalid.',
        ],

        'consumption.*' => [
            'numeric' => 'The consumption amount is invalid.',
        ],

        'ip_list.*' => [
            'ip' => 'You have entered an invalid Ip, try again.',
        ],

        'invoicingConcepts.*' => [
            'numeric' => 'The selected invoicing concepts are invalid.',
            'exists' => 'The selected invoicing concepts are invalid.',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'position_id'           => 'Position',
        'office_id'             => 'Office',
        'postal_code_id'        => 'Postal code',
        'community_id'          => 'Community',
        'page_id'               => 'Page',
        'tag_id'                => 'Tag',
        'model_id'              => 'Model',
        'reception_id'          => 'Reception',
        'reason_id'             => 'Reason',
        'supplier_id'           => 'Supplier',
        'sector_id'             => 'Sector',
        'type_id'               => 'Communicator type',
        'file1'                 => 'File 1',
        'file2'                 => 'File 2',
        'file3'                 => 'File 3',
        'file4'                 => 'File 4',
        'file_title1'           => 'File title 1',
        'file_title2'           => 'File title 2',
        'file_title3'           => 'File title 3',
        'file_title4'           => 'File title 4',
        'publish_file1'         => 'Publish file 1',
        'publish_file2'         => 'Publish file 2',
        'publish_file3'         => 'Publish file 3',
        'publish_file4'         => 'Publish file 4',
        'invoicing_concept_id'  => 'Invoicing concept',
        'rate_id'               => 'Rate',
        'signature_main'        => 'Signature',
        'aaff_program'          => 'Admón program, Farms used',
        'aaff_email'            => 'AAFF E-mail',
        'language_id'           => 'Language',
        'currency_id'           => 'Currency',
        'continent_id'          => 'Continent',
        'zone_id'               => 'Zone',
        'number_format_id'      => 'Number format',
        'date_format_id'        => 'Date format',
        'charge_date'           => 'Charge date to owner',
        'bank_request'          => 'Ask the bank for the payment of the remittance',
        'payment_date'          => 'Date of payment of the remittance',
        'issue_receipt_id'      => 'Issue receipt',
        'property_id'           => 'Property',
    ],

];

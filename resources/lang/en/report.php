<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Reports Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during reports for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'logo_mini_alt' => 'Logotipo',
    'logo_lg_alt' => 'Logotipo de la aplicación',

];

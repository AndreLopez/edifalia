<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Forms Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during forms for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'select_yes' => 'Yes',
    'select_not' => 'Not',
    'select_null_option' => 'Selection',
    'name_label' => 'Name',
    'name_placeholder' => 'Specify the name',
    'acronym_label' => 'Acronym',
    'acronym_placeholder' => 'Specify the acronym',
    'type_label' => 'Type',
    'type_placeholder' => 'Specify the type',
    'code_label' => 'Code',
    'code_placeholder' => 'Specify the code',
    'country_label' => 'Country',
    'country_placeholder' => 'Specify the country',
    'town_label' => 'Town',
    'town_placeholder' => 'Specify the town',
    'province_label' => 'Province',
    'province_placeholder' => 'Specify the province',
    'region_label' => 'Region',
    'region_placeholder' => 'Specify the region',
    'address_label' => 'Address',
    'address_placeholder' => 'Specify the address',
    'nif_label' => 'N.I.F.',
    'nif_placeholder' => 'Fiscal Identification Number',
    'structure_year_label' => 'Structure Year',
    'structure_year_placeholder' => '0',
    'elevator_number_label' => 'Elevator Numbers',
    'elevator_number_placeholder' => '0',
    'platform_accounting_label' => 'Platform Accounting',
    'longitude_label' => 'Longitude',
    'longitude_placeholder' => 'Longitude coordinate',
    'latitude_label' => 'Latitude',
    'latitude_placeholder' => 'Latitude coordinate',
    'note_label' => 'Observations',
    'note_placeholder' => 'Specify the notes or observations',
    'status_label' => 'Status',
    'postal_code_label' => 'Postal Code',
    'active' => 'Active',
    'active_yes' => 'YES',
    'active_no' => 'NOT',
    'president_name_label' => 'President',
    'president_name_placeholder' => 'Add the President name',
    'bank_accounts_label' => 'Bank Accounts',
    'iban_placeholder' => 'IBAN',
    'bank_account_sepa_label' => 'Bank Account (SEPA)',
    'sufix_label' => 'Sufix',
    'accounting_account_label' => 'Accounting Account',
    'accounting_account_placeholder' => 'Enter accounting account',
    'account_title_label' => 'Account Title',
    'account_title_placeholder' => 'Enter accounting title',
    'bic_label' => 'BIC',
    'other_treasury_accounts' => 'Other Treasury Accounts',

    'billing_concept_title_1_label' => 'Reduced Title 1',
    'billing_concept_title_1_placeholder' => 'Specify reduced title 1',
    'billing_concept_title_2_label' => 'Reduced Title 2',
    'billing_concept_title_2_placeholder' => 'Specify reduced title 2'

     /**
     *  Form Property
     */
    'building_number_label' => 'Building number',
    'building_number_placeholder' => 'Enter the building number',
    'property_number_label' => 'Property number',
    'property_number_placeholder' => 'Enter the property number',
    'registered_property_label' => 'Property registration',
    'registered_property_placeholder' => 'Enter registration property',
    'status_label' => 'Status',
    'coefficient_label' => 'Coefficient',
    'coefficient_placeholder' => 'Enter the coefficient',
    'coefficient_total_label' => 'Total Coefficient',
    'coefficient_individual' => 'Individual coefficient',
    'coefficient_calc_tooltip' => 'Calculating the individual coefficient equitably among all properties',
    'group_properties_label' => 'Group properties',
    'group_concepts_label' => 'Group concepts',
    'person_relation_label' => 'Person relation',
    'person_relation_placeholder' => 'Enter related person',
    'community_label' => 'Community',
    'community_placeholder' => 'Enter Community',
    'accounting_account_label' => 'Accounting Account',
    'accounting_account_placeholder' => 'Enter accounting account',
    'payment_method_label' => 'Payment method',
    'payment_method_placeholder' => 'Enter payment method',
    'community_select_label' => 'Communities',
    'property_label' => 'Property',
    'properties_label' => 'Properties',
];

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Community Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during community for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'management_title' => 'Community Management',
    'listing_title' => 'Listing Communities',
    'list_title' => 'Communities List',
    'add_title' => 'Adding Community',
    'new_title' => 'New Community',

];

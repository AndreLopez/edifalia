<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Tooltip Text Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during tooltip text for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'see_listing'   => 'See listing',
    'add_record' => 'Add record',
    'add_bank_account' => 'Add bank account',
    'add_accounting_account' => 'Add accounting account',

];

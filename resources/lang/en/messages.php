<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Messages Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during messages for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'msg_info_title' => 'Information',
    'msg_delete_confirm' => 'Are you sure to deleting record?',
    'msg_record_success' => 'Successfully stored record',
    'msg_updated_success' => 'Successfully updated record',
    'msg_deleted_success' => 'Successfully deleted record',
    'msg_ajax_error' => 'Error Request',
    'msg_error_title' => 'Error',
    'msg_error_text' => 'Failed to delete record',
    'msg_dont_have_records' => 'Don\'t have records',

];

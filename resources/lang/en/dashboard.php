<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Dashboard Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during dashboard ui for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'welcome' => 'Welcome',
    'title' => 'Dashboard',
    'main' => 'Main',
    'home' => 'Home',
    'profile'   => 'Profile',
    'logout' => 'Sign out',
    'nav_title' => 'MAIN NAVIGATION',
    'mnu_data' => 'Data',

    'mnu_communities' => 'Communities',
    'mnu_properties' => 'Properties',
    'mnu_people' => 'People',
    'mnu_suppliers' => 'Suppliers',
    'mnu_coefficients' => 'Coefficients',
    'mnu_contracts' => 'Contracts',
    'mnu_postal_codes' => 'Postal Codes',
    'mnu_tables' => 'Tables',
    'mnu_invoicing_concept' => 'Invoicing Concept',
    'mnu_properties_type' => 'Property Types',
    'mnu_management' => 'Management',
    'mnu_post_docs' => 'Post Documents',
    'mnu_chat_channels' => 'Chat Channels',
    'mnu_templates' => 'Templates',
    'mnu_incidences' => 'Incidences',
    'mnu_budgets' => 'Budgets',
    'mnu_counters_reading' => 'Counters Readings',
    'mnu_quota_receipts' => 'Quota Receipts',
    'mnu_issue_receipts' => 'Issue Receipts',
    'mnu_receipts_to_bank' => 'Receipts to Bank',
    'mnu_charges' => 'Charges',
    'mnu_payments' => 'Payments',
    'mnu_predefined_pay' => 'Predefined Payments',
    'mnu_treasury' => 'Treasury',
    'mnu_email' => 'Email',
    'mnu_labels' => 'Labels',
    'mnu_mailing_sms' => 'Mailing and SMS',
    'mnu_received' => 'Received',
    'mnu_sent' => 'Sent',
    'mnu_sending_logalty' => 'Sending LOGALTY',
    'mnu_mail_templates' => 'Mail Templates',
    'mnu_reports' => 'Reports',
    'mnu_dossier_sign' => 'Dossier Signatures',
    'mnu_meetings' => 'Meetings',
    'mnu_economic_reports' => 'Economic Reports',
    'mnu_liquidate_cons' => 'Liquidate Consumption',
    'mnu_liquidations' => 'Liquidations',
    'mnu_compare_budget_spending' => 'Compare Budget / Spending',
    'mnu_accounting' => 'Accounting',
    'mnu_accounting_plan' => 'Accounting Plan',
    'mnu_daily' => 'Daily',
    'mnu_greater' => 'Greater',
    'mnu_balance' => 'Balance Sums and Expenses',
    'mnu_closing_seat' => 'Closing Seat',
    'mnu_standard' => 'Standard 43',
    'mnu_utilities' => 'Utilities',
    'mnu_user_data' => 'Primary User Data',
    'mnu_office_management' => 'Offices Management',
    'mnu_user_access' => 'User Management Access',
    'mnu_backups' => 'Backups',
    'mnu_intranet_access' => 'Intranet Access',
    'mnu_vo_visits' => 'Virtual Office Visits',
    'mnu_online_users' => 'Online Users',
    'mnu_action_history' => 'Action history',
    'mnu_pages' => 'Pages',
    'mnu_tags' => 'Tags',
    'mnu_models' => 'Models',
    'mnu_communicator_types' => 'Communicator Types',
    'mnu_receptions' => 'Receptions',
    'mnu_reasons' => 'Reasons',
    'mnu_sectors' => 'Sectors',
    'mnu_positions' => 'Positions',
    'mnu_rates' => 'Rates',
    'mnu_currencies' => 'Currencies',
    'mnu_zones' => 'Zones',

    'titles' => [
        'offices_list' => 'Offices list',
        'add_office' => 'Adding office',
        'edit_office' => 'Editing office',
        'office_info' => 'Office information',
        'delete_record' => 'Delete record',
        'communities' => 'Communities',
        'add_employee' => 'Adding employee',
        'edit_employee' => 'Editing employee',
        'employees_list' => 'Employees list',
        'employee_info' => 'Employee information',
        'positions_list' => 'Positions list',
        'add_position' => 'Adding position',
        'edit_position' => 'Editing position',
        'login' => 'Log in',
        'signin' => 'Enter data to login',
        'users_online_list' => 'Users online list',
        'accesses_list' => 'Accesses list',
        'history' => 'Actions list',
        'not_found' => 'Page not found',
        'permissions' => 'Permissions',
        'tables_list' => 'Tables list',
        'pages_list' => 'Pages list',
        'add_page' => 'Adding page',
        'edit_page' => 'Editing page',
        'tags_list' => 'Tags list',
        'add_tag' => 'Adding tag',
        'edit_tag' => 'Editing tag',
        'doc_list' => 'Documents list',
        'add_doc' => 'Adding document',
        'edit_doc' => 'Editing document',
        'doc_info' => 'Document information',
        'add_new_tag' => 'Adding new tag',
        'models_list' => 'Models list',
        'add_model' => 'Adding model',
        'edit_model' => 'Editing model',
        'template_list' => 'Templates list',
        'add_template' => 'Adding template',
        'edit_template' => 'Editing template',
        'incidences_list' => 'Incidences list',
        'add_incidence' => 'Adding incidence',
        'edit_incidence' => 'Editing incidence',
        'incidence_info' => 'Incidence information',
        'communicator_types_list' => 'Communicator types list',
        'add_communicator_type' => 'Adding communicator type',
        'edit_communicator_type' => 'Editing communicator type',
        'receptions_list' => 'Receptions list',
        'add_reception' => 'Adding reception',
        'edit_reception' => 'Editing reception',
        'reasons_list' => 'Reasons list',
        'add_reason' => 'Adding reason',
        'edit_reason' => 'Editing reason',
        'sectors_list' => 'Sectors list',
        'add_sector' => 'Adding sector',
        'edit_sector' => 'Editing sector',
        'published' => 'Published',
        'unpublished' => 'Unpublished',
        'supplier' => 'Supplier',
        'concluded' => 'Concluded',
        'unconcluded' => 'Unconcluded',
        'communicator' => 'Communicator',
        'invoicing_concepts_list' => 'Invoicing concepts list',
        'add_invoicing_concept' => 'Adding invoicing concept',
        'edit_invoicing_concept' => 'Editing invoicing concept',
        'budgets_list' => 'Budgets list',
        'add_budget' => 'Adding budget',
        'edit_budget' => 'Editing budget',
        'rates_list' => 'Rates list',
        'add_rate' => 'Adding rate',
        'edit_rate' => 'Editing rate',
        'readings_list' => 'Counters Readings list',
        'add_reading' => 'Adding counter reading',
        'edit_reading' => 'Editing counter reading',
        'edit_main_user' => 'Editing main user data',
        'currencies_list' => 'Currencies list',
        'add_currency' => 'Adding currency',
        'edit_currency' => 'Editing currency',
        'zones_list' => 'Zones list',
        'add_zone' => 'Adding zone',
        'edit_zone' => 'Editing zone',
        'issue_receipts_list' => 'Issue receipts list',
        'add_issue_receipt' => 'Adding issue receipt',
        'edit_issue_receipt' => 'Editing issue receipt',
        'issue_receipt_info' => 'Issue receipt information',
        'receipts_to_banks_list' => 'List of receipts to banks',
        'add_receipts_to_bank' => 'Adding Receipts to bank',
        'receipts_to_bank_info' => 'Information of receipts to bank',
        'pdf_generated' => 'PDF generated',
        'charges_list' => 'Charges list',
        'add_charge' => 'Adding charge',
        'edit_charge' => 'Editing charge',
    ],

    'buttons' => [
        'new' => 'New',
        'edit' => 'Edit',
        'delete' => 'Delete',
        'info' => 'More information',
        'back' => 'Back',
        'save' => 'Save',
        'update' => 'Update',
        'yes_delete' => 'Yes, I am sure to remove',
        'no_delete' => 'Upon reflection, I do not want to remove',
        'undo' => 'Undo',
        'login' => 'Log in',
        'home' => 'Go to home',
        'refresh' => 'Refresh',
        'export' => 'Export',
        'add' => 'Add',
        'search' => 'Search',
    ],

    'messages' => [
        'notice' => 'Notice!',
        'no_stored_records' => 'There are no stored records.',
        'errors' => 'Check the following errors',
        'created' => 'Record created successfully.',
        'updated' => 'Record edited successfully.',
        'deleted' => 'Record deleted successfully.',
        'empty_community' => 'You must select at least one community.',
        'no_communities' => 'There are no available communities. You can not create an office without assigning community.',
        'delete_record' => 'Are you sure you want to delete this record?',
        'wait' => 'Please wait ...',
        'error' => 'An error has occurred, please refresh the page.',
        'empty_permissions' => 'You must select at least one permission.',
        'saved' => 'Saved successfully.',
        'created_record' => 'created a record',
        'deleted_record' => 'remove a record',
        'undelete_record' => 'restored a record',
        'changed_record' => 'changed the field ',
        'from' => ' from ',
        'to' => ' to ',
        'no_pages_tags' => 'There are no available pages or tags. You can not create an document without assigning page and tag.',
        'no_doc' => 'Document not found.',
        'file_error' => 'There was an error uploading, verify the file ',
        'try_again' => ', try again.',
        'doc_public' => 'Document public',
        'all_offices' => 'All offices',
        'no_models_offices' => 'There are no available models or offices. You can not create an template without assigning model and office.',
        'no_file' => 'File not found.',
        'no_attachments' => 'The incidence has no attachments.',
        'no_communicator' => 'The incidence has no communicator.',
        'no_properties' => 'The selected community has no assigned properties. You can save and add expenses later.',
        'not_zones' => 'Continent does not have zones.',
        'not_edit_record' => 'You can not edit the record.',
        'no_receipt_dues' => 'This community currently has no receipt dues',
        'no_recent_receipt' => 'There are no recent receipt dues for selected communities.',
        'no_receipts' => 'There are no associated receipts for this community.',
        'no_properties' => 'There are no associated properties for this community.',
    ],

    'tables' => [
        'actions' => 'Actions',
        'identifier' => 'Identifier',
        'name' => 'Name',
        'last_name' => 'Last name',
        'address' => 'Address',
        'phone' => 'Phone',
        'fax' => 'Fax',
        'email' => 'E-mail',
        'postal_code' => 'Postal code',
        'population' => 'Population',
        'province' => 'Province',
        'code' => 'Code',
        'nif' => 'NIF',
        'position' => 'Position',
        'office' => 'Office',
        'active' => 'Active',
        'user' => 'User',
        'connected_from' => 'Connected from',
        'ip_address' => 'IP address',
        'start_date' => 'Start date',
        'end_date' => 'End date',
        'access' => 'Access',
        'time' => 'Time',
        'user_action' => 'User action',
        'action_date' => 'Execution date',
        'user_name' => 'User name',
        'dni' => 'D.N.I',
        'department' => 'Department',
        'extension' => 'Extension',
        'country' => 'Country',
        'registration_date' => 'Registration date',
        'publication_date' => 'Publication date',
        'title' => 'Title',
        'page' => 'Page',
        'tag' => 'Tag',
        'community' => 'Community',
        'permissions' => 'Permissions',
        'document' => 'Document',
        'model' => 'Model',
        'priority' => 'Priority',
        'reason' => 'Reason',
        'state' => 'State',
        'reception' => 'Reception',
        'public' => 'Public',
        'concluded' => 'Concluded',
        'details' => 'Details',
        'sending' => 'Sending',
        'sector' => 'Sector',
        'supplier' => 'Supplier',
        'other_annotations' => 'Other annotations',
        'type' => 'Type',
        'other_phone' => 'Other phone',
        'invoicing_concept' => 'Invoicing concept',
        'rate' => 'Rate',
        'symbol' => 'Symbol',
        'continent' => 'Continent',
        'date' => 'Date',
        'message' => 'Message',
        'issued' => 'Issued',
        'amount_to_pay' => 'Amount to pay',
        'file_format' => 'File format',
        'date_sent_to_bank' => 'Date of sent to bank',
        'charge_date' => 'Charge date',
        'to' => ' to: ',
        'pdf' => 'PDF - Click to see',
        'floor' => 'Floor',
        'receipt' => 'Receipt',
        'property' => 'Property',
        'owner' => 'Owner',
        'amount' => 'Amount',
        'pending' => 'Pending',
        'charge_type' => 'Charge type',
    ],

    'fields' => [
        'active' => 'Active',
        'remember' => 'Remember me',
        'identifier' => 'Identifier',
        'name' => 'Name',
        'address' => 'Address',
        'postal_code' => 'Postal code',
        'population' => 'Population',
        'province' => 'Province',
        'phone' => 'Phone',
        'phone2' => 'Phone 2',
        'fax' => 'Fax',
        'email' => 'E-mail',
        'user_name' => 'User name',
        'deactivate' => 'Deactivate',
        'password' => 'Password',
        'password_confirmation' => 'Confirm password',
        'last_name' => 'Last name',
        'dni' => 'D.N.I.',
        'extension' => 'Extension',
        'department' => 'Department',
        'access_data' => 'Access to Data',
        'access_management' => 'Access to Management',
        'access_mail' => 'Access to Mail',
        'access_incidents' => 'Access to Incidents',
        'access_reports' => 'Access to Reports',
        'access_accounting' => 'Access to Accounting',
        'position' => 'Position',
        'office' => 'Office',
        'disable_user' => 'Check box to disable user',
        'select_communities' => 'Select one or more communities',
        'see_communities' => 'select an office to see the communities',
        'select_all' => 'Select all',
        'select_position' => 'Select position',
        'select_office' => 'Select office',
        'select_permissions' => 'Select permissions',
        'select_postal_code' => 'Select postal code',
        'remember_token' => 'Token',
        'select_tables' => 'Select table(s)',
        'ctrl_key' => 'Hold down the Ctrl key to select multiple',
        'format' => 'Select format',
        'csv' => 'CSV',
        'excel' => 'Excel',
        'sql' => 'SQL',
        'community' => 'Community',
        'select_community' => 'Select community',
        'select_page' => 'Select page',
        'select_tag' => 'Select tag',
        'select_owner' => 'Select owner(s)',
        'doc_public' => 'If you do not choose any, the document will be public',
        'document' => 'Document (pdf, doc, docx, xls, xlsx, jpg, jpeg or png)',
        'current_doc' => 'Current document: ',
        'title' => 'Title',
        'publication_date' => 'Publication date',
        'page' => 'Page',
        'tag' => 'Tag',
        'permissions' => 'Permissions',
        'current_file' => 'Current file: ',
        'file' => 'File',
        'model' => 'Model',
        'select_model' => 'Select model',
        'temp_all_office' => 'If you do not choose any, the template will be for all offices',
        'priority' => 'Prioridad',
        'select_priority' => 'Select priority',
        'reception' => 'Recepción',
        'select_reception' => 'Select reception',
        'normal' => 'Normal',
        'medium' => 'Medium',
        'high' => 'High',
        'reason' => 'Reason',
        'select_reason' => 'Select reason',
        'controls' => 'Controls',
        'select_controls' => 'Assign tracking to',
        'publish' => 'Publish (optional)',
        'conclude' => 'Conclude (optional)',
        'incidence_details' => 'Incidence details (optional)',
        'other_annotations' => 'Other annotations that are not published (optional)',
        'communicator' => 'Communicator (optional)',
        'communicator_type' => 'Communicator type',
        'select_communicator_type' => 'Select type communicator',
        'other_phone' => 'Other phone',
        'supplier' => 'Supplier',
        'select_supplier' => 'Select supplier',
        'sector' => 'Sector',
        'select_sector' => 'Select sector',
        'send' => 'Send',
        'all' => 'All',
        'sms' => 'Text message',
        'attach_files' => 'Attach files to the incidence (optional)',
        'file1' => 'File 1',
        'file_title1' => 'File title 1',
        'publish_file1' => 'Publish file 1',
        'file2' => 'File 2',
        'file_title2' => 'File title 2',
        'publish_file2' => 'Publish file 2',
        'file3' => 'File 3',
        'file_title3' => 'File title 3',
        'publish_file3' => 'Publish file 3',
        'file4' => 'File 4',
        'file_title4' => 'File title 4',
        'publish_file4' => 'Publish file 4',
        'attached_files' => 'Attached files',
        'signature' => 'Your signature in HTML. It will appear in Mail -> Mailing and SMS when you select this user in the list of accounts Send from',
        'text' => 'Place the text here',
        'budget_title' => 'Budget title',
        'saved_budgets' => 'Saved budgets (optional)',
        'select_budget' => 'Select budget',
        'no_budget' => 'There are no budget',
        'invoicing_concept' => 'Invoicing concept',
        'select_invoicing_concept' => 'Select invoicing concept',
        'periodicity' => 'Periodicity',
        'select_periodicity' => 'Select periodicity',
        'rounding' => 'Rounding (digits)',
        'digits_number' => 'Digits number',
        'duration' => 'Duration',
        'months' => 'Months',
        'quota' => 'Schedule this quota starting from',
        'month' => 'Month',
        'year' => 'Year',
        'expenses' => 'Properties expenses',
        'general' => 'General amount',
        'amount' => 'Amount receipt',
        'select_reading' => 'Select counters reading',
        'no_reading' => 'There are no counters reading',
        'saved_readings' => 'Saved counters readings (optional)',
        'select_rate' => 'Select rate',
        'charge' => 'Month and year to charge this consumption',
        'concept_receipt' => 'Concept in receipt',
        'previous' => 'Previous',
        'current' => 'Current',
        'consumption' => 'Consumption',
        'date' => 'Date',
        'rate' => 'Rate',
        'company' => 'Company',
        'signature_main' => 'Signature',
        'collegiate_number' => 'Collegiate number',
        'aaff_program' => 'Admón program, Farms used',
        'letterhead' => 'Letterhead',
        'ip_list_option' => 'Restriction of access by ip to intranet',
        'access_allowed_ip' => 'Access allowed from all IP networks except those listed below',
        'access_denied_ip' => 'Access denied for all IP networks except those listed below',
        'enter_ip' => 'Enter the IP address and click the "Add" button. (Optional)',
        'ip_list' => 'List: Select all the IP you want to save. Non-selects will be deleted.',
        'neither' => 'Neither',
        'symbol' => 'Symbol',
        'language' => 'Language',
        'select_language' => 'Select language',
        'bank_account' => 'Bank account',
        'bank_account_info' => 'CAC (Customer account code)',
        'currency' => 'Currency',
        'select_currency' => 'Select currency',
        'numbers' => 'Numbers (format)',
        'select_number_format' => 'Select number format',
        'dates' => 'Dates (format)',
        'select_date_format' => 'Select date format',
        'continent' => 'Continent',
        'select_continent' => 'Select continent',
        'zone' => 'Zone',
        'select_zone' => 'Select zone',
        'amounts' => 'Amounts',
        'search' => 'Search',
        'message' => 'Message to include',
        'invoicing_concepts_exclude' => 'Invoicing concepts to exclude',
        'issue' => 'Issue',
        'optional' => '(Optional)',
        'receipts_issued_date' => 'Receipts issued whose date of dispatch to bank is',
        'select_state' => 'Select state',
        'payment_date' => 'Payment date',
        'charge_date_owner' => 'Charge date to owner',
        'remittance_pay' => 'Ask the bank to the remittance pay in a single bank note',
        'payment_remittance_daily' => 'To count the payment of this remittance in the daily with date',
        'presenters' => 'Presenters CSB-19',
        'issue_receipt' => 'Issue Receipt',
        'property' => 'Property',
        'pending' => 'Pending',
        'charge_type' => 'Charge type',
        'select_charge_type' => 'Select charge type',
        'select_receipts' => 'Select receipt',
        'select_properties' => 'Select property',
        'from' => 'From',
        'until' => 'Until',
    ],

    'periodicity' => [
        'monthly' => 'Monthly',
        'quarterly' => 'Quarterly',
        'biannual' => 'Biannual',
        'annual' => 'Annual',
    ],

    'state' => [
        'previous' => 'Previous',
        'later' => 'Later',
    ],

    'file_format' => [
        '1' => 'Notebook 19',
        '2' => 'SEPA (CORE)',
        '3' => 'SEPA (COR1)',
    ],

    'charge_type' => [
        '1' => 'Domiciled',
        '2' => 'Transfer',
    ],
];

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Common Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during common for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'new_record_title'   => 'New Record',
    'new_record_submit' => 'Save',
    'edit_record_title' => 'Update Record',
    'edit_record_submit' => 'Update',
    'registers_label' => 'Registers',
    'total_label' => 'Total',
    'lang_management_title' => 'Language Management',
    'lang_list_title' => 'Listing languages',
    'lang_add_title' => 'Adding language',
    'lang_upd_title' => 'Updating language',
    'billing_concept_management_title' => 'Billing Concepts Management', 
    'billing_concept_list_title' => 'Listing Billing Concepts',
    'contact_type_management_title' => 'Contact Types Management',
    'contact_type_list_title' => 'Listing Contact Types',
    'contact_type_add_title' => 'Adding Contact Types',
    'contact_type_upd_title' => 'Updating Contact Types',
    'country_management_title' => 'Countries Management',
    'country_list_title' => 'Listing Countries',
    'country_add_title' => 'Adding Country',
    'country_upd_title' => 'Updating Country',
    'payment_method_management_title' => 'Payment Methods Management',
    'payment_method_list_title' => 'Listing Payment Methods',
    'payment_method_add_title' => 'Adding Payment Method',
    'payment_method_upd_title' => 'Updating Payment Method',
    'position_management_title' => 'Positions Management',
    'position_list_title' => 'Listing Positions',
    'position_add_title' => 'Adding Position',
    'position_upd_title' => 'Updating Position',
    'postal_code_management_title' => 'Postal Codes Management',
    'postal_code_list_title' => 'Listing Postal Codes',
    'postal_code_add_title' => 'Adding Postal Code',
    'postal_code_show_title' => 'Showing Postal Code',
    'postal_code_upd_title' => 'Updating Postal Code',
    'status_management_title' => 'Status Management',
    'status_list_title' => 'Listing Status',
    'status_add_title' => 'Adding Status',
    'status_upd_title' => 'Updating Status',
    'billing_concept_add_title' => 'Adding Billing Concept',
    'billing_concept_upd_title' => 'Updating Billing Concept',


    'property_management_title' => 'Property Management',
    'property_list_title' => 'Property Listing',
    'property_add_title' => 'Adding Property', 
    'property_upd_title' => 'Updating property', 


    'property_type_management_title' => 'Property Type Management',
    'property_type_list_title' => 'Listing Property Type',
    'property_type_add_title' => 'Adding Property Type', 
    'property_type_upd_title' => 'Updating Property Type',

    /**
     * Buttons
     */
    'btn_cancel_text' => 'Cancel',
    'btn_cancel_tooltip' => 'Click to cancel',
    'btn_clear_text' => 'Clear',
    'btn_clear_tooltip' => 'Click to clear the form',
    'btn_save_text' => 'Save',
    'btn_save_tooltip' => 'Click to record information',
    'btn_back' => 'Go back',

    /**
     * Lists
     */
    'lst_new_record' => 'New',
    'lst_action_title' => 'Action',
    'lst_edit_record' => 'Edit record',
    'lst_delete_record' => 'Delete record',
    'lst_language_title' => 'Languages List',
    'lst_country_title' => 'Countries List',
    'lst_status_title' => 'Status List',
    'lst_payment_method_title' => 'Payment Methods List',
    'lst_contact_type_title' => 'Contact Types List',
    'lst_postal_code_title' => 'Postal Codes List',
    'lst_position_title' => 'Positions List',
    'lst_col_name' => 'Name',
    'lst_col_acronym' => 'Acronym',
    'lst_col_type' => 'Type',
    'lst_col_code' => 'Code',
    'lst_col_country' => 'Country',
    'lst_col_province' => 'Province',
    'lst_col_group_properties' => 'Group properties',
    'lst_col_group_concepts' => 'Group concepts',
    'lst_act_edit' => 'Edit',
    'lst_act_delete' => 'Delete',
    'lst_act_show' => 'More information',
    'lst_property_title' => 'List Properties',
    'lst_property_type_title' => 'List Properties Types',


    /**
     * Forms
     */
    'new_language_title' => 'New Language',
    'edit_language_title' => 'Edit Language',
    'new_country_title' => 'New Country',
    'edit_country_title' => 'Edit Country',
    'new_status_title' => 'New Status',
    'edit_status_title' => 'Edit Status',
    'new_payment_method_title' => 'New Payment Method',
    'edit_payment_method_title' => 'Edit Payment Method',
    'new_contact_type_title' => 'New Contact Type',
    'edit_contact_type_title' => 'Edit Contact Type',
    'new_postal_code_title' => 'New Postal Code',
    'edit_postal_code_title' => 'Edit Postal Code',
    'show_postal_code_title' => 'Postal Code Info',
    'new_position_title' => 'New Position',
    'edit_position_title' => 'Edit Position',
    'new_billing_concept_type_title' => 'New Billing Concept',
    'edit_billing_concept_type_title' => 'Update Billing Concept',
    'new_property_title' => 'New Property',
    'edit_property_title' => 'Updating property',
    'new_property_type_title' => 'New Property Type',
    'edit_property_type_title' => 'Edit Property Type', 
     /**
     * Accessors Models
     */

    'affirmative_answer' => 'Yes',
    'negative_answer' => 'No'


];

<?php

return [
    'subject' => 'Reset Password',
    'greeting' => 'Hello',
    'receive_request' => 'You are receiving this email because we received a password reset request for your account.',
    'action' => 'Reset Password',
    'delete_email' => 'If you did not request a password reset, no further action is required.',
    'regards' => 'Regards',
    'trouble' => 'If you’re having trouble clicking the ":button" button, copy and paste the URL below into your web browser:',
    'attachment' => 'You should check the attachment',
    'kind_regards' => 'Kind regards',
    'issue_receipt' => [
        'name' => 'Edifalia',
        'receipt' => 'Receipt',
    ],
];
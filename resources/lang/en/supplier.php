<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Supplier Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during community for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'management_title' => 'Supplier Management',
    'listing_title' => 'Listing Suppliers',
    'list_title' => 'Suppliers List',
    'add_title' => 'Adding Supplier',
    'new_title' => 'New Supplier',

];

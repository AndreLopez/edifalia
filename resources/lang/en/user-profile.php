<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Common Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during common for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
 
'user_profile_management_title' => 'User Profile',
'position_list_title' => 'Profile',
'email_title' => 'Email', 
'created_at_title' => 'Created at',
'active_title' => 'Active',  
 'user_information_title' => 'User Information',
 'lst_edit_user' => 'Edit', 
 'email_insert' => 'Insert email', 
 'password_title' => 'Password', 
 'password_insert' => 'Insert New Password', 
 'confirm_password' => 'Confirm Password', 

'user_administrator' => 'Administrator', 
'user_employee' => 'Empleyee', 
'user_owner' => 'Owner', 
'user_supplier' => 'Supplier', 
'user_active_yes' => 'YES', 
'user_active_no' => 'NO', 

];


<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Sistema para la gestión de condominios y comunidades.">
    <meta name="keywords" content="Servicios, Condominio, Comunidades, Inmobiliaria">
    <meta name="author" content="Roldan Vargas, Gustavo Meza, Andrea Vargas, ISMCENTER">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="image/png" href="{{ asset('static/images/favicon.png') }}" />
    <title>EDIFALIA</title>
	{{-- Bootstrap styles --}}
    {!! Html::style('static/bootstrap/css/bootstrap.min.css') !!}
    {{-- AdminLTE --}}
    {!! Html::style('static/adminlte/css/AdminLTE.min.css') !!}
    {!! Html::style('static/adminlte/css/skins/_all-skins.min.css') !!}
    {{-- Select2 (required) --}}
    {!! Html::style('static/select2/css/select2.css') !!}
    {!! Html::style('static/select2/css/select2-bootstrap.css') !!}

    {{-- Datatables (required) --}}
    {!! Html::style('static/datatables/extensions/TableTools/css/dataTables.tableTools.min.css') !!}
    {!! Html::style('static/datatables/css/dataTables.bootstrap.css') !!}
    {!! Html::style('static/datatables/extensions/Responsive/css/dataTables.responsive.css') !!}
    {{-- Bootstrap Switch (required) --}}
    {!! Html::style('static/bootstrap-switch/css/bootstrap-switch.min.css') !!}
    {{-- Toastr Notifications (required) --}}
    {!! Html::style('static/toastr/css/toastr.min.css') !!}
    {{-- Icon Pack from ionicons, icomoon and font-awesome --}}
    {!! Html::style('static/ionicons/css/ionicons.min.css') !!}
    {!! Html::style('static/font-awesome/css/font-awesome.min.css') !!}
    {{-- Custom apps styles --}}
    {!! Html::style('static/css/styles.custom.css') !!}
    {{-- OpenLayers Styles and Scripts --}}
    {!! Html::style('http://openlayers.org/en/v3.15.1/css/ol.css') !!}
    {!! Html::script('http://openlayers.org/en/v3.15.1/build/ol.js') !!}
    {!! Html::style('static/ol3-geocoder/ol3-geocoder.min.css') !!}
    {!! Html::script('static/ol3-geocoder/ol3-geocoder-debug.js') !!}

    {{--iCheck for checkboxes and radio inputs--}}
    {!! Html::style('static/iCheck/all.css') !!}
    {{-- Toastr Notifications (required) --}}
    {!! Html::style('static/toastr/css/toastr.min.css') !!}
    {{-- Bootstrap-wysihtml5 is a javascript plugin --}}
    {!! Html::style('static/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.min.css') !!}
    {{-- Para styles extras --}}
    @section('styles')
    @show
</head>
@extends('layouts.dashboard')

@if (!empty($data['breadcrumb_link']))
	@section('breadcrumb_link')
		{{ $data['breadcrumb_link'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_icon']))
	@section('breadcrumb_icon')
		{{ $data['breadcrumb_icon'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_title']))
	@section('breadcrumb_title')
		{{ $data['breadcrumb_title'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_subtitle']))
	@section('breadcrumb_subtitle')
		{{ $data['breadcrumb_subtitle'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_map']))
	@section('breadcrumb_map')
		{{ $data['breadcrumb_map'] }}
	@endsection
@endif

@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title inline-block">
						<i class="glyphicon glyphicon-edit"></i>&#160;
						{{ $data['panel_title'] }}
					</h3>
					<div class="btn-group btn-group-xs pull-right">
						<a data-toggle="tooltip" class="btn btn-primary" title="@lang('common.btn_back')" 
						   href="{{ $data['breadcrumb_link'] }}">
						    <i class="fa fa-reply"></i>
						</a>
					</div>
				</div>
				@if(isset($new)) {!! Form::open($header) !!} @else {!! Form::model($model, $header) !!} @endif
					<div class="box-body">
						@include($fields_template)
					</div>
					<div class="box-footer">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							@if(isset($data['breadcrumb_link']))
								<button type="button" class="btn btn-default col-xs-12 col-sm-12 col-md-1 col-lg-1 col-md-offset-8 col-lg-offset-8 btn-form" data-toggle="tooltip" 
				            		title="{{ trans('common.btn_cancel_tooltip') }}" 
				            		onclick="location.href='{{ $data['breadcrumb_link'] }}'">
				                	<i class="glyphicon glyphicon-ban-circle"></i> @lang('common.btn_cancel_text')
					            </button>
					        @endif
							<button type="reset" class="btn btn-warning btn-form col-xs-12 col-sm-12 col-md-1 col-lg-1 @if(!isset($data['breadcrumb_link'])) col-md-offset-9 col-lg-offset-9 @endif" data-toggle="tooltip" 
										title="{{ trans('common.btn_clear_tooltip') }}">
					                <i class="fa fa-eraser"></i> @lang('common.btn_clear_text')
					        </button>
					        @if (isset($data['btn_record_submit']))
					        	<button type="submit" class="btn btn-info btn-form col-xs-12 col-sm-12 col-md-1 col-lg-1" data-toggle="tooltip" 
										title="{{ trans('common.btn_save_tooltip') }}">
					                <i class="fa fa-save"></i> {{ $data['btn_record_submit'] }}
					            	</button>
					        @endif
					         @if (isset($data['btn_send_email']))
					        	<button type="submit" class="btn btn-info btn-form col-xs-12 col-sm-12 col-md-1 col-lg-1" data-toggle="tooltip" 
										title="{{ trans('common.btn_save_tooltip') }}">
					                <i class="fa fa-envelope-o"></i> {{ $data['btn_send_email'] }}
					            </button>
					        @endif
								
						</div>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>

	@if (isset($person_id))
		<div id="new-person-form" style="display:none">
			@include('common._pop_up-create-person-to-properties')
		</div>
	@endif
	
@endsection



<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="shortcut icon" type="image/png" href="{{ asset('static/images/favicon.png') }}" />
    {!! Html::style('static/bootstrap/css/bootstrap.min.css') !!}
    {!! Html::style('static/ionicons/css/ionicons.min.css') !!}
    {!! Html::style('static/font-awesome/css/font-awesome.min.css') !!}
    {!! Html::style('static/adminlte/css/AdminLTE.min.css') !!}
    {!! Html::style('static/iCheck/square/blue.css') !!}
    <title>Edifalia | @yield('title', trans('dashboard.titles.login'))</title>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition login-page">
@yield('content')

{!! Html::script('static/js/jquery.min.js') !!}
{!! Html::script('static/bootstrap/js/bootstrap.min.js') !!}
{!! Html::script('static/iCheck/icheck.min.js') !!}

<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
</body>
</html>

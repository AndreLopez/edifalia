{{-- Scripts --}}
{{-- Jquery Library --}}
{!! Html::script('static/js/jquery.min.js') !!}
{{-- Bootstrap --}}
{!! Html::script('static/bootstrap/js/bootstrap.min.js') !!}
{{-- AdminLTE --}}
{!! Html::script('static/adminlte/js/app.min.js') !!}
{{-- Bootbox modal dialog --}}
{!! Html::script('static/bootbox/bootbox.min.js') !!}
{{-- Select2 (required) --}}
{!! Html::script('static/select2/js/select2.min.js') !!}
{{-- Bootstrap Switch (required) --}}
{!! Html::script('static/bootstrap-switch/js/bootstrap-switch.min.js') !!}
{{-- Toastr Notifications (required) --}}
{!! Html::script('static/toastr/js/toastr.min.js') !!}
{{-- Datatables (required) --}}
{!! Html::script('static/datatables/js/jquery.dataTables.min.js') !!}
{!! Html::script('static/datatables/extensions/TableTools/js/dataTables.tableTools.min.js') !!}
{!! Html::script('static/datatables/js/dataTables.bootstrap.js') !!}
{!! Html::script('static/datatables/extensions/Responsive/js/dataTables.responsive.min.js') !!}
{!! Html::script('https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places') !!}
{{-- Icheck (required) --}}
{!! Html::script('static/iCheck/icheck.min.js') !!}

{{-- Jquery Validation --}}
{!! Html::script('static/jquery-validation/dist/jquery.validate.min.js') !!}
{!! Html::script('static/jquery-validation/dist/additional-methods.min.js') !!}

{{--  Bootstrap-wysihtml5 is a javascript plugin --}}
{!! Html::script('static/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.min.js') !!}

{{-- Para scripts extras --}}
@section('scripts')
@show

<script>
    $(document).ready(function () {
        $("div#logout a").on("click", function(event){
            event.preventDefault();
            $("#logout-form").submit();
        });

        /*
         * Select2 Init
         */
        var select2 = $(".select2");
        if (select2.length > 0) {
            select2.select2({
                width: '100%'
            });
        }

        /*
         * wysihtml5 Init
         */
        $('.wysihtml5-textarea').wysihtml5();

    });
</script>

@include('layouts.toastr-messages')

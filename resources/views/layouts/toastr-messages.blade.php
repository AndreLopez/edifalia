<script>
    $(document).ready(function () {
        toastr.options = {
            "progressBar": true
        };
        @if(session('message'))
            //toastr.success("...");
            toastr.{{session('message')['alert']}}(
                    "{{session('message')['text']}}",
                    "@lang('dashboard.messages.notice')"
            );
            {{Session::forget('message')}}
        @endif
    });
</script>
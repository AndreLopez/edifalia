@extends('layouts.dashboard')

@if(!empty($data['breadcrumb_link']))
	@section('breadcrumb_link')
		{{ $data['breadcrumb_link'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_icon']))
	@section('breadcrumb_icon')
		{{ $data['breadcrumb_icon'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_title']))
	@section('breadcrumb_title')
		{{ $data['breadcrumb_title'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_subtitle']))
	@section('breadcrumb_subtitle')
		{{ $data['breadcrumb_subtitle'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_map']))
	@section('breadcrumb_map')
		{{ $data['breadcrumb_map'] }}
	@endsection
@endif

@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title inline-block">
						<i class="fa fa-eye"></i>&#160;
						{{ $data['panel_title'] }}
					</h3>
					<div class="btn-group btn-group-sm pull-right">
						<a data-toggle="tooltip" class="btn btn-primary" title="@lang('dashboard.buttons.back')" 
						   href="{{ $data['breadcrumb_link'] }}">
						    <i class="fa fa-reply"></i>
						</a>
					</div>
				</div>
				<div class="box-body table-responsive">
					@include($show_template)
				</div>
			</div>
		</div>
	</div>
@endsection
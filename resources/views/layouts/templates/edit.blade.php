@extends('layouts.dashboard')

@section('title','Editar Comunidad' . $comunidad->name)

@section('content')


<div class="col-md-9">
<div class="nav-tabs-custom" >
<ul class="nav nav-tabs">

<li >
  <a href="{{ route('registro.create')}}"  >Registro</a>  
</li>  

<li class="active">
  <a href="{{ route('registro.index') }}"  >Lista </a>  
</li>  



</ul>


<div class="tab-content">
<div class="tab-pane active">

{!! Form::open(['route' => ['registro.update',$comunidad], 'method' =>'PUT', 'class' => 'form-horizontal']) !!}


<div class="form-group">
<label for="text" class="col-sm-2 control-label">Código</label>
	<div class="col-sm-10"> 
<input type="text" class="form-control" id="inputName" name="code" placeholder="Código" value="{{$comunidad->code}}">
</div>
</div>


<div class="form-group">
<label for="inputEmail" class="col-sm-2 control-label">Nombre</label>
	<div class="col-sm-10"> 
<input type="text" class="form-control" id="inputName" name="name" placeholder="Nombre" value="{{$comunidad->name}}">
</div>
</div>

<div class="form-group">
<label for="inputEmail" class="col-sm-2 control-label">Dirección</label>
	<div class="col-sm-10"> 
<input type="text" class="form-control" id="inputName" name="address" placeholder="Dirección" value="{{$comunidad->address}}">
</div>
</div>

<div class="form-group">
<label for="inputEmail" class="col-sm-2 control-label">N.I.F</label>
	<div class="col-sm-10"> 
<input type="text" class="form-control" id="inputName" name="nif" placeholder="N.I.F" value="{{$comunidad->nif}}">
</div>
</div>


<div class="form-group">
<label for="inputEmail" class="col-sm-2 control-label">Año Construcción</label>
	<div class="col-sm-10"> 
<input type="text" class="form-control" id="inputName" name="structure_year" placeholder="Año Construcción" value="{{$comunidad->structure_year}}">
</div>
</div>


<div class="form-group">
<label for="inputEmail" class="col-sm-2 control-label">Núm. Ascensores</label>
	<div class="col-sm-10"> 
<input type="text" class="form-control" id="inputName" name="elevator_numbers" placeholder="Núm. Ascensores" value="{{$comunidad->elevator_numbers}}">
</div>
</div>


<div class="form-group">
<label for="inputEmail" class="col-sm-2 control-label">Cont. Plataforma</label>
	<div class="col-sm-10"> 
<input type="text" class="form-control" id="inputName" name="platform_accounting" placeholder="Cont. Plataforma" value="{{$comunidad->platform_accounting}}">
</div>
</div>


<div class="form-group">
<label for="inputEmail" class="col-sm-2 control-label">Longitud</label>
	<div class="col-sm-10"> 
<input type="text" class="form-control" id="inputName" name="longitude" placeholder="Longitud" value="{{$comunidad->longitude}}">
</div>
</div>


<div class="form-group">
<label for="inputEmail" class="col-sm-2 control-label">Latitud</label>
	<div class="col-sm-10"> 
<input type="text" class="form-control" id="inputName" name="latitude" placeholder="Latitud" value="{{$comunidad->latitude}}">
</div>
</div>

<div class="form-group">
<label for="inputEmail" class="col-sm-2 control-label">Notas</label>
<div class="col-sm-10"> 
<textarea class="form-control" id="inputExperience" name="notes" placeholder="Notas" >{{$comunidad->notes}}</textarea>
</div>
</div>



<div class="form-group">
<div class="col-sm-offset-2 col-sm-10"> 
 <button type="submit" class="btn btn-danger">Editar</button>
  </div>
</div>


</div>
</div>
</div>
</div>


{!! Form::close() !!}


@endsection
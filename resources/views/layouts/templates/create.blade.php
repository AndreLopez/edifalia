@extends('layouts.dashboard')

@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="nav-tabs-custom" >
				<ul class="nav nav-tabs">
					<li class="active">
						<a href="{{ route('community.create')}}">Registro</a>
					</li>
					<li>
						<a href="{{ route('community.index') }}">Lista</a>
					</li>
				</ul>

				<div class="tab-content">
					<div class="tab-pane active">
						{!! Form::open(['route' => 'community.index', 'method' =>'POST', 'class' => 'form-horizontal']) !!}
							<div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
								<label class="col-sm-2 control-label" for="text">@lang('forms.code_label')</label>
								<div class="col-sm-9">
									<div class="input-group">
								    	{!! Form::text(
								    		'code', (isset($model))?$model->code:null, [
								    			'class' => 'form-control', 'id' => 'code', 
								    			'placeholder' => trans('forms.code_placeholder')
								    		]
								    	) !!}
								    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
								    </div>
								</div>
								@if ($errors->has('code'))
									<div class="col-sm-9 col-sm-offset-2">
								        <span class="help-block">
								            <strong>{{ $errors->first('code') }}</strong>
								        </span>
								    </div>
								@endif
							</div>

							<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
								<label for="name" class="col-sm-2 control-label">@lang('forms.name_label')</label>
								<div class="col-sm-9">
									<div class="input-group">
								    	{!! Form::text(
								    		'name', (isset($model))?$model->name:null, [
								    			'class' => 'form-control', 'id' => 'name', 
								    			'placeholder' => trans('forms.name_placeholder')
								    		]
								    	) !!}
								    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
								    </div>
								</div>
								@if ($errors->has('name'))
									<div class="col-sm-9 col-sm-offset-2">
								        <span class="help-block">
								            <strong>{{ $errors->first('name') }}</strong>
								        </span>
								    </div>
								@endif
							</div>

							<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
								<label for="address" class="col-sm-2 control-label">@lang('forms.address_label')</label>
								<div class="col-sm-9">
									{!! Form::textarea('address', null, [
										'class' => 'form-control', 'id' => 'address', 'rows' => '3',
										'placeholder' => trans('forms.address_placeholder')
									]) !!}
								</div>
								@if ($errors->has('address'))
									<div class="col-sm-9 col-sm-offset-2">
								        <span class="help-block">
								            <strong>{{ $errors->first('address') }}</strong>
								        </span>
								    </div>
								@endif
							</div>

							<div class="form-group{{ $errors->has('nif') ? ' has-error' : '' }}">
								<label for="nif" class="col-sm-2 control-label">@lang('forms.nif_label')</label>
								<div class="col-sm-9">
									<div class="input-group">
								    	{!! Form::text(
								    		'nif', (isset($model))?$model->nif:null, [
								    			'class' => 'form-control', 'id' => 'nif',
								    			'placeholder' => trans('forms.nif_placeholder')
								    		]
								    	) !!}
								    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
								    </div>
								</div>
								@if ($errors->has('nif'))
									<div class="col-sm-9 col-sm-offset-2">
								        <span class="help-block">
								            <strong>{{ $errors->first('nif') }}</strong>
								        </span>
								    </div>
								@endif
							</div>

							<div class="form-group{{ $errors->has('structure_year') ? ' has-error' : '' }}">
								<label for="structure_year" class="col-sm-2 control-label">
									@lang('forms.structure_year_label')
								</label>
								<div class="col-sm-9">
									<div class="input-group">
								    	{!! Form::text(
								    		'structure_year', (isset($model))?$model->structure_year:null, [
								    			'class' => 'form-control', 'id' => 'structure_year',
								    			'placeholder' => trans('forms.structure_year_placeholder')
								    		]
								    	) !!}
								    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
								    </div>
								</div>
								@if ($errors->has('structure_year'))
									<div class="col-sm-9 col-sm-offset-2">
								        <span class="help-block">
								            <strong>{{ $errors->first('structure_year') }}</strong>
								        </span>
								    </div>
								@endif
							</div>

							<div class="form-group{{ $errors->has('elevator_numbers') ? ' has-error' : '' }}">
								<label for="elevator_numbers" class="col-sm-2 control-label">
									@lang('forms.elevator_number_label')
								</label>
								<div class="col-sm-2">
									{!! Form::number(
							    		'elevator_numbers', (isset($model))?$model->elevator_numbers:null, [
							    			'class' => 'form-control', 'id' => 'elevator_numbers',
							    			'placeholder' => trans('forms.elevator_number_placeholder'),
							    		]
							    	) !!}
								</div>
								@if ($errors->has('elevator_numbers'))
									<div class="col-sm-9 col-sm-offset-2">
								        <span class="help-block">
								            <strong>{{ $errors->first('elevator_numbers') }}</strong>
								        </span>
								    </div>
								@endif
							</div>

							<div class="form-group{{ $errors->has('platform_accounting') ? ' has-error' : '' }}">
								<label for="platform_accounting" class="col-sm-2 control-label">
									@lang('forms.platform_accounting_label')
								</label>
								<div class="col-sm-9">
									{!! Form::checkbox('platform_accounting', True) !!}
								</div>
								@if ($errors->has('platform_accounting'))
									<div class="col-sm-9 col-sm-offset-2">
								        <span class="help-block">
								            <strong>{{ $errors->first('platform_accounting') }}</strong>
								        </span>
								    </div>
								@endif
							</div>

							<div class="form-group{{ $errors->has('longitude') ? ' has-error' : '' }}">
								<label for="longitude" class="col-sm-2 control-label">@lang('forms.longitude_label')</label>
								<div class="col-sm-3">
									{!! Form::text(
							    		'longitude', (isset($model))?$model->longitude:null, [
							    			'class' => 'form-control', 'id' => 'longitude',
							    			'placeholder' => trans('forms.longitude_placeholder')
							    		]
							    	) !!}
								</div>
								@if ($errors->has('longitude'))
									<div class="col-sm-9 col-sm-offset-2">
								        <span class="help-block">
								            <strong>{{ $errors->first('longitude') }}</strong>
								        </span>
								    </div>
								@endif
							</div>

							<div class="form-group{{ $errors->has('latitude') ? ' has-error' : '' }}">
								<label for="latitude" class="col-sm-2 control-label">@lang('forms.latitude_label')</label>
								<div class="col-sm-3">
									{!! Form::text(
							    		'latitude', (isset($model))?$model->latitude:null, [
							    			'class' => 'form-control', 'id' => 'latitude',
							    			'placeholder' => trans('forms.latitude_placeholder')
							    		]
							    	) !!}
								</div>
								@if ($errors->has('latitude'))
									<div class="col-sm-9 col-sm-offset-2">
								        <span class="help-block">
								            <strong>{{ $errors->first('latitude') }}</strong>
								        </span>
								    </div>
								@endif
							</div>

							<div class="form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
								<label for="notes" class="col-sm-2 control-label">@lang('forms.note_label')</label>
								<div class="col-sm-9">
									{!! Form::textarea('notes', null, [
										'class' => 'form-control', 'id' => 'notes', 'rows' => '3',
										'placeholder' => trans('forms.note_placeholder')
									]) !!}
								</div>
								@if ($errors->has('notes'))
									<div class="col-sm-9 col-sm-offset-2">
								        <span class="help-block">
								            <strong>{{ $errors->first('notes') }}</strong>
								        </span>
								    </div>
								@endif
							</div>
							<div class="form-group">
								@if(isset($data['breadcrumb_link']))
									<button type="button" class="btn btn-default col-xs-12 col-sm-12 col-md-1 col-lg-1 col-md-offset-8 col-lg-offset-8 btn-form" data-toggle="tooltip" 
					            		title="{{ trans('common.btn_cancel_tooltip') }}" 
					            		onclick="location.href='{{ $data['breadcrumb_link'] }}'">
					                	<i class="glyphicon glyphicon-ban-circle"></i> @lang('common.btn_cancel_text')
						            </button>
						            
						        @endif
									<button type="reset" class="btn btn-warning btn-form col-xs-12 col-sm-12 col-md-1 col-lg-1 @if(!isset($data['breadcrumb_link'])) col-md-offset-9 col-lg-offset-9 @endif" data-toggle="tooltip" 
											title="{{ trans('common.btn_clear_tooltip') }}">
						                <i class="fa fa-eraser"></i> @lang('common.btn_clear_text')
						            </button>
									<button type="submit" class="btn btn-info btn-form col-xs-12 col-sm-12 col-md-1 col-lg-1" data-toggle="tooltip" 
											title="{{ trans('common.btn_save_tooltip') }}">
						                <i class="fa fa-save"></i> @lang('common.btn_save_text')
						            </button>
							</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@extends('layouts.dashboard')

@section('content')



<div class="col-md-9">
<div class="nav-tabs-custom" >
<ul class="nav nav-tabs">

<li >
  <a href="{{ route('registro.create')}}"  >Registro</a>  
</li>  

<li class="active">
  <a href="{{ route('registro.index') }}"  >Lista </a>  
</li>  



</ul>



<div class="row">
<div class="col-xs-12">
<div class="box">
<div class="box-header">
<h3 class="box-title">Comunidades Registradas</h3>
</div>
<div class="box-body">
<div class="row">
<div class="col-sm-12">
  <table class="table table-bordered table-hover dataTable" >
   	  <tr role="row">
       <th class="sorting" tabindex="0" rowspan="1" colspan="1" >Código</th>
        <th class="sorting" tabindex="0" rowspan="1" colspan="1" >Nombre</th>
        <th class="sorting" tabindex="0" rowspan="1" colspan="1">Dirección</th>
        <th class="sorting" tabindex="0" rowspan="1" colspan="1">N.I.F</th>
        <th class="sorting" tabindex="0" rowspan="1" colspan="1">Año de construcción</th>
        <th class="sorting" tabindex="0" rowspan="1" colspan="1">Contabilidad de la plataforma</th>
        <th class="sorting" tabindex="0" rowspan="1" colspan="1">Notas</th>   	  	
         <th class="sorting" tabindex="0" rowspan="1" colspan="1">Acción</th>
     </tr>
           @foreach($comunidad as $comunidades)
        <tr>
           <td>{{ $comunidades->code }}</td>
        	 <td>{{ $comunidades->name }}</td>
           <td>{{ $comunidades->address }}</td>
        	 <td>{{ $comunidades->nif }}</td>
           <td>{{ $comunidades->structure_year }}</td>
           <td>{{ $comunidades->platform_accounting }}</td>
           <td>{{ $comunidades->notes }}</td>
           <td><a href="{{ route('registro.edit', $comunidades->id) }}" class="btn btn-block btn-primary">Editar</a><a href="{{ route('registro.destroy', $comunidades->id)}}" onclick="return confirm('¿Esta seguro de querer eliminar la comunidad?')" class="btn btn-block btn-danger">Eliminar</a></td>
        </tr> 
     @endforeach   
 </table>
  
  </div>
</div>
</div>
</div>
</div>
</div>
</div></div></div>

@endsection
<li class="treeview">
    <a href="#">
        <i class="fa fa-file-text-o"></i>
        <span>@lang('dashboard.mnu_accounting')</span> <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li>
            <a href="#">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_accounting_plan')
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_daily')
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_greater')
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_balance')
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_closing_seat')
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_standard')
            </a>
        </li>
    </ul>
</li>
<li class="treeview">
    <a href="#">
        <i class="fa fa-envelope-o"></i>
        <span>@lang('dashboard.mnu_email')</span> <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li>
            <a href="#">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_labels')
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-chevron-circle-right"></i>
                @lang('dashboard.mnu_mailing_sms')
            </a>
            <ul class="treeview-menu">
                <li>
                    <a href="{{route('message.create')}}">
                        <i class="fa fa-chevron-circle-right"></i>
                        Emails
                    </a>
                </li>
                <li>
                    <a href="{{ route('text.messages.create') }}">
                        <i class="fa fa-chevron-circle-right"></i>
                        SMS
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="{{ route('received.messages') }}">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_received')
            </a>
        </li>
        <li>
            <a href="{{ route('sent.messages') }}">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_sent')
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_sending_logalty')
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_mail_templates')
            </a>
        </li>
    </ul>
</li>
<li class="treeview">
    <a href="#">
        <i class="fa fa-tasks"></i>
        <span>@lang('dashboard.mnu_management')</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li>
            <a href="#"><i class="fa fa-circle-o"></i>
                <span class="pull-right-container">
                                            @lang('dashboard.mnu_post_docs')
                                        </span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a href="{{route('pages.index')}}">
                        <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_pages')
                    </a>
                </li>
                <li>
                    <a href="{{route('tags.index')}}">
                        <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_tags')
                    </a>
                </li>
                <li>
                    <a href="{{route('documents.index')}}">
                        <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_post_docs')
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#"><i class="fa fa-circle-o"></i>
                <span class="pull-right-container">
                                            @lang('dashboard.mnu_templates')
                                        </span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a href="{{route('models.index')}}">
                        <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_models')
                    </a>
                </li>
                <li>
                    <a href="{{route('templates.index')}}">
                        <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_templates')
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#"><i class="fa fa-circle-o"></i>
                <span class="pull-right-container">
                                            @lang('dashboard.mnu_incidences')
                                        </span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a href="{{route('communicator-types.index')}}">
                        <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_communicator_types')
                    </a>
                </li>
                <li>
                    <a href="{{route('receptions.index')}}">
                        <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_receptions')
                    </a>
                </li>
                <li>
                    <a href="{{route('reasons.index')}}">
                        <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_reasons')
                    </a>
                </li>
                <li>
                    <a href="{{route('sectors.index')}}">
                        <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_sectors')
                    </a>
                </li>
                <li>
                    <a href="{{route('incidences.index')}}">
                        <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_incidences')
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#"><i class="fa fa-circle-o"></i>
                <span class="pull-right-container">
                                            @lang('dashboard.mnu_budgets')
                                        </span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a href="{{route('invoicing-concepts.index')}}">
                        <i class="fa fa-chevron-circle-right"></i>
                        @lang('dashboard.mnu_invoicing_concept')
                    </a>
                </li>
                <li>
                    <a href="{{route('budgets.index')}}">
                        <i class="fa fa-chevron-circle-right"></i>
                        @lang('dashboard.mnu_budgets')
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#"><i class="fa fa-circle-o"></i>
                <span class="pull-right-container">
                                            @lang('dashboard.mnu_counters_reading')
                                        </span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a href="{{route('rates.index')}}">
                        <i class="fa fa-chevron-circle-right"></i>
                        @lang('dashboard.mnu_rates')
                    </a>
                </li>
                <li>
                    <a href="{{route('readings.index')}}">
                        <i class="fa fa-chevron-circle-right"></i>
                        @lang('dashboard.mnu_counters_reading')
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="{{ route('receiptdues.index') }}">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_quota_receipts')
            </a>
        </li>
        <li>
            <a href="{{route('issue-receipts.index')}}">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_issue_receipts')
            </a>
        </li>
        <li>
            <a href="{{route('receipts-banks.index')}}">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_receipts_to_bank')
            </a>
        </li>
        <li>
            <a href="{{route('charges.index')}}">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_charges')
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_payments')
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_predefined_pay')
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_treasury')
            </a>
        </li>
    </ul>
</li>
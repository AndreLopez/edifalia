<li class="treeview">
    <a href="#">
        <i class="fa fa-print"></i>
        <span>@lang('dashboard.mnu_reports')</span> <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li>
            <a href="{{ route('report.dossier') }}">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_dossier_sign')
            </a>
        </li>
        <li>
            <a href="{{ route('report.meeting') }}">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_meetings')
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o"></i>
                @lang('dashboard.mnu_economic_reports')
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a href="{{ route('economic.account')}}">
                        <i class="fa fa-chevron-circle-right"></i>
                        Agrupados por cuentas
                    </a>
                </li>
                <li>
                    <a href="{{ route('economic.movement') }}">
                        <i class="fa fa-chevron-circle-right"></i>
                        Detalles de movimiento
                    </a>
                </li>
                <li>
                    <a href="{{ route('economic.clasification-group') }}">
                        <i class="fa fa-chevron-circle-right"></i>
                        Clasificados por grupos
                    </a>
                </li>
                <li>
                    <a href="{{ route('economic.month') }}">
                        <i class="fa fa-chevron-circle-right"></i>
                        Por meses
                    </a>
                </li>
                <li>
                    <a href="{{ route('economic.property') }}">
                        <i class="fa fa-chevron-circle-right"></i>
                        Ingresos por propiedades
                    </a>
                </li>
                <li>
                    <a href="{{route('economic.receipt')}}">
                        <i class="fa fa-chevron-circle-right"></i>
                        Recibos emitidos
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-circle-o"></i>
                        Comunidades Agrupadas
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="{{ route('economic.total') }}">
                                <i class="fa fa-chevron-circle-right"></i>
                                Total de gastos
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('economic.payment-supplier') }}">
                                <i class="fa fa-chevron-circle-right"></i>
                                Pagos a proveedores
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('economic.supplier-pending') }}">
                                <i class="fa fa-chevron-circle-right"></i>
                                Pagos pendientes a proveedores
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-circle-o"></i>
                        AEAT
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="#">
                                <i class="fa fa-chevron-circle-right"></i>
                                Libro de facturas emitidas
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-chevron-circle-right"></i>
                                Libro de facturas recibidas
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-chevron-circle-right"></i>
                                Proveedores 347
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-chevron-circle-right"></i>
                                Clientes 347
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li>
            <a href="{{ route('report.liquidate-cons') }}">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_liquidate_cons')
            </a>
        </li>
        <li>
            <a href="{{ route('report.liquidation') }}">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_liquidations')
            </a>
        </li>
        <li>
            <a href="{{ route('report.spending') }}">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_compare_budget_spending')
            </a>
        </li>
    </ul>
</li>
<li class="treeview">
    <a href="#">
        <i class="fa fa-wrench"></i>
        <span>@lang('dashboard.mnu_utilities')</span> <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li>
            <a href="{{route('main-users.index')}}">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_user_data')
            </a>
        </li>
        <li>
            <a href="{{route('offices.index')}}">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_office_management')
            </a>
        </li>
        <li>
            <a href="#"><i class="fa fa-circle-o"></i>
                <span class="pull-right-container"> @lang('dashboard.mnu_user_access')</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a href="{{route('positions.index')}}">
                        <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_positions')
                    </a>
                </li>
                <li>
                    <a href="{{route('employees.index')}}">
                        <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_user_access')
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="{{route('backups.index')}}">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_backups')
            </a>
        </li>
        <li>
            <a href="{{route('connections.intranet')}}">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_intranet_access')
            </a>
        </li>
        {{--<li>
            <a href="#">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_vo_visits')
            </a>
        </li>--}}
        <li>
            <a href="{{route('connections.online')}}">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_online_users')
            </a>
        </li>
        <li>
            <a href="{{route('history.all')}}">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_action_history')
            </a>
        </li>
        <li>
            <a href="#"><i class="fa fa-circle-o"></i>
                <span class="pull-right-container"> @lang('dashboard.mnu_reports_admin')</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a href="{{route('report.controls')}}">
                        <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_reports_controls')
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</li>
<li class="treeview">
    <a href="#">
        <i class="fa fa-pencil-square-o"></i>
        <span>@lang('dashboard.mnu_data')</span> <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li>
            <a href="{{route('community.index')}}">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_communities')
            </a>
        </li>
        <li>
            <a href="{{ route('property.index') }}">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_properties')
            </a>
        </li>
        <li>
            <a href="{{route('person.index')}}">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_persons')
            </a>
        </li>
        <li>
            <a href="{{route('supplier.index')}}">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_suppliers')
            </a>
        </li>
        <li>
            <a href="{{ route('coefficient.index') }}">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_coefficients')
            </a>
        </li>
        <li>
            <a href="{{route('contract.index')}}">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_contracts')
            </a>
        </li>
        <li>
            <a href="#">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_postal_codes')
            </a>
        </li>
        {{--<li>
            <a href="#">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_tables')
            </a>
        </li>--}}
        <li class="treeview">
            <a href="#">
                <i class="fa fa-chevron-circle-right"></i> @lang('dashboard.mnu_tables')
            </a>
            <ul class="treeview-menu">
                <li>
                    {{--<a href="{{ route('invoicing-concepts.index') }}"><i class="fa fa-chevron-circle-right"></i>@lang('dashboard.mnu_invoicing_concept')</a>--}}
                    <a href="{{ route('propertyType.index') }}"><i class="fa fa-chevron-circle-right"></i>@lang('dashboard.mnu_properties_type')</a>
                </li>
            </ul>
        </li>
    </ul>
</li>
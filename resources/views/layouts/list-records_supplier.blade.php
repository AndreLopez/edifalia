@extends('layouts.dashboard')

@if(!empty($data['breadcrumb_link']))
	@section('breadcrumb_link')
		{{ $data['breadcrumb_link'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_icon']))
	@section('breadcrumb_icon')
		{{ $data['breadcrumb_icon'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_title']))
	@section('breadcrumb_title')
		{{ $data['breadcrumb_title'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_subtitle']))
	@section('breadcrumb_subtitle')
		{{ $data['breadcrumb_subtitle'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_map']))
	@section('breadcrumb_map')
		{{ $data['breadcrumb_map'] }}
	@endsection
@endif

@section('content')
	<div class="box box-default">
		<div class="box-header with-border">
			<div class="panel-title inline-block">
				<h5 class="box-title">
					<i class="glyphicon glyphicon-list-alt"></i>&#160;
					{{ $data['panel_title'] }}
				</h5>
			</div>
		</div>
		<div class="box-body">
			<div class="">
				<table class="table table-hover table-striped dt-responsive nowrap display dataTable" width="100%">
					<thead>
						<tr>
							@foreach ($data['theads'] as $thead)
								<th class="{{ $thead['align'] }}">{{ trans($thead['business_name']) }}</th>
								@endforeach
							<th class="text-center">@lang('common.lst_action_title')</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($data['tbody'] as $tbody)
							<tr id="{{ $tbody['record_id'] }}">
								{!! $tbody['cols'] !!}
								<td class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-center">
									{!! Form::open(['method' => 'DELETE', 'id' => 'formDelete', 'route' => [$tbody['url_delete'], $tbody['record_id']]]) !!}
										<div class="btn-group btn-group-xs">
											@if(isset($tbody['url_show']))
				                                {!! Form::button('<i class="fa fa-eye"></i>', [
				                                	'type' => 'button', 'class' => 'btn btn-info',
				                                	'data-toggle' => 'tooltip', 'title' => trans('common.lst_act_show'),
				                                	'onclick' => "location='" . $tbody['url_show'] . "'"
				                                ]) !!}
				                            @endif
				                            {!! Form::button('<i class="fa fa-pencil-square-o"></i>', [
				                            	'type' => 'button', 'class' => 'btn btn-info',
				                            	'data-toggle' => 'tooltip', 'title' => trans('common.lst_act_edit'),
				                            	'onclick' => "location='" . $tbody['url_edit'] . "'"
				                            ]) !!}
			                                {!! Form::button('<i class="fa fa-trash-o"></i>', [
			                                	'type' => 'button', 'class' => 'btn btn-xs btn-info inline-block delete_record',
			                                	'data-id' => $tbody['record_id'], 'data-toggle' => 'tooltip',
			                                	'title' => trans('common.lst_act_delete')
			                                ]) !!}
			                                
			                            </div>
		                            {!! Form::close() !!}
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<div class="box-footer">
			<a data-toggle="tooltip" class="btn btn-primary col-xs-12 col-sm-12 col-md-1 col-lg-1 pull-right"
               title="@lang('common.lst_new_record')"
               href="{{ $data['url_new'] }}">
                <i class="fa fa-plus"></i> @lang('common.lst_new_record')</a>
		</div>
	</div>
@endsection

@section('extra_scripts')
	<script>
		/*
		 * Delete the selected record
		 */
		$('.delete_record').on('click', function(e) {
			$("#formDelete").submit(false);
			var inputData = $(this).closest("#formDelete").serialize();
			var dataId = $(this).attr('data-id');
			var action_url = $(this).closest("#formDelete").attr('action');

			bootbox.confirm('{{ trans('messages.msg_delete_confirm') }}', function(result) {
				if (result) {
				    $.ajax({
		                type: 'PUT',
		                dataType: 'JSON',
		                url: action_url,
		                data: inputData,
		                success: function(data){
		                	if (data.result) {
			                	location.reload();
			                }
		                },
		                error: function(jqxhr, textStatus, error) {
		                	var err = textStatus + ", " + error;
			                bootbox.alert( '{{ trans('messages.msg_ajax_error') }}: ' + err );
			                console.log('{{ trans('messages.msg_ajax_error') }}: ' + err);
		                }
		            })
		            .fail(function() {
		                console.log("error");
		            });
				}
			});
		});

		$(document).ready(function() {
			/*
             * DataTable init and styles
             */
            var data_table = $('.dataTable');
            if (data_table.length > 0) {
                $.extend( $.fn.dataTableExt.oStdClasses, {
                    "sFilterInput": "form-control input-sm",
                    "sLengthSelect": "select2 input-group input-xs",
                });
                {{-- Init elements for the dataTable --}}
                data_table.dataTable({
                    "language": {
                        //"url": "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json",
                        "processing":     "Procesando...",
                        "lengthMenu": "Mostrar _MENU_ registros",
                        "zeroRecords": "Lo sentimos - no existen registros",
                        "infoEmpty": "No hay registros disponibles",
                        "emptyTable": "Ningún dato disponible en esta tabla",
                        "info": "Página _PAGE_ de _PAGES_",
                        "search": "Buscar:",
                        "paginate": {
                            "first":    "Primero",
                            "last":     "Último",
                            "next":     "Siguiente",
                            "previous": "Anterior"
                        },
                        "loadingRecords": "Cargando...",
                        "infoThousands":  ",",
                        "infoFiltered":   "(filtrado de un total de _MAX_ registros)",
                        "infoPostFix":    "",
                        "aria": {
                            "sortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                    "ordering": true,
                    "order": [[0, 'asc']],
                    "bDestroy": true,
                    "bPaginate": true,
                    "bInfo": true,
                    "initComplete": function(settings, json) {
                        $('.dataTables_length select').select2();
                    }
                });
            }
		});
	</script>
@endsection
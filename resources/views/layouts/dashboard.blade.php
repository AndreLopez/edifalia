<!DOCTYPE html>
<html>
	@include('layouts.head')
	<body class="hold-transition skin-black sidebar-mini">
		@section('modals')
		@show
		<div class="wrapper">
			{{-- Header Dashboard --}}
			<header class="main-header">
				<!-- Logo -->
				<a href="{{ url('/') }}" class="logo">
					{{-- mini logo for sidebar mini 50x50 pixels --}}
					<span class="logo-mini">
						<img src="{{ asset('static/images/logo-mini.png') }}" class="img-responsive img-logo-mini" 
							 alt="{{ trans('app.logo_mini_alt') }}">
					</span>
					{{-- logo for regular state and mobile devices --}}
					<span class="logo-lg">
						<img src="{{ asset('static/images/logo-horizontal.png') }}" class="img-responsive img-logo-lg" 
							 alt="{{ trans('app.logo_lg_alt') }}">
					</span>
				</a>
				{{-- Header Navbar --}}
				<nav class="navbar navbar-static-top" role="navigation">
					{{-- Sidebar toggle button --}}
					<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
						<span class="sr-only">Toggle navigation</span>
					</a>
					<div class="navbar-custom-menu">
						<ul class="nav navbar-nav">
							{{-- User Account --}}
							<li class="dropdown user user-menu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<span class="hidden-xs">
										@if (Auth::check())
											{{ Auth::user()->name }}
										@endif
									</span>
								</a>
								<ul class="dropdown-menu">
									{{-- User info --}}
									<li class="user-header">
										<p>
											@if (Auth::check())
												{{-- Nombre y Cargo o nivel de acceso --}}
												{{ Auth::user()->name }}
												<small>
													{{ Auth::user()->create_at }}
												</small>
											@endif
											
										</p>
									</li>
									{{-- Menu Footer --}}
									<li class="user-footer">
										<div class="pull-left">
											<a href="{{ route('user-profile.index')}}" class="btn btn-default btn-flat">
												@lang('dashboard.profile')
											</a>
										</div>
										<div class="pull-right" id="logout">
                                            <a href="#" class="btn btn-default btn-flat">
                                                @lang('dashboard.logout')
                                            </a>
											<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
												{{ csrf_field() }}
											</form>
										</div>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</nav>
			</header>

			{{-- Left side column. contains the logo and sidebar --}}
			<aside class="main-sidebar">
				{{-- sidebar --}}
				<section class="sidebar">
					{{-- sidebar menú --}}
					<ul class="sidebar-menu">
						<li class="header">@lang('dashboard.nav_title')</li>
						<li>
							<a href="{{ url('/') }}">
								<i class="fa fa-dashboard"></i>
								<span>@lang('dashboard.title')</span>
							</a>
						</li>
						{{-- data --}}
						@can('admin')
							@include('layouts.menu.data')
						@elsecan('employee')
							@can('access_data')
								@include('layouts.menu.data')
							@endcan
						@endcan
						{{-- management --}}
						@can('admin')
							@include('layouts.menu.management')
						@elsecan('employee')
							@can('access_management')
								@include('layouts.menu.management')
							@endcan
						@endcan
						{{-- email --}}
						@can('admin')
							@include('layouts.menu.email')
						@elsecan('employee')
							@can('access_mail')
								@include('layouts.menu.email')
							@endcan
						@endcan
						{{-- reports --}}
						@can('admin')
							@include('layouts.menu.reports')
						@elsecan('employee')
							@can('access_reports')
								@include('layouts.menu.reports')
							@endcan
						@endcan
						{{-- accounting --}}
						@can('admin')
							@include('layouts.menu.accounting')
						@elsecan('employee')
							@can('access_accounting')
								@include('layouts.menu.accounting')
							@endcan
						@endcan
						{{-- utilities --}}
						@can('admin')
							@include('layouts.menu.utilities')
						@endif
					</ul>
				</section>
				{{-- /end sidebar section --}}
			</aside>

			{{-- content wrapper section --}}
			<div class="content-wrapper">
				{{-- Content Header (Page header) --}}
				<section class="content-header">
					<h1>
						@hasSection('breadcrumb_title')
							@yield('breadcrumb_title')
						@else
							@lang('dashboard.title')
						@endif
						<small>
							@hasSection('breadcrumb_subtitle')
								@yield('breadcrumb_subtitle')
							@else
								@lang('dashboard.main')
							@endif
						</small>
					</h1>
					<ol class="breadcrumb">
						@hasSection('breadcrumb_map')
							@yield('breadcrumb_map')
						@else
							<li>
								<a href="{{ url('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('dashboard.home')</a>
							</li>
							<li class="active">@lang('dashboard.title')</li>
						@endif
					</ol>
				</section>

				{{-- Main content --}}
				<section class="content">
					@yield('content')
				</section>{{-- /end content section --}}
			</div>{{-- /end content wrapper --}}
		</div>
		@include('layouts.footer')

		<script>
			$(document).ready(function() {
				/** Instrucciones para mostrar la selección del menú */
				var url = window.location.href, sidebar_menu = $('.sidebar-menu'),
					treeview_l1 = $('.treeview-menu li a'),
					treeview_l2 = $('.treeview-menu li .treeview-menu li a');

				sidebar_menu.find('li').removeClass('active');

				/** Opción del menú de nivel 0 */
				sidebar_menu.find('a').filter(function() {
					return this.href == url;
				}).closest('li').addClass('active');

				/** Opción del menú con treeview de nivel 1 */
				treeview_l1.filter(function() {
		            return this.href == url;
		        }).parent().parent().parent().addClass('active');

				/** Opción del menú con treeview de nivel 2 */
		        treeview_l2.filter(function() {
		            return this.href == url;
		        }).parent().parent().parent().parent().parent().addClass('active');
			});
		</script>

		{{-- Section for extra scripts if is required --}}
        @yield('extra_scripts')
        @yield('script_report')
	</body>
</html>
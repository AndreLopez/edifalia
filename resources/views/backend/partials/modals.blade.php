<div class="modal modal-danger" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">@lang('dashboard.titles.delete_record')</h4>
            </div>
            <div class="modal-body" style="text-align: center">
                <h4>@lang('dashboard.messages.delete_record')</h4>
                <form id="form-delete" action="" method="DELETE"></form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left"
                        data-dismiss="modal">
                    @lang('dashboard.buttons.no_delete')
                </button>
                <button type="button" class="btn btn-outline" id="yes-delete" data-dismiss="modal">
                    @lang('dashboard.buttons.yes_delete')
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal" id="pleaseWait" data-backdrop="static">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    <span class="glyphicon glyphicon-time"></span>
                    @lang('dashboard.messages.wait')
                </h4>
            </div>
            <div class="modal-body">
                <div class="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-info active" style="width: 100%">
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
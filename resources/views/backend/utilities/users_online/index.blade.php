@extends('layouts.dashboard')

@section('breadcrumb_title')
    @lang('dashboard.mnu_online_users')
@endsection

@section('breadcrumb_subtitle')
    @lang('dashboard.titles.users_online_list')
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}"><i class="fa fa-dashboard"></i> @lang('dashboard.home')</a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_utilities')</a></li>
    <li class="active">@lang('dashboard.mnu_online_users')</li>
@endsection

@section('content')
    <div class="box">
        <div class="box-body table-responsive">
            <table class="table table-hover">
                @if($connections->count())
                    <tr>
                        <th>@lang('dashboard.tables.user')</th>
                        <th>@lang('dashboard.tables.office')</th>
                        <th>@lang('dashboard.tables.ip_address')</th>
                        <th>@lang('dashboard.tables.country')</th>
                        <th>@lang('dashboard.tables.connected_from')</th>
                    </tr>
                @endif
                @forelse($connections as $connection)
                    <tr>
                        @if($connection->user->isAdmin())
                            <td>{{$connection->user->name}}</td>
                            <td></td>
                        @elseif($connection->user->isEmployee())
                            <td>{{$connection->user->employee->first_name}}</td>
                            <td>{{$connection->user->employee->office->name}}</td>
                        @endif
                        <td>{{$connection->ip_address}}</td>
                        <td>{{$connection->country}}</td>
                        <td>{{$connection->created_at->format('d/m/Y H:i:s')}}</td>
                    </tr>
                @empty
                    <div class="alert alert-warning">
                        <h4><i class="icon fa fa-warning"></i> @lang('dashboard.messages.notice')</h4>
                        @lang('dashboard.messages.no_stored_records')
                    </div>
                @endforelse
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            {{$connections->links()}}
        </div>
        <!-- /.box-footer-->
    </div>
@endsection
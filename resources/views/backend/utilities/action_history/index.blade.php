@extends('layouts.dashboard')

@section('breadcrumb_title')
    @lang('dashboard.mnu_action_history')
@endsection

@section('breadcrumb_subtitle')
    @lang('dashboard.titles.history')
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_utilities')</a></li>
    <li class="active">@lang('dashboard.mnu_action_history')</li>
@endsection

@section('content')
    <div class="box">
        <div class="box-body table-responsive">
            <table class="table table-striped">
                @if($revisions->count())
                    <tr>
                        <th>@lang('dashboard.tables.user_action')</th>
                        <th>@lang('dashboard.tables.action_date')</th>
                    </tr>
                @endif
                @forelse($revisions as $history)
                    @if($history->key == 'created_at' && !$history->old_value)
                        <tr>
                            <td>
                                @if($history->userResponsible()->isEmployee())
                                    {{$history->userResponsible()->employee->first_name}}
                                @elseif($history->userResponsible()->isAdmin())
                                    {{$history->userResponsible()->name}}
                                @endif
                                @lang('dashboard.messages.created_record')
                                ({{substr(strrchr($history->revisionable_type, '\\'), 1)}}: Id {{$history->revisionable_id}}).
                            </td>
                            <td>{{$history->created_at->format('d/m/Y H:i:s')}}</td>
                        </tr>
                    @elseif($history->key == 'deleted_at' && !$history->old_value)
                        <tr>
                            <td>
                                @if($history->userResponsible()->isEmployee())
                                    {{$history->userResponsible()->employee->first_name}}
                                @elseif($history->userResponsible()->isAdmin())
                                    {{$history->userResponsible()->name}}
                                @endif
                                @lang('dashboard.messages.deleted_record')
                                ({{substr(strrchr($history->revisionable_type, '\\'), 1)}}: Id {{$history->revisionable_id}}).
                            </td>
                            <td>{{$history->created_at->format('d/m/Y H:i:s')}}</td>
                        </tr>
                    @elseif($history->key == 'deleted_at' && !$history->new_value)
                        <tr>
                            <td>
                                @if($history->userResponsible()->isEmployee())
                                    {{$history->userResponsible()->employee->first_name}}
                                @elseif($history->userResponsible()->isAdmin())
                                    {{$history->userResponsible()->name}}
                                @endif
                                @lang('dashboard.messages.undelete_record')
                                ({{substr(strrchr($history->revisionable_type, '\\'), 1)}}: Id {{$history->revisionable_id}}).
                            </td>
                            <td>{{$history->created_at->format('d/m/Y H:i:s')}}</td>
                        </tr>
                    @else
                        <tr>
                            <td>
                                @if($history->userResponsible()->isEmployee())
                                    {{$history->userResponsible()->employee->first_name}}
                                @elseif($history->userResponsible()->isAdmin())
                                    {{$history->userResponsible()->name}}
                                @endif
                                @lang('dashboard.messages.changed_record') <strong>{{trans('dashboard.fields.'.$history->fieldName())}}</strong>
                                @lang('dashboard.messages.from') "{{$history->oldValue()}}"
                                @lang('dashboard.messages.to') "{{$history->newValue()}}"
                                ({{substr(strrchr($history->revisionable_type, '\\'), 1)}}: Id {{$history->revisionable_id}}).
                            </td>
                            <td>{{$history->created_at->format('d/m/Y H:i:s')}}</td>
                        </tr>
                     @endif
                @empty
                    <div class="alert alert-warning">
                        <h4><i class="icon fa fa-warning"></i> @lang('dashboard.messages.notice')</h4>
                        @lang('dashboard.messages.no_stored_records')
                    </div>
                @endforelse
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            {{$revisions->links()}}
        </div>
        <!-- /.box-footer-->
    </div>
@endsection
@extends('layouts.dashboard')

@section('breadcrumb_title')
    @lang('dashboard.mnu_backups')
@endsection

@section('breadcrumb_subtitle')
    @lang('dashboard.titles.tables_list')
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_utilities')</a></li>
    <li class="active">@lang('dashboard.mnu_backups')</li>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('dashboard.mnu_backups')</h3>
        </div>
        {!! Form::open([
            'route' => 'backups.export',
            'method' => 'POST',
            'role' => 'form',
        ]) !!}
        <div class="box-body">
            @include('backend.partials.messages')
            <div class="form-group">
                {!! Form::label(trans('dashboard.fields.format')) !!}:
                <div class="checkbox">
                    <label>
                        {!! Form::radio('format', 'csv', null) !!}
                        @lang('dashboard.fields.csv')
                    </label>&nbsp;&nbsp;&nbsp;
                    <label>
                        {!! Form::radio('format', 'xls', true) !!}
                        @lang('dashboard.fields.excel')
                    </label>&nbsp;&nbsp;&nbsp;
                    <label>
                        {!! Form::radio('format', 'sql', null) !!}
                        @lang('dashboard.fields.sql')
                    </label>&nbsp;&nbsp;&nbsp;
                </div>
            </div>
            <div class="form-group col-md-6">
                {!! Form::label(trans('dashboard.fields.select_tables')) !!}.
                 @lang('dashboard.fields.ctrl_key'):
                {!! Form::select('tables[]', $tables, null, [
                    'class' => 'form-control',
                    'multiple' => true,
                    'size' => 15]) !!}
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-info">
                <i class="fa fa-download"></i> @lang('dashboard.buttons.export')
            </button>
        </div>
        {!! Form::close() !!}
    </div>
@endsection


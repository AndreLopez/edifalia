@extends('layouts.dashboard')

@section('breadcrumb_title')
    @lang('dashboard.mnu_intranet_access')
@endsection

@section('breadcrumb_subtitle')
    @lang('dashboard.titles.accesses_list')
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_utilities')</a></li>
    <li class="active">@lang('dashboard.mnu_intranet_access')</li>
@endsection

@section('content')
    <div class="box">
        <div class="box-body table-responsive">
            <table class="table table-hover">
                @if($accesses->count())
                    <tr>
                        <th>@lang('dashboard.tables.start_date')</th>
                        <th>@lang('dashboard.tables.ip_address')</th>
                        <th>@lang('dashboard.tables.user')</th>
                        <th>@lang('dashboard.tables.country')</th>
                        <th>@lang('dashboard.tables.access')</th>
                        <th>@lang('dashboard.tables.end_date')</th>
                        <th>@lang('dashboard.tables.time')</th>
                    </tr>
                @endif
                @forelse($accesses as $access)
                    <tr>
                        <td>{{$access->start_date->format('d/m/Y H:i:s')}}</td>
                        <td>{{$access->ip_address}}</td>
                        <td>
                            @if($access->user->isAdmin())
                                {{$access->user->name}}
                            @elseif($access->user->isEmployee())
                                {{$access->user->employee->first_name}}
                            @endif
                        </td>
                        <td>{{$access->country}}</td>
                        @if($access->user->active)
                            <td><i class="icon fa fa-check"></i></td>
                        @else
                            <td><i class="icon fa fa-close"></i></td>
                        @endif
                        <td>{{$access->end_date->format('d/m/Y H:i:s')}}</td>
                        <td>{{$access->time}}</td>
                    </tr>
                @empty
                    <div class="alert alert-warning">
                        <h4><i class="icon fa fa-warning"></i> @lang('dashboard.messages.notice')</h4>
                        @lang('dashboard.messages.no_stored_records')
                    </div>
                @endforelse
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            {{$accesses->links()}}
        </div>
        <!-- /.box-footer-->
    </div>
@endsection
@extends('layouts.dashboard')

@section('breadcrumb_title')
    @lang('dashboard.mnu_utilities')
@endsection

@section('breadcrumb_subtitle')
    @lang('dashboard.titles.zones_list')
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_utilities')</a></li>
    <li class="active">@lang('dashboard.mnu_zones')</li>
@endsection

@section('modals')
    @include('backend.partials.modals')
@endsection

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('dashboard.mnu_zones')</h3>
        </div>
        <div class="box-body table-responsive">
            <table class="table table-hover">
                @if($zones->count())
                    <tr>
                        <th>@lang('dashboard.tables.name')</th>
                        <th>@lang('dashboard.tables.continent')</th>
                        <th>@lang('dashboard.tables.actions')</th>
                    </tr>
                @endif
                @forelse($zones as $zone)
                    <tr data-id="{{$zone->id}}"
                        data-rundo="{{route('zones.undelete', $zone->id)}}">
                        <td>{{$zone->name}}</td>
                        <td>{{$zone->continent->name}}</td>
                        <td>
                            <div class="btn-group btn-group-xs">
                                <a data-toggle="tooltip" class="btn btn-info"
                                   title="@lang('dashboard.buttons.edit')"
                                   href="{{route('zones.edit', $zone->id)}}">
                                    <i class="fa fa-pencil-square-o"></i></a>

                                <a data-toggle="tooltip" class="btn btn-info delete_record"
                                   title="@lang('dashboard.buttons.delete')"
                                   href="{{route('zones.destroy', $zone->id)}}">
                                    <i class="fa fa-trash-o"></i></a>
                            </div>
                        </td>
                    </tr>
                @empty
                    <div class="alert alert-warning">
                        <h4><i class="icon fa fa-warning"></i> @lang('dashboard.messages.notice')</h4>
                        @lang('dashboard.messages.no_stored_records')
                    </div>
                @endforelse
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            {{$zones->links()}}
            <a data-toggle="tooltip" class="btn btn-primary pull-right"
               title="@lang('dashboard.buttons.new')"
               href="{{route('zones.create')}}">
                <i class="fa fa-plus"></i> @lang('dashboard.buttons.new')</a>
        </div>
        <!-- /.box-footer-->
    </div>
@endsection

@section('scripts')
    {{-- Backend scripts  --}}
    {!! Html::script('js/code01.js') !!}
@endsection
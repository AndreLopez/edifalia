@inject('continents', 'Edifalia\Services\Continents')

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('name') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.name')) !!}
        {!! Form::text('name', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.name'),
            'required' => true])
        !!}
    </div>
    <div class="form-group col-md-6 {{ $errors->has('continent_id') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.continent')) !!}
        {!! Form::select('continent_id', $continents->get(), null,
            ['class' => 'form-control', 'required' => true])
        !!}
    </div>
</div>
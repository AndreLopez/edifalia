@extends('layouts.dashboard')

@section('breadcrumb_title')
    @lang('dashboard.mnu_office_management')
@endsection

@section('breadcrumb_subtitle')
    @lang('dashboard.titles.office_info')
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_utilities')</a></li>
    <li class="active">@lang('dashboard.mnu_office_management')</li>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('dashboard.mnu_office_management')</h3>
            <div class="btn-group btn-group-sm pull-right">
                <a data-toggle="tooltip" class="btn btn-primary"
                   title="@lang('dashboard.buttons.back')"
                   href="{{route('offices.index')}}">
                    <i class="fa fa-reply"></i></a>
            </div>
        </div>
        <div class="box-body table-responsive">
            <h4>@lang('dashboard.titles.office_info'):</h4>
            <table class="table table-hover">
                <tr class="info">
                    <th>@lang('dashboard.tables.identifier')</th>
                    <th>@lang('dashboard.tables.name')</th>
                    <th>@lang('dashboard.tables.address')</th>
                    <th>@lang('dashboard.tables.phone') 1</th>
                    <th>@lang('dashboard.tables.phone') 2</th>
                </tr>
                <tr>
                    <td>{{$office->identifier}}</td>
                    <td>{{$office->name}}</td>
                    <td>{{$office->address}}</td>
                    <td>{{$office->phone1}}</td>
                    <td>{{$office->phone2}}</td>
                </tr>
            </table><hr>
            <table class="table table-hover">
                <tr class="info">
                    <th>@lang('dashboard.tables.fax')</th>
                    <th>@lang('dashboard.tables.email')</th>
                    <th>@lang('dashboard.tables.postal_code')</th>
                    <th>@lang('dashboard.tables.population')</th>
                    <th>@lang('dashboard.tables.province')</th>
                </tr>
                <tr>
                    <td>{{$office->fax}}</td>
                    <td>{{$office->email}}</td>
                    <td>{{$office->postalCode->code}}</td>
                    <td>{{$office->postalCode->town}}</td>
                    <td>{{$office->postalCode->province}}</td>
                </tr>
            </table><hr>

            <h4>@lang('dashboard.titles.communities'):</h4>
            <table class="table table-hover">
                <tr class="info">
                    <th>@lang('dashboard.tables.code')</th>
                    <th>@lang('dashboard.tables.name')</th>
                    <th>@lang('dashboard.tables.address')</th>
                    <th>@lang('dashboard.tables.nif')</th>
                </tr>
                @foreach($office->communities as $community)
                <tr>
                    <td>{{$community->code}}</td>
                    <td>{{$community->name}}</td>
                    <td>{{$community->address}}</td>
                    <td>{{$community->nif}}</td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection
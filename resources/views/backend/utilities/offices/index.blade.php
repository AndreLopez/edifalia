@extends('layouts.dashboard')

@section('breadcrumb_title')
    @lang('dashboard.mnu_office_management')
@endsection

@section('breadcrumb_subtitle')
    @lang('dashboard.titles.offices_list')
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_utilities')</a></li>
    <li class="active">@lang('dashboard.mnu_office_management')</li>
@endsection

@section('modals')
    @include('backend.partials.modals')
@endsection

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('dashboard.mnu_office_management')</h3>
            <div class="box-tools">
                {!! Form::model(Request::all(),
                        [	'route' => 'offices.index',
                            'method' => 'GET',
                            'rol' => 'search'
                        ])
                    !!}
                    <div class="input-group input-group-sm" style="width: 150px;">
                        {!! Form::text('name', null, [
                            'class' => 'form-control pull-right',
                            'placeholder' => trans('dashboard.fields.name')])
                        !!}
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default">
                                <i class="fa fa-search"></i>
                            </button>
                            <a data-toggle="tooltip" class="btn btn-default"
                               title="@lang('dashboard.buttons.refresh')"
                               href="{{route('offices.index')}}">
                                <i class="fa fa-refresh"></i></a>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="box-body table-responsive">
            <table class="table table-hover">
                @if($offices->count())
                <tr>
                    <th>@lang('dashboard.tables.identifier')</th>
                    <th>@lang('dashboard.tables.name')</th>
                    <th>@lang('dashboard.tables.phone')</th>
                    <th>@lang('dashboard.tables.actions')</th>
                </tr>
                @endif
                @forelse($offices as $office)
                    <tr data-id="{{$office->id}}"
                        data-rundo="{{route('offices.undelete', $office->id)}}">
                        <td>{{$office->identifier}}</td>
                        <td>{{$office->name}}</td>
                        <td>{{$office->phone1}}</td>
                        <td>
                            <div class="btn-group btn-group-xs">
                                <a data-toggle="tooltip" class="btn btn-info"
                                   title="@lang('dashboard.buttons.info')"
                                   href="{{route('offices.show', $office->id)}}">
                                    <i class="fa fa-eye"></i></a>

                                <a data-toggle="tooltip" class="btn btn-info"
                                   title="@lang('dashboard.buttons.edit')"
                                   href="{{route('offices.edit', $office->id)}}">
                                    <i class="fa fa-pencil-square-o"></i></a>

                                <a data-toggle="tooltip" class="btn btn-info delete_record"
                                   title="@lang('dashboard.buttons.delete')"
                                   href="{{route('offices.destroy', $office->id)}}">
                                    <i class="fa fa-trash-o"></i></a>
                            </div>
                        </td>
                    </tr>
                @empty
                    <div class="alert alert-warning">
                        <h4><i class="icon fa fa-warning"></i> @lang('dashboard.messages.notice')</h4>
                        @lang('dashboard.messages.no_stored_records')
                    </div>
                @endforelse
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            {{$offices->links()}}
            <a data-toggle="tooltip" class="btn btn-primary pull-right"
               title="@lang('dashboard.buttons.new')"
               href="{{route('offices.create')}}">
                <i class="fa fa-plus"></i> @lang('dashboard.buttons.new')</a>
        </div>
        <!-- /.box-footer-->
    </div>
@endsection

@section('scripts')
    {{-- Backend scripts  --}}
    {!! Html::script('js/code01.js') !!}
@endsection
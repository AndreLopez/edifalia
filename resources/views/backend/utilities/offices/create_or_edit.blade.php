@extends('layouts.dashboard')

@section('breadcrumb_title')
    @lang('dashboard.mnu_office_management')
@endsection

@section('breadcrumb_subtitle')
    {{$title}}
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_utilities')</a></li>
    <li class="active">@lang('dashboard.mnu_office_management')</li>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('dashboard.mnu_office_management')</h3>
            <div class="btn-group btn-group-sm pull-right">
            <a data-toggle="tooltip" class="btn btn-primary"
               title="@lang('dashboard.buttons.back')"
               href="{{route('offices.index')}}">
                <i class="fa fa-reply"></i></a>
                @if(isset($edit))
                    <a data-toggle="tooltip" class="btn btn-primary"
                       title="@lang('dashboard.buttons.new')"
                       href="{{route('offices.create')}}">
                        <i class="fa fa-plus"></i></a>
                @endif
            </div>
        </div>
        @inject('communities', 'Edifalia\Services\Communities')
        @if(isset($new))
            {!! Form::open($header) !!}
            <div class="box-body">
                @if(!$communities->get()->count())
                    <div class="alert alert-warning">
                        <h4><i class="icon fa fa-warning"></i> @lang('dashboard.messages.notice')</h4>
                        @lang('dashboard.messages.no_communities')
                    </div>
                @endif
                @include('backend.partials.messages')
                @include('backend.utilities.offices.fields')
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-info">
                    <i class="fa fa-save"></i> @lang('dashboard.buttons.save')
                </button>
            </div>
            {!! Form::close() !!}
        @elseif(isset($edit))
            {!! Form::model($office, $header) !!}
            <div class="box-body">
                @include('backend.partials.messages')
                @include('backend.utilities.offices.fields')
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-info">
                    <i class="fa fa-save"></i> @lang('dashboard.buttons.update')
                </button>
            </div>
            {!! Form::close() !!}
        @endif
    </div>
@endsection

@section('scripts')
    <script>
        $("input#all").on("click", function(){
            if ($(this).is(':checked')) {
                $("input#check").prop('checked', true);
            }
            else {
                $("input#check").prop('checked', false);
            }
        });
    </script>
@endsection
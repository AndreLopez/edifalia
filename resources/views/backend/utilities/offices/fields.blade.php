@inject('codes', 'Edifalia\Services\PostalCodes')
<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('identifier') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.identifier')) !!}
        {!! Form::text('identifier', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.identifier'),
            'required' => true])
        !!}
    </div>
    <div class="form-group col-md-6 {{ $errors->has('name') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.name')) !!}
        {!! Form::text('name', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.name'),
            'required' => true])
        !!}
    </div>
</div>

<div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
    {!! Form::label(trans('dashboard.fields.address')) !!}
    {!! Form::textarea('address', null, [
        'rows' => 3,
        'class' => 'form-control',
        'placeholder' => trans('dashboard.fields.address'),
        'required' => true])
    !!}
</div>

<div class="form-group {{ $errors->has('postal_code_id') ? ' has-error' : '' }}">
    {!! Form::label(trans('dashboard.fields.postal_code')) !!}
    {!! Form::select('postal_code_id', $codes->get(),
    null, ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('phone1') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.phone').' 1') !!}
        {!! Form::text('phone1', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.phone'),
            'required' => true])
        !!}
    </div>

    <div class="form-group col-md-6 {{ $errors->has('phone2') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.phone').' 2') !!}
        {!! Form::text('phone2', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.phone')])
        !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('fax') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.fax')) !!}
        {!! Form::text('fax', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.fax')])
        !!}
    </div>

    <div class="form-group col-md-6 {{ $errors->has('email') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.email')) !!}
        {!! Form::email('email', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.email')])
        !!}
    </div>
</div>

<div class="form-group">
    <h4>@lang('dashboard.fields.select_communities'):</h4>
    <div class="checkbox">
        <label>
            {!! Form::checkbox('select_all', 'all', null, ['id' => 'all']) !!}
            @lang('dashboard.fields.select_all')
        </label>
    </div>
    <div class="checkbox">
        @if(isset($new))
            @foreach($communities->get() as $community)
                <label>
                    {!! Form::checkbox('communities[]', $community->id, null, ['id' => 'check'])!!}
                    {{$community->name}}
                </label>&nbsp;&nbsp;&nbsp;
            @endforeach
        @elseif(isset($edit))
            @foreach($communities->get($office->id) as $community)
                <label>
                    {!! Form::checkbox('communities[]', $community->id,
                        $community->hasOffice($office->id), ['id' => 'check'])!!}
                    {{$community->name}}
                </label>&nbsp;&nbsp;&nbsp;
            @endforeach
        @endif
    </div>
</div>
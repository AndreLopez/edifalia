<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('name') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.name')) !!}
        {!! Form::text('name', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.name'),
            'required' => true])
        !!}
    </div>

    <div class="form-group col-md-6 {{ $errors->has('symbol') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.symbol')) !!}
        {!! Form::text('symbol', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.symbol'),
            'required' => true])
        !!}
    </div>
</div>
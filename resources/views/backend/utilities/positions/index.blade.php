@extends('layouts.dashboard')

@section('breadcrumb_title')
    @lang('dashboard.mnu_management')
@endsection

@section('breadcrumb_subtitle')
    @lang('dashboard.titles.positions_list')
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_management')</a></li>
    <li class="active">@lang('dashboard.mnu_positions')</li>
@endsection

@section('modals')
    @include('backend.partials.modals')
@endsection

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('dashboard.mnu_positions')</h3>
        </div>
        <div class="box-body table-responsive">
            <table class="table table-hover">
                @if($positions->count())
                    <tr>
                        <th>@lang('dashboard.tables.name')</th>
                        <th>@lang('dashboard.tables.actions')</th>
                    </tr>
                @endif
                @forelse($positions as $position)
                    <tr data-id="{{$position->id}}"
                        data-rundo="{{route('positions.undelete', $position->id)}}">
                        <td>{{$position->name}}</td>
                        <td>
                            <div class="btn-group btn-group-xs">
                                <a data-toggle="tooltip" class="btn btn-info"
                                   title="@lang('dashboard.buttons.edit')"
                                   href="{{route('positions.edit', $position->id)}}">
                                    <i class="fa fa-pencil-square-o"></i></a>

                                <a data-toggle="tooltip" class="btn btn-info delete_record"
                                   title="@lang('dashboard.buttons.delete')"
                                   href="{{route('positions.destroy', $position->id)}}">
                                    <i class="fa fa-trash-o"></i></a>
                            </div>
                        </td>
                    </tr>
                @empty
                    <div class="alert alert-warning">
                        <h4><i class="icon fa fa-warning"></i> @lang('dashboard.messages.notice')</h4>
                        @lang('dashboard.messages.no_stored_records')
                    </div>
                @endforelse
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            {{$positions->links()}}
            <a data-toggle="tooltip" class="btn btn-primary pull-right"
               title="@lang('dashboard.buttons.new')"
               href="{{route('positions.create')}}">
                <i class="fa fa-plus"></i> @lang('dashboard.buttons.new')</a>
        </div>
        <!-- /.box-footer-->
    </div>
@endsection

@section('scripts')
    {{-- Backend scripts  --}}
    {!! Html::script('js/code01.js') !!}
@endsection
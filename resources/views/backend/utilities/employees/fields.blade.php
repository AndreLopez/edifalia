@inject('positions', 'Edifalia\Services\Positions')
@inject('offices', 'Edifalia\Services\Offices')

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('email') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.email')) !!}
        {!! Form::email('email', (isset($edit))?$employee->user->email:null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.email'),
            'required' => true])
        !!}
    </div>
    <div class="form-group col-md-6 {{ $errors->has('name') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.user_name')) !!}
        {!! Form::text('name', (isset($edit))?$employee->user->name:null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.user_name'),
            'required' => true])
        !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('password') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.password')) !!}
        {!! Form::password('password', [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.password')])
        !!}
    </div>
    <div class="form-group col-md-6">
        {!! Form::label(trans('dashboard.fields.password_confirmation')) !!}
        {!! Form::password('password_confirmation', [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.password_confirmation')])
        !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('first_name') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.name')) !!}
        {!! Form::text('first_name', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.name'),
            'required' => true])
        !!}
    </div>
    <div class="form-group col-md-6 {{ $errors->has('last_name') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.last_name')) !!}
        {!! Form::text('last_name', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.last_name'),
            'required' => true])
        !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('dni') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.dni')) !!}
        {!! Form::text('dni', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.dni'),
            'required' => true])
        !!}
    </div>
    <div class="form-group col-md-6 {{ $errors->has('position_id') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.position')) !!}
        {!! Form::select('position_id', $positions->get(),
        null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('phone') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.phone')) !!}
        {!! Form::text('phone', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.phone')])
        !!}
    </div>
    <div class="form-group col-md-6 {{ $errors->has('extension') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.extension')) !!}
        {!! Form::text('extension', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.extension')])
        !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('department') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.department')) !!}
        {!! Form::text('department', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.department')])
        !!}
    </div>
    <div class="form-group col-md-6 {{ $errors->has('office_id') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.office')) !!}
        {!! Form::select('office_id', $offices->get(),null, [
            'id' => 'office',
            'class' => 'form-control',
            'data-route' => route('offices.communities')])
        !!}
    </div>
</div>

<div class="form-group">
    <h4>@lang('dashboard.fields.select_communities') (@lang('dashboard.fields.see_communities')):</h4>
    <div class="checkbox">
        <label>
            {!! Form::checkbox('select_all', 'all', null, ['id' => 'all_communities']) !!}
            @lang('dashboard.fields.select_all_communities')
        </label>
    </div>
    <div class="checkbox" id="communities"></div>
</div>
<hr>
<div class="form-group">
    <h4>@lang('dashboard.fields.select_permissions'):</h4>
    <div class="checkbox">
        <label>
            {!! Form::checkbox('select_all', 'all', null, ['id' => 'all_permissions']) !!}
            @lang('dashboard.fields.select_all')
        </label>
    </div>
    <div class="checkbox">
        <label>
            {!! Form::checkbox('access_data', 1, null, ['id' => 'check-access']) !!}
            @lang('dashboard.fields.access_data')
        </label>&nbsp;&nbsp;&nbsp;
        <label>
            {!! Form::checkbox('access_management', 1, null, ['id' => 'check-access']) !!}
            @lang('dashboard.fields.access_management')
        </label>&nbsp;&nbsp;&nbsp;
        <label>
            {!! Form::checkbox('access_mail', 1, null, ['id' => 'check-access']) !!}
            @lang('dashboard.fields.access_mail')
        </label>&nbsp;&nbsp;&nbsp;
        <label>
            {!! Form::checkbox('access_incidents', 1, null, ['id' => 'check-access']) !!}
            @lang('dashboard.fields.access_incidents')
        </label>
        <label>
            {!! Form::checkbox('access_reports', 1, null, ['id' => 'check-access']) !!}
            @lang('dashboard.fields.access_reports')
        </label>&nbsp;&nbsp;&nbsp;
        <label>
            {!! Form::checkbox('access_accounting', 1, null, ['id' => 'check-access']) !!}
            @lang('dashboard.fields.access_accounting')
        </label>&nbsp;&nbsp;&nbsp;
    </div>
</div>
<hr>
<div class="form-group">
    {!! Form::label(trans('dashboard.fields.disable_user')) !!}:
    <div class="checkbox">
        <label>
            {!! Form::checkbox('active', 0, (isset($edit) && !$employee->user->active)?'checked':'') !!}
            @lang('dashboard.fields.deactivate')
        </label>
    </div>
</div>
<hr>
<div class="form-group">
    {!! Form::label(trans('dashboard.fields.signature')) !!}:
    <textarea class="textarea" name="signature" placeholder="@lang('dashboard.fields.text')" style="width: 100%;
    height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd;
    padding: 10px;">
        @if(old('signature'))
            {{old('signature')}}
        @elseif(isset($employee))
            {{ html_entity_decode($employee->signature) }}
        @endif
    </textarea>
</div>
@extends('layouts.dashboard')

@section('styles')
    <!-- bootstrap wysihtml5 - text editor -->
    {!! Html::style('static/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}
@endsection

@section('breadcrumb_title')
    @lang('dashboard.mnu_user_access')
@endsection

@section('breadcrumb_subtitle')
    {{$title}}
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_utilities')</a></li>
    <li class="active">@lang('dashboard.mnu_user_access')</li>
@endsection

@section('modals')
    @include('backend.partials.modals')
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('dashboard.mnu_user_access')</h3>
            <div class="btn-group btn-group-sm pull-right">
            <a data-toggle="tooltip" class="btn btn-primary"
               title="@lang('dashboard.buttons.back')"
               href="{{route('employees.index')}}">
                <i class="fa fa-reply"></i></a>
                @if(isset($edit))
                    <a data-toggle="tooltip" class="btn btn-primary"
                       title="@lang('dashboard.buttons.new')"
                       href="{{route('employees.create')}}">
                        <i class="fa fa-plus"></i></a>
                @endif
            </div>
        </div>
        @if(isset($new))
            {!! Form::open($header) !!}
            <div class="box-body">
                @include('backend.partials.messages')
                @include('backend.utilities.employees.fields')
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-info">
                    <i class="fa fa-save"></i> @lang('dashboard.buttons.save')
                </button>
            </div>
            {!! Form::close() !!}
        @elseif(isset($edit))
            {!! Form::model($employee, $header) !!}
            <div class="box-body" data-employee="{{$employee->id}}">
                @include('backend.partials.messages')
                @include('backend.utilities.employees.fields')
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-info">
                    <i class="fa fa-save"></i> @lang('dashboard.buttons.update')
                </button>
            </div>
            {!! Form::close() !!}
        @endif
    </div>
@endsection

@section('scripts')
<!-- Bootstrap WYSIHTML5 -->
{!! Html::script('static/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') !!}
<script>
    $(document).ready(function(){
        //bootstrap WYSIHTML5 - text editor
        $(".textarea").wysihtml5();

        function optionsSelect(){
            $("#pleaseWait").modal("show");
            var office_id = $('#office').val();
            var route = $('#office').data('route');
            var employee_id = $('div.box-body').data('employee');
            if ($.trim(office_id) != '') {
                if (employee_id){
                    $.get(route, {office_id: office_id, employee_id: employee_id}, function(data){
                        if (data.data){
                            $('#communities').html(data.html);
                            $("#pleaseWait").modal("hide");
                        }
                        else {
                            $("#pleaseWait").modal("hide");
                            toastr.error(data.message);
                        }
                    });
                }
                else {
                    $.get(route, {office_id: office_id}, function(data){
                        if (data.data){
                            $('#communities').html(data.html);
                            $("#pleaseWait").modal("hide");
                        }
                        else {
                            $("#pleaseWait").modal("hide");
                            toastr.error(data.message);
                        }
                    });
                }
            }else {
                $("#pleaseWait").modal("hide");
            }
        }
        optionsSelect();
        $('#office').on('change', optionsSelect);

        $("input#all_communities").on("click", function(){
            if ($(this).is(':checked')) {
                $("input#check-commu").prop('checked', true);
            }
            else {
                $("input#check-commu").prop('checked', false);
            }
        });

        $("input#all_permissions").on("click", function(){
            if ($(this).is(':checked')) {
                $("input#check-access").prop('checked', true);
            }
            else {
                $("input#check-access").prop('checked', false);
            }
        });
    });
</script>
@endsection
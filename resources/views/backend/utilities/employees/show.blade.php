@extends('layouts.dashboard')

@section('breadcrumb_title')
    @lang('dashboard.mnu_user_access')
@endsection

@section('breadcrumb_subtitle')
    @lang('dashboard.titles.employee_info')
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_utilities')</a></li>
    <li class="active">@lang('dashboard.mnu_user_access')</li>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('dashboard.mnu_user_access')</h3>
            <div class="btn-group btn-group-sm pull-right">
                <a data-toggle="tooltip" class="btn btn-primary"
                   title="@lang('dashboard.buttons.back')"
                   href="{{route('employees.index')}}">
                    <i class="fa fa-reply"></i></a>
            </div>
        </div>
        <div class="box-body table-responsive">
            <h4>@lang('dashboard.titles.employee_info'):</h4>
            <table class="table table-hover">
                <tr class="info">
                    <th>@lang('dashboard.tables.user_name')</th>
                    <th>@lang('dashboard.tables.email')</th>
                    <th>@lang('dashboard.tables.name')</th>
                    <th>@lang('dashboard.tables.last_name')</th>
                    <th>@lang('dashboard.tables.dni')</th>
                </tr>
                <tr>
                    <td>{{$employee->user->name}}</td>
                    <td>{{$employee->user->email}}</td>
                    <td>{{$employee->first_name}}</td>
                    <td>{{$employee->last_name}}</td>
                    <td>{{$employee->dni}}</td>
                </tr>
            </table><hr>
            <table class="table table-hover">
                <tr class="info">
                    <th>@lang('dashboard.tables.department')</th>
                    <th>@lang('dashboard.tables.phone')</th>
                    <th>@lang('dashboard.tables.extension')</th>
                    <th>@lang('dashboard.tables.position')</th>
                    <th>@lang('dashboard.tables.office')</th>
                </tr>
                <tr>
                    <td>{{$employee->department}}</td>
                    <td>{{$employee->phone}}</td>
                    <td>{{$employee->extension}}</td>
                    <td>{{$employee->position->name}}</td>
                    <td>{{$employee->office->name}}</td>
                </tr>
            </table><hr>

            <h4>@lang('dashboard.titles.communities'):</h4>
            <table class="table table-hover">
                <tr class="info">
                    <th>@lang('dashboard.tables.code')</th>
                    <th>@lang('dashboard.tables.name')</th>
                    <th>@lang('dashboard.tables.address')</th>
                    <th>@lang('dashboard.tables.nif')</th>
                </tr>
                @foreach($employee->communities as $community)
                <tr>
                    <td>{{$community->code}}</td>
                    <td>{{$community->name}}</td>
                    <td>{{$community->address}}</td>
                    <td>{{$community->nif}}</td>
                </tr>
                @endforeach
            </table><hr>

            <h4>@lang('dashboard.titles.permissions'):</h4>
            <table class="table table-hover" style="text-align: center">
                <tr class="info">
                    <th style="text-align: center">@lang('dashboard.fields.access_data')</th>
                    <th style="text-align: center">@lang('dashboard.fields.access_management')</th>
                    <th style="text-align: center">@lang('dashboard.fields.access_mail')</th>
                    <th style="text-align: center">@lang('dashboard.fields.access_incidents')</th>
                    <th style="text-align: center">@lang('dashboard.fields.access_reports')</th>
                    <th style="text-align: center">@lang('dashboard.fields.access_accounting')</th>
                </tr>
                <tr>
                    <td class="{{($employee->access_data)?'success':'danger'}}">
                        <i class="icon fa fa-{{($employee->access_data)?'check':'close'}}"></i>
                    </td>
                    <td class="{{($employee->access_management)?'success':'danger'}}">
                        <i class="icon fa fa-{{($employee->access_management)?'check':'close'}}"></i>
                    </td>
                    <td class="{{($employee->access_mail)?'success':'danger'}}">
                        <i class="icon fa fa-{{($employee->access_mail)?'check':'close'}}"></i>
                    </td>
                    <td class="{{($employee->access_incidents)?'success':'danger'}}">
                        <i class="icon fa fa-{{($employee->access_incidents)?'check':'close'}}"></i>
                    </td>
                    <td class="{{($employee->access_reports)?'success':'danger'}}">
                        <i class="icon fa fa-{{($employee->access_reports)?'check':'close'}}"></i>
                    </td>
                    <td class="{{($employee->access_accounting)?'success':'danger'}}">
                        <i class="icon fa fa-{{($employee->access_accounting)?'check':'close'}}"></i>
                    </td>
                </tr>
            </table>
        </div>
    </div>
@endsection
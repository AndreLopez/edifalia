@inject('offices', 'Edifalia\Services\Offices')
@inject('postalCodes', 'Edifalia\Services\PostalCodes')
@inject('languages', 'Edifalia\Services\Languages')
@inject('currencies', 'Edifalia\Services\Currencies')
@inject('continents', 'Edifalia\Services\Continents')
@inject('numberFormats', 'Edifalia\Services\NumberFormats')
@inject('dateFormats', 'Edifalia\Services\DateFormats')

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('email') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.email')) !!}
        {!! Form::email('email', (isset($main_user))?$main_user->user->email:null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.email'),
            'required' => true])
        !!}
    </div>
    <div class="form-group col-md-6 {{ $errors->has('name') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.user_name')) !!}
        {!! Form::text('name', (isset($main_user))?$main_user->user->name:null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.user_name'),
            'required' => true])
        !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('password') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.password')) !!}
        {!! Form::password('password', [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.password')])
        !!}
    </div>
    <div class="form-group col-md-6">
        {!! Form::label(trans('dashboard.fields.password_confirmation')) !!}
        {!! Form::password('password_confirmation', [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.password_confirmation')])
        !!}
    </div>
</div>
<hr>
<div class="row">
    <div class="form-group col-md-12 {{ $errors->has('company') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.company')) !!}
        {!! Form::text('company', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.company')
            ])
        !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('first_name') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.name')) !!}
        {!! Form::text('first_name', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.name'),
            'required' => true])
        !!}
    </div>
    <div class="form-group col-md-6 {{ $errors->has('last_name') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.last_name')) !!}
        {!! Form::text('last_name', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.last_name'),
            'required' => true])
        !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-12 {{ $errors->has('address') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.address')) !!}
        {!! Form::text('address', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.address')
            ])
        !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('dni') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.dni')) !!}
        {!! Form::text('dni', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.dni'),
            'required' => true])
        !!}
    </div>
    <div class="form-group col-md-6 {{ $errors->has('postal_code_id') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.postal_code')) !!}
        {!! Form::select('postal_code_id', $postalCodes->get(), null,
            ['class' => 'form-control'])
        !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('phone') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.phone')) !!}
        {!! Form::text('phone', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.phone')])
        !!}
    </div>
    <div class="form-group col-md-6 {{ $errors->has('fax') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.fax')) !!}
        {!! Form::text('fax', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.fax')])
        !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label(trans('dashboard.fields.signature_main')) !!}:
    <textarea class="textarea" name="signature" placeholder="@lang('dashboard.fields.text')" style="width: 100%;
    height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd;
    padding: 10px;">
        @if(old('signature'))
            {{old('signature')}}
        @elseif(isset($main_user))
            {{ html_entity_decode($main_user->signature) }}
        @endif
    </textarea>
</div>

<hr>
<div class="form-group">
    {!! Form::label(trans('dashboard.fields.ip_list_option')) !!}:
    <div class="radio">
        <label>
            {!! Form::radio('ip_list_option', 1, null) !!}
            @lang('dashboard.fields.access_allowed_ip')
        </label>
    </div>
    <div class="radio">
        <label>
            {!! Form::radio('ip_list_option', 2, null) !!}
            @lang('dashboard.fields.access_denied_ip')
        </label>
    </div>
</div>
<div class="row">
    <div class="form-group col-md-6">
        {!! Form::text('current_ip', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.enter_ip'),
            'v-model' => 'ipValue'])
        !!}
    </div>
    <div class="form-group col-md-6">
        <button type="button" class="btn btn-primary" v-on:click="addIp">
            <i class="fa fa-plus"></i> @lang('dashboard.buttons.add')
        </button>
    </div>
</div>

<div class="form-group">
    <label>@lang('dashboard.fields.ip_list')</label><br>
    @lang('dashboard.fields.ctrl_key')
    <select multiple class="form-control" name="ip_list[]" id="ip-list">
        <option value="">@lang('dashboard.fields.neither')</option>
        <option v-for="ip in ipArray" :value="ip" selected>@{{ip}}</option>
    </select>
    <div style="display:none">
        <select multiple id="ip-list-temp">
            @if(isset($main_user))
                @if($main_user->ipAccesses()->exists())
                    @foreach($main_user->ipAccesses as $ip)
                        <option value="{{$ip->ip}}" selected>{{$ip->ip}}</option>
                    @endforeach
                @endif
            @endif
        </select>
    </div>
</div>
<hr>

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('collegiate_number') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.collegiate_number')) !!}
        {!! Form::text('collegiate_number', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.collegiate_number')])
        !!}
    </div>
    <div class="form-group col-md-6 {{ $errors->has('aaff_program') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.aaff_program')) !!}
        {!! Form::text('aaff_program', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.aaff_program')])
        !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-12 {{ $errors->has('aaff_email') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.email')) !!}
        {!! Form::email('aaff_email', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.email')])
        !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label(trans('dashboard.fields.letterhead')) !!}:
    <textarea class="textarea" name="letterhead" placeholder="@lang('dashboard.fields.text')" style="width: 100%;
    height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd;
    padding: 10px;">
        @if(old('letterhead'))
            {{old('letterhead')}}
        @elseif(isset($main_user))
            {{ html_entity_decode($main_user->letterhead) }}
        @endif
    </textarea>
</div>
<hr>

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('language_id') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.language')) !!}
        {!! Form::select('language_id', $languages->get(), null,
            ['class' => 'form-control'])
        !!}
    </div>
    <div class="form-group col-md-6">
        {!! Form::label(trans('dashboard.fields.bank_account')) !!}
        <p>@lang('dashboard.fields.bank_account_info')</p>
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('currency_id') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.currency')) !!}
        {!! Form::select('currency_id', $currencies->get(), null,
            ['class' => 'form-control'])
        !!}
    </div>
    <div class="form-group col-md-6" {{ $errors->has('number_format_id') ? ' has-error' : '' }}>
        {!! Form::label(trans('dashboard.fields.numbers')) !!}
        {!! Form::select('number_format_id', $numberFormats->get(), null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6">
        {!! Form::label(trans('dashboard.fields.amounts')) !!}
        <p>{{ $main_user->amounts }}</p>
    </div>
    <div class="form-group col-md-6" {{ $errors->has('date_format_id') ? ' has-error' : '' }}>
        {!! Form::label(trans('dashboard.fields.dates')) !!}
        {!! Form::select('date_format_id', $dateFormats->get(), null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('continent_id') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.continent')) !!}
        {!! Form::select('continent_id', $continents->get(), null,
            ['class' => 'form-control', 'id' => 'continent', 'data-route' => route('main_users.zones')])
        !!}
    </div>
    <div class="form-group col-md-6" {{ $errors->has('zone_id') ? ' has-error' : '' }}>
        {!! Form::label(trans('dashboard.fields.zone')) !!}
        {!! Form::select('zone_id', [], null,
            ['class' => 'form-control', 'id' => 'zones'])
        !!}
    </div>
</div>
<hr>

<!--<div class="form-group">
    {!! Form::label(trans('dashboard.fields.search')) !!}
    {!! Form::text('search', null, [
        'class' => 'form-control',
        'placeholder' => trans('dashboard.fields.search')])
    !!}
</div>-->

<div class="form-group">
    <div class="col-sm-12">
        <div id="map-container"></div>
        <div id="popup" class="ol-popup">
            <a href="#" id="popup-closer" class="ol-popup-closer"></a>
            <div id="popup-content"></div>
        </div>
        {!! Form::hidden('coordinates', (isset($main_user))?$main_user->coordinates:null, ['id' => 'coordinates']) !!}
    </div>
</div>
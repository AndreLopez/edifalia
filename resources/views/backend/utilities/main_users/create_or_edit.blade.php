@extends('layouts.dashboard')

@section('styles')
    <!-- bootstrap wysihtml5 - text editor -->
    {!! Html::style('static/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}
@endsection

@section('breadcrumb_title')
    @lang('dashboard.mnu_user_data')
@endsection

@section('breadcrumb_subtitle')
    {{$title}}
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_utilities')</a></li>
    <li class="active">@lang('dashboard.mnu_user_data')</li>
@endsection

@section('modals')
    @include('backend.partials.modals')
@endsection

@section('content')
    <div class="box box-info" id="ipapp">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('dashboard.mnu_user_data')</h3>
            {{--<div class="btn-group btn-group-sm pull-right">
            <a data-toggle="tooltip" class="btn btn-primary"
               title="@lang('dashboard.buttons.back')"
               href="{{route('employees.index')}}">
                <i class="fa fa-reply"></i></a>
                @if(isset($edit))
                    <a data-toggle="tooltip" class="btn btn-primary"
                       title="@lang('dashboard.buttons.new')"
                       href="{{route('employees.create')}}">
                        <i class="fa fa-plus"></i></a>
                @endif
            </div>--}}
        </div>
        @if(isset($main_user))
            {!! Form::model($main_user, $header) !!}
            <div class="box-body" data-employee="{{$main_user->id}}">
                @include('backend.partials.messages')
                @include('backend.utilities.main_users.fields')
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-info">
                    <i class="fa fa-save"></i> @lang('dashboard.buttons.update')
                </button>
            </div>
            {!! Form::close() !!}
        @endif
    </div>
@endsection

@section('scripts')
    {{-- VueJs --}}
    {!! Html::script('js/vue.min.js') !!}

    {{-- Bootstrap WYSIHTML5 --}}
    {!! Html::script('static/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') !!}

    <script>
        $(document).ready(function(){
            //bootstrap WYSIHTML5 - text editor
            $(".textarea").wysihtml5();

            function selectZone(){
                $("#pleaseWait").modal("show");
                var continent_id = $('#continent').val();
                var route = $('#continent').data('route');
                if ($.trim(continent_id) != '') {
                    $.get(route, {continent_id: continent_id}, function(data){
                        if (data.data){
                            $('#zones').empty();
                            $.each(data.result, function(index, value){
                                if(data.userZoneId == index)
                                    $('#zones').append("<option value='" + index + "' selected>" + value + "</option>");
                                else
                                    $('#zones').append("<option value='" + index + "'>" + value + "</option>");
                            });
                            $("#pleaseWait").modal("hide");
                        }
                        else {
                            $("#pleaseWait").modal("hide");
                            toastr.error(data.result);
                        }
                    });
                }else {
                    $("#pleaseWait").modal("hide");
                }
            }
            selectZone();
            $('#continent').on('change', selectZone);
        });

        /*var sel = document.getElementById("selperiodo");

        for (var i = 0; i < sel.length; i++) {
            //  Aca haces referencia al "option" actual
            var opt = sel[i];

            // Haces lo que te de la gana aca
            console.log(opt.value);
        }*/

        var vue = new Vue({
            el: '#ipapp',
            data: {
                ipValue: "",
                ipArray: []
            },

            methods: {
                addIp: function () {
                    if(this.ipValue != ''){
                        if(this.ipArray.length == 0){
                            this.ipArray.push(this.ipValue);
                        }
                        else {
                            if(this.ipArray.indexOf(this.ipValue) == -1){
                                this.ipArray.push(this.ipValue);
                            }
                        }
                    }
                    this.ipValue = "";
                }
            }
        });

        $("#ip-list-temp option").each(function() {
            vue.ipArray.push($(this).val());
        });
    </script>

    @include('backend.utilities.main_users.map_script')
@endsection
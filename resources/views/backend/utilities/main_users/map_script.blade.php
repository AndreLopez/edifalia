<script>
    var osm = new ol.layer.Tile({
        title: 'OSM',
        type: 'base',
        source: new ol.source.OSM()
    });
    var initCoord = [
        {{ (isset($model) && $model->longitude)?$model->longitude:-3.7046 }},
        {{ (isset($model) && $model->latitude)?$model->latitude:40.4164 }}
    ];

    // Marcado inicial Longitud, Latitud
    var iconGeometry = new ol.geom.Point(initCoord).transform('EPSG:4326', 'EPSG:3857');

    var pointFeature = new ol.Feature({
        geometry: iconGeometry,
        name: 'Mark'
    });

    var iconStyle = new ol.style.Style({
        image: new ol.style.Icon(({
            anchor: [0.5, 15], // Posicion del icono en el eje X Y
            anchorXUnits: 'fraction', // Unidad de medida para el posicionamiento del icon en el eje X
            anchorYUnits: 'pixels', // Unidad de medida para el posicionamiento del icon en el eje X
            src: '{{ url('/static/images/mark.png') }}',
            scale: 1.1 // Tamaño de la imagen de acuerdo a la escala en base al tamaño original
        }))
    });

    pointFeature.setStyle(iconStyle);

    var vectorLayer = new ol.layer.Vector({
        source: new ol.source.Vector({
            features: [pointFeature]
        })
    });

    var mousePosition = new ol.control.MousePosition({
        coordinateFormat: ol.coordinate.createStringXY(4),
        projection: 'EPSG:4326',
        undefinedHTML: '&nbsp;'
    });

    /**
       * Elements that make up the popup.
       */
      var container = document.getElementById('popup');
      var content = document.getElementById('popup-content');
      var closer = document.getElementById('popup-closer');


      /**
       * Create an overlay to anchor the popup to the map.
       */
      var overlay = new ol.Overlay(/** @type {olx.OverlayOptions} */ ({
        element: container,
        autoPan: true,
        autoPanAnimation: {
          duration: 250
        }
      }));


      /**
       * Add a click handler to hide the popup.
       * @return {boolean} Don't follow the href.
       */
      closer.onclick = function() {
        overlay.setPosition(undefined);
        closer.blur();
        return false;
      };

    var map = new ol.Map({
        overlays: [overlay],
        target: 'map-container',
        layers: [osm],
        view: new ol.View({
            center: ol.proj.transform(initCoord, 'EPSG:4326', 'EPSG:3857'), // Centra el mapa en la Longitud y Latitud dada
            zoom: 6
        })
    });

    /**
     * Control Search
     */
    //Instantiate with some options and add the Control
    var geocoder = new Geocoder('nominatim', {
        provider: 'osm',
        //targetType: 'text-input',
        lang: 'es',
        placeholder: 'Buscar ...',
        autoComplete: true,
        limit: 5,
        keepOpen: false,
        animate: false
    });
    map.addControl(geocoder);

    //Listen when an address is chosen
    geocoder.on('addresschosen', function (evt) {
        content.innerHTML = '<p>' + evt.address.formatted + '</p>';
        overlay.setPosition(evt.coordinate);
        $("#coordinates").val(evt.coordinate);
    });

    map.on('singleclick', function(evt) {
        var LonLat = $('.ol-mouse-position').html().split(", ");
        $("#latitude").val(LonLat[1]);
        $("#longitude").val(LonLat[0]);
        // Modifica la posición actual del marcador al hacer click sobre un punto en el mapa
        iconGeometry.setCoordinates(evt.coordinate);
    });

    map.addControl(mousePosition);

    map.on('loadstart', function() {
        cargando.showPleaseWait();
    });

    map.on('loadend', function() {
        cargando.hidePleaseWait();
    });

    map.updateSize();
</script>
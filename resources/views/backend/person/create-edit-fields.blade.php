<div class="row">
	<div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
		<label class="col-sm-3 control-label" for="text">@lang('forms_persons.first_name_title')</label>
		<div class="col-sm-5">
			<div class="input-group">
		    	{!! Form::text(
		    		'first_name', (isset($model))?$model->first_name:null, [
		    			'class' => 'form-control', 'id' => 'first_name', 
		    			'placeholder' => trans('forms_persons.first_name_placeholder')
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('first_name'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('first_name') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
		<label class="col-sm-3 control-label" for="text">@lang('forms_persons.last_name_title')</label>
		<div class="col-sm-5">
			<div class="input-group">
		    	{!! Form::text(
		    		'last_name', (isset($model))?$model->last_name:null, [
		    			'class' => 'form-control', 'id' => 'last_name', 
		    			'placeholder' => trans('forms_persons.last_name_placeholder')
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('last_name'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('last_name') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>	

<div class="row">
	<div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('address') ? ' has-error' : '' }}">
		<label class="col-sm-3 control-label" for="text">@lang('forms_persons.address_title')</label>
		<div class="col-sm-5">
			<div class="input-group">
		  {!! Form::textarea('address', null, [
				'class' => 'form-control', 'id' => 'address', 'rows' => '2',
				'placeholder' => trans('forms_persons.address_placeholder')
			]) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('address'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('address') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('dni') ? ' has-error' : '' }}">
		<label class="col-sm-3 control-label" for="text">@lang('forms_persons.dni_title')</label>
		<div class="col-sm-5">
			<div class="input-group">
		    	{!! Form::text(
		    		'dni', (isset($model))?$model->dni:null, [
		    			'class' => 'form-control', 'id' => 'dni', 
		    			'placeholder' => trans('forms_persons.dni_placeholder')
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('dni'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('dni') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>	

<div class="row">
	<div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('reference') ? ' has-error' : '' }}">
		<label class="col-sm-3 control-label" for="text">@lang('forms_persons.reference_title')</label>
		<div class="col-sm-5">
			<div class="input-group">
		    	{!! Form::text(
		    		'reference', (isset($model))?$model->reference:null, [
		    			'class' => 'form-control', 'id' => 'reference', 
		    			'placeholder' => trans('forms_persons.reference_placeholder')
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('reference'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('reference') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('strength_language') ? ' has-error' : '' }}">
		<label class="col-sm-3 control-label" for="text">@lang('forms_persons.strength_language_title')</label>
		<div class="col-sm-5">
			<div class="input-group">
		    	{!! Form::text(
		    		'strength_language', (isset($model))?$model->strength_language:null, [
		    			'class' => 'form-control', 'id' => 'strength_language', 
		    			'placeholder' => trans('forms_persons.strength_language_placeholder')
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('strength_language'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('strength_language') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>

<div class="row">
<div class="col-sm-6 form-group{{ $errors->has('message_email_only') ? ' has-error' : '' }}">
		<label for="platform_accounting" class="col-sm-3 control-label">
			@lang('forms_persons.message_email_only_title')
		</label>
		<div class="col-sm-9">
			{!! Form::checkbox('message_email_only', True, False, ['data-size' => 'mini']) !!}
		</div>
		@if ($errors->has('message_email_only'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('message_email_only') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6 form-group{{ $errors->has('message_email_only') ? ' has-error' : '' }}">
		<label for="platform_accounting" class="col-sm-3 control-label">
			@lang('forms_persons.group_properties_title')
		</label>
		<div class="col-sm-9">
			{!! Form::checkbox('group_properties', True, False, ['data-size' => 'mini']) !!}
		</div>
		@if ($errors->has('group_properties'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('group_properties') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>

	<div class="row">
<div class="col-sm-6 form-group{{ $errors->has('group_invoice_concepts') ? ' has-error' : '' }}">
		<label for="platform_accounting" class="col-sm-3 control-label">
			@lang('forms_persons.group_invoice_concepts_title')
		</label>
		<div class="col-sm-9">
			{!! Form::checkbox('group_invoice_concepts', True, False, ['data-size' => 'mini']) !!}
		</div>
		@if ($errors->has('group_invoice_concepts'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('group_invoice_concepts') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

<div class="col-sm-6 form-group{{ $errors->has('dont_get_email') ? ' has-error' : '' }}">
		<label for="platform_accounting" class="col-sm-3 control-label">
			@lang('forms_persons.dont_get_email_title')
		</label>
		<div class="col-sm-9">
			{!! Form::checkbox('dont_get_email', True, False, ['data-size' => 'mini']) !!}
		</div>
		@if ($errors->has('dont_get_email'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('dont_get_email') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>

<div class="row">
<div class="col-sm-6 form-group{{ $errors->has('can_update_own_data') ? ' has-error' : '' }}">
		<label for="platform_accounting" class="col-sm-3 control-label">
			@lang('forms_persons.can_update_own_data_title')
		</label>
		<div class="col-sm-9">
			{!! Form::checkbox('can_update_own_data', True, False, ['data-size' => 'mini']) !!}
		</div>
		@if ($errors->has('can_update_own_data'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('can_update_own_data') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6 form-group{{ $errors->has('exclusiveness') ? ' has-error' : '' }}">
		<label for="platform_accounting" class="col-sm-3 control-label">
			@lang('forms_persons.exlusiveness_title')
		</label>
		<div class="col-sm-9">
			{!! Form::checkbox('exclusiveness', True, False, ['data-size' => 'mini']) !!}
		</div>
		@if ($errors->has('exclusiveness'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('exclusiveness') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>

<div class="row">
<div class="col-sm-6 form-group{{ $errors->has('virtual_office_access') ? ' has-error' : '' }}">
		<label for="platform_accounting" class="col-sm-3 control-label">
			@lang('forms_persons.virtual_office_access_title')
		</label>
		<div class="col-sm-9">
			{!! Form::checkbox('virtual_office_access', True, False, ['data-size' => 'mini']) !!}
		</div>
		@if ($errors->has('virtual_office_access'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('virtual_office_access') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6 form-group{{ $errors->has('update_data_from_virtual_office') ? ' has-error' : '' }}">
		<label for="platform_accounting" class="col-sm-3 control-label">
			@lang('forms_persons.update_data_from_virtual_office_title')
		</label>
		<div class="col-sm-9">
			{!! Form::checkbox('update_data_from_virtual_office', True, False, ['data-size' => 'mini']) !!}
		</div>
		@if ($errors->has('update_data_from_virtual_office'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('update_data_from_virtual_office') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>

<div class="row">
	<div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
		<label class="col-sm-3 control-label" for="text">@lang('forms_persons.notes_title')</label>
		<div class="col-sm-5">
			<div class="input-group">
		  {!! Form::textarea('notes', null, [
				'class' => 'form-control', 'id' => 'notes', 'rows' => '2',
				'placeholder' => trans('forms_persons.notes_placeholder')
			]) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('notes'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('notes') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6 form-group{{ $errors->has('postal_code_id') ? ' has-error' : '' }}">
		<label for="postal_code_id" class="col-sm-3 control-label">@lang('forms.postal_code_label')</label>
		<div class="col-sm-5">
			<div class="input-group">
	    		{!! Form::select(
		    		'postal_code_id', isset($postalCodes) ? $postalCodes : [], null, [
		    			'id' => 'postal_code_id', 'class' => 'select2', 
		    			'placeholder' => trans('forms.select_null_option'),
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('postal_code_id'))
			<div class="col-sm-9 col-sm-offset-3	">
		        <span class="help-block">
		            <strong>{{ $errors->first('postal_code_id') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>

<div class="row">
<div class="col-sm-6 form-group{{ $errors->has('payment_method_id') ? ' has-error' : '' }}">
		<label for="postal_code_id" class="col-sm-3 control-label">@lang('forms_contracts.payment_method_title')</label>
		<div class="col-sm-5">
			<div class="input-group">
	    		{!! Form::select(
		    		'payment_method_id', isset($paymentMethods) ? $paymentMethods : [], null, [
		    			'id' => 'payment_method_id', 'class' => 'select2', 
		    			'placeholder' => trans('forms_supplier.select_null_option'),
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('payment_method_id'))
			<div class="col-sm-9 col-sm-offset-3	">
		        <span class="help-block">
		            <strong>{{ $errors->first('payment_method_id') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	
<div class="col-sm-6 form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
		<label for="postal_code_id" class="col-sm-3 control-label">@lang('forms_persons.user_id_title')</label>
		<div class="col-sm-5">
			<div class="input-group">
	    		{!! Form::select(
		    		'user_id', isset($users) ? $users : [], null, [
		    			'id' => 'user_id', 'class' => 'select2', 
		    			'placeholder' => trans('forms_supplier.select_null_option'),
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('user_id'))
			<div class="col-sm-9 col-sm-offset-3	">
		        <span class="help-block">
		            <strong>{{ $errors->first('user_id') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
	</div>


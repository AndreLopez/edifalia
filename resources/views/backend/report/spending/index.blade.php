@extends('backend.report.baseReport')

@section('breadcrumb_title')
    @lang('dashboard.mnu_compare_budget_spending')
@endsection

@section('content')
<div class="box" ng-app="edifaliaApp" ng-controller="SpendingController as spending">
    <div class="box-header with-border">
        <h3 class="box-title">@lang('dashboard.mnu_compare_budget_spending')</h3>
    </div>
    <div class="box-body table-responsive">
    	<form class="form-horizontal">
          	<div class="box-body">
	            <div class="form-group">
	              <label class="col-sm-2 control-label">@lang('report.dossier.form.communities')</label>
	              	<div class="col-sm-10">
		              	<select class="form-control" id="selectCommunity" ng-model="spending.model.community" ng-change="spending.selectProperty()" ng-options="dataCommunty as dataCommunty.name for dataCommunty in spending.community track by dataCommunty.id">
			            	<option value=''>@lang('report.dossier.form.select-community')</option>
			          	</select>	                	
	                </div>
	            </div>
	            <div class="form-group">
	              <label class="col-sm-2 control-label">@lang('report.report.budget')</label>
	              <div class="col-sm-10">
	                	<select class="form-control" id="selectBudget" ng-model="spending.model.budget" ng-change="spending.selectBudget()" ng-options="dataBudget as dataBudget.title for dataBudget in spending.budgets track by dataBudget.id" ng-disabled="!spending.model.community">
		                    <option value="">@lang('report.report.select-budget') </option>
	                  	</select>
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-sm-2 control-label">Gastos</label>
	              <div class="col-sm-10">
	                	<select class="form-control" ng-model="spending.model.expense" ng-disabled="!spending.model.budget || !spending.model.community">
		                    <option value="">Seleccione Ejercicio Contable para comparar Gastos </option>
	                  	</select>
	              </div>
	            </div>
	            <div class="form-group">
	            	<label class="col-sm-2 control-label">@lang('report.dossier.form.include-owner'):</label>
	            	<div class="col-sm-10">
	                    <input type="checkbox"> Aplicar GRUPOS DE GASTOS
	                    <input type="checkbox"> Aplicar presupuesto original
	            	</div>
	            </div>
	        </div>
          <!-- /.box-body -->
          	<div class="box-footer">
            	<a href type="submit" class="btn btn-primary pull-right" ng-click="spending.generatePdf()" ng-disabled="!spending.model.community || !spending.model.budget">@lang('report.buttons.generate')</a>
          	</div>
          <!-- /.box-footer -->
        </form>
    </div>
</div>
@endsection
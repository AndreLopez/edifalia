<style type="text/css">
	.border{
		/*border: 1px solid #000;*/
	}

	th.title{
		background-color: #F2F2F2;
		/*text-align: right;*/
	}

	.title-tb{
		background-color: #979797;
		font-size: 14px;
	}
	.foot{
		background-color: #B2B2B2;
	}

	td.body{
		text-align: left;
	}
</style>
<br><br><br>
<table width="100%">
	<thead>
		<tr>
			<th class="title" width="30%"><b>@lang('report.report.community'):</b> </th>
			<th width="80%">&nbsp;{{ $community->name }}</th>
		</tr>
		{{--<tr>
			<th class="title" width="20%">@lang('report.report.address'): </th>
			<th width="80%">{{ $community->address }}</th>
		</tr>--}}
		{{--<tr>
			<th class="title" width="20%">@lang('report.report.nif'): </th>
			<th width="80%">{{ $community->nif }}</th>
		</tr>		--}}
	</thead>	
</table><br><br>
<table width="100%">
	<thead>
		<tr>
			<th class="title" width="30%"><b>@lang('report.report.budget'):</b> </th>
			<th width="80%">&nbsp;{{ $budget->title }}</th>
		</tr>
		<tr>
			<th class="title" width="30%"><b>Año:</b> </th>
			<th width="80%">&nbsp;{{ $budget->year }}</th>
		</tr>
		<tr>
			<th class="title" width="30%"><b>Monto total:</b> </th>
			<th width="80%">&nbsp;{{ $amountTotal }}</th>
		</tr>
	</thead>	
</table>
<br><br>
<table width="100%" class="border">
	<thead>
		<tr>
			<th class="title-tb" width="35%">Mes: </th>			
			<th class="title-tb" width="35%">Año: </th>
			<th class="title-tb" width="70%">Importe: </th>			
		</tr>		
	</thead>
	<tbody>
		@foreach($receipt as $row)
		<tr>
			<td width="35%">{{ $row->month }}</td>
			<td width="35%">{{ $row->year }}</td>
			<td width="70%">{{ $row->total_amount }}</td>
		</tr>
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<td class="foot">Total Gastos</td>
			<td class="foot"></td>
			<td class="foot">{{ $totalGastos }}</td>
		</tr>
		<tr>
			<td class="foot">Diferencia</td>
			<td class="foot"></td>
			<td class="foot">{{ $dif }}</td>
		</tr>
	</tfoot>
</table>
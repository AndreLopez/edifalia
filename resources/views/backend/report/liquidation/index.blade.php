@extends('backend.report.baseReport')

@section('breadcrumb_title')
    @lang('dashboard.mnu_liquidations')
@endsection

@section('content')
<div class="box" ng-app="edifaliaApp" ng-controller="LiquidationController as liquidation">
    <div class="box-header with-border">
        <h3 class="box-title">@lang('dashboard.mnu_liquidations')</h3>
    </div>
    <div class="box-body table-responsive">
    	<form class="form-horizontal">
          	<div class="box-body">
	            <div class="form-group">
	              <label class="col-sm-2 control-label">@lang('report.dossier.form.communities')</label>
	              	<div class="col-sm-10">
		              	<select class="form-control" id="selectCommunity" ng-model="liquidation.model.community" ng-change="liquidation.selectProperty()" ng-options="dataCommunty as dataCommunty.name for dataCommunty in liquidation.community track by dataCommunty.id">
			            	<option value=''>@lang('report.dossier.form.select-community')</option>
			          	</select>	                	
	                </div>
	            </div>
	            <div class="form-group">
	              <label class="col-sm-2 control-label">@lang('report.liquidation.exercise')</label>
	              <div class="col-sm-10">
	                	<select class="form-control" ng-disabled="!liquidation.model.community">
		                    <option>@lang('report.liquidation.exercise-select')</option>
	                  	</select>
	              </div>
	            </div>
	        </div>
          	<div class="box-footer">
            	<a href class="btn btn-primary pull-right" ng-click="liquidation.generateReport()" ng-disabled="!liquidation.model.community">@lang('report.buttons.generate')</a>
          	</div>
        </form>
    </div>
</div>
@endsection
<style type="text/css">
	th.title{
		background-color: #D7D7D7;
		/*text-align: right;*/
	}
	
	.tx-title{
		background-color: #979797;
	}

	.tx{
		font-size: 16px;
		text-align: center;
	}

	td.body{
		text-align: left;
	}

	.tfoot{
		background-color: #D7D7D7;
		font-size: 16px;
	}
</style>
<br><br>
<br><br>
<table width="100%">
	<thead>
		<tr>
			<th class="tx-title tx" width="75%"><b>Gastos</b></th>
			<th class="tx-title tx" width="75%"><b>Ingresos</b></th>			
		</tr>
	</thead>
	<thead>
		<tr>
			<th class="title" width="50%">Concepto </th>
			<th class="title" width="25%">Importe</th>
			<th class="title" width="50%">Concepto </th>
			<th class="title" width="25%">Importe</th>			
		</tr>
	</thead>
	<tbody>
		@foreach($properties as $row)
			<tr>
				<td width="50%">{{ $row->title }}</td>
				<td width="25%">{{ $row->general }}</td>
				<td width="50%">{{ $row->title }}</td>
				<td width="25%">{{ $row->amount }}</td>				
			</tr>
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<td class="tfoot">Total</td>
			<td class="tfoot">{{ $totalGeneral }}</td>
			<td class="tfoot"></td>
			<td class="tfoot">{{ $totalAmount }}</td>
		</tr>
	</tfoot>
</table><br><br><br>

{{--<h3>Gastos</h3>
<table width="100%">
	<thead>
		<tr>
			<th class="title" width="35%">Propiedad </th>			
			<th class="title" width="35%">Monto General </th>
			<th class="title" width="35%">Importe Recibido </th>			
		</tr>		
	</thead>
	<tbody>
		@foreach($properties as $row)
			<tr>
				<td width="35%">{{ $row->property_number }}</td>
				<td width="35%">{{ $row->general }}</td>
				<td width="35%">{{ $row->amount }}</td>
			</tr>
		@endforeach
	</tbody>
</table>--}}
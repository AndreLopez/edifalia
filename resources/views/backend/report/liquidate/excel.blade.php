<html>
{!! Html::style('static/angular/style-excel.css') !!}
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<table>		
	<tr>
		<td><img src="static/images/logo-horizontal.png" widtd="35%" height="42%"></td>
	</tr>
	<tr>
		<td></td>	
		<td><b>{{ $title }}</b></td>
	</tr>		
</table>
<br><br><br>
<table width="100%">
	<thead>
		<tr>
			<th class="title" width="15%">Titulo: </th>
			<td width="80%">{{ $dataReading->title }}</td>
		</tr>		
		<tr>
			<th class="title" width="15%">Fecha: </th>
			<td width="100%">{{ $dataReading->date }}</td>
		</tr>		
	</thead>	
</table><br><br>
<!-- <h4>Detalles</h4> -->
<table width="100%" class="table-tx">
	<thead>
		<tr>
			<th class="title-tb" width="25%">Propiedad</th>
			<th class="title-tb" width="25%">Anterior</th>
			<th class="title-tb" width="25%">Actual</th>
			<th class="title-tb" width="25%">Consumo</th>
		</tr>
	</thead>
	<tbody>
		@foreach($readings as $row)
		<tr>
			<td width="25%" class="body">{{ $row->property_number }}</td>
			<td width="25%" class="body" align="right">{{ $row->previous }}</td>
			<td width="25%" class="body" align="right">{{ $row->current }}</td>
			<td width="25%" class="body" align="right">{{ $row->consumption }}</td>
		</tr>
	    @endforeach
	</tbody>
	<tfoot>
		<tr>
			<td class="title xt footer"><b>Total:</b></td>
			<td class="footer">{{ $arrayTotales['totalPrevious'] }}</td>
			<td class="footer">{{ $arrayTotales['totalCurrent'] }}</td>
			<td class="footer">{{ $arrayTotales['totalConsumption'] }}</td>
		</tr>
	</tfoot>
</table><br><br>
</html>

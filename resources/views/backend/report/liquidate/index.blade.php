@extends('backend.report.baseReport')

@section('breadcrumb_title')
    @lang('dashboard.mnu_liquidate_cons')
@endsection

@section('content')
<div class="box" ng-app="edifaliaApp" ng-controller="LiquidateConsController as liquidate">
    <div class="box-header with-border">
        {{-- <h3 class="box-title">@lang('dashboard.mnu_liquidate_cons')</h3> --}}
        <!-- <a href="{{ route('meeting.create') }}" class="btn btn-app">
            <i class="fa fa-save"></i> @lang('report.buttons.load')
        </a> -->
        <a href class="btn btn-app" ng-click="liquidate.reset()">
            <i class="fa fa-undo"></i> @lang('report.buttons.reset')
        </a>
        <a href ng-click="liquidate.model.reading && liquidate.generatePdf()" class="btn btn-app" disabled="!liquidate.model.reading">
            <i class="fa fa-file-pdf-o"></i> @lang('report.buttons.pdf')
        </a>
        <a href ng-click="liquidate.model.reading && liquidate.generateExcel()" class="btn btn-app" disabled="!liquidate.model.reading">
            <i class="fa fa-file-excel-o"></i> @lang('report.buttons.excel')
        </a>
    </div>
    <div class="box-body table-responsive">
    	<form class="form-horizontal">
          	<div class="box-body">
	            <div class="form-group">
	              <label class="col-sm-2 control-label">@lang('report.dossier.form.communities')</label>
	              	<div class="col-sm-10">
		              	<select class="form-control" id="selectCommunity" ng-model="liquidate.model.community" ng-change="liquidate.selectCommunity()" ng-options="dataCommunty as dataCommunty.name for dataCommunty in liquidate.community track by dataCommunty.id">
			            	<option value=''>@lang('report.dossier.form.select-community')</option>
			          	</select>	                	
	                </div>
	            </div>
	            <div class="form-group">
	                <label class="col-sm-2 control-label">@lang('report.liquidation_cons.liquidate'):</label>
	                <div class="col-sm-10">
	                	<select class="form-control" id="selectCommunity" ng-model="liquidate.model.reading" ng-change="liquidate.selectReading()" ng-options="dataReading as dataReading.title for dataReading in liquidate.readingData track by dataReading.id" ng-disabled="!liquidate.model.community">
			            	<option value=''>@lang('report.liquidation_cons.liquidation-select')</option>
			          	</select>	
	                </div>
	            </div>
	            <div class="col-sm-12">
	              <div class="nav-tabs-custom" disabled>
		            <ul class="nav nav-tabs">
		              <li class="active"><a href="#tab_1" data-toggle="tab">@lang('report.meeting.characteristics')</a></li>		              
		            </ul>
		            <div class="tab-content">
		              	<div class="tab-pane active" id="tab_1">
		              		<div class="form-group">
        						<label class="col-sm-2 control-label">@lang('report.meeting.title'):</label>
        						<div class="col-sm-4">
        							<input type="text" class="form-control" placeholder="@lang('report.meeting.title')" ng-model="meeting.model.formMeeting.title" required ng-disabled="!liquidate.model.reading">
        						</div>        					
                                <label class="col-sm-2 control-label">Unidades de Consumo:</label>
                                <div class="col-sm-4">
                                    <select class="form-control" ng-disabled="!liquidate.model.reading">
                                        <option value="">Seleccione</option>
                                        <option value="">Horas</option>
                                        <option value="">Litros</option>
                                        <option value="">Kw.</option>
                                        <option value="">m3</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
        						<label class="col-sm-2 control-label">Grupo de reparto:</label>
        						<div class="col-sm-4">
        							<select class="form-control" ng-disabled="!liquidate.model.reading">
                                        <option value="0">Seleccione</option>
                                        <option value="1">General</option>
                                        <option value="2">Detallado</option>
        								<option value="3">Agrupado por Propiedad</option>
        							</select>
        						</div>
                                <label class="col-sm-2 control-label">Total consumido:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" disabled value="{$ liquidate.model.consumption | number : 2 $}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Ejercicio contable:</label>
                                <div class="col-sm-4">
                                    <select class="form-control" ng-disabled="!liquidate.model.reading">
                                        <option value="">Sin ejercicio contable cargado...</option>
                                    </select>
                                </div>
                                <label class="col-sm-2 control-label">Total Pagado:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" disabled value="{$ liquidate.model.payment | number : 2 $}">
                                </div>                            
                            </div>
                            <div class="form-group">
                                <label class="col-sm-8 control-label">Porcentaje del total pagado repartido en partes iguales:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" ng-disabled="!liquidate.model.reading">
                                </div>                            
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Fecha:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" value="{$ liquidate.reading.date $}" disabled>
                                </div>                                
                            </div> 
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                      <th>@lang('report.report.property')</th>
                                      <th>Anterior</th>
                                      <th>Actual</th>
                                      <th>Consumo</th>
                                    </tr>
                                </thead>
                                <tbody>                                  
                                    <tr ng-repeat="row in liquidate.propertyReadings">
                                      <td>{$ row.property_number $}</td>
                                      <td>{$ row.previous $}</td>
                                      <td>{$ row.current $}</td>
                                      <td>{$ row.consumption $}</td>
                                    </tr>
                                </tbody>
                            </table>       					
		              	</div>		              
		        	</div>				            
		    	</div>				          
		    </div>
            </div>
	        </div>
          <!-- /.box-body -->
          	<div class="box-footer">
            	{{-- <a href class="btn btn-primary pull-right" ng-click="liquidate.generateReport()" ng-disabled="!liquidation.model.community">@lang('report.buttons.generate')</a> --}}
          	</div>
          <!-- /.box-footer -->
        </form>
    </div>
</div>
@endsection
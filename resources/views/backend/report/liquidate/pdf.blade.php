<style type="text/css">
	th.title{
		background-color: #F2F2F2;
		font-size: 14px;
		font-weight: bold;
	}

	.xt{
		font-size: 15px;
	}

	.title-tb{
		background-color: #979797;
		font-size: 14px;
	}

	.table-tx{
		/*border: 1px solid #000;*/
	}

	td{
		/*border: 1px solid #000;*/
	}

	td.body{
		text-align: left;
	}

	td.footer{
		background-color: #A6A6A6;
	}
</style>
<br><br><br>
<table width="100%">
	<thead>
		<tr>
			<th class="title" width="20%">Titulo: </th>
			<th width="80%">{{ $dataReading->title }}</th>
		</tr>		
		<tr>
			<th class="title" width="20%">Fecha: </th>
			<th width="100%">{{ $dataReading->date }}</th>
		</tr>		
	</thead>	
</table><br><br>
<!-- <h4>Detalles</h4> -->
<table width="100%" class="table-tx">
	<thead>
		<tr>
			<th class="title-tb" width="38%">Propiedad</th>
			<th class="title-tb" width="38%">Anterior</th>
			<th class="title-tb" width="38%">Actual</th>
			<th class="title-tb" width="38%">Consumo</th>
		</tr>
	</thead>
	<tbody>
		@foreach($readings as $row)
		<tr>
			<td width="38%" class="body">{{ $row->property_number }}</td>
			<td width="38%" class="body" align="right">{{ $row->previous }}</td>
			<td width="38%" class="body" align="right">{{ $row->current }}</td>
			<td width="38%" class="body" align="right">{{ $row->consumption }}</td>
		</tr>
	    @endforeach
	</tbody>
	<tfoot>
		<tr>
			<td class="title xt footer"><b>Total:</b></td>
			<td class="footer">{{ $arrayTotales['totalPrevious'] }}</td>
			<td class="footer">{{ $arrayTotales['totalCurrent'] }}</td>
			<td class="footer">{{ $arrayTotales['totalConsumption'] }}</td>
		</tr>
	</tfoot>
</table><br><br>
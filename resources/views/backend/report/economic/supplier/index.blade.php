@extends('backend.report.baseReport')

@section('breadcrumb_title')
    @lang('report.economic.account-groups')
@endsection

@section('content')
<div class="box" ng-app="edifaliaApp" ng-controller="DossierController as dossier">
    <div class="box-header with-border">
        <h3 class="box-title">@lang('dashboard.mnu_dossier_sign')</h3>
    </div>
    <div class="box-body table-responsive">
    	<form class="form-horizontal">
          	<div class="box-body">
	            <div class="form-group">
	              <label class="col-sm-2 control-label">@lang('report.dossier.form.communities')</label>
	              	<div class="col-sm-10">
		              	<select class="form-control" id="selectCommunity" ng-model="dossier.model.community" ng-change="dossier.selectProperty()" ng-options="dataCommunty as dataCommunty.name for dataCommunty in dossier.community track by dataCommunty.id">
			            	<option value=''>@lang('report.dossier.form.select-community')</option>
			          	</select>	                	
	                </div>
	            </div>
	            <div class="form-group">
	              <label class="col-sm-2 control-label">Ejercicio contable</label>
	              <div class="col-sm-10">
	                	<select class="form-control" ng-disabled="!dossier.model.community">
		                    <option>@lang('report.dossier.form.select-coefficients')</option>
	                  	</select>
	              </div>
	            </div>

	            
	        </div>          
          	<div class="box-footer">
            	<button type="submit" class="btn btn-default">@lang('report.buttons.cancel')</button>
            	<button type="submit" class="btn btn-primary pull-right" ng-click="dossier.generateReport()" ng-disabled="!dossier.model.community">@lang('report.buttons.generate')</button>
          	</div>          
        </form>
    </div>
</div>
@endsection
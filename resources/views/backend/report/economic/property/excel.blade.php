<html>
{{-- {!! Html::style('static/angular/style-excel.css') !!} --}}
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<table>		
		<tr>
			<td><img src="static/images/logo-horizontal.png" widtd="35%" height="42%"></td>
		</tr>
		<tr>
			<td></td>	
			<td><b>{{ $title }}</b></td>
		</tr>		
	</table>
    <table width="95%">
		<thead>
			<tr>
				<th class="title" width="25%">Datos propiedad</th>
				<th class="title" width="25%">Cuota ordinaria</th>
				@foreach($preData as $dataM)
					<th class="title" width="25%">{{ $dataM['name'] }}</th>
				@endforeach				
			</tr>
		</thead>
		<tbody>			
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					@foreach($preData as $dataMont)
						<td>
							<table>								
								@foreach($data[$dataMont['id']]['data'] as $row)
									<tr>
										<td>{{ $row['amount'] }}</td>
									</tr>
								@endforeach
							</table>							
						</td>
					@endforeach
				</tr>
		</tbody>
	</table>
</html>

@extends('backend.report.baseReport')

@section('breadcrumb_title')
    Ingresos realizados por propiedades
@endsection

@section('content')
<div class="box" ng-app="edifaliaApp" ng-controller="EconomicPropertyController as property">
    <div class="box-header with-border">
        <h3 class="box-title">Ingresos realizados por propiedades</h3>
    </div>
    <div class="box-body table-responsive">
    	<form class="form-horizontal">
          	<div class="box-body">
	            <div class="form-group">
	              <label class="col-sm-2 control-label">@lang('report.dossier.form.communities')</label>
	              	<div class="col-sm-10">
		              	<select class="form-control" id="selectCommunity" ng-model="property.model.community" ng-change="property.selectProperty()" ng-options="dataCommunty as dataCommunty.name for dataCommunty in property.community track by dataCommunty.id">
			            	<option value=''>@lang('report.dossier.form.select-community')</option>
			          	</select>	                	
	                </div>
	            </div>
	            {{-- <div class="form-group">
	              <label class="col-sm-2 control-label">Desde</label>
	              <div class="col-sm-4">
	                	<input type="text" class="form-control datepicker" placeholder="" required ng-disabled="!property.model.community" ng-model="property.model.dateStart">
	              </div>
	              <label class="col-sm-2 control-label">Hasta</label>
	              <div class="col-sm-4">
	                	<input type="text" class="form-control datepicker" placeholder="" required ng-disabled="!property.model.community" ng-model="property.model.dateEnd">
	              </div>
	            </div> --}}
	        </div>          
          	<div class="box-footer">
            	<button type="submit" class="btn btn-default">@lang('report.buttons.cancel')</button>
            	<button type="submit" class="btn btn-primary pull-right" ng-click="property.generateReportPdf()" ng-disabled="!property.model.community">@lang('report.buttons.generate')</button>
          	</div>          
        </form>
    </div>
</div>
@endsection
@extends('backend.report.baseReport')

@section('breadcrumb_title')
    Total de gastos
@endsection

@section('content')
<div class="box" ng-app="edifaliaApp" ng-controller="EconomicTotalController as report">
    <div class="box-header with-border">
        <h3 class="box-title">Total de gastos</h3>
    </div>
    <div class="box-body table-responsive">
    	<form class="form-horizontal">
          	<div class="box-body">
	            <div class="form-group">
	              <label class="col-sm-2 control-label">@lang('report.dossier.form.communities')</label>
	              	<div class="col-sm-10">
		              	<select class="form-control" id="selectCommunity" ng-model="report.model.community" ng-change="report.selectProperty()" ng-options="dataCommunty as dataCommunty.name for dataCommunty in report.community track by dataCommunty.id">
			            	<option value=''>@lang('report.dossier.form.select-community')</option>
			          	</select>	                	
	                </div>
	            </div>
	            <div class="form-group">
	              <label class="col-sm-2 control-label">Ejercicio contable</label>
	              <div class="col-sm-10">
	                	<select class="form-control" ng-disabled="!report.model.community">
		                    <option>@lang('report.dossier.form.select-coefficients')</option>
	                  	</select>
	              </div>
	            </div>

	            
	        </div>          
          	<div class="box-footer">
            	<button type="submit" class="btn btn-default">@lang('report.buttons.cancel')</button>
            	<button type="submit" class="btn btn-primary pull-right" ng-click="report.generateReportExcel()" ng-disabled="!report.model.community">@lang('report.buttons.generate')</button>
          	</div>          
        </form>
    </div>
</div>
@endsection
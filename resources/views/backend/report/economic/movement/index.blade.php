@extends('backend.report.baseReport')

@section('breadcrumb_title')
    Ingresos y gastos con detalles de movimiento
@endsection

@section('content')
<div class="box" ng-app="edifaliaApp" ng-controller="EconomicMovementController as report">
    <div class="box-header with-border">
        <h3 class="box-title">Ingresos y gastos con detalles de movimiento</h3>
    </div>
    <div class="box-body table-responsive">
    	<form class="form-horizontal">
          	<div class="box-body">
	            <div class="form-group">
	              <label class="col-sm-2 control-label">@lang('report.dossier.form.communities')</label>
	              	<div class="col-sm-10">
		              	<select class="form-control" id="selectCommunity" ng-model="report.model.community" ng-change="dossier.selectProperty()" ng-options="dataCommunty as dataCommunty.name for dataCommunty in report.community track by dataCommunty.id">
			            	<option value=''>@lang('report.dossier.form.select-community')</option>
			          	</select>	                	
	                </div>
	            </div>
	            <div class="form-group">
		            <div class="col-sm-4">
		            	<input type="checkbox" ng-model="report.model.includeAll"> Hasta mes actual
		            </div>
	              	<label class="col-sm-2 control-label">Hasta mes</label>
			            <div class="col-sm-6">
				            <select class="form-control" id="selectMonth" ng-model="report.model.months" ng-change="report.selectProperty()" ng-options="dataMonth as dataMonth.name for dataMonth in report.months track by dataMonth.id" ng-disabled="report.model.includeAll"></select>	                	
			            </div>
			        </div>
	            <div class="form-group">
	              <label class="col-sm-2 control-label">Ejercicio contable</label>
	              <div class="col-sm-10">
	                	<select class="form-control" ng-disabled="!report.model.community">
		                    <option>Seleccine ejercicio contable</option>
	                  	</select>
	              </div>
	            </div>

	            
	        </div>          
          	<div class="box-footer">
            	<button type="submit" class="btn btn-default">@lang('report.buttons.cancel')</button>
            	<button type="submit" class="btn btn-primary pull-right" ng-click="report.generateReportExcel()" ng-disabled="!report.model.community">@lang('report.buttons.generate')</button>
          	</div>          
        </form>
    </div>
</div>
@endsection
<html>
{{-- {!! Html::style('static/angular/style-excel.css') !!} --}}

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<table>		
		<tr>
			<td><img src="static/images/logo-horizontal.png" widtd="35%" height="42%"></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td></td>	
			<td><b>{{ $title }}</b></td>
		</tr>		
	</table>
    <table width="95%">
		<thead>
			<tr>
				<td width="5%"></td>
				<th class="title" width="40%">Desglose de saldo incial</th>
				<th class="title" width="20%"></th>
				<th class="title" width="20%"></th>
				<th class="title" width="20%"></th>
			</tr>
		</thead>
		<tbody>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tbody>	
	</table>
	<br><br>
	<table width="95%">
		<thead>
			<tr>
				<td width="5%"></td>
				<th class="title" width="40%">Conceptos</th>
				<th class="title" width="20%">Ingresos</th>
				<th class="title" width="20%">Gastos</th>
				<th class="title" width="20%">Saldos</th>
			</tr>
		</thead>
		<tbody>
			@foreach($dataBudget as $row)
				<tr>
					<td></td>
					<td><b>{{ $row['title'] }}</b></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td>{{ $row['data']['title'] }}</td>
					<td align="right">{{ $row['data']['amount'] }}</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			@endforeach
		</tbody>
		<tfoot>
			<tr>
				<td></td>
				<td><b>Totales</b></td>
				<td align="right"><b>{{ $total['totalIn'] }}</b></td>
				<td align="right"><b>{{ $total['totalOut'] }}</b></td>
				<td align="right"><b>{{ $total['sald'] }}</b></td>
			</tr>
		</tfoot>	
	</table>
</html>

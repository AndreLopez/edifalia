@extends('backend.report.baseReport')

@section('breadcrumb_title')
    @lang('report.economic.account-groups')
@endsection

@section('content')
<div class="box" ng-app="edifaliaApp" ng-controller="EconomicAccountController as report">
    <div class="box-header with-border">
        <h3 class="box-title">Ingresos y gastos agrupados por cuentas</h3>
    </div>
    <div class="box-body table-responsive">
    	<form class="form-horizontal">
          	<div class="box-body">
	            <div class="form-group">
	              <label class="col-sm-2 control-label">@lang('report.dossier.form.communities')</label>
	              	<div class="col-sm-10">
		              	<select class="form-control" id="selectCommunity" ng-model="report.model.community" ng-change="dossier.selectProperty()" ng-options="dataCommunty as dataCommunty.name for dataCommunty in report.community track by dataCommunty.id">
			            	<option value=''>@lang('report.dossier.form.select-community')</option>
			          	</select>	                	
	                </div>
	            </div>
	            <div class="form-group">
	              <label class="col-sm-2 control-label">Ejercicio contable</label>
	              <div class="col-sm-10">
	                	<select class="form-control" ng-disabled="!dossier.model.community">
		                    <option>Seleccine ejercicio contable</option>
	                  	</select>
	              </div>
	            </div>
	            <div class="panel panel-default">
	            <div class="panel-heading">
	            	<div class="panel-title">Parámetros de configuración del informe</div>
	            </div>
	            <div class="panel-body">
	            	<div class="form-group">
		            	<label class="col-sm-2 control-label">Justificación de saldo inicial:</label>
		            	<div class="col-sm-10">
		                    <input name="option1" id="option1" value="option1" checked="" type="radio"> No mostrar
		                    <input name="option1" id="option1" value="option1" checked="" type="radio"> Resumido
		                    <input name="option1" id="option1" value="option1" checked="" type="radio"> Detallado
		            	</div>
		            </div>
		            <div class="form-group">
		            	<label class="col-sm-2 control-label">Justificación de saldo final:</label>
		            	<div class="col-sm-10">
		                    <input name="option2" id="option2" value="option2" checked="" type="radio"> No mostrar
		                    <input name="option2" id="option2" value="option2" checked="" type="radio"> Resumido
		                    <input name="option2" id="option2" value="option2" checked="" type="radio"> Detallado
		            	</div>
		            </div>
		            <div class="form-group">
		            	<label class="col-sm-2 control-label">Ordenar por:</label>
		            	<div class="col-sm-10">
		                    <input name="option3" id="option3" value="option3" checked="" type="radio"> Titulo de cuenta
		                    <input name="option3" id="option3" value="option3" checked="" type="radio"> Código de cuenta
		                    <input name="option3" id="option3" value="option3" checked="" type="radio"> Grupo de gastos
		            	</div>
		            </div>
		            <div class="form-group">
		            	<label class="col-sm-2 control-label">Incluir:</label>
		            	<div class="col-sm-10">
		                    <input type="checkbox"> Solo ingresos reales
		            	</div>
		            </div>
	            </div>
	            </div>

	            
	        </div>          
          	<div class="box-footer">
            	<button type="submit" class="btn btn-default">@lang('report.buttons.cancel')</button>
            	<button type="submit" class="btn btn-primary pull-right" ng-click="report.generateReportExcel()" ng-disabled="!report.model.community">@lang('report.buttons.generate')</button>
          	</div>          
        </form>
    </div>
</div>
@endsection
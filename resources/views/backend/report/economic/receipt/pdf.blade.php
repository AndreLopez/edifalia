<style type="text/css">
	table{
		/*border: 1px solid #000;*/
	}

	td{
		/*border: 1px solid #000;*/
	}

	th.title{
		background-color: #C5C5C5;
		/*text-align: right;*/
		font-weight: bold;
		font-size: 14px;
	}

	.title{
		background-color: #E2E2E2;
	}

	.title-td{
		background-color: #E6E6E6;
	}

	td{
		text-align: left;
	}
</style>
<br><br><br>
<table width="100%">
	<thead>
		<tr>
			<th class="title" width="50%">Comunidad</th>
			<td class="title" width="70%">{{ $community->name }}</td>
		</tr>
		<tr>
			<th class="title" width="50%">Recibos emitidos desde:</th>
			<td class="title" width="70%">{{ $data->dateStart }} al {{ $data->dateEnd }}</td>
		</tr>
	</thead>	
</table>
<br><br>
<table width="100%">
	<thead>
		<tr>
			<th class="title" width="60%">Datos de Propiedad</th>
			<th class="title" width="40%">Mensaje</th>
			<th class="title" width="35%">Cuota Ordinaria</th>
			<th class="title" width="25%">Extra #TA#</th>			
		</tr>		
	</thead>
	<tbody>
		@foreach($receipt as $row)
			<tr>
				<td width="60%">{{ $row->property_number }}</td>
				<td width="40%">{{ $row->message }}</td>
				<td width="35%">{{ $row->amount }}</td>
				<td width="25%">{{ $row->id }}</td>
			</tr>
			@endforeach
	</tbody>
</table>
@extends('backend.report.baseReport')

@section('breadcrumb_title')
    @lang('report.economic.receipt')
@endsection

@section('content')
<div class="box" ng-app="edifaliaApp" ng-controller="ReceiptReportController as receipt">
    <div class="box-header with-border">
        <h3 class="box-title">Recibos Emitidos en un Peíodo</h3>
    </div>
    <div class="box-body table-responsive">
    	<form class="form-horizontal">
          	<div class="box-body">
	            <div class="form-group">
	              <label class="col-sm-2 control-label">@lang('report.dossier.form.communities')</label>
	              	<div class="col-sm-10">
		              	<select class="form-control" id="selectCommunity" ng-model="receipt.model.community" ng-change="receipt.selectProperty()" ng-options="dataCommunty as dataCommunty.name for dataCommunty in receipt.community track by dataCommunty.id">
			            	<option value=''>@lang('report.dossier.form.select-community')</option>
			          	</select>	                	
	                </div>
	            </div>
	            <div class="form-group">
	              <label class="col-sm-2 control-label">Desde</label>
	              <div class="col-sm-4">
	                	<input type="text" class="form-control datepicker" placeholder="" required ng-disabled="!receipt.model.community" ng-model="receipt.model.dateStart">
	              </div>
	              <label class="col-sm-2 control-label">Hasta</label>
	              <div class="col-sm-4">
	                	<input type="text" class="form-control datepicker" placeholder="" required ng-disabled="!receipt.model.community" ng-model="receipt.model.dateEnd">
	              </div>
	            </div>
	        </div>          
          	<div class="box-footer">
            	<button type="submit" class="btn btn-default">@lang('report.buttons.cancel')</button>
            	<button type="submit" class="btn btn-primary pull-right" ng-click="receipt.generateReportExcel()" ng-disabled="!receipt.model.community">@lang('report.buttons.generate')</button>
          	</div>          
        </form>
    </div>
</div>
@endsection
@extends('backend.report.baseReport')

@section('breadcrumb_title')
    Ingresos y gastos por meses
@endsection

@section('content')
<div class="box" ng-app="edifaliaApp" ng-controller="EconomicMonthController as month">
    <div class="box-header with-border">
        <h3 class="box-title">Ingresos y gastos por meses</h3>
    </div>
    <div class="box-body table-responsive">
    	<form class="form-horizontal">
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-2 control-label">@lang('report.dossier.form.communities')</label>
                <div class="col-sm-10">
                  <select class="form-control" id="selectCommunity" ng-model="month.model.community" ng-options="dataCommunty as dataCommunty.name for dataCommunty in month.community track by dataCommunty.id">
                  <option value=''>@lang('report.dossier.form.select-community')</option>
                </select>                   
                </div>
            </div>
            <div class="form-group">
	            <div class="col-sm-4">
	            	<input type="checkbox" ng-model="month.model.includeAll"> Incluir todos
	            </div>
              <label class="col-sm-2 control-label">Mes</label>
	            <div class="col-sm-6">
		            <select multiple class="form-control" id="selectMonth" ng-model="month.model.months" ng-change="month.selectProperty()" ng-options="dataMonth as dataMonth.name for dataMonth in month.months track by dataMonth.id" ng-disabled="month.model.includeAll"></select>	                	
	            </div>
	          </div>	            
	        </div>          
          <div class="box-footer">
            <button class="btn btn-default">@lang('report.buttons.cancel')</button>
            {{-- <button class="btn btn-primary pull-right" ng-click="month.generateReportPdf()" ng-disabled="!month.model.community || !month.model.months">@lang('report.buttons.generate')</button> --}}
            <button class="btn btn-primary pull-right" ng-click="month.generateReportExcel()" ng-disabled="!month.model.community || !month.model.months">@lang('report.buttons.generate')</button>
          </div>          
        </form>
    </div>
</div>
@endsection
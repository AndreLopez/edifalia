@extends('layouts.dashboard')

@section('script_report')
    {{-- JsRoutes --}}
    {!! Html::script('js/routes.js') !!}
	{!! Html::script('static/angular/angular/angular.min.js') !!}
	{!! Html::script('static/angular/angular-sanitize/angular-sanitize.min.js') !!}
	{{-- ng-table --}}
	{!! Html::script('static/angular/ng-table/ng-table.min.js') !!}
	{!! Html::style('static/angular/ng-table/ng-table.min.css') !!}
    {{-- ngToast --}}
    {!! Html::style('static/angular/ngToast/ngToast.min.css') !!}
    {!! Html::script('static/angular/ngToast/ngToast.min.js') !!}
	{{-- Modules for App --}}
	{!! Html::script('static/angular/edifalia/app.js') !!}
	{!! Html::script('static/angular/edifalia/masterService.js') !!}
	{{-- ui-bootstrap  --}}
	{!! Html::script('static/angular/ui-bootstrap/ui-bootstrap-tpls-2.3.0.min.js') !!}
	{{-- bootstrap datepicker --}}
    {!! Html::style('static/plugins/datepicker/datepicker3.css') !!}
    {{-- bootstrap datepicker --}}
    {!! Html::script('static/plugins/datepicker/bootstrap-datepicker.js') !!}
    {{-- Estilo General Reportes --}}
    {!! Html::style('static/angular/style-reports.css') !!}
    <script>
	    $(document).ready(function(){
	        $("#datepicker").datepicker({
		      autoclose: true
		    });
		    $(".datepicker").datepicker({
		      autoclose: true
		    });
	    });
	</script>    
@endsection
{!! Html::style('static/angular/style-reports.css') !!}
<div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
	    <div class="modal-header" style="background: #0695cb;color: #fff;border-bottom: 5px solid #1a2226;">
	      <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #fff;" ng-click="dossierView.cancel()">
	      <span aria-hidden="true">&times;</span></button>
	      <h4 class="modal-title">@lang('dashboard.mnu_dossier_sign')</h4>
	    </div>
	    <div class="box-header">
          	<a href class="btn btn-app" style="color: rgb(227, 3, 3);" ng-click="dossierView.exportDossier()">
            	<i class="fa fa-file-pdf-o"></i> @lang('report.buttons.pdf')
          	</a>
          	<a class="btn btn-app" style="color: rgb(25, 117, 1);" ng-click="dossierView.exportDossierExcel()">
            	<i class="fa fa-file-excel-o"></i> @lang('report.buttons.excel')
          	</a>
        </div>           
	  	<div class="modal-body">
			<div class="box-body">
				<table>
					<thead>
						<tr>
							<th>#</th>
							<th>@lang('report.report.property')</th>
							<th>@lang('report.report.coefficient')</th>
							<th>@lang('report.report.owner')</th>
							<th ng-if="dossierView.include.dni">@lang('report.report.dni')</th>
							<th ng-if="dossierView.include.email">@lang('report.report.email')</th>
							<th ng-if="dossierView.include.phone">@lang('report.report.phone')</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="row in dossierView.resultReport | orderBy:propertyName:reverse">
							<td>{$ $index + 1 $}</td>
							<td>{$ row.property $}</td>
							<td>{$ row.coefficient $}</td>
							<td>{$ row.person $}</td>
							<td ng-if="row.dni">{$ row.dni $}</td>
							<td ng-if="row.email">{$ row.email $}</td>
							<td ng-if="row.phone">{$ row.phone $}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="box-footer">
				<button type="submit" class="btn btn-default" ng-click="dossierView.cancel()">@lang('report.buttons.exit')</button>	
			</div>
		</div>
	</div>
</div>
<html>
{!! Html::style('static/angular/style-excel.css') !!}
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<table>		
		<tr>
			<td><img src="static/images/logo-horizontal.png" widtd="35%" height="42%"></td>
		</tr>
		<tr>
			<td></td>	
			<td><b>{{ $title }}</b></td>
		</tr>		
	</table>
    <table width="95%">
		<thead>
			<tr>
				<th class="title" width="15%">@lang('report.report.property')</th>
				<th class="title" width="25%">@lang('report.report.coefficient')</th>
				<th class="title" width="25%">@lang('report.report.owner')</th>
				{{-- @if($data->include->dni) <th class="title" width="25%">@lang('report.report.dni')</th>@endif
				@if($data->include->email) <th class="title" width="25%">@lang('report.report.email')</th>@endif
				@if($data->include->phone) <th class="title" width="25%">@lang('report.report.phone')</th>@endif --}}
			</tr>
		</thead>
		<tbody>
			@forelse($data->list as $row)
			<tr>
				<td width="15%" class="body">{{ $row->property }}</td>
				<td width="25%" class="body">{{ $row->coefficient }}%</td>
				<td width="25%" class="body">{{ $row->person }}</td>				
			</tr>
			@if($data->include->dni)<tr><td width="25%">@lang('report.report.dni'): {{ $row->dni }}</td></tr>@endif
			@if($data->include->email)<tr><td width="25%">@lang('report.report.email'): {{ $row->email }}</td></tr>@endif
			@if($data->include->phone)<tr><td width="25%">@lang('report.report.phone'): {{ $row->phone }}</td></tr>@endif
		    @empty
		    	@lang('report.report.assistant-fail')
		    @endforelse
		</tbody>	
	</table>
</html>

<style type="text/css">
	table{
		/*border: 1px solid #000;*/
	}

	td{
		/*border: 1px solid #000;*/
	}

	th.title{
		background-color: #C5C5C5;
		/*text-align: right;*/
		font-weight: bold;
		font-size: 14px;
	}

	.title{
		background-color: #E2E2E2;
	}

	.title-td{
		background-color: #E6E6E6;
	}

	td{
		text-align: left;
	}
</style>
<br><br><br>
<table width="100%">
	<thead>
		<tr>
			<th class="title" width="70%">@lang('report.report.owner')</th>
			<th class="title" width="50%">@lang('report.report.property')</th>
			<th class="title" width="40%">@lang('report.report.coefficient')</th>
			{{-- @if($data->include->dni) <th class="title" width="25%">@lang('report.report.dni')</th>@endif --}}
			{{-- @if($data->include->email) <th class="title" width="25%">@lang('report.report.email')</th>@endif --}}
			{{-- @if($data->include->phone) <th class="title" width="25%">@lang('report.report.phone')</th>@endif --}}
		</tr>
	</thead>
	<tbody>
		@forelse($data->list as $row)
		<tr>
			<td width="70%" class="body title-td">
				<b>{{ $row->person }}</b>
			</td>
			<td width="50%" class="body title-td">{{ $row->property }}</td>
			<td width="40%" class="body title-td">{{ $row->coefficient }}%</td>			
		</tr>
		@if($data->include->dni)
			<tr><td>@lang('report.report.dni'): {{ $row->dni }}</td></tr>
		@endif
		@if($data->include->email)
			<tr><td>@lang('report.report.email'): {{ $row->email }}</td></tr>
		@endif
		@if($data->include->phone)
			<tr><td>@lang('report.report.phone'): {{ $row->phone }}</td></tr>
		@endif
	    @empty
	    	@lang('report.report.assistant-fail')
	    @endforelse
	</tbody>	
</table>
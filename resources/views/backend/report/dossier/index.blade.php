@extends('backend.report.baseReport')

@section('breadcrumb_title')
    @lang('dashboard.mnu_dossier_sign')
@endsection

@section('content')
<div class="box" ng-app="edifaliaApp" ng-controller="DossierController as dossier">
    <div class="box-header with-border">
        <h3 class="box-title">@lang('dashboard.mnu_dossier_sign')</h3>
    </div>
    <div class="box-body table-responsive">
    	<form class="form-horizontal">
          	<div class="box-body">
	            <div class="form-group">
	              <label class="col-sm-2 control-label">@lang('report.dossier.form.communities')</label>
	              	<div class="col-sm-10">
		              	<select class="form-control" id="selectCommunity" ng-model="dossier.model.community" ng-change="dossier.selectProperty()" ng-options="dataCommunty as dataCommunty.name for dataCommunty in dossier.community track by dataCommunty.id">
			            	<option value=''>@lang('report.dossier.form.select-community')</option>
			          	</select>	                	
	                </div>
	            </div>
	            <div class="form-group">
	              <label class="col-sm-2 control-label">@lang('report.dossier.form.coefficients')</label>
	              <div class="col-sm-10">
	                	<select class="form-control" ng-disabled="!dossier.model.community">
		                    <option>@lang('report.dossier.form.select-coefficients')</option>
	                  	</select>
	              </div>
	            </div>
	            <div class="form-group">
	            	<div class="col-sm-5">
		              <label>@lang('report.dossier.form.available-properties')</label>
		              	{{-- <select multiple class="form-control" id="selectAvailableProperties" ng-model="dossier.model.availableProperties" ng-options="dataProperties as dataProperties.building_number for dataProperties in dossier.availableProperties track by dataProperties.id" ng-disabled="!dossier.model.community">
		              	</select> --}}
		              	<select multiple class="form-control" ng-disabled="!dossier.model.community" ng-model="dossier.model.property">
		              		<option ng-repeat="row in dossier.property.available" id="available_{$ $index $}" value="{$ $index $}">
		              			{$ $index + 1 $} - {$ row.property_number $}
		              		</option>
		              	</select>
		            </div>
		            <div class="col-sm-2" align="center">
		            	<br><button
		            		class="btn btn-block btn-success btn-sm" 
		            		style="width: 60%; font-size: 15px;" 
		            		ng-click="dossier.propertyExclude()" 
		            		ng-disabled="!dossier.property.available.length">
		            		<i class="fa fa-angle-double-right"></i>
		            	</button>
		            	<button 
		            		class="btn btn-block btn-success btn-sm" 
		            		style="width: 60%; font-size: 15px;" 
		            		ng-click="dossier.propertyAvailable()" 
		            		ng-disabled="!dossier.property.exclude.length">
		            		<i class="fa fa-angle-double-left"></i>
		            	</button>
		            </div>
	            	<div class="col-sm-5">
		              <label>@lang('report.dossier.form.exclude-properties')</label>
		              	<select multiple class="form-control" ng-disabled="!dossier.model.community" ng-model="dossier.model.propertyExclude">
		              		<option ng-repeat="row in dossier.property.exclude" id="exclude_{$ $index $}" value="{$ $index $}">
		              			{$ $index + 1 $} - {$ row.property_number $}
		              		</option>
		              	</select>
		            </div>
	            </div>
	            <div class="form-group">
	            	<div class="col-sm-12">
		            	<label>@lang('report.dossier.form.title')</label>
		            	<textarea class="form-control" ng-model="dossier.model.title"></textarea>
	            	</div>
	            </div>	            
	            <div class="form-group">
	            	<label class="col-sm-2 control-label">@lang('report.dossier.form.include'):</label>
	            	<div class="col-sm-10">
	                    <input type="checkbox" ng-model="dossier.model.include.dni"> @lang('report.dossier.form.dni')
	                    <input type="checkbox" ng-model="dossier.model.include.email"> @lang('report.dossier.form.email')
	                    <input type="checkbox" ng-model="dossier.model.include.phone"> @lang('report.dossier.form.phone')
	                    <input type="checkbox" ng-model="dossier.model.include.notData" ng-click="dossier.notData"> @lang('report.dossier.form.do-not-show-data')
	            	</div>
	            </div>
	            <div class="form-group">
	            	<label class="col-sm-2 control-label">@lang('report.dossier.form.order'):</label>
	            	<div class="col-sm-10">
	                    <input name="optionRadio" id="option1" value="option1" checked="" type="radio" ng-click="dossier.orderBy('person')"> 
	                    @lang('report.dossier.form.fullname')
	                    <input name="optionRadio" id="option2" value="option2" checked="" type="radio" ng-click="dossier.orderBy('property')"> 
	                    @lang('report.dossier.form.property')
	                    <input name="optionRadio" id="option3" value="option3" checked="" type="radio" ng-click="dossier.orderBy('property')"> 
	                    @lang('report.dossier.form.n-property')                   
	            	</div>
	            </div>
	            <div class="form-group">
	            	<label class="col-sm-2 control-label">@lang('report.dossier.form.include-owner'):</label>
	            	<div class="col-sm-10">
	                    <input type="checkbox"> @lang('report.dossier.form.payer')
	                    <input type="checkbox"> @lang('report.dossier.form.spouse')
	                    <input type="checkbox"> @lang('report.dossier.form.attorney')
	                    <input type="checkbox"> @lang('report.dossier.form.joint-owners')
	            	</div>
	            </div>
	        </div>          
          	<div class="box-footer">
            	<button type="submit" class="btn btn-default">@lang('report.buttons.cancel')</button>
            	<button type="submit" class="btn btn-primary pull-right" ng-click="dossier.generateReport()" ng-disabled="!dossier.model.community">@lang('report.buttons.generate')</button>
          	</div>
        </form>
    </div>
</div>
@endsection
@extends('backend.report.baseReport')

@section('breadcrumb_title')
    @lang('dashboard.mnu_meetings')
@endsection

@section('content')
<section class="invoice">
  {{-- Detalles de Junta --}}
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        {{ $meeting->title }}
      </h2>
    </div>    
  </div>
  <div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
      @lang('report.meeting.characteristics')
      <address>
        <strong>@lang('report.meeting.title'):</strong>&nbsp;{{ $typeMeeting[$meeting->type] }}<br>
        <strong>@lang('report.meeting.date'):</strong>&nbsp;{{ $meeting->date_in }}<br>
        <strong>@lang('report.meeting.place'):</strong>&nbsp;{{ $meeting->place }}<br>            
      </address>
    </div>
    <div class="col-sm-8 invoice-col">
      <b>@lang('report.meeting.order_day')</b>
      <address>
        {{ $meeting->order_day }}
      </address>
    </div>        
  </div>
  {{-- Asistentes --}}
  <p class="lead">Asistentes</p>
  <div class="row">
    <div class="col-xs-12 table-responsive">
      <table class="table table-striped">
        <thead>
        <tr>
          <th>@lang('report.report.property')</th>
          <th>@lang('report.report.coefficient')</th>
          <th>@lang('report.report.owner')</th>              
        </tr>
        </thead>
        <tbody>
          @forelse($meetingAssistant as $row)
            <tr>
              <td>{{ $row->property->property_number }}</td>
              <td>{{ $row->property->coefficient }}</td>		              
              <td>{{ $row->property->person->FullName }}</td>
            </tr>                    
          @empty
              <div class="alert alert-warning">
                  <h4><i class="icon fa fa-warning"></i> @lang('dashboard.messages.notice')</h4>
                  @lang('dashboard.messages.no_stored_records')
              </div>
          @endforelse            
        </tbody>
      </table>
    </div>
  </div>
  {{-- Acta --}}
  <div class="row">
      <div class="col-xs-12">
        <p class="lead">@lang('report.meeting.minute')</p>
        {{ $meeting->minute }}
      </div>
  </div><br><br>
  {{-- Detalles de Votaciones --}}
  <div class="row">
    <div class="col-xs-6">
      <p class="lead">Detalles de Votación</p>      
        <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;"><b>Descripción: </b>{{ $votingDescription }}</p>      
    </div>
    <div class="col-xs-6">    
      <p class="lead">Votaciones</p>
      @foreach($meetingVotingDetail as $row)
        <table class="table">
          <tbody>
            <tr>
              <th width="20%">Acuerdo:</th>
              <td>{{ $row['agreement'] }}</td>
            </tr>
            @foreach($row['data'] as $rowVotings)
              <tr>
                <td>{{ $rowVotings['person'] }}</td>
                <td>
                @if ($rowVotings['voting'] == 1)
                  @lang('report.meeting.voting.selectVotingYes')
                @else
                  @lang('report.meeting.voting.selectVotingNo')
                @endif
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      @endforeach
    </div>
  </div>
  {{-- Botones --}}
  <div class="row no-print">
    <div class="col-xs-12">
      <a href="{{route('report.meeting')}}" class="btn btn-default"><i class="fa fa-mail-reply"></i> @lang('report.buttons.return')</a>
      <a href="{{ route('meeting.export-excel', ['id' => $meeting->id ])}}" class="btn btn-success pull-right">
        <i class="fa fa-file-excel-o" style="color:#fff;"></i> @lang('report.buttons.generate_excel')
      </a>
      <a href="{{ route('meeting.export-pdf', ['id' => $meeting->id ])}}" class="btn btn-primary pull-right" style="margin-right: 5px;">
        <i class="fa fa-file-pdf-o" style="color:#fff;"></i> @lang('report.buttons.generate_pdf')
      </a>
    </div>
  </div>
</section>
@endsection
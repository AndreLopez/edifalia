<html>
{!! Html::style('static/angular/style-excel.css') !!}
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<table>		
		<tr>
			<td><img src="static/images/logo-horizontal.png" widtd="35%" height="42%"></td>
		</tr>
		<tr>
			<td></td>	
			<td><b>{{ $meeting->title }}</b></td>
		</tr>
	</table>
<br><br>
<table width="100%">
	<thead>
		<tr>
			<th class="title" width="20%">Titulo: </th>
			<td width="80%">{{ $meeting->title }}</td>
		</tr>
		<tr>
			<th class="title" width="20%">Tipo: </th>
			<td width="80%">{{ $typeMeeting[$meeting->type] }}</td>
		</tr>
		<tr>
			<th class="title" width="20%">Fecha: </th>
			<td width="80%">{{ $meeting->date_in }}</td>
		</tr>
		<tr>
			<th class="title" width="20%">Lugar: </th>
			<td width="80%">{{ $meeting->place }}</td>
		</tr>
	</thead>	
</table><br><br>
<table width="100%">
	<thead>
		<tr>
			<th></th>
			<th width="70%">Orden: </th>
			<th></th>
		</tr>
		<tr>
			<td>{{ $meeting->order_day }}</td>
		</tr>
	</thead>	
</table><br><br>
<h4>Listado de Asistentes</h4>
<table width="100%">
	<thead>
		<tr>
			<th class="title" width="20%">Propiedad</th>
			<th class="title" width="20%">Coeficiente</th>
			<th class="title" width="20%">Propietario</th>
		</tr>
	</thead>
	<tbody>
		@forelse($meetingAssistant as $row)
		<tr>
			<td width="20%" class="body">{{ $row->property->property_number }}</td>
			<td width="20%" class="body">{{ $row->property->coefficient }}%</td>
			<td width="20%" class="body">{{ $row->property->person->FullName }}</td>
		</tr>
	    @empty
	    Sin asistentes
	    @endforelse
	</tbody>	
</table><br><br>
<table width="100%">
	<thead>
		<tr>
			<th></th>
			<th width="20%">@lang('report.meeting.minute'): </th>			
			<th></th>
		</tr>
		<tr>
			<td>
				{{ $meeting->minute }}	
			</td>
		</tr>
	</thead>	
</table><br>
{{-- Votaciones --}}
<table>
	<thead>
		<tr>
			<th></th>
			<th class="title-xd" width="70%" align="center">@lang('report.meeting.voting.details')</th>
			<th></th>
		</tr>
		<tr>
			<th class="title" width="15%">@lang('report.meeting.voting.description')</th>
			<td width="30%">{{ $votingDescription }}</td>
		</tr>
		<tr>
			<th class="title-list"></th>
			<th class="title-list" width="50%">@lang('report.meeting.voting.title')</th>
			<th class="title-list"></th>
		</tr>
	</thead>
</table>
@foreach($meetingVotingDetail as $row)
	<table>
	  <tbody>
	    <tr>
	      <th width="15%" class="title">Acuerdo:</th>
	      <td align="left" width="15%">&nbsp;{{ $row['agreement'] }}</td>
	    </tr>
	    @foreach($row['data'] as $rowVotings)
	      <tr>
	        <td align="left">{{ $rowVotings['person'] }}</td>
	        <td></td>
	        <td width="30%">
	        @if ($rowVotings['voting'] == 1)
	          @lang('report.meeting.voting.selectVotingYes')
	        @else
	          @lang('report.meeting.voting.selectVotingNo')
	        @endif
	        </td>
	      </tr>
	    @endforeach
	    <br><br>
	  </tbody>
	</table>
@endforeach   
</html>
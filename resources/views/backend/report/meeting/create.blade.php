@extends('backend.report.baseReport')

@section('breadcrumb_title')
    @lang('dashboard.mnu_meetings')
@endsection

@section('content')
<div class="box" ng-app="edifaliaApp" ng-controller="MeetingCreateController as meeting">
    <div class="box-header with-border">
        <h3 class="box-title">@lang('dashboard.mnu_meetings')</h3>
    </div>
    <div class="box-body table-responsive">
    	<form class="form-horizontal" ng-submit="meeting.generateMeeting()">
          	<div class="box-body">
	            <div class="form-group">
	              <label class="col-sm-2 control-label">@lang('report.dossier.form.communities')</label>
	              	<div class="col-sm-10">
		              	<select class="form-control" id="selectCommunity" ng-model="meeting.model.community" ng-change="meeting.selectMeeting()" ng-options="dataCommunty as dataCommunty.name for dataCommunty in meeting.community track by dataCommunty.id" required>
			            	<option value=''>@lang('report.dossier.form.select-community')</option>
			          	</select>	                	
	                </div>
	            </div>
	            <div class="form-group">
	              	<label class="col-sm-2 control-label">@lang('report.meeting.form.title')</label>
	              	<div class="col-sm-10">
                        <select class="form-control" id="selectMeeting" ng-model="meeting.model.meeting" ng-options="dataCommunty as dataMeeting.title for dataMeeting in meeting.meetingData track by dataMeeting.id" ng-disabled="!meeting.model.community">
		                    <option value="">@lang('report.meeting.form.select')</option>
                        </select>                       
	              	</div>
	            </div>
                <div class="col-sm-12" ng-show="!meeting.model.community">
                    <div class="alert alert-warning">
                        @lang('report.meeting.form.message_select')
                    </div>
                </div>
            	<div class="col-sm-12" ng-show="meeting.model.community">
	              <div class="nav-tabs-custom" disabled>
		            <ul class="nav nav-tabs">
		              <li class="active"><a href="#tab_1" data-toggle="tab">@lang('report.meeting.characteristics')</a></li>
		              <li><a href="#tab_2" data-toggle="tab">@lang('report.meeting.order_day')</a></li>
                      <li><a href="#tab_3" data-toggle="tab">@lang('report.meeting.assistant')</a></li>                          
                      <li><a href="#tab_4" data-toggle="tab">@lang('report.meeting.voting.title')</a></li>                          
		              <li><a href="#tab_5" data-toggle="tab">@lang('report.meeting.minute')</a></li>
		              {{-- <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li> --}}
		            </ul>
		            <div class="tab-content">
		              	<div class="tab-pane active" id="tab_1">
		              		<div class="form-group">
				            	<label class="col-sm-2 control-label">@lang('report.meeting.type.title')</label>
				            	<div class="col-sm-10">
                                    @foreach($meetingType as $row)
    				                    <input name="optionRadio" id="option1" value="{{ $loop->index + 1 }}" checked="" type="radio" ng-model="meeting.model.formMeeting.typeMeeting" ng-click="meeting.typeMeeting({{$loop->index + 1}})">{{ $row }}                                        
                                    @endforeach
				            	</div>
				            </div>
        					<div class="form-group">
        						<label class="col-sm-1 control-label">@lang('report.meeting.title'):</label>
        						<div class="col-sm-11">
        							<input type="text" class="form-control" placeholder="@lang('report.meeting.title')" ng-model="meeting.model.formMeeting.title" required>
        						</div>
        					</div>
        					<div class="form-group">
        						<label class="col-sm-1 control-label">@lang('report.meeting.controlled.title'):</label>
        						<div class="col-sm-11">
        							<select class="form-control" id="selectMeeting" ng-model="meeting.model.meetingControl" ng-options="dataMeetingControl as dataMeetingControl.title for dataMeetingControl in meeting.meetingControl track by dataMeetingControl.id" ng-disabled="!meeting.model.community">
                                        <option value="">@lang('report.meeting.controlled.select')</option>
                                    </select> 
        						</div>
        					</div>
        					<div class="form-group">
        						<label class="col-sm-1 control-label">@lang('report.meeting.date'):</label>
                                <div class="col-sm-3">
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control" placeholder="@lang('report.meeting.date')" ng-model="meeting.model.formMeeting.date_in" id="datepicker" required>                                    
                                    </div>
        						</div>
        						<label class="col-sm-3 control-label">@lang('report.meeting.place'):</label>
        						<div class="col-sm-5">
        							<input type="text" class="form-control" placeholder="@lang('report.meeting.place')" ng-model="meeting.model.formMeeting.place" required>
        						</div>
        					</div>
        					<div class="form-group">
        						<label class="col-sm-1 control-label">@lang('report.meeting.celebration'):</label>
        						<label class="col-sm-1 control-label">@lang('report.meeting.conv_1'):</label>
        						<div class="col-sm-2">
        							<input type="text" class="form-control" placeholder="@lang('report.meeting.conv_1')" ng-model="meeting.model.formMeeting.time_one" required>
        						</div>
        						<label class="col-sm-1 control-label">@lang('report.meeting.conv_2'):</label>
        						<div class="col-sm-2">
        							<input type="text" class="form-control" placeholder="@lang('report.meeting.conv_2')" ng-model="meeting.model.formMeeting.time_two" required>
        						</div>
        						<label class="col-sm-1 control-label">@lang('report.meeting.ending'):</label>
        						<div class="col-sm-3">
        							<input type="text" class="form-control" placeholder="@lang('report.meeting.ending')" ng-model="meeting.model.formMeeting.time_end" required>
        						</div>
        					</div>
        					{{-- <div class="form-group">
        						<label class="col-sm-5 control-label">Incluir detalles de propiedad con recibos pendientes:</label>
        						<div class="col-sm-7">
        							<a class="btn btn-primary">Calcular</a>
        						</div>
        					</div> --}}
		              	</div>
		              	<!-- /.tab-pane -->
		              	<div class="tab-pane" id="tab_2">
		                	<div class="form-group">
        						<label class="col-sm-2 control-label">@lang('report.meeting.order_day'):</label>
        						<div class="col-sm-10">
        							<textarea type="text" class="form-control" placeholder="@lang('report.meeting.order_day')" ng-model="meeting.model.formMeeting.order_day" required></textarea>
        						</div>        						
        					</div>
		              	</div>
		              <!-- /.tab-pane -->
		              <div class="tab-pane" id="tab_3">
                        <a href class="btn btn-primary" ng-click="meeting.model.community && meeting.searchAssistant()" ng-disabled="!meeting.model.community"><i class="fa fa-search"></i></a><br>
		                <table ng-show="meeting.dataAssistant.length">
                            <thead>
                                <tr>
                                    <th style="color:#2B2B2B;">{$ meeting.assistant.present $} Presentes {$ meeting.detailAssistant.perAssistant | number : 0 $}% y {$ meeting.assistant.represent $} Representantes {$ meeting.detailAssistant.perRepre | number : 0 $}% suman {$ meeting.detailAssistant.suma $} Propietarios {$ meeting.detailAssistant.total | number : 0 $}%</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="row in meeting.dataAssistant">
                                    <td>{$ $index + 1 $}</td>
                                    <td>{$ row.coefficient $}&nbsp;%</td>
                                    <td>{$ row.first_name $} {$ row.last_name $}</td>
                                    <td></td>
                                    <td><input name="radio_{$row.id$}" id="radio_{$row.id$}" value="1" checked="false" type="radio" ng-click="meeting.actionAssistant(row.id, 1)">Presente</td>
                                    <td><input name="radio_{$row.id$}" id="radio_{$row.id$}" value="0" checked="false" type="radio" ng-click="meeting.actionAssistant(row.id, 0)">Representado</td>
                                    {{--<td><i class="fa fa-trash" ng-click="meeting.trashAssistant($index)"></i></td>--}}
                                </tr>
                            </tbody>
                        </table>
		              </div>
                        <div class="tab-pane" id="tab_4">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">@lang('report.meeting.voting.description'):</label>
                                <div class="col-sm-10">
                                    <textarea type="text" class="form-control" placeholder="@lang('report.meeting.voting.description')" ng-model="meeting.model.formMeeting.voting" required></textarea>
                                </div>                              
                            </div>
                            <a href class="btn btn-primary" ng-click="meeting.dataAssistant.length && meeting.addVoting()" ng-disabled="!meeting.dataAssistant.length"><i class="fa fa-plus"></i> Añadir Motivo de Votación</a><br>
                            <div class="row" ng-repeat="row in meeting.formVoting">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Votación:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" placeholder="{$ row.placeholder $}" ng-model="meeting.modelVoting.title[row.name]">                                        
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <table class="table" ng-repeat="(key, value) in row.child" ng-init="paso= (value | myHashKey);">
                                        <thead>
                                            <tr>                                            
                                                <th>Propiedad</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td width="40%">{$ value.first_name $} {$ value.last_name $}</td>
                                                <td>
                                                    <input name="radio_{$ paso $}" id="radio_{$ paso $}" value="1" checked="false" type="radio" ng-model="meeting.modelVoting.user[row.name][paso]">SI                                                                    
                                                </td>
                                                <td>
                                                    <input name="radio_{$ paso $}" id="radio_{$ paso $}" value="0" checked="false" type="radio" ng-model="meeting.modelVoting.user[row.name][paso]">NO                                                    
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_5">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">@lang('report.meeting.minute'):</label>
                                <div class="col-sm-10">
                                    <textarea type="text" class="form-control" placeholder="@lang('report.meeting.minute')" ng-model="meeting.model.formMeeting.minute" required></textarea>
                                </div>
                            </div>
                        </div>
		              <!-- /.tab-pane -->
		            </div>				            
		          </div>				          
		        </div>
            </div>
	      	<div class="box-footer row">
                <div class="col-sm-2">
	        	  <a href="{{ route('report.meeting')}}" class="btn btn-default">@lang('report.buttons.return')</a>                    
                </div>
                <div class="col-sm-8">
                    <p ng-show="meeting.showMessage">
                        {$ meeting.message.message $}
                        <a href="{$ meeting.message.url $}">Ver Junta</a>
                    </p>
                </div>
                <div class="col-sm-2" ng-show="meeting.model.community">
	        	  <button type="submit" class="btn btn-primary pull-right">@lang('report.buttons.generate')</button>
                </div>
	      	</div>
    	</form>
    </div>
</div>
@endsection
<style type="text/css">
	th.title{
		background-color: #F2F2F2;
		/*text-align: right;*/
		font-size: 13px;
		font-weight: bold;
	}

	.title-list{
		background-color: #A1A1A1;
		font-size: 13px;
		font-weight: bold;
	}

	.title-xd{
		background-color: #878787;
		font-size: 14px;
		font-weight: bold;	
	}

	td.body{
		text-align: left;
	}	
</style>
<br><br><br>
<table width="100%">
	<thead>
		<tr>
			<th class="title" width="20%">Titulo: </th>
			<th width="80%">{{ $meeting->title }}</th>
		</tr>
		<tr>
			<th class="title" width="20%">Tipo: </th>
			<th width="80%">{{ $typeMeeting[$meeting->type] }}</th>
		</tr>
		<tr>
			<th class="title" width="20%">Fecha: </th>
			<th width="80%">{{ $meeting->date_in }}</th>
		</tr>
		<tr>
			<th class="title" width="20%">Lugar: </th>
			<th width="80%">{{ $meeting->place }}</th>
		</tr>
	</thead>	
</table><br><br>
<table width="100%">
	<thead>
		<tr>
			<th class="title-xd" width="150%" align="center">@lang('report.meeting.order_day'): </th>
		</tr>
		<tr>
			<td width="150%">{{ $meeting->order_day }}</td>			
		</tr>	
	</thead>	
</table><br><br>

<table width="100%">
	<thead>
		<tr>
			<th width="150%" align="center" class="title-xd">@lang('report.meeting.assistant')</th>
		</tr>
		<tr>
			<th class="title-list" width="50%">Propiedad</th>
			<th class="title-list" width="50%">Coeficiente</th>
			<th class="title-list" width="50%">Propietario</th>
		</tr>
	</thead>
	<tbody>
		@forelse($meetingAssistant as $row)
		<tr>
			<td width="50%" class="body">{{ $row->property->property_number }}</td>
			<td width="50%" class="body">{{ $row->property->coefficient }}%</td>
			<td width="50%" class="body">{{ $row->property->person->FullName }}</td>
		</tr>
	    @empty
	    Sin asistentes
	    @endforelse
	</tbody>	
</table><br><br>

{{-- Acta --}}
<table width="100%">
	<thead>
		<tr>
			<th class="title-xd" width="150%" align="center">@lang('report.meeting.minute'): </th>
		</tr>
		<tr>
			<td width="150%">{{ $meeting->minute }}</td>
		</tr>
	</thead>	
</table><br><br>

{{-- Votaciones --}}
<table>
	<thead>
		<tr>
			<th class="title-xd" width="150%" align="center">@lang('report.meeting.voting.details')</th>
		</tr>
		<tr>
			<th class="title" width="30%">@lang('report.meeting.voting.description')</th>
			<td width="120%">{{ $votingDescription }}</td>
		</tr>
		<tr>
			<th class="title-list" width="150%">@lang('report.meeting.voting.title')</th>
		</tr>
	</thead>
</table>
@foreach($meetingVotingDetail as $row)
	<table>
	  <tbody>
	    <tr>
	      <th width="50%" class="title">Acuerdo:</th>
	      <td align="left" width="90%">&nbsp;{{ $row['agreement'] }}</td>
	    </tr>
	    @foreach($row['data'] as $rowVotings)
	      <tr>
	        <td align="left">{{ $rowVotings['person'] }}</td>
	        <td width="90%">
	        @if ($rowVotings['voting'] == 1)
	          @lang('report.meeting.voting.selectVotingYes')
	        @else
	          @lang('report.meeting.voting.selectVotingNo')
	        @endif
	        </td>
	      </tr>
	    @endforeach
	    <br><br>
	  </tbody>
	</table>
@endforeach    
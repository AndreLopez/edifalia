{!! Html::style('static/angular/style-reports.css') !!}
<div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
	    <div class="modal-header" style="background: #0695cb;color: #fff;border-bottom: 5px solid #1a2226;">
	      <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: #fff;" ng-click="assistant.cancel()">
	      <span aria-hidden="true">&times;</span></button>
	      <h4 class="modal-title">@lang('dashboard.mnu_dossier_sign')</h4>
	    </div>       
	  	<div class="modal-body">
			<div class="box-body">
				<table>
					<thead>
						<tr>
							<th></th>
							<th>#</th>
							<th>@lang('report.report.property')</th>
							<th>@lang('report.report.coefficient')</th>
							<th>@lang('report.report.owner')</th>							
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="row in assistant.property">
							<td><input type="checkbox" id="person_{$ row.id $}" ng-click="assistant.selectPerson(row)" value="0"></td>
							<td>{$ $index + 1 $}</td>
							<td>{$ row.building_number $}</td>
							<td>{$ row.coefficient $}</td>
							<td>{$ row.first_name $} {$ row.last_name $}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="box-footer">
				<button class="btn btn-default" ng-click="assistant.cancel()">@lang('report.buttons.exit')</button>	
				<button class="btn btn-primary" ng-click="assistant.select()">@lang('report.buttons.to_accept')</button>	
			</div>
		</div>
	</div>
</div>
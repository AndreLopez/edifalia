@extends('backend.report.baseReport')

@section('breadcrumb_title')
    @lang('dashboard.mnu_meetings')
@endsection

@section('content')
<div ng-app="edifaliaApp">
	<div class="box" ng-controller="MeetingController as meeting">
	    <div class="box-header with-border">
	        <a href="{{ route('meeting.create') }}" class="btn btn-app">
            	<i class="fa fa-plus"></i> @lang('report.buttons.load')
          	</a>
	        <a class="btn btn-app" ng-click="meeting.inData.length && meeting.printPdf()" ng-disabled="!meeting.inData.length">
            	<i class="fa fa-file-pdf-o"></i> @lang('report.buttons.pdf')
          	</a>
          	<a class="btn btn-app" ng-click="meeting.inData.length && meeting.printExcel()" ng-disabled="!meeting.inData.length">
            	<i class="fa fa-file-excel-o"></i> @lang('report.buttons.excel')
          	</a>
	    </div>
	    <div class="box-body table-responsive">    	
	      	<div class="box-body">
		        <div class="alert alert-warning" ng-show="!meeting.inData.length">
                  <h4><i class="icon fa fa-warning"></i> @lang('dashboard.messages.notice')</h4>
                  @lang('dashboard.messages.no_stored_records')
              	</div>
	            <table ng-table="meeting.tableParams" class="table table-condensed table-bordered table-striped" ng-show="$data.length">
		            <tr ng-repeat="row in $data">
		            	<td><input name="radio_action" id="radio_action" value="0" checked="false" type="radio" ng-click="meeting.selectItem(row)"></td>
		              	<td data-title="'@lang('report.meeting.title')'" sortable="'title'"><a href="show/{$row.id$}">{$ row.title $}</a></td>
		              	<td data-title="'@lang('report.meeting.date')'" sortable="'date_in'">{$ row.date_in $}</td>
		              	<td data-title="'@lang('report.meeting.place')'" sortable="'place'">{$ row.place $}</td>
		              	<td data-title="'@lang('report.report.actions')'" sortable="'place'">
		              		<a href class="btn btn-danger" ng-click="meeting.remove(row)"><i class="fa fa-trash" style="color:#fff;"></i></a>
		              	</td>
		            </tr>
		        </table>
	        </div>
	    </div>
	</div>	
</div>
@endsection
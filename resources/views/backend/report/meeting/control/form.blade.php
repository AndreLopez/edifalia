{!! Html::style('static/angular/style-reports.css') !!}
{{-- Select2 (required) --}}
{!! Html::style('static/select2/css/select2.css') !!}
<style type="text/css">
	.modal{
		margin-left: 15%;
	}
</style>
<div class="modal-dialog" role="document" style="margin-left: 0%;">
	<div class="modal-content">
	    <div class="modal-header" style="background: #0695cb;color: #fff;border-bottom: 5px solid #1a2226;">
	      Carga de encargado de control
	    </div>
	    <div class="modal-body">
			<div class="box-body">
				<input type="text" class="form-control" ng-model="control.modelForm.title" placeholder="Titulo"> 
				<br>
				<select class="form-control select2" ng-model="control.modelForm.person">
						<option value="">Seleccione...</option>
					@foreach($person as $row)
						<option value="{{ $row->id }}">{{ $row->FullName }}</option>		    		
	    			@endforeach
				</select>
			</div>
			<div class="box-footer">
				<button type="submit" class="btn btn-default" ng-click="control.cancel()">@lang('report.buttons.exit')</button>	
				<button type="submit" class="btn btn-primary pull-right" ng-click="control.create()">@lang('report.buttons.create')</button>	
			</div>
		</div>
	</div>
</div>
{{-- Select2 (required) --}}
{!! Html::script('static/select2/js/select2.min.js') !!}
<script type="text/javascript">
	$(document).ready(function () {
		
        /*
         * Select2 Init
         */
        var select2 = $(".select2");
    });
</script>
@extends('backend.report.baseReport')

@section('breadcrumb_title')
    @lang('dashboard.mnu_reports_controls')
@endsection

@section('content')
<div ng-app="edifaliaApp">
	<div class="box" ng-controller="MeetingControlController as meeting">
	    <div class="box-header with-border">
	        <a href class="btn btn-default" ng-click="meeting.create()">
            	<i class="fa fa-plus"></i> @lang('report.buttons.create')
          	</a>
	    </div>
	    <div class="box-body table-responsive">    	
	      	<div class="box-body">
	            <table ng-table="meeting.tableParams" class="table table-condensed table-bordered table-striped">
		            <tr ng-repeat="row in $data">
		              <td title="'#'">{$ $index + 1 $}</td>
		              <td data-title="'@lang('report.meeting.title')'" sortable="'title'"><a href="show/{$row.id$}">{$ row.title $}</a></td>
		              <td data-title="'@lang('report.meeting.person')'" sortable="'person'">{$ row.first_name $} {$ row.last_name $}</td>
		              <td>
		              	<a class="btn btn-danger" href ng-click="meeting.remove(row)"><i class="fa fa-trash" style="color:#fff;"></i></a>
		              </td>
		            </tr>
		        </table>
	        </div>
	    </div>
	</div>	
</div>
@endsection
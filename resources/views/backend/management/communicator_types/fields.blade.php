<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
    {!! Form::label(trans('dashboard.fields.name')) !!}
    {!! Form::text('name', null, [
        'class' => 'form-control',
        'placeholder' => trans('dashboard.fields.name'),
        'required' => true])
    !!}
</div>
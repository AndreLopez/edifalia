@extends('layouts.dashboard')

@section('breadcrumb_title')
    @lang('dashboard.mnu_management')
@endsection

@section('breadcrumb_subtitle')
    @lang('dashboard.titles.budgets_list')
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_management')</a></li>
    <li class="active">@lang('dashboard.mnu_budgets')</li>
@endsection

@section('modals')
    @include('backend.partials.modals')
@endsection

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('dashboard.mnu_budgets')</h3>
        </div>
        <div class="box-body table-responsive">
            <table class="table table-hover">
                @if($budgets->count())
                    <tr>
                        <th>@lang('dashboard.tables.title')</th>
                        <th>@lang('dashboard.tables.community')</th>
                        <th>@lang('dashboard.tables.invoicing_concept')</th>
                        <th>@lang('dashboard.tables.actions')</th>
                    </tr>
                @endif
                @forelse($budgets as $budget)
                    <tr data-id="{{$budget->id}}"
                        data-rundo="{{route('budgets.undelete', $budget->id)}}">
                        <td>{{$budget->title}}</td>
                        <td>{{$budget->community->name}}</td>
                        <td>{{$budget->invoicingConcept->name}}</td>
                        <td>
                            <div class="btn-group btn-group-xs">
                                <a data-toggle="tooltip" class="btn btn-info"
                                   title="@lang('dashboard.buttons.edit')"
                                   href="{{route('budgets.edit', $budget->id)}}">
                                    <i class="fa fa-pencil-square-o"></i></a>

                                <a data-toggle="tooltip" class="btn btn-info delete_record"
                                   title="@lang('dashboard.buttons.delete')"
                                   href="{{route('budgets.destroy', $budget->id)}}">
                                    <i class="fa fa-trash-o"></i></a>
                            </div>
                        </td>
                    </tr>
                @empty
                    <div class="alert alert-warning">
                        <h4><i class="icon fa fa-warning"></i> @lang('dashboard.messages.notice')</h4>
                        @lang('dashboard.messages.no_stored_records')
                    </div>
                @endforelse
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            {{$budgets->links()}}
            <a data-toggle="tooltip" class="btn btn-primary pull-right"
               title="@lang('dashboard.buttons.new')"
               href="{{route('budgets.create')}}">
                <i class="fa fa-plus"></i> @lang('dashboard.buttons.new')</a>
        </div>
        <!-- /.box-footer-->
    </div>
@endsection

@section('scripts')
    {{-- Backend scripts  --}}
    {!! Html::script('js/code01.js') !!}
@endsection
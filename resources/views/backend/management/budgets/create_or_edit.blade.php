@extends('layouts.dashboard')

@section('breadcrumb_title')
    @lang('dashboard.mnu_budgets')
@endsection

@section('breadcrumb_subtitle')
    {{$title}}
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_management')</a></li>
    <li class="active">@lang('dashboard.mnu_budgets')</li>
@endsection

@section('modals')
    @include('backend.partials.modals')
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('dashboard.mnu_budgets')</h3>
            <div class="btn-group btn-group-sm pull-right">
                <a data-toggle="tooltip" class="btn btn-primary"
                   title="@lang('dashboard.buttons.back')"
                   href="{{route('budgets.index')}}">
                    <i class="fa fa-reply"></i></a>
                @if(isset($edit))
                    <a data-toggle="tooltip" class="btn btn-primary"
                       title="@lang('dashboard.buttons.new')"
                       href="{{route('budgets.create')}}">
                        <i class="fa fa-plus"></i></a>
                @endif
            </div>
        </div>
        @if(isset($new))
            {!! Form::open($header) !!}
        @elseif(isset($edit))
            {!! Form::model($budget, $header) !!}
        @endif
        <div class="box-body">
            @include('backend.partials.messages')
            @include('backend.management.budgets.fields')
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-info">
                <i class="fa fa-save"></i>
                @if(isset($new)) @lang('dashboard.buttons.save') @elseif(isset($edit))
                    @lang('dashboard.buttons.update')
                @endif
            </button>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    @include('backend.management.budgets.js_get_communities')
    <script>
        $(document).ready(function(){
            selectBudget();
            $('#budget').on('change', selectBudget);
            function selectBudget(){
                $("#pleaseWait").modal("show");
                var budget_id = $('#budget').val();
                var route = $('#budget').data('route');
                if ($.trim(budget_id) != ''){
                    $.get(route, {budget_id:budget_id}, function(data){
                        if(data.data){
                            $("#invoicing option[value="+data.invoicing+"]").prop("selected", true);
                            $("#title").val(data.title);
                            $("#periodicity option[value="+data.periodicity+"]").prop("selected", true);
                            $("#duration").val(data.duration);
                            $("#rounding").val(data.rounding);
                            $("#month").val(data.month);
                            $("#year").val(data.year);
                        }
                        else {
                            toastr.error(data.message);
                        }
                        $("#pleaseWait").modal("hide");
                    });
                }
                else {
                    $("#pleaseWait").modal("hide");
                }
            }
        });
    </script>
@endsection
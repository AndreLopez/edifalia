<script>
    $(document).ready(function(){
        selectCommunity();
        $('#community').on('change', selectCommunity);
        function selectCommunity(){
            $("#pleaseWait").modal("show");
            var community_id = $('#community').val();
            var route = $('#community').data('route');
            var objective_id = $('#community').data('objective');
            if ($.trim(community_id) != ''){
                if(objective_id == '') {
                    $.get(route, {community_id:community_id}, function(data){
                        if(data.data){
                            $("#properties").html(data.html);
                        }
                        else {
                            toastr.error(data.message);
                        }
                        $("#pleaseWait").modal("hide");
                    });
                }
                else {
                    $.get(route, {community_id:community_id, objective_id:objective_id}, function(data){
                        if(data.data){
                            $("#properties").html(data.html);
                        }
                        else {
                            toastr.error(data.message);
                        }
                        $("#pleaseWait").modal("hide");
                    });
                }
            }
            else {
                $("#pleaseWait").modal("hide");
            }
        }
    });
</script>
@inject('communities', 'Edifalia\Services\Communities')
@inject('invoicing_concepts', 'Edifalia\Services\InvoicingConcepts')
@inject('budgets', 'Edifalia\Services\Budgets')

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('community_id') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.community')) !!}
        {!! Form::select('community_id', Auth::user()->can('admin') ? $communities->all() : $communities->ofEmployee(), null, [
            'class' => 'form-control',
            'id' => 'community',
            'data-route' => route('budgets.properties'),
            'data-objective' => (isset($budget))?$budget->id:'' ])
        !!}
    </div>
    <div class="form-group col-md-6 {{ $errors->has('budget_title') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.saved_budgets')) !!}
        {!! Form::select('saved_budget', $budgets->get(), null,[
            'class' => 'form-control',
            'id' => 'budget',
            'data-route' => route('budgets.data')])
        !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('invoicing_concept_id') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.invoicing_concept')) !!}
        {!! Form::select('invoicing_concept_id', $invoicing_concepts->get(), null,
            ['class' => 'form-control',
             'id' => 'invoicing', ])
        !!}
    </div>
    <div class="form-group col-md-6 {{ $errors->has('title') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.title')) !!}
        {!! Form::text('title', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.title'),
            'required' => true,
            'id' => 'title',])
        !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-4 {{ $errors->has('periodicity') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.periodicity')) !!}
        {!! Form::select('periodicity', [
                '' => trans('dashboard.fields.select_periodicity'),
                '1' => trans('dashboard.periodicity.monthly'),
                '2' => trans('dashboard.periodicity.quarterly'),
                '3' => trans('dashboard.periodicity.biannual'),
                '4' => trans('dashboard.periodicity.annual'),
            ], null,['class' => 'form-control', 'id' => 'periodicity']) !!}
    </div>
    <div class="form-group col-md-4 {{ $errors->has('duration') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.duration')) !!}
        {!! Form::text('duration', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.months'),
            'required' => true,
            'id' => 'duration', ])
        !!}
    </div>
    <div class="form-group col-md-4 {{ $errors->has('rounding') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.rounding')) !!}
        {!! Form::number('rounding', null, [
            'max' => 6,
            'min' => 2,
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.digits_number'),
            'required' => true,
            'id' => 'rounding', ])
        !!}
    </div>
</div>

<div class="form-group">{!! Form::label(trans('dashboard.fields.quota')) !!}:</div>
<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('month') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.month')) !!}
        {!! Form::number('month', null, [
            'max' => 12,
            'min' => 1,
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.month'),
            'required' => true,
            'id' => 'month', ])
        !!}
    </div>
    <div class="form-group col-md-6 {{ $errors->has('year') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.year')) !!}
        {!! Form::number('year', null, [
            'max' => 2200,
            'min' => 2017,
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.year'),
            'required' => true,
            'id' => 'year', ])
        !!}
    </div>
</div>

<hr><div class="form-group"><h4>@lang('dashboard.fields.expenses'):</h4></div>
<div id="properties">
</div>

{{-- <div class="row">
        <div class="form-group col-md-4">
            <p><strong>aqui va el texto</strong></p>
        </div>
        <div class="form-group col-md-4 {{ $errors->has('general') ? ' has-error' : '' }}">
            {!! Form::text('general', null, [
                'class' => 'form-control',
                'placeholder' => trans('dashboard.fields.general')])
            !!}
        </div>
        <div class="form-group col-md-4 {{ $errors->has('amount') ? ' has-error' : '' }}">
            {!! Form::text('amount', null, [
                'class' => 'form-control',
                 'placeholder' => trans('dashboard.fields.amount') ])
            !!}
        </div>
    </div> --}}
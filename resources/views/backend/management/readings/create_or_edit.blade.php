@extends('layouts.dashboard')

@section('breadcrumb_title')
    @lang('dashboard.mnu_counters_reading')
@endsection

@section('breadcrumb_subtitle')
    {{$title}}
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_management')</a></li>
    <li class="active">@lang('dashboard.mnu_counters_reading')</li>
@endsection

@section('modals')
    @include('backend.partials.modals')
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('dashboard.mnu_counters_reading')</h3>
            <div class="btn-group btn-group-sm pull-right">
                <a data-toggle="tooltip" class="btn btn-primary"
                   title="@lang('dashboard.buttons.back')"
                   href="{{route('readings.index')}}">
                    <i class="fa fa-reply"></i></a>
                @if(isset($edit))
                    <a data-toggle="tooltip" class="btn btn-primary"
                       title="@lang('dashboard.buttons.new')"
                       href="{{route('readings.create')}}">
                        <i class="fa fa-plus"></i></a>
                @endif
            </div>
        </div>
        @if(isset($new))
            {!! Form::open($header) !!}
        @elseif(isset($edit))
            {!! Form::model($reading, $header) !!}
        @endif
        <div class="box-body">
            @include('backend.partials.messages')
            @include('backend.management.readings.fields')
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-info">
                <i class="fa fa-save"></i>
                @if(isset($new)) @lang('dashboard.buttons.save') @elseif(isset($edit))
                    @lang('dashboard.buttons.update')
                @endif
            </button>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('styles')
    {{-- bootstrap datepicker --}}
    {!! Html::style('static/plugins/datepicker/datepicker3.css') !!}
@endsection

@section('scripts')
    {{-- bootstrap datepicker --}}
    {!! Html::script('static/plugins/datepicker/bootstrap-datepicker.js') !!}
    @include('backend.management.budgets.js_get_communities')
    <script>
        $(document).ready(function(){
            $("#datepicker").datepicker();
            selectReading();
            $('#reading').on('change', selectReading);
            function selectReading(){
                $("#pleaseWait").modal("show");
                var reading_id = $('#reading').val();
                var route = $('#reading').data('route');
                if ($.trim(reading_id) != ''){
                    $.get(route, {reading_id:reading_id}, function(data){
                        if(data.data){
                            $("#invoicing option[value="+data.invoicing+"]").prop("selected", true);
                            $("#title").val(data.title);
                            $("#periodicity option[value="+data.periodicity+"]").prop("selected", true);
                            $("#rate").val(data.rate);
                            $("#month").val(data.month);
                            $("#year").val(data.year);
                            $("#concept_receipt").val(data.concept_receipt);
                            $("#datepicker").val(data.date);
                        }
                        else {
                            toastr.error(data.message);
                        }
                        $("#pleaseWait").modal("hide");
                    });
                }
                else {
                    $("#pleaseWait").modal("hide");
                }
            }
        });
    </script>
@endsection
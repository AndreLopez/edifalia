@inject('communities', 'Edifalia\Services\Communities')
@inject('invoicing_concepts', 'Edifalia\Services\InvoicingConcepts')
@inject('readings', 'Edifalia\Services\Readings')
@inject('rates', 'Edifalia\Services\Rates')

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('community_id') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.community')) !!}
        {!! Form::select('community_id', Auth::user()->can('admin') ? $communities->all() : $communities->ofEmployee(), null, [
            'class' => 'form-control',
            'id' => 'community',
            'data-route' => route('readings.properties'),
            'data-objective' => (isset($reading))?$reading->id:'' ])
        !!}
    </div>
    <div class="form-group col-md-6">
        {!! Form::label(trans('dashboard.fields.saved_readings')) !!}
        {!! Form::select('saved_reading', $readings->get(), null,[
            'class' => 'form-control',
            'id' => 'reading',
            'data-route' => route('readings.data')])
        !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('invoicing_concept_id') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.invoicing_concept')) !!}
        {!! Form::select('invoicing_concept_id', $invoicing_concepts->get(), null,
            ['class' => 'form-control',
             'id' => 'invoicing', ])
        !!}
    </div>
    <div class="form-group col-md-6 {{ $errors->has('title') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.title')) !!}
        {!! Form::text('title', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.title'),
            'required' => true,
            'id' => 'title',])
        !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('periodicity') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.periodicity')) !!}
        {!! Form::select('periodicity', [
                '' => trans('dashboard.fields.select_periodicity'),
                '1' => trans('dashboard.periodicity.monthly'),
                '2' => trans('dashboard.periodicity.quarterly'),
                '3' => trans('dashboard.periodicity.biannual'),
                '4' => trans('dashboard.periodicity.annual'),
            ], null,['class' => 'form-control', 'id' => 'periodicity']) !!}
    </div>
    <div class="form-group col-md-6 {{ $errors->has('rate_id') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.rate')) !!}
        {!! Form::select('rate_id', $rates->get(), null,
            ['class' => 'form-control',
             'id' => 'rate', ])
        !!}
    </div>
</div>

<div class="form-group">{!! Form::label(trans('dashboard.fields.charge')) !!}:</div>
<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('month') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.month')) !!}
        {!! Form::number('month', null, [
            'max' => 12,
            'min' => 1,
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.month'),
            'required' => true,
            'id' => 'month', ])
        !!}
    </div>
    <div class="form-group col-md-6 {{ $errors->has('year') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.year')) !!}
        {!! Form::number('year', null, [
            'max' => 2200,
            'min' => 2017,
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.year'),
            'required' => true,
            'id' => 'year', ])
        !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('concept_receipt') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.concept_receipt')) !!}:
        {!! Form::textarea('concept_receipt', null, ['class' => 'form-control', 'rows' => 5, 'id' => 'concept_receipt'])!!}
    </div>
    <div class="form-group col-md-6 {{ $errors->has('date') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.date')) !!}
        <div class="input-group date">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
            {!! Form::text('date', null, [
                'class' => 'form-control pull-right',
                'placeholder' => trans('dashboard.fields.date'),
                'id' => 'datepicker',
                'required' => true])
            !!}
        </div>
    </div>
</div>

<hr>
<div id="properties">
</div>
@extends('layouts.dashboard')

@section('breadcrumb_title')
    @lang('dashboard.mnu_invoicing_concept')
@endsection

@section('breadcrumb_subtitle')
    {{$title}}
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_utilities')</a></li>
    <li class="active">@lang('dashboard.mnu_invoicing_concept')</li>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('dashboard.mnu_invoicing_concept')</h3>
            <div class="btn-group btn-group-sm pull-right">
                <a data-toggle="tooltip" class="btn btn-primary"
                   title="@lang('dashboard.buttons.back')"
                   href="{{route('invoicing-concepts.index')}}">
                    <i class="fa fa-reply"></i></a>
                @if(isset($edit))
                    <a data-toggle="tooltip" class="btn btn-primary"
                       title="@lang('dashboard.buttons.new')"
                       href="{{route('invoicing-concepts.create')}}">
                        <i class="fa fa-plus"></i></a>
                @endif
            </div>
        </div>
        @if(isset($new))
            {!! Form::open($header) !!}
        @elseif(isset($edit))
            {!! Form::model($invoicingConcept, $header) !!}
        @endif
        <div class="box-body">
            @include('backend.partials.messages')
            @include('backend.management.invoicing_concepts.fields')
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-info">
                <i class="fa fa-save"></i>
                @if(isset($new)) @lang('dashboard.buttons.save') @elseif(isset($edit))
                    @lang('dashboard.buttons.update')
                @endif
            </button>
        </div>
        {!! Form::close() !!}
    </div>
@endsection
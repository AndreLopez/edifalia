<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('file') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.file')) !!}
        {!! Form::file('file') !!}
        @if(isset($template))
            {!! Form::label(trans('dashboard.fields.current_file')) !!}
            @if($template->existsFile())
                <a href="{{asset('templates_files/'.$template->file)}}" target="_blank">
                    {{$template->file}}
                </a>
            @else
                <p class="text-danger">
                    <i class="fa fa-warning"></i>
                    @lang('dashboard.messages.no_file')
                </p>
            @endif
        @endif
    </div>
    <div class="form-group col-md-6 {{ $errors->has('title') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.title')) !!}
        {!! Form::text('title', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.title'),
            'required' => true])
        !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('model_id') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.model')) !!}
        {!! Form::select('model_id', $models->get(),null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-6 {{ $errors->has('office_id') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.office')) !!}
        {!! Form::select('office_id', $offices->get(),null, ['class' => 'form-control']) !!}
        <p class="help-block">@lang('dashboard.fields.temp_all_office')</p>
    </div>
</div>
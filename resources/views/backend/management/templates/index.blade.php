@extends('layouts.dashboard')

@section('breadcrumb_title')
    @lang('dashboard.mnu_management')
@endsection

@section('breadcrumb_subtitle')
    @lang('dashboard.titles.template_list')
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_management')</a></li>
    <li class="active">@lang('dashboard.mnu_templates')</li>
@endsection

@section('modals')
    @include('backend.partials.modals')
@endsection

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('dashboard.mnu_templates')</h3>
        </div>
        <div class="box-body table-responsive">
            <table class="table table-hover">
                @if($templates->count())
                    <tr>
                        <th>@lang('dashboard.tables.title')</th>
                        <th>@lang('dashboard.tables.model')</th>
                        <th>@lang('dashboard.tables.office')</th>
                        <th>@lang('dashboard.tables.actions')</th>
                    </tr>
                @endif
                @forelse($templates as $template)
                    <tr data-id="{{$template->id}}"
                        data-rundo="{{route('templates.undelete', $template->id)}}">
                        <td>
                            @if($template->existsFile())
                                <a href="{{asset('templates_files/'.$template->file)}}" target="_blank">
                                    {{$template->title}}
                                </a>
                            @else
                                <p class="text-danger">
                                    <i class="fa fa-warning"></i>
                                    @lang('dashboard.messages.no_file')
                                </p>
                            @endif
                        </td>
                        <td>{{$template->model->name}}</td>
                        <td>
                            {{(!is_null($template->office_id))?$template->office->name:trans('dashboard.messages.all_offices')}}
                        </td>
                        <td>
                            <div class="btn-group btn-group-xs">
                                <a data-toggle="tooltip" class="btn btn-info"
                                   title="@lang('dashboard.buttons.edit')"
                                   href="{{route('templates.edit', $template->id)}}">
                                    <i class="fa fa-pencil-square-o"></i></a>

                                <a data-toggle="tooltip" class="btn btn-info delete_record"
                                   title="@lang('dashboard.buttons.delete')"
                                   href="{{route('templates.destroy', $template->id)}}">
                                    <i class="fa fa-trash-o"></i></a>
                            </div>
                        </td>
                    </tr>
                @empty
                    <div class="alert alert-warning">
                        <h4><i class="icon fa fa-warning"></i> @lang('dashboard.messages.notice')</h4>
                        @lang('dashboard.messages.no_stored_records')
                    </div>
                @endforelse
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            {{$templates->links()}}
            <a data-toggle="tooltip" class="btn btn-primary pull-right"
               title="@lang('dashboard.buttons.new')"
               href="{{route('templates.create')}}">
                <i class="fa fa-plus"></i> @lang('dashboard.buttons.new')</a>
        </div>
        <!-- /.box-footer-->
    </div>
@endsection

@section('scripts')
    {{-- Backend scripts  --}}
    {!! Html::script('js/code01.js') !!}
@endsection
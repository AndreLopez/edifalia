@extends('layouts.dashboard')

@section('breadcrumb_title')
    @lang('dashboard.mnu_charges')
@endsection

@section('breadcrumb_subtitle')
    {{$title}}
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_management')</a></li>
    <li class="active">@lang('dashboard.mnu_charges')</li>
@endsection

@section('modals')
    @include('backend.partials.modals')
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('dashboard.mnu_charges')</h3>
            <div class="btn-group btn-group-sm pull-right">
                <a data-toggle="tooltip" class="btn btn-primary"
                   title="@lang('dashboard.buttons.back')"
                   href="{{route('charges.index')}}">
                    <i class="fa fa-reply"></i></a>
                @if(isset($charge))
                    <a data-toggle="tooltip" class="btn btn-primary"
                       title="@lang('dashboard.buttons.new')"
                       href="{{route('charges.create')}}">
                        <i class="fa fa-plus"></i></a>
                @endif
            </div>
        </div>
        @if(!isset($charge))
            {!! Form::open($header) !!}
        @else
            {!! Form::model($charge, $header) !!}
        @endif
        <div class="box-body">
            @include('backend.partials.messages')
            @include('backend.management.charges.fields')
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-info">
                <i class="fa fa-save"></i>
                @if(!isset($charge)) @lang('dashboard.buttons.save') @else
                    @lang('dashboard.buttons.update')
                @endif
            </button>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            selectCommunity();
            $('#community').on('change', selectCommunity);

            function selectCommunity() {
                $("#pleaseWait").modal("show");
                var community_id = $('#community').val();
                var route = $('#community').data('route');
                var charge = $('#community').data('id');
                if ($.trim(community_id) != '') {
                    $.get(route, {community_id:community_id, charge_id:charge}, function(data) {
                        if (data.data) {
                            $('#issue_receipt').empty();
                            $('#issue_receipt').append("<option value=''>" + data.select_receipts + "</option>");
                            $.each(data.receipts, function(index, value){
                                $('#issue_receipt').append("<option value='" + index + "'>" + value + "</option>");
                            });
                            $("#issue_receipt option[value='" + data.issueReceiptValue + "']").attr("selected", true);

                            $('#property').empty();
                            $('#property').append("<option value=''>" + data.select_properties + "</option>");
                            $.each(data.properties, function(index, value){
                                $('#property').append("<option value='" + index + "'>" + value + "</option>");
                            });
                            $("#property option[value='" + data.propertyValue + "']").attr("selected", true);
                        } else {
                            toastr.error(data.message);
                        }
                        $("#pleaseWait").modal("hide");
                    });
                } else {
                    $("#pleaseWait").modal("hide");
                }
            }
        });
    </script>
@endsection
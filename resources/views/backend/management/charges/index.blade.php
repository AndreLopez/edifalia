@extends('layouts.dashboard')

@section('breadcrumb_title')
    @lang('dashboard.mnu_charges')
@endsection

@section('breadcrumb_subtitle')
    @lang('dashboard.titles.charges_list')
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_management')</a></li>
    <li class="active">@lang('dashboard.mnu_charges')</li>
@endsection

@section('modals')
    @include('backend.partials.modals')
@endsection

@section('content')
    @include('backend.management.charges.search')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('dashboard.mnu_charges')</h3>
        </div>
        <div class="box-body table-responsive">
            <table class="table table-hover">
                @if($charges->count())
                    <tr>
                        <th>@lang('dashboard.tables.floor')</th>
                        <th>@lang('dashboard.tables.date')</th>
                        <th>@lang('dashboard.tables.receipt')</th>
                        <th>@lang('dashboard.tables.property')</th>
                        <th>@lang('dashboard.tables.owner')</th>
                        <th>@lang('dashboard.tables.amount')</th>
                        <th>@lang('dashboard.tables.state')</th>
                        <th>@lang('dashboard.tables.pending')</th>
                        <th>@lang('dashboard.tables.charge_type')</th>
                        <th>@lang('dashboard.tables.actions')</th>
                    </tr>
                @endif
                @forelse($charges as $charge)
                    <tr data-id="{{$charge->id}}"
                        data-rundo="{{route('charges.undelete', $charge->id)}}">
                        <td>{{$charge->property->building_number}}</td>
                        <td>{{$charge->issueReceipt->date}}</td>
                        <td>{{$charge->issueReceipt->id}}</td>
                        <td>{{$charge->property->property_number}}</td>
                        <td>{{$charge->owner_full_name}}</td>
                        <td>{{$charge->amount}}</td>
                        <td>
                            <div class="text-{{$charge->state_class}}">
                                <i class='fa fa-{{$charge->state}}'></i>
                            </div>
                        </td>
                        <td>{{$charge->pending}}</td>
                        <td>@lang('dashboard.charge_type.'.$charge->charge_type)</td>
                        <td>
                            <div class="btn-group btn-group-xs">
                                <a data-toggle="tooltip" class="btn btn-info"
                                   title="@lang('dashboard.buttons.edit')"
                                   href="{{route('charges.edit', $charge->id)}}">
                                    <i class="fa fa-pencil-square-o"></i></a>

                                <a data-toggle="tooltip" class="btn btn-info delete_record"
                                   title="@lang('dashboard.buttons.delete')"
                                   href="{{route('charges.destroy', $charge->id)}}">
                                    <i class="fa fa-trash-o"></i></a>
                            </div>
                        </td>
                    </tr>
                @empty
                    <div class="alert alert-warning">
                        <h4><i class="icon fa fa-warning"></i> @lang('dashboard.messages.notice')</h4>
                        @lang('dashboard.messages.no_stored_records')
                    </div>
                @endforelse
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            {{$charges->links()}}
            <a data-toggle="tooltip" class="btn btn-primary pull-right"
               title="@lang('dashboard.buttons.new')"
               href="{{route('charges.create')}}">
                <i class="fa fa-plus"></i> @lang('dashboard.buttons.new')</a>
        </div>
        <!-- /.box-footer-->
    </div>
@endsection

@section('scripts')
    {{-- Backend scripts  --}}
    {!! Html::script('js/code01.js') !!}
    {{-- bootstrap datepicker --}}
    {!! Html::script('static/plugins/datepicker/bootstrap-datepicker.js') !!}
    <script>
        $(document).ready(function(){
            $("#datepicker").datepicker();
        });
        $(document).ready(function(){
            $("#datepicker2").datepicker();
        });
    </script>
@endsection

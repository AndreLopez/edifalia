@inject('communities', 'Edifalia\Services\Communities')

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('community_id') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.community')) !!}
        @can('admin')
            {!! Form::select('community_id', $communities->all(), null, [
                'class' => 'form-control',
                'id' => 'community',
                'data-route' => route('charges.receipts_and_properties'),
                'data-id' => (isset($charge))?$charge->id:''])
            !!}
        @elsecan('employee')
            {!! Form::select('community_id', $communities->ofEmployee(), null, [
                'class' => 'form-control',
                'id' => 'community',
                'data-route' => route('charges.receipts_and_properties'),
                'data-id' => (isset($charge))?$charge->id:''])
            !!}
        @endcan
    </div>
    <div class="form-group col-md-6 {{ $errors->has('issue_receipt_id') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.issue_receipt')) !!}
        {!! Form::select('issue_receipt_id', [], null,
            ['class' => 'form-control',
             'id' => 'issue_receipt',])
        !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('property_id') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.property')) !!}
        {!! Form::select('property_id', [], null,
            ['class' => 'form-control',
             'id' => 'property'])
        !!}
    </div>
    <div class="form-group col-md-6 {{ $errors->has('pending') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.pending')) !!}
        {!! Form::text('pending', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.pending'),
            'required' => true])
        !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-4 {{ $errors->has('charge_type') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.charge_type')) !!}
        {!! Form::select('charge_type', [
                '' => trans('dashboard.fields.select_charge_type'),
                '1' => trans('dashboard.charge_type.1'),
                '2' => trans('dashboard.charge_type.2'),
            ], null,['class' => 'form-control'])
        !!}
    </div>
</div>
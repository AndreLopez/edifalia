@inject('communities', 'Edifalia\Services\Communities')

{!! Form::model(Request::all(),
    [	'route' => 'charges.index',
        'method' => 'GET',
        'rol' => 'search',
        'class' => 'form-inline'
    ])
!!}
    <div class="radio">
        <label>
            <div class="text-success">
                {!! Form::radio('state', 1, null) !!}
                <i class='fa fa-check'></i>
            </div>
        </label>
    </div>
    <div class="checkbox">
        <label>
            <div class="text-danger">
                {!! Form::radio('state', 2, null) !!}
                <i class='fa fa-close'></i>
            </div>
        </label>
    </div>
    @can('admin')
        <div class="form-group">
            {!! Form::select('community', $communities->all(), null, [
                'class' => 'form-control'])
            !!}
        </div>
    @elsecan('employee')
        <div class="form-group">
            {!! Form::select('community', $communities->ofEmployee(), null, [
                'class' => 'form-control'])
            !!}
        </div>
    @endcan
    <div class="form-group">
        {!! Form::text('date_from', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.from'),
            'id' => 'datepicker'])
        !!}
    </div>
    <div class="form-group">
        {!! Form::text('date_until', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.until'),
            'id' => 'datepicker2'])
        !!}
    </div>

    <button type="submit" class="btn btn-default" data-toggle="tooltip" title="@lang('dashboard.buttons.search')">
        <i class="fa fa-search"></i>
    </button>
    <a data-toggle="tooltip" class="btn btn-default"
       title="@lang('dashboard.buttons.refresh')"
       href="{{route('charges.index')}}">
        <i class="fa fa-refresh"></i></a>
{!! Form::close() !!}
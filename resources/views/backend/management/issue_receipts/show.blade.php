@extends('layouts.dashboard')

@section('breadcrumb_title')
    @lang('dashboard.mnu_issue_receipts')
@endsection

@section('breadcrumb_subtitle')
    @lang('dashboard.titles.issue_receipt_info')
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_management')</a></li>
    <li class="active">@lang('dashboard.mnu_issue_receipts')</li>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('dashboard.mnu_issue_receipts')</h3>
            <div class="btn-group btn-group-sm pull-right">
                <a data-toggle="tooltip" class="btn btn-primary"
                   title="@lang('dashboard.buttons.back')"
                   href="{{route('issue-receipts.index')}}">
                    <i class="fa fa-reply"></i></a>
            </div>
        </div>
        <div class="box-body table-responsive">
            <h4>@lang('dashboard.titles.issue_receipt_info'):</h4>
            <table class="table table-hover" style="text-align: center">
                <tr class="info">
                    <th style="text-align: center">@lang('dashboard.tables.date')</th>
                    <th style="text-align: center">@lang('dashboard.tables.message')</th>
                </tr>
                <tr>
                    <td>{{$issueReceipt->date}}</td>
                    <td>{{$issueReceipt->message}}</td>
                </tr>
            </table>
            <h4>@lang('dashboard.titles.pdf_generated'):</h4>
            <table class="table table-hover" style="text-align: center">
                <tr class="info">
                    <th style="text-align: center">#</th>
                    <th style="text-align: center">@lang('dashboard.tables.pdf')</th>
                </tr>
                @foreach($issueReceipt->issueReceiptPdfs as $pdf)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>
                            @if(\Storage::disk('local_pdf_receipt')->exists($pdf->pdf_name))
                                <a href="{{asset('storage/pdf_issue_receipts').'/'.$pdf->pdf_name}}" target="_blank">
                                    {{$pdf->pdf_name}}
                                </a>
                            @else
                                <strong>@lang('dashboard.messages.no_file')</strong>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </table><hr>

            @foreach($issueReceipt->communities as $community)
                <h4>{{$community->name}}:</h4>
                @forelse($community->receiptDues()->where('issue_receipt_id', $issueReceipt->id)->get() as $receiptDue)
                    @if(!in_array($receiptDue->invoicing_concept_id, $idsToExclude))
                        <table class="table table-hover" style="text-align: center">
                            <tr class="success">
                                <th style="text-align: center">@lang('receipt_due.table_concept')</th>
                                <th style="text-align: center">@lang('receipt_due.table_month')</th>
                                <th style="text-align: center">@lang('receipt_due.table_year')</th>
                                <th style="text-align: center">@lang('receipt_due.table_amount')</th>
                            </tr>
                            <tr>
                                <td>{{$receiptDue->invoicingConcept->name}}</td>
                                <td>{{$receiptDue->month}}</td>
                                <td>{{$receiptDue->year}}</td>
                                <td>{{$receiptDue->total_amount}}</td>
                            </tr>
                        </table>
                        <table class="table table-hover" style="text-align: center">
                            <tr class="success">
                                <th style="text-align: center">@lang('receipt_due.table_bloq')</th>
                                <th style="text-align: center">@lang('receipt_due.table_property')</th>
                                <th style="text-align: center">@lang('dashboard.tables.amount_to_pay')</th>
                            </tr>
                            @foreach($receiptDue->properties as $property)
                                <tr>
                                    <td>{{$property->building_number}}</td>
                                    <td>{{$property->property_number}}</td>
                                    <td>{{$property->pivot->amount}}</td>
                                </tr>
                            @endforeach
                        </table><br><hr>
                    @endif
                @empty
                    <h4 class="text-danger">@lang('dashboard.messages.no_receipt_dues')</h4>
                @endforelse
            @endforeach
        </div>
    </div>
@endsection
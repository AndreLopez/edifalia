@extends('layouts.dashboard')

@section('breadcrumb_title')
    @lang('dashboard.mnu_issue_receipts')
@endsection

@section('breadcrumb_subtitle')
    {{$title}}
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_management')</a></li>
    <li class="active">@lang('dashboard.mnu_issue_receipts')</li>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('dashboard.mnu_issue_receipts')</h3>
            <div class="btn-group btn-group-sm pull-right">
                <a data-toggle="tooltip" class="btn btn-primary"
                   title="@lang('dashboard.buttons.back')"
                   href="{{route('issue-receipts.index')}}">
                    <i class="fa fa-reply"></i></a>
                @if(isset($issueReceipt))
                    <a data-toggle="tooltip" class="btn btn-primary"
                       title="@lang('dashboard.buttons.new')"
                       href="{{route('issue-receipts.create')}}">
                        <i class="fa fa-plus"></i></a>
                @endif
            </div>
        </div>
        @if(!isset($issueReceipt))
            {!! Form::open($header) !!}
        @else
            {!! Form::model($issueReceipt, $header) !!}
        @endif
        <div class="box-body">
            @include('backend.partials.messages')
            @include('backend.management.issue_receipts.fields')
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-info">
                <i class="fa fa-save"></i>
                @if(!isset($issueReceipt))
                    @lang('dashboard.buttons.save')
                @else
                    @lang('dashboard.buttons.update')
                @endif
            </button>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('styles')
    {{-- bootstrap datepicker --}}
    {!! Html::style('static/plugins/datepicker/datepicker3.css') !!}
@endsection

@section('scripts')
    {{-- bootstrap datepicker --}}
    {!! Html::script('static/plugins/datepicker/bootstrap-datepicker.js') !!}
    <script>
        $(document).ready(function(){
            $("#datepicker").datepicker();
        });
    </script>
@endsection
@inject('invoicing_concepts', 'Edifalia\Services\InvoicingConcepts')
@inject('communities', 'Edifalia\Services\Communities')

<div class="row">
    <div class="form-group col-md-5{{ $errors->has('date') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.date')) !!}
        <div class="input-group date">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
            {!! Form::text('date', null, [
                'class' => 'form-control pull-right',
                'placeholder' => trans('dashboard.fields.date'),
                'id' => 'datepicker',
                'required' => true])
            !!}
        </div>
    </div>
    <div class="form-group col-md-7 {{ $errors->has('message') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.message')) !!}
        {!! Form::text('message', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.message'),
            'required' => true])
        !!}
    </div>
</div>
<br>
<div class="row">
    <div class="form-group col-md-7">
        {!! Form::label(trans('dashboard.fields.select_communities')) !!}.
        @lang('dashboard.fields.ctrl_key'):
        @can('admin')
            @if(!isset($issueReceipt))
                {!! Form::select('communities[]', $communities->all(), null, [
                    'class' => 'form-control',
                    'multiple' => true,
                    'size' => 15]) !!}
            @else
                <select name="communities[]" class="form-control" multiple="multiple" size="15">
                    @foreach($communities->all() as $key => $community)
                        @if($issueReceipt->communitySaved($key))
                            <option selected value="{{$key}}">{{$community}}</option>
                        @else
                            <option value="{{$key}}">{{$community}}</option>
                        @endif
                    @endforeach
                </select>
            @endif
        @elsecan('employee')
            @if(!isset($issueReceipt))
                {!! Form::select('communities[]', $communities->ofEmployee(), null, [
                'class' => 'form-control',
                'multiple' => true,
                'size' => 15]) !!}
            @else
                <select name="communities[]" class="form-control" multiple="multiple" size="15">
                    @foreach($communities->ofEmployee() as $key => $community)
                        @if($issueReceipt->communitySaved($key))
                            <option selected value="{{$key}}">{{$community}}</option>
                        @else
                            <option value="{{$key}}">{{$community}}</option>
                        @endif
                    @endforeach
                </select>
            @endif
        @endcan
    </div>
    <div class="form-group col-md-5">
        {!! Form::label(trans('dashboard.fields.invoicing_concepts_exclude')) !!}.
        @lang('dashboard.fields.optional')
        <br>
        @lang('dashboard.fields.ctrl_key'):
        @if(!isset($issueReceipt))
            {!! Form::select('invoicingConcepts[]', $invoicing_concepts->get(), null, [
                'class' => 'form-control',
                'multiple' => true,
                'size' => 13]) !!}
        @else
            <select name="invoicingConcepts[]" class="form-control" multiple="multiple" size="15">
                @foreach($invoicing_concepts->get() as $key => $invoicingConcept)
                    @if($issueReceipt->invoicingConceptSaved($key))
                        <option selected value="{{$key}}">{{$invoicingConcept}}</option>
                    @else
                        <option value="{{$key}}">{{$invoicingConcept}}</option>
                    @endif
                @endforeach
            </select>
        @endif
    </div>
</div>
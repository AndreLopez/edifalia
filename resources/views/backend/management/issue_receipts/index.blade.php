@extends('layouts.dashboard')

@section('breadcrumb_title')
    @lang('dashboard.mnu_issue_receipts')
@endsection

@section('breadcrumb_subtitle')
    @lang('dashboard.titles.issue_receipts_list')
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_management')</a></li>
    <li class="active">@lang('dashboard.mnu_issue_receipts')</li>
@endsection

@section('modals')
    @include('backend.partials.modals')
@endsection

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('dashboard.mnu_issue_receipts')</h3>
        </div>
        <div class="box-body table-responsive">
            <table class="table table-hover">
                @if($issueReceipts->count())
                    <tr>
                        <th>@lang('dashboard.tables.date')</th>
                        <th>@lang('dashboard.tables.message')</th>
                        <th>@lang('dashboard.tables.actions')</th>
                    </tr>
                @endif
                @forelse($issueReceipts as $issueReceipt)
                    <tr data-id="{{$issueReceipt->id}}"
                        data-rundo="{{route('issue-receipts.undelete', $issueReceipt->id)}}">
                        <td>{{$issueReceipt->date}}</td>
                        <td>{{$issueReceipt->message}}</td>
                        <td>
                            <div class="btn-group btn-group-xs">
                                <a data-toggle="tooltip" class="btn btn-info"
                                   title="@lang('dashboard.buttons.info')"
                                   href="{{route('issue-receipts.show', $issueReceipt->id)}}">
                                    <i class="fa fa-eye"></i></a>

                                {{--<a data-toggle="tooltip" class="btn btn-info"
                                   title="@lang('dashboard.buttons.edit')"
                                   href="{{route('issue-receipts.edit', $issueReceipt->id)}}">
                                    <i class="fa fa-pencil-square-o"></i></a>--}}

                                <a data-toggle="tooltip" class="btn btn-info delete_record"
                                   title="@lang('dashboard.buttons.delete')"
                                   href="{{route('issue-receipts.destroy', $issueReceipt->id)}}">
                                    <i class="fa fa-trash-o"></i></a>
                            </div>
                        </td>
                    </tr>
                @empty
                    <div class="alert alert-warning">
                        <h4><i class="icon fa fa-warning"></i> @lang('dashboard.messages.notice')</h4>
                        @lang('dashboard.messages.no_stored_records')
                    </div>
                @endforelse
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            {{$issueReceipts->links()}}
            <a data-toggle="tooltip" class="btn btn-primary pull-right"
               title="@lang('dashboard.buttons.new')"
               href="{{route('issue-receipts.create')}}">
                <i class="fa fa-plus"></i> @lang('dashboard.buttons.new')</a>
        </div>
        <!-- /.box-footer-->
    </div>
@endsection

@section('scripts')
    {{-- Backend scripts  --}}
    {!! Html::script('js/code01.js') !!}
@endsection
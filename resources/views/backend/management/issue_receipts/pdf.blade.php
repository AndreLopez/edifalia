<div class="table-responsive">
    <h4>@lang('dashboard.titles.issue_receipt_info'): #{{$issueReceipt->id}}</h4>
    <table class="table table-hover">
        <tr class="info">
            <th><strong>@lang('dashboard.tables.date')</strong></th>
            <th><strong>@lang('dashboard.tables.message')</strong></th>
            <th><strong>@lang('dashboard.tables.community')</strong></th>
            <th><strong>@lang('receipt_due.table_bloq')</strong></th>
            <th><strong>@lang('receipt_due.table_property')</strong></th>
        </tr>
        <tr>
            <td>{{$issueReceipt->date}}</td>
            <td>{{$issueReceipt->message}}</td>
            <td>{{$community->name}}</td>
            <td>{{$property->building_number}}</td>
            <td>{{$property->property_number}}</td>
        </tr>
    </table>
    <h4>@lang('dashboard.mnu_quota_receipts'):</h4>
    <table class="table table-hover">
        <tr class="success">
            <th><strong>@lang('dashboard.tables.invoicing_concept')</strong></th>
            <th><strong>@lang('receipt_due.table_amount')</strong></th>
            <th><strong>@lang('receipt_due.table_month')</strong></th>
            <th><strong>@lang('receipt_due.table_year')</strong></th>
            <th><strong>@lang('dashboard.tables.amount_to_pay')</strong></th>
        </tr>
        @foreach($receiptDues as $receiptDue)
            @if(!in_array($receiptDue->invoicing_concept_id, $idsToExclude))
                <tr>
                    <td>{{$receiptDue->invoicingConcept->name}}</td>
                    <td>{{$receiptDue->total_amount}}</td>
                    <td>{{$receiptDue->month}}</td>
                    <td>{{$receiptDue->year}}</td>
                    <td>{{$receiptDue->pivot->amount}}</td>
                </tr>
            @endif
        @endforeach
    </table>
</div>
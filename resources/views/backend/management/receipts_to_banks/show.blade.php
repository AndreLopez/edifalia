@extends('layouts.dashboard')

@section('breadcrumb_title')
    @lang('dashboard.mnu_receipts_to_bank')
@endsection

@section('breadcrumb_subtitle')
    @lang('dashboard.titles.receipts_to_bank_info')
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_management')</a></li>
    <li class="active">@lang('dashboard.mnu_receipts_to_bank')</li>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('dashboard.mnu_receipts_to_bank')</h3>
            <div class="btn-group btn-group-sm pull-right">
                <a data-toggle="tooltip" class="btn btn-primary"
                   title="@lang('dashboard.buttons.back')"
                   href="{{route('receipts-banks.index')}}">
                    <i class="fa fa-reply"></i></a>
            </div>
        </div>
        <div class="box-body table-responsive">
            <h4>@lang('dashboard.titles.receipts_to_bank_info'):</h4>
            <table class="table table-hover" style="text-align: center">
                <tr class="info">
                    <th style="text-align: center">@lang('dashboard.tables.file_format')</th>
                    <th style="text-align: center">@lang('dashboard.tables.date_sent_to_bank')</th>
                    <th style="text-align: center">@lang('dashboard.tables.charge_date')</th>
                    <th style="text-align: center">@lang('dashboard.fields.remittance_pay')</th>
                    <th style="text-align: center">@lang('dashboard.fields.payment_remittance_daily')</th>
                </tr>
                <tr>
                    <td>@lang('dashboard.file_format.'.$receiptsToBank->file_format)</td>
                    <td>{{$receiptsToBank->state_and_date}}</td>
                    <td>{{$receiptsToBank->charge_date}}</td>
                    @if($receiptsToBank->bank_request)
                        <td><i class="fa fa-check"></i></td>
                    @else
                        <td><i class="fa fa-close"></i></td>
                    @endif
                    <td>{{$receiptsToBank->payment_date}}</td>
                </tr>
            </table>
            <hr>
            <h4>@lang('dashboard.titles.communities'):</h4>
            <table class="table table-hover" style="text-align: center">
                <tr class="info">
                    <th style="text-align: center">@lang('dashboard.tables.name')</th>
                    <th style="text-align: center">@lang('dashboard.tables.address')</th>
                    <th style="text-align: center">@lang('dashboard.tables.nif')</th>
                </tr>
                @foreach($receiptsToBank->communities as $community)
                    <tr>
                        <td>{{$community->name}}</td>
                        <td>{{$community->address}}</td>
                        <td>{{$community->nif}}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection
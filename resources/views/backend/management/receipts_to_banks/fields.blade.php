@inject('communities', 'Edifalia\Services\Communities')

<div class="form-group">
    {!! Form::label(trans('dashboard.tables.file_format')) !!}
    <div class="checkbox">
        <label>
            {!! Form::checkbox('file_format', 1) !!}
            @lang('dashboard.file_format.1')
        </label>
    </div>
    <div class="checkbox">
        <label>
            {!! Form::checkbox('file_format', 2) !!}
            @lang('dashboard.file_format.2')
        </label>
    </div>
    <div class="checkbox">
        <label>
            {!! Form::checkbox('file_format', 3) !!}
            @lang('dashboard.file_format.2')
        </label>
    </div>
</div>

<div class="form-group">
    {!! Form::label(trans('dashboard.fields.receipts_issued_date')) !!}
</div>

<div class="row">
    <div class="form-group col-md-5{{ $errors->has('state') ? ' has-error' : '' }}">
        {!! Form::select('state', [
                '' => trans('dashboard.fields.select_state'),
                '1' => trans('dashboard.state.previous'),
                '2' => trans('dashboard.state.later')],
            null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-md-1">
        {!! Form::label(trans('dashboard.tables.to')) !!}
    </div>

    <div class="form-group col-md-6{{ $errors->has('date_sent_to_bank') ? ' has-error' : '' }}">
        <div class="input-group date">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
            {!! Form::text('date_sent_to_bank', null, [
                'class' => 'form-control pull-right',
                'placeholder' => trans('dashboard.tables.date_sent_to_bank'),
                'id' => 'datepicker',
                'required' => true])
            !!}
        </div>
    </div>
</div>

<div class="form-group {{ $errors->has('charge_date') ? ' has-error' : '' }}">
    {!! Form::label(trans('dashboard.fields.charge_date_owner')) !!}
    <div class="input-group date">
        <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
        </div>
        {!! Form::text('charge_date', null, [
            'class' => 'form-control pull-right',
            'id' => 'datepicker2',
            'placeholder' => trans('dashboard.tables.charge_date'),
            'required' => true])
        !!}
    </div>
</div>

<div class="form-group">
    <div class="checkbox">
        <label>
            {!! Form::checkbox('bank_request', 1) !!}
            @lang('dashboard.fields.remittance_pay')
        </label>
    </div>
</div>
<hr>

<div class="form-group {{ $errors->has('payment_date') ? ' has-error' : '' }}">
    {!! Form::label(trans('dashboard.fields.payment_remittance_daily')) !!}
    <div class="input-group date">
        <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
        </div>
        {!! Form::text('payment_date', null, [
            'class' => 'form-control pull-right',
            'id' => 'datepicker3',
            'placeholder' => trans('dashboard.fields.payment_date'),
            'required' => true])
        !!}
    </div>
</div>
<hr>

<div class="row">
    <div class="form-group col-md-7">
        {!! Form::label(trans('dashboard.fields.select_communities')) !!}.
        @lang('dashboard.fields.ctrl_key'):
        @can('admin')
            {!! Form::select('communities[]', $communities->all(), null, [
                'class' => 'form-control',
                'multiple' => true,
                'size' => 15])
            !!}
        @elsecan('employee')
            {!! Form::select('communities[]', $communities->ofEmployee(), null, [
                'class' => 'form-control',
                'multiple' => true,
                'size' => 15])
            !!}
         @endcan
    </div>
    <div class="form-group col-md-5">
        {!! Form::label(trans('dashboard.fields.presenters')) !!}.
        @lang('dashboard.fields.optional')
        <br>
        @lang('dashboard.fields.ctrl_key'):
        {!! Form::select('presenters[]', [], null, [
            'class' => 'form-control',
            'multiple' => true,
            'size' => 13])
        !!}
    </div>
</div>
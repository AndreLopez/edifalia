@extends('layouts.dashboard')

@section('breadcrumb_title')
    @lang('dashboard.mnu_receipts_to_bank')
@endsection

@section('breadcrumb_subtitle')
    {{$title}}
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_management')</a></li>
    <li class="active">@lang('dashboard.mnu_receipts_to_bank')</li>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('dashboard.mnu_receipts_to_bank')</h3>
            <div class="btn-group btn-group-sm pull-right">
                <a data-toggle="tooltip" class="btn btn-primary"
                   title="@lang('dashboard.buttons.back')"
                   href="{{route('receipts-banks.index')}}">
                    <i class="fa fa-reply"></i></a>
            </div>
        </div>
        {!! Form::open($header) !!}
            <div class="box-body">
                @include('backend.partials.messages')
                @include('backend.management.receipts_to_banks.fields')
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-info">
                    <i class="fa fa-save"></i>
                    @lang('dashboard.buttons.save')
                </button>
            </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('styles')
    {{-- bootstrap datepicker --}}
    {!! Html::style('static/plugins/datepicker/datepicker3.css') !!}
@endsection

@section('scripts')
    {{-- bootstrap datepicker --}}
    {!! Html::script('static/plugins/datepicker/bootstrap-datepicker.js') !!}
    <script>
        $(document).ready(function(){
            $("#datepicker").datepicker();
        });
        $(document).ready(function(){
            $("#datepicker2").datepicker();
        });
        $(document).ready(function(){
            $("#datepicker3").datepicker();
        });
    </script>
@endsection
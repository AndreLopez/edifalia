@extends('layouts.dashboard')

@section('breadcrumb_title')
    @lang('dashboard.mnu_receipts_to_bank')
@endsection

@section('breadcrumb_subtitle')
    @lang('dashboard.titles.receipts_to_banks_list')
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_management')</a></li>
    <li class="active">@lang('dashboard.mnu_receipts_to_bank')</li>
@endsection

@section('modals')
    @include('backend.partials.modals')
@endsection

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('dashboard.mnu_receipts_to_bank')</h3>
        </div>
        <div class="box-body table-responsive">
            <table class="table table-hover">
                @if($receiptsToBanks->count())
                    <tr>
                        <th>@lang('dashboard.tables.file_format')</th>
                        <th>@lang('dashboard.tables.date_sent_to_bank')</th>
                        <th>@lang('dashboard.tables.charge_date')</th>
                        <th>@lang('dashboard.tables.actions')</th>
                    </tr>
                @endif
                @forelse($receiptsToBanks as $receiptsToBank)
                    <tr data-id="{{$receiptsToBank->id}}"
                        data-rundo="{{route('receipts-banks.undelete', $receiptsToBank->id)}}">
                        <td>@lang('dashboard.file_format.'.$receiptsToBank->file_format)</td>
                        <td>{{$receiptsToBank->state_and_date}}</td>
                        <td>{{$receiptsToBank->charge_date}}</td>
                        <td>
                            <div class="btn-group btn-group-xs">
                                <a data-toggle="tooltip" class="btn btn-info"
                                   title="@lang('dashboard.buttons.info')"
                                   href="{{route('receipts-banks.show', $receiptsToBank->id)}}">
                                    <i class="fa fa-eye"></i></a>

                                <a data-toggle="tooltip" class="btn btn-info delete_record"
                                   title="@lang('dashboard.buttons.delete')"
                                   href="{{route('receipts-banks.destroy', $receiptsToBank->id)}}">
                                    <i class="fa fa-trash-o"></i></a>
                            </div>
                        </td>
                    </tr>
                @empty
                    <div class="alert alert-warning">
                        <h4><i class="icon fa fa-warning"></i> @lang('dashboard.messages.notice')</h4>
                        @lang('dashboard.messages.no_stored_records')
                    </div>
                @endforelse
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            {{$receiptsToBanks->links()}}
            <a data-toggle="tooltip" class="btn btn-primary pull-right"
               title="@lang('dashboard.buttons.new')"
               href="{{route('receipts-banks.create')}}">
                <i class="fa fa-plus"></i> @lang('dashboard.buttons.new')</a>
        </div>
        <!-- /.box-footer-->
    </div>
@endsection

@section('scripts')
    {{-- Backend scripts  --}}
    {!! Html::script('js/code01.js') !!}
@endsection
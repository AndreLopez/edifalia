@extends('layouts.dashboard')

@section('breadcrumb_title')
    @lang('dashboard.mnu_management')
@endsection

@section('breadcrumb_subtitle')
    @lang('dashboard.titles.doc_info')
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_management')</a></li>
    <li class="active">@lang('dashboard.mnu_post_docs')</li>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('dashboard.mnu_management')</h3>
            <div class="btn-group btn-group-sm pull-right">
                <a data-toggle="tooltip" class="btn btn-primary"
                   title="@lang('dashboard.buttons.back')"
                   href="{{route('documents.index')}}">
                    <i class="fa fa-reply"></i></a>
            </div>
        </div>
        <div class="box-body table-responsive">
            <h4>@lang('dashboard.titles.doc_info'):</h4>
            <table class="table table-hover">
                <tr class="info">
                    <th>@lang('dashboard.tables.title')</th>
                    <th>@lang('dashboard.tables.document')</th>
                    <th>@lang('dashboard.tables.registration_date')</th>
                    <th>@lang('dashboard.tables.publication_date')</th>

                </tr>
                <tr>
                    <td>{{$document->title}}</td>
                    <td>
                        @if($document->existsFile())
                            <a href="{{asset('docs/'.$document->document)}}" target="_blank">
                                {{$document->document}}
                            </a>
                        @else
                            <p class="text-danger">
                                <i class="fa fa-warning"></i>
                                @lang('dashboard.messages.no_doc')
                            </p>
                        @endif
                    </td>
                    <td>{{$document->created_at->format('d/m/Y')}}</td>
                    <td>{{$document->publication_date}}</td>
                </tr>
            </table><hr>
            <table class="table table-hover">
                <tr class="info">
                    <th>@lang('dashboard.tables.community')</th>
                    <th>@lang('dashboard.tables.page')</th>
                    <th>@lang('dashboard.tables.tag')</th>
                    <th>@lang('dashboard.tables.permissions')</th>
                </tr>
                <tr>
                    <td>{{$document->community->name}}</td>
                    <td>{{$document->page->name}}</td>
                    <td>{{$document->tag->name}}</td>
                    <td>
                        @forelse($document->persons as $person)
                            {{$person->first_name}}@if(!$loop->last),@endif
                        @empty
                            @lang('dashboard.messages.doc_public')
                        @endforelse
                    </td>
                </tr>
            </table><hr>
        </div>
    </div>
@endsection
<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('document') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.document')) !!}
        {!! Form::file('document') !!}
        @if(isset($document))
            {!! Form::label(trans('dashboard.fields.current_doc')) !!}
            @if($document->existsFile())
                <a href="{{asset('docs/'.$document->document)}}" target="_blank">
                    {{$document->document}}
                </a>
            @else
                <p class="text-danger">
                    <i class="fa fa-warning"></i>
                    @lang('dashboard.messages.no_doc')
                </p>
            @endif
        @endif
    </div>
    <div class="form-group col-md-6 {{ $errors->has('community_id') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.community')) !!}
        {!! Form::select('community_id', Auth::user()->can('admin') ? $communities->all() : $communities->ofEmployee(), null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('title') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.title')) !!}
        {!! Form::text('title', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.title'),
            'required' => true])
        !!}
    </div>
    <div class="form-group col-md-6 {{ $errors->has('publication_date') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.publication_date')) !!}
        <div class="input-group date">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
            {!! Form::text('publication_date', null, [
                'class' => 'form-control pull-right',
                'placeholder' => trans('dashboard.fields.publication_date'),
                'id' => 'datepicker',
                'required' => true])
            !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group col-md-4 {{ $errors->has('page_id') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.page')) !!}
        {!! Form::select('page_id', $pages->get(),null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4 {{ $errors->has('tag_id') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.tag')) !!}
        {!! Form::select('tag_id', $tags->get(),null, ['class' => 'form-control']) !!}
        {{ link_to_route('tags.create', $title = trans('dashboard.titles.add_new_tag')) }}
    </div>
    <div class="form-group col-md-4 {{ $errors->has('owners') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.permissions')) !!}
        @if(isset($new))
            {!! Form::select('owners[]', $owners->get(),null, [
                'class' => 'form-control select2',
                'multiple' => true,
                'data-placeholder' => trans('dashboard.fields.select_owner') ])
            !!}
        @elseif(isset($edit))
            <select name="owners[]" class="form-control select2" multiple="multiple" data-placeholder="@lang('dashboard.fields.select_owner')">
            @foreach($owners->get() as $key => $person)
                {{$key}}
                @if($document->personSaved($key))
                    <option selected value="{{$key}}">{{$person}}</option>
                @else
                    <option value="{{$key}}">{{$person}}</option>
                @endif
            @endforeach
            </select>
        @endif
        <p class="help-block">@lang('dashboard.fields.doc_public')</p>
    </div>
</div>
@extends('layouts.dashboard')

@section('breadcrumb_title')
    @lang('dashboard.mnu_management')
@endsection

@section('breadcrumb_subtitle')
    @lang('dashboard.titles.doc_list')
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_management')</a></li>
    <li class="active">@lang('dashboard.mnu_post_docs')</li>
@endsection

@section('modals')
    @include('backend.partials.modals')
@endsection

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('dashboard.mnu_post_docs')</h3>
        </div>
        <div class="box-body table-responsive">
            <table class="table table-hover">
                @if($documents->count())
                    <tr>
                        <th>@lang('dashboard.tables.registration_date')</th>
                        <th>@lang('dashboard.tables.publication_date')</th>
                        <th>@lang('dashboard.tables.title')</th>
                        <th>@lang('dashboard.tables.page')</th>
                        <th>@lang('dashboard.tables.tag')</th>
                        <th>@lang('dashboard.tables.actions')</th>
                    </tr>
                @endif
                @forelse($documents as $document)
                    <tr data-id="{{$document->id}}"
                        data-rundo="{{route('documents.undelete', $document->id)}}">
                        <td>{{$document->created_at->format('d/m/Y')}}</td>
                        <td>{{$document->publication_date}}</td>
                        <td>{{$document->title}}</td>
                        <td>{{$document->page->name}}</td>
                        <td>{{$document->tag->name}}</td>
                        <td>
                            <div class="btn-group btn-group-xs">
                                <a data-toggle="tooltip" class="btn btn-info"
                                   title="@lang('dashboard.buttons.info')"
                                   href="{{route('documents.show', $document->id)}}">
                                    <i class="fa fa-eye"></i></a>

                                <a data-toggle="tooltip" class="btn btn-info"
                                   title="@lang('dashboard.buttons.edit')"
                                   href="{{route('documents.edit', $document->id)}}">
                                    <i class="fa fa-pencil-square-o"></i></a>

                                <a data-toggle="tooltip" class="btn btn-info delete_record"
                                   title="@lang('dashboard.buttons.delete')"
                                   href="{{route('documents.destroy', $document->id)}}">
                                    <i class="fa fa-trash-o"></i></a>
                            </div>
                        </td>
                    </tr>
                @empty
                    <div class="alert alert-warning">
                        <h4><i class="icon fa fa-warning"></i> @lang('dashboard.messages.notice')</h4>
                        @lang('dashboard.messages.no_stored_records')
                    </div>
                @endforelse
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            {{$documents->links()}}
            <a data-toggle="tooltip" class="btn btn-primary pull-right"
               title="@lang('dashboard.buttons.new')"
               href="{{route('documents.create')}}">
                <i class="fa fa-plus"></i> @lang('dashboard.buttons.new')</a>
        </div>
        <!-- /.box-footer-->
    </div>
@endsection

@section('scripts')
    {{-- Backend scripts  --}}
    {!! Html::script('js/code01.js') !!}
@endsection
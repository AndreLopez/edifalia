@extends('layouts.dashboard')

@section('breadcrumb_title')
    @lang('dashboard.mnu_management')
@endsection

@section('breadcrumb_subtitle')
    {{$title}}
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_management')</a></li>
    <li class="active">@lang('dashboard.mnu_post_docs')</li>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('dashboard.mnu_management')</h3>
            <div class="btn-group btn-group-sm pull-right">
            <a data-toggle="tooltip" class="btn btn-primary"
               title="@lang('dashboard.buttons.back')"
               href="{{route('documents.index')}}">
                <i class="fa fa-reply"></i></a>
                @if(isset($edit))
                    <a data-toggle="tooltip" class="btn btn-primary"
                       title="@lang('dashboard.buttons.new')"
                       href="{{route('documents.create')}}">
                        <i class="fa fa-plus"></i></a>
                @endif
            </div>
        </div>

        @inject('pages', 'Edifalia\Services\Pages')
        @inject('tags', 'Edifalia\Services\Tags')
        @inject('owners', 'Edifalia\Services\Persons')
        @inject('communities', 'Edifalia\Services\Communities')

        @if(isset($new))
            {!! Form::open($header) !!}
        @elseif(isset($edit))
            {!! Form::model($document, $header) !!}
        @endif
            <div class="box-body">
                @if(!count($pages->get()) || !count($tags->get()))
                    <div class="alert alert-warning">
                        <h4><i class="icon fa fa-warning"></i> @lang('dashboard.messages.notice')</h4>
                        @lang('dashboard.messages.no_pages_tags')
                    </div>
                @endif

                @include('backend.partials.messages')
                @include('backend.management.documents.fields')
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-info">
                    <i class="fa fa-save"></i>
                    @if(isset($new)) @lang('dashboard.buttons.save') @elseif(isset($edit))
                        @lang('dashboard.buttons.update')
                    @endif
                </button>
            </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('styles')
    {{-- bootstrap datepicker --}}
    {!! Html::style('static/plugins/datepicker/datepicker3.css') !!}
    {{-- Select2 --}}
    {!! Html::style('static/plugins/select2/select2.min.css') !!}
@endsection

@section('scripts')
    {{-- bootstrap datepicker --}}
    {!! Html::script('static/plugins/datepicker/bootstrap-datepicker.js') !!}
    {{-- Select2 --}}
    {!! Html::script('static/plugins/select2/select2.full.min.js') !!}
    <script>
        $(document).ready(function(){
            $(".select2").select2();

            $("#datepicker").datepicker();

            /*$("#datepicker").datepicker({
                autoclose: true,
                minDate: new Date(2007, 1 - 1, 1),
                dateFormat: "yy-mm-dd"
            });*/
        });
    </script>
@endsection
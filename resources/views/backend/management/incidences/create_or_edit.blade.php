@extends('layouts.dashboard')

@section('breadcrumb_title')
    @lang('dashboard.mnu_incidences')
@endsection

@section('breadcrumb_subtitle')
    {{$title}}
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_management')</a></li>
    <li class="active">@lang('dashboard.mnu_incidences')</li>
@endsection

@section('modals')
    @include('backend.partials.modals')
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('dashboard.mnu_incidences')</h3>
            <div class="btn-group btn-group-sm pull-right">
                <a data-toggle="tooltip" class="btn btn-primary"
                   title="@lang('dashboard.buttons.back')"
                   href="{{route('incidences.index')}}">
                    <i class="fa fa-reply"></i></a>
                @if(isset($edit))
                    <a data-toggle="tooltip" class="btn btn-primary"
                       title="@lang('dashboard.buttons.new')"
                       href="{{route('incidences.create')}}">
                        <i class="fa fa-plus"></i></a>
                @endif
            </div>
        </div>
        @if(isset($new))
            {!! Form::open($header) !!}
        @elseif(isset($edit))
            {!! Form::model($incidence, $header) !!}
        @endif
        <div class="box-body">
            @include('backend.partials.messages')
            @include('backend.management.incidences.fields')
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-info">
                <i class="fa fa-save"></i>
                @if(isset($new)) @lang('dashboard.buttons.save') @elseif(isset($edit))
                    @lang('dashboard.buttons.update')
                @endif
            </button>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            function getCommunities(){
                var office_id = $('#office').val();
                if ($.trim(office_id) != '') {
                    $("#pleaseWait").modal("show");
                    var route = $('#office').data('route');
                    var incidence = $('#office').data('id');
                    $.get(route, {id: office_id, incidence_id:incidence}, function(data){
                        $('#communities').empty();
                        $('#communities').append("<option value=''>" + data.select + "</option>");
                        $.each(data.communities, function(index, value){
                            $('#communities').append("<option value='" + index + "'>" + value + "</option>");
                        });
                        $("#communities option[value='" + data.value + "']").attr("selected", true);
                        $("#pleaseWait").modal("hide");
                    });
                }
            }
            getCommunities();
            $('#office').on('change', getCommunities);

            function lockAndUnlock() {
                var communicator = $('#communicator').val();
                if ($.trim(communicator) != '') {
                    $("div#communicator-inputs input").attr('disabled', false);
                }
                else {
                    $("div#communicator-inputs input").val(null);
                    $("div#communicator-inputs input").attr('disabled', true);
                }
            }
            lockAndUnlock();
            $('#communicator').on('change', lockAndUnlock);

        });
    </script>
@endsection
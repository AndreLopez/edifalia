@inject('offices', 'Edifalia\Services\Offices')
@inject('receptions', 'Edifalia\Services\Receptions')
@inject('reasons', 'Edifalia\Services\Reasons')
@inject('types', 'Edifalia\Services\CommunicatorTypes')
@inject('suppliers', 'Edifalia\Services\Suppliers')
@inject('sectors', 'Edifalia\Services\Sectors')

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('office_id') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.office')) !!}
        {!! Form::select('office_id', $offices->get(), null,
            [
                'class' => 'form-control',
                'id' => 'office',
                'data-route' => route('incidences.communities'),
                'data-id' => (isset($incidence))?$incidence->id:''
            ])
        !!}
    </div>
    <div class="form-group col-md-6 {{ $errors->has('community_id') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.community')) !!}
        {!! Form::select('community_id', [], null,
            [
                'class' => 'form-control',
                'id' => 'communities'
            ])
        !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('priority') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.priority')) !!}
        {!! Form::select('priority', [
            '' => trans('dashboard.fields.select_priority'),
            '1' => trans('dashboard.fields.normal'),
            '2' => trans('dashboard.fields.medium'),
            '3' => trans('dashboard.fields.high')],
         null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-6 {{ $errors->has('reception_id') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.reception')) !!}
        {!! Form::select('reception_id', $receptions->get(), null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('reason_id') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.reason')) !!}
        {!! Form::select('reason_id', $reasons->get(), null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-6 {{ $errors->has('controls') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.controls')) !!}
        {!! Form::select('controls', [
            '' => trans('dashboard.fields.select_controls'),
            '1' => 'Juan',
            '2' => 'Pepe',
            '3' => 'Angulo'],
         null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('title') ? ' has-error' : '' }}">
        {!! Form::label(trans('dashboard.fields.title')) !!}
        {!! Form::text('title', null, [
            'class' => 'form-control',
            'placeholder' => trans('dashboard.fields.title'),
            'required' => true])
        !!}
    </div>
    <div class="form-group col-md-6">
        <div class="checkbox">
            <label>
                {!! Form::checkbox('publish', 1) !!}
                @lang('dashboard.fields.publish')
            </label>
        </div>
        <div class="checkbox">
            <label>
                {!! Form::checkbox('conclude', 1) !!}
                @lang('dashboard.fields.conclude')
            </label>
        </div>
    </div>
</div>

<div class="form-group {{ $errors->has('details') ? ' has-error' : '' }}">
    {!! Form::label(trans('dashboard.fields.incidence_details')) !!}:
    {!! Form::textarea('details', null, ['class' => 'form-control', 'rows' => 5])!!}
</div>

<div class="form-group {{ $errors->has('other_annotations') ? ' has-error' : '' }}">
    {!! Form::label(trans('dashboard.fields.other_annotations')) !!}:
    {!! Form::textarea('other_annotations', null, ['class' => 'form-control', 'rows' => 5])!!}
</div>

<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">@lang('dashboard.fields.supplier')</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="form-group col-md-6 {{ $errors->has('supplier_id') ? ' has-error' : '' }}">
                {!! Form::label(trans('dashboard.fields.supplier')) !!}
                {!! Form::select('supplier_id', $suppliers->get(), null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-md-6 {{ $errors->has('sector_id') ? ' has-error' : '' }}">
                {!! Form::label(trans('dashboard.fields.sector')) !!}
                {!! Form::select('sector_id', $sectors->get(), null, ['class' => 'form-control']) !!}
            </div>
        </div>

        {!! Form::label(trans('dashboard.fields.send')) !!}:
        <div class="row">
            <div class="form-group col-md-4">
                <div class="radio">
                    <label>
                        {!! Form::radio('send', 0) !!}
                        @lang('dashboard.fields.all')
                    </label>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div class="radio">
                    <label>
                        {!! Form::radio('send', 1) !!}
                        @lang('dashboard.fields.email')
                    </label>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div class="radio">
                    <label>
                        {!! Form::radio('send', 2) !!}
                        @lang('dashboard.fields.sms')
                    </label>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
</div>

<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">@lang('dashboard.fields.communicator')</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="form-group {{ $errors->has('type_id') ? ' has-error' : '' }}">
            {!! Form::label(trans('dashboard.fields.communicator_type')) !!}
            @if(isset($incidence) and !is_null($incidence->communicator))
                <select name="type_id" class="form-control" id="communicator">
                    @foreach($types->get() as $id => $type)
                        @if($incidence->communicator->type_id == $id)
                            <option selected value="{{$id}}">{{$type}}</option>
                        @else
                            <option value="{{$id}}">{{$type}}</option>
                        @endif
                    @endforeach
                </select>
            @else
                {!! Form::select('type_id', $types->get(), null,
                    [
                        'class' => 'form-control',
                        'id' => 'communicator'
                    ])
                !!}
            @endif
        </div>
        <div id="communicator-inputs">
            <div class="row">
                <div class="form-group col-md-6 {{ $errors->has('name') ? ' has-error' : '' }}">
                    {!! Form::label(trans('dashboard.fields.name')) !!}
                    {!! Form::text('name', (isset($incidence) and !is_null($incidence->communicator))?$incidence->communicator->name:null,
                    [
                        'class' => 'form-control',
                        'placeholder' => trans('dashboard.fields.name'),
                        'required' => true])
                    !!}
                </div>
                <div class="form-group col-md-6 {{ $errors->has('last_name') ? ' has-error' : '' }}">
                    {!! Form::label(trans('dashboard.fields.last_name')) !!}
                    {!! Form::text('last_name', (isset($incidence) and !is_null($incidence->communicator))?$incidence->communicator->last_name:null, [
                        'class' => 'form-control',
                        'placeholder' => trans('dashboard.fields.last_name')])
                    !!}
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6 {{ $errors->has('address') ? ' has-error' : '' }}">
                    {!! Form::label(trans('dashboard.fields.address')) !!}
                    {!! Form::text('address', (isset($incidence) and !is_null($incidence->communicator))?$incidence->communicator->address:null, [
                        'class' => 'form-control',
                        'placeholder' => trans('dashboard.fields.address')])
                    !!}
                </div>
                <div class="form-group col-md-6 {{ $errors->has('email') ? ' has-error' : '' }}">
                    {!! Form::label(trans('dashboard.fields.email')) !!}
                    {!! Form::email('email', (isset($incidence) and !is_null($incidence->communicator))?$incidence->communicator->email:null, [
                        'class' => 'form-control',
                        'placeholder' => trans('dashboard.fields.email')])
                    !!}
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6 {{ $errors->has('phone') ? ' has-error' : '' }}">
                    {!! Form::label(trans('dashboard.fields.phone')) !!}
                    {!! Form::text('phone', (isset($incidence) and !is_null($incidence->communicator))?$incidence->communicator->phone:null, [
                        'class' => 'form-control',
                        'placeholder' => trans('dashboard.fields.phone')])
                    !!}
                </div>
                <div class="form-group col-md-6 {{ $errors->has('other_phone') ? ' has-error' : '' }}">
                    {!! Form::label(trans('dashboard.fields.other_phone')) !!}
                    {!! Form::text('other_phone', (isset($incidence) and !is_null($incidence->communicator))?$incidence->communicator->other_phone:null, [
                        'class' => 'form-control',
                        'placeholder' => trans('dashboard.fields.other_phone')])
                    !!}
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
</div>

<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">
            @if(isset($incidence) and $incidence->files()->exists())
                @lang('dashboard.fields.attached_files')
            @else
                @lang('dashboard.fields.attach_files')
            @endif
        </h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        @if(isset($incidence) and $incidence->files()->exists())
            <table class="table table-hover">
                <tr>
                    <th>@lang('dashboard.tables.title')</th>
                    <th>@lang('dashboard.tables.state')</th>
                </tr>
                @foreach($incidence->files as $file)
                    <tr>
                        <td>
                            @if($file->existsFile())
                                <a href="{{asset('incidence_files/'.$file->file)}}" target="_blank">
                                    {{$file->title}}
                                </a>
                            @else
                                <p class="text-danger">
                                    <i class="fa fa-warning"></i>
                                    @lang('dashboard.messages.no_file')
                                </p>
                            @endif
                        </td>
                        <td>
                            @if($file->publish)
                                @lang('dashboard.titles.published')
                            @else
                                @lang('dashboard.titles.unpublished')
                            @endif
                        </td>
                    </tr>
                @endforeach
            </table>
        @else
            <div class="row">
                <div class="form-group col-md-4 {{ $errors->has('file1') ? ' has-error' : '' }}">
                    {!! Form::label(trans('dashboard.fields.file1')) !!}
                    {!! Form::file('file1') !!}
                </div>
                <div class="form-group col-md-4 {{ $errors->has('file_title1') ? ' has-error' : '' }}">
                    {!! Form::label(trans('dashboard.fields.file_title1')) !!}
                    {!! Form::text('file_title1', null, [
                        'class' => 'form-control',
                        'placeholder' => trans('dashboard.fields.file_title1')])
                    !!}
                </div>
                <div class="form-group col-md-4 {{ $errors->has('publish_file1') ? ' has-error' : '' }}">
                    <div class="checkbox">
                        <label>
                            {!! Form::checkbox('publish_file1', 1) !!}
                            @lang('dashboard.fields.publish_file1')
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4 {{ $errors->has('file2') ? ' has-error' : '' }}">
                    {!! Form::label(trans('dashboard.fields.file2')) !!}
                    {!! Form::file('file2') !!}
                </div>
                <div class="form-group col-md-4 {{ $errors->has('file_title2') ? ' has-error' : '' }}">
                    {!! Form::label(trans('dashboard.fields.file_title2')) !!}
                    {!! Form::text('file_title2', null, [
                        'class' => 'form-control',
                        'placeholder' => trans('dashboard.fields.file_title2')])
                    !!}
                </div>
                <div class="form-group col-md-4 {{ $errors->has('publish_file2') ? ' has-error' : '' }}">
                    <div class="checkbox">
                        <label>
                            {!! Form::checkbox('publish_file2', 1) !!}
                            @lang('dashboard.fields.publish_file2')
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4 {{ $errors->has('file3') ? ' has-error' : '' }}">
                    {!! Form::label(trans('dashboard.fields.file3')) !!}
                    {!! Form::file('file3') !!}
                </div>
                <div class="form-group col-md-4 {{ $errors->has('file_title3') ? ' has-error' : '' }}">
                    {!! Form::label(trans('dashboard.fields.file_title3')) !!}
                    {!! Form::text('file_title3', null, [
                        'class' => 'form-control',
                        'placeholder' => trans('dashboard.fields.file_title3')])
                    !!}
                </div>
                <div class="form-group col-md-4 {{ $errors->has('publish_file3') ? ' has-error' : '' }}">
                    <div class="checkbox">
                        <label>
                            {!! Form::checkbox('publish_file3', 1) !!}
                            @lang('dashboard.fields.publish_file3')
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4 {{ $errors->has('file4') ? ' has-error' : '' }}">
                    {!! Form::label(trans('dashboard.fields.file4')) !!}
                    {!! Form::file('file4') !!}
                </div>
                <div class="form-group col-md-4 {{ $errors->has('file_title4') ? ' has-error' : '' }}">
                    {!! Form::label(trans('dashboard.fields.file_title4')) !!}
                    {!! Form::text('file_title4', null, [
                        'class' => 'form-control',
                        'placeholder' => trans('dashboard.fields.file_title4')])
                    !!}
                </div>
                <div class="form-group col-md-4 {{ $errors->has('publish_file4') ? ' has-error' : '' }}">
                    <div class="checkbox">
                        <label>
                            {!! Form::checkbox('publish_file4', 1) !!}
                            @lang('dashboard.fields.publish_file4')
                        </label>
                    </div>
                </div>
            </div>
        @endif
    </div>
    <!-- /.box-body -->
</div>
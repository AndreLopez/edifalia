@extends('layouts.dashboard')

@section('breadcrumb_title')
    @lang('dashboard.mnu_management')
@endsection

@section('breadcrumb_subtitle')
    @lang('dashboard.titles.incidences_list')
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_management')</a></li>
    <li class="active">@lang('dashboard.mnu_incidences')</li>
@endsection

@section('modals')
    @include('backend.partials.modals')
@endsection

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('dashboard.mnu_incidences')</h3>
        </div>
        <div class="box-body table-responsive">
            <table class="table table-hover">
                @if($incidences->count())
                    <tr>
                        <th>@lang('dashboard.tables.code')</th>
                        <th>@lang('dashboard.tables.title')</th>
                        <th>@lang('dashboard.tables.office')</th>
                        <th>@lang('dashboard.tables.community')</th>
                        <th>@lang('dashboard.tables.priority')</th>
                        <th>@lang('dashboard.tables.reason')</th>
                        <th>@lang('dashboard.tables.actions')</th>
                    </tr>
                @endif
                @forelse($incidences as $incidence)
                    <tr data-id="{{$incidence->id}}"
                        data-rundo="{{route('incidences.undelete', $incidence->id)}}">
                        <td>{{$incidence->id}}</td>
                        <td>{{$incidence->title}}</td>
                        <td>{{$incidence->office->name}}</td>
                        <td>{{$incidence->community->name}}</td>
                        <td>
                            @if($incidence->priority == 1)
                                <p class="text-success">
                                    @lang('dashboard.fields.normal')
                                </p>
                            @elseif($incidence->priority == 2)
                                <p class="text-warning">
                                    @lang('dashboard.fields.medium')
                                </p>
                            @elseif($incidence->priority == 3)
                                <p class="text-danger">
                                    @lang('dashboard.fields.high')
                                </p>
                            @endif
                        </td>
                        <td>{{$incidence->reason->name}}</td>
                        <td>
                            <div class="btn-group btn-group-xs">
                                <a data-toggle="tooltip" class="btn btn-info"
                                   title="@lang('dashboard.buttons.info')"
                                   href="{{route('incidences.show', $incidence->id)}}">
                                    <i class="fa fa-eye"></i></a>
                                <a data-toggle="tooltip" class="btn btn-info"
                                   title="@lang('dashboard.buttons.edit')"
                                   href="{{route('incidences.edit', $incidence->id)}}">
                                    <i class="fa fa-pencil-square-o"></i></a>

                                <a data-toggle="tooltip" class="btn btn-info delete_record"
                                   title="@lang('dashboard.buttons.delete')"
                                   href="{{route('incidences.destroy', $incidence->id)}}">
                                    <i class="fa fa-trash-o"></i></a>
                            </div>
                        </td>
                    </tr>
                @empty
                    <div class="alert alert-warning">
                        <h4><i class="icon fa fa-warning"></i> @lang('dashboard.messages.notice')</h4>
                        @lang('dashboard.messages.no_stored_records')
                    </div>
                @endforelse
            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            {{$incidences->links()}}
            <a data-toggle="tooltip" class="btn btn-primary pull-right"
               title="@lang('dashboard.buttons.new')"
               href="{{route('incidences.create')}}">
                <i class="fa fa-plus"></i> @lang('dashboard.buttons.new')</a>
        </div>
        <!-- /.box-footer-->
    </div>
@endsection

@section('scripts')
    {{-- Backend scripts  --}}
    {!! Html::script('js/code01.js') !!}
@endsection
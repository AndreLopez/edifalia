@extends('layouts.dashboard')

@section('breadcrumb_title')
    @lang('dashboard.mnu_management')
@endsection

@section('breadcrumb_subtitle')
    @lang('dashboard.titles.incidence_info')
@endsection

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_management')</a></li>
    <li class="active">@lang('dashboard.mnu_incidences')</li>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">@lang('dashboard.mnu_management')</h3>
            <div class="btn-group btn-group-sm pull-right">
                <a data-toggle="tooltip" class="btn btn-primary"
                   title="@lang('dashboard.buttons.back')"
                   href="{{route('incidences.index')}}">
                    <i class="fa fa-reply"></i></a>
            </div>
        </div>
        <div class="box-body">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        @lang('dashboard.titles.incidence_info') -
                        @lang('dashboard.tables.code'): {{$incidence->id}}
                    </h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body table-responsive">
                    <table class="table table-hover">
                        <tr class="info">
                            <th>@lang('dashboard.tables.publication_date')</th>
                            <th>@lang('dashboard.tables.title')</th>
                            <th>@lang('dashboard.tables.office')</th>
                            <th>@lang('dashboard.tables.community')</th>
                            <th>@lang('dashboard.tables.priority')</th>
                            <th>@lang('dashboard.tables.reception')</th>
                            <th>@lang('dashboard.tables.reason')</th>
                        </tr>
                        <tr>
                            <td>{{$incidence->created_at->format('d/m/Y')}}</td>
                            <td>{{$incidence->title}}</td>
                            <td>{{$incidence->office->name}}</td>
                            <td>{{$incidence->community->name}}</td>
                            <td>
                                @if($incidence->priority == 1)
                                    <p class="text-success">
                                        @lang('dashboard.fields.normal')
                                    </p>
                                @elseif($incidence->priority == 2)
                                    <p class="text-warning">
                                        @lang('dashboard.fields.medium')
                                    </p>
                                @elseif($incidence->priority == 3)
                                    <p class="text-danger">
                                        @lang('dashboard.fields.high')
                                    </p>
                                @endif
                            </td>
                            <td>{{$incidence->reception->name}}</td>
                            <td>{{$incidence->reason->name}}</td>
                        </tr>
                    </table><hr>
                    <table class="table table-hover">
                        <tr class="info">
                            <th>@lang('dashboard.tables.public')</th>
                            <th>@lang('dashboard.tables.concluded')</th>
                            <th>@lang('dashboard.tables.details')</th>
                            <th>@lang('dashboard.tables.other_annotations')</th>
                        </tr>
                        <tr>
                            <td>
                                @if($incidence->publish)
                                    @lang('dashboard.titles.published')
                                @else
                                    @lang('dashboard.titles.unpublished')
                                @endif
                            </td>
                            <td>
                                @if($incidence->conclude)
                                    @lang('dashboard.titles.concluded')
                                @else
                                    @lang('dashboard.titles.unconcluded')
                                @endif
                            </td>
                            <td>{{$incidence->details}}</td>
                            <td>{{$incidence->other_annotations}}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        @lang('dashboard.titles.supplier')
                    </h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body table-responsive">
                    <table class="table table-hover">
                        <tr class="info">
                            <th>@lang('dashboard.tables.supplier')</th>
                            <th>@lang('dashboard.tables.sector')</th>
                            <th>@lang('dashboard.tables.sending')</th>
                        </tr>
                        <tr>
                            <td>{{$incidence->supplier->tradename}}</td>
                            <td>{{$incidence->sector->name}}</td>
                            <td>
                                @if($incidence->send == 0)
                                        @lang('dashboard.fields.all')
                                @elseif($incidence->send == 1)
                                        @lang('dashboard.fields.email')
                                @elseif($incidence->send == 2)
                                    @lang('dashboard.fields.sms')
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        @lang('dashboard.titles.communicator')
                    </h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body table-responsive">
                    @if(!is_null($incidence->communicator))
                        <table class="table table-hover">
                            <tr class="info">
                                <th>@lang('dashboard.tables.type')</th>
                                <th>@lang('dashboard.tables.name')</th>
                                <th>@lang('dashboard.tables.last_name')</th>
                                <th>@lang('dashboard.tables.address')</th>
                                <th>@lang('dashboard.tables.email')</th>
                                <th>@lang('dashboard.tables.phone')</th>
                                <th>@lang('dashboard.tables.other_phone')</th>
                            </tr>
                            <tr>
                                <td>{{$incidence->communicator->type->name}}</td>
                                <td>{{$incidence->communicator->name}}</td>
                                <td>{{$incidence->communicator->last_name}}</td>
                                <td>{{$incidence->communicator->address}}</td>
                                <td>{{$incidence->communicator->email}}</td>
                                <td>{{$incidence->communicator->phone}}</td>
                                <td>{{$incidence->communicator->other_phone}}</td>
                            </tr>
                        </table>
                    @else
                        <p class="text-danger">
                            <i class="fa fa-warning"></i>
                            @lang('dashboard.messages.no_communicator')
                        </p>
                    @endif
                </div>
            </div>

            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        @lang('dashboard.fields.attached_files')
                    </h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body table-responsive">
                    @if($incidence->files()->count())
                        <table class="table table-hover">
                            <tr class="info">
                                <th>@lang('dashboard.tables.title')</th>
                                <th>@lang('dashboard.tables.state')</th>
                            </tr>
                            @foreach($incidence->files as $file)
                                <tr>
                                    <td>
                                        @if($file->existsFile())
                                            <a href="{{asset('incidence_files/'.$file->file)}}" target="_blank">
                                                {{$file->title}}
                                            </a>
                                        @else
                                            <p class="text-danger">
                                                <i class="fa fa-warning"></i>
                                                @lang('dashboard.messages.no_file')
                                            </p>
                                        @endif
                                    </td>
                                    <td>
                                        @if($file->publish)
                                            @lang('dashboard.titles.published')
                                        @else
                                            @lang('dashboard.titles.unpublished')
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    @else
                        <p class="text-danger">
                            <i class="fa fa-warning"></i>
                            @lang('dashboard.messages.no_attachments')
                        </p>
                    @endif
                </div>
            </div>

        </div>
    </div>
@endsection
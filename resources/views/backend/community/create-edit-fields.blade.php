<div class="row">
	<div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('code') ? ' has-error' : '' }}">
		<label class="col-sm-3 control-label" for="text">@lang('forms.code_label')</label>
		<div class="col-sm-5">
			<div class="input-group">
		    	{!! Form::text(
		    		'code', (isset($model))?$model->code:null, [
		    			'class' => 'form-control', 'id' => 'code', 
		    			'placeholder' => trans('forms.code_placeholder'), 'maxlength' => '4'
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('code'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('code') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6 form-group{{ $errors->has('status_id') ? ' has-error' : '' }}">
		<label class="col-sm-3 control-label" for="status_id">@lang('forms.status_label')</label>
	    <div class="col-sm-9">
	    	<div class="input-group">
	    		{!! Form::select(
		    		'status_id', $status, null, [
		    			'id' => 'status_id', 'class' => 'select2', 
		    			'placeholder' => trans('forms.select_null_option'),
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
	    </div>
		@if ($errors->has('status_id'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('status_id') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>

<div class="row">
	<div class="col-sm-6 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
		<label for="name" class="col-sm-3 control-label">@lang('forms.name_label')</label>
		<div class="col-sm-9">
			<div class="input-group">
		    	{!! Form::text(
		    		'name', (isset($model))?$model->name:null, [
		    			'class' => 'form-control', 'id' => 'name', 
		    			'placeholder' => trans('forms.name_placeholder'), 'maxlength' => '75'
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('name'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('name') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6 form-group{{ $errors->has('nif') ? ' has-error' : '' }}">
		<label for="nif" class="col-sm-3 control-label">@lang('forms.nif_label')</label>
		<div class="col-sm-9">
			<div class="input-group">
		    	{!! Form::text(
		    		'nif', (isset($model))?$model->nif:null, [
		    			'class' => 'form-control', 'id' => 'nif', 'maxlength' => '15',
		    			'placeholder' => trans('forms.nif_placeholder')
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('nif'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('nif') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>

<div class="row">
	<div class="col-sm-6 form-group{{ $errors->has('country') ? ' has-error' : '' }}">
		<label for="country" class="col-sm-3 control-label">@lang('forms.country_label')</label>
		<div class="col-sm-9">
			<div class="input-group">
	    		{!! Form::select(
		    		'country', $countries, null, [
		    			'id' => 'country', 'class' => 'select2', 
		    			'placeholder' => trans('forms.select_null_option'),
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('country'))
			<div class="col-sm-9 col-sm-offset-3	">
		        <span class="help-block">
		            <strong>{{ $errors->first('country') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6 form-group{{ $errors->has('postal_code_id') ? ' has-error' : '' }}">
		<label for="postal_code_id" class="col-sm-3 control-label">@lang('forms.postal_code_label')</label>
		<div class="col-sm-9">
			<div class="input-group">
	    		{!! Form::select(
		    		'postal_code_id', $postal_codes, null, [
		    			'id' => 'postal_code_id', 'class' => 'select2', 
		    			'placeholder' => trans('forms.select_null_option'),
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('postal_code_id'))
			<div class="col-sm-9 col-sm-offset-3	">
		        <span class="help-block">
		            <strong>{{ $errors->first('postal_code_id') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>

<div class="row">
	<div class="col-sm-6 form-group{{ $errors->has('town') ? ' has-error' : '' }}">
		<label for="town" class="col-sm-3 control-label">@lang('forms.town_label')</label>
		<div class="col-sm-9">
			{!! Form::text(
	    		'town', null, [
	    			'class' => 'form-control', 'id' => 'town', 'readonly' => 'readonly',
	    			'placeholder' => trans('forms.town_placeholder')
	    		]
	    	) !!}
		</div>
		@if ($errors->has('town'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('town') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6 form-group{{ $errors->has('province') ? ' has-error' : '' }}">
		<label for="province" class="col-sm-3 control-label">@lang('forms.province_label')</label>
		<div class="col-sm-9">
			{!! Form::text(
	    		'province', null, [
	    			'class' => 'form-control', 'id' => 'province', 'readonly' => 'readonly',
	    			'placeholder' => trans('forms.province_placeholder')
	    		]
	    	) !!}
		</div>
		@if ($errors->has('province'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('province') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>

<div class="row">
	<div class="col-sm-6 form-group{{ $errors->has('region') ? ' has-error' : '' }}">
		<label for="region" class="col-sm-3 control-label">@lang('forms.region_label')</label>
		<div class="col-sm-9">
			{!! Form::text(
	    		'region', null, [
	    			'class' => 'form-control', 'id' => 'region', 'readonly' => 'readonly',
	    			'placeholder' => trans('forms.region_placeholder')
	    		]
	    	) !!}
		</div>
		@if ($errors->has('region'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('region') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6 form-group{{ $errors->has('platform_accounting') ? ' has-error' : '' }}">
		<label for="platform_accounting" class="col-sm-3 control-label">
			@lang('forms.platform_accounting_label')
		</label>
		<div class="col-sm-9">
			{!! Form::checkbox('platform_accounting', True, False, [
				'data-size' => 'mini', 'class' => 'switch'
			]) !!}
		</div>
		@if ($errors->has('platform_accounting'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('platform_accounting') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>

<div class="row">
	<div class="col-sm-6 form-group{{ $errors->has('structure_year') ? ' has-error' : '' }}">
		<label for="structure_year" class="col-sm-3 control-label">
			@lang('forms.structure_year_label')
		</label>
		<div class="col-sm-9">
			<div class="input-group">
		    	{!! Form::text(
		    		'structure_year', (isset($model))?$model->structure_year:null, [
		    			'class' => 'form-control', 'id' => 'structure_year', 'maxlength' => '4',
		    			'placeholder' => trans('forms.structure_year_placeholder')
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('structure_year'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('structure_year') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6 form-group{{ $errors->has('elevator_numbers') ? ' has-error' : '' }}">
		<label for="elevator_numbers" class="col-sm-3 control-label">
			@lang('forms.elevator_number_label')
		</label>
		<div class="col-sm-2">
			{!! Form::number(
	    		'elevator_numbers', (isset($model))?$model->elevator_numbers:'0', [
	    			'class' => 'form-control', 'id' => 'elevator_numbers',
	    			'placeholder' => trans('forms.elevator_number_placeholder'),
	    		]
	    	) !!}
		</div>
		@if ($errors->has('elevator_numbers'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('elevator_numbers') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>

<div class="row">
	<!-- contenedor para el mapa interactivo -->
	<div class="col-sm-12">
		<div id="map-container"></div>
	</div>
</div>

<div class="row">
	<div class="col-sm-6 form-group{{ $errors->has('longitude') ? ' has-error' : '' }}">
		<label for="longitude" class="col-sm-3 control-label">@lang('forms.longitude_label')</label>
		<div class="col-sm-5">
			{!! Form::text(
	    		'longitude', (isset($model))?$model->longitude:'', [
	    			'class' => 'form-control', 'id' => 'longitude', 'readonly' => 'readonly',
	    			'placeholder' => trans('forms.longitude_placeholder')
	    		]
	    	) !!}
		</div>
		@if ($errors->has('longitude'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('longitude') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6 form-group{{ $errors->has('latitude') ? ' has-error' : '' }}">
		<label for="latitude" class="col-sm-3 control-label">@lang('forms.latitude_label')</label>
		<div class="col-sm-5">
			{!! Form::text(
	    		'latitude', (isset($model))?$model->latitude:'', [
	    			'class' => 'form-control', 'id' => 'latitude', 'readonly' => 'readonly',
	    			'placeholder' => trans('forms.latitude_placeholder')
	    		]
	    	) !!}
		</div>
		@if ($errors->has('latitude'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('latitude') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>

<div class="row">
	<div class="col-sm-6 form-group{{ $errors->has('address') ? ' has-error' : '' }}">
		<label for="address" class="col-sm-3 control-label">@lang('forms.address_label')</label>
		<div class="col-sm-9">
			{!! Form::textarea('address', null, [
				'class' => 'form-control', 'id' => 'address', 'rows' => '3',
				'placeholder' => trans('forms.address_placeholder')
			]) !!}
		</div>
		@if ($errors->has('address'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('address') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6 form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
		<label for="notes" class="col-sm-3 control-label">@lang('forms.note_label')</label>
		<div class="col-sm-9">
			{!! Form::textarea('notes', null, [
				'class' => 'form-control', 'id' => 'notes', 'rows' => '3',
				'placeholder' => trans('forms.note_placeholder')
			]) !!}
		</div>
		@if ($errors->has('notes'))
			<div class="col-sm-9 col-sm-offset-2">
		        <span class="help-block">
		            <strong>{{ $errors->first('notes') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>

<div class="row">
	<div class="col-sm-6 form-group{{ $errors->has('president_name') ? ' has-error' : '' }}">
		<label for="president_name" class="col-sm-3 control-label">
			@lang('forms.president_name_label')
		</label>
		<div class="col-sm-9">
			<div class="input-group">
		    	{!! Form::text(
		    		'president_name', (isset($model))?$model->president_name:null, [
		    			'class' => 'form-control', 'id' => 'president_name', 'readonly' => 'readonly',
		    			'placeholder' => trans('forms.president_name_placeholder')
		    		]
		    	) !!}
		    	{!! Form::hidden('president_id') !!}
		    	<span class="input-group-addon" role="button"><i class="fa fa-search"></i></span>
		    </div>
		</div>
		@if ($errors->has('president_name'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('president_name') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>
@include('common.bank-account-fields')
@include('common.accounting-account-fields')

{{-- Script para la sección del mapa --}}
<script>
	var osm = new ol.layer.Tile({
		title: 'OSM',
		type: 'base',
		source: new ol.source.OSM()
	});
	var initCoord = [
		{{ (isset($model) && $model->longitude)?$model->longitude:-3.7046 }},
		{{ (isset($model) && $model->latitude)?$model->latitude:40.4164 }}
	];

	// Marcado inicial Longitud, Latitud
	var iconGeometry = new ol.geom.Point(initCoord).transform('EPSG:4326', 'EPSG:3857');

	var pointFeature = new ol.Feature({
		geometry: iconGeometry,
		name: 'Mark'
	});

	var iconStyle = new ol.style.Style({
		image: new ol.style.Icon(({
			anchor: [0.5, 15], // Posicion del icono en el eje X Y
			anchorXUnits: 'fraction', // Unidad de medida para el posicionamiento del icon en el eje X
			anchorYUnits: 'pixels', // Unidad de medida para el posicionamiento del icon en el eje X
			src: '{{ url('/static/images/mark.png') }}',
			scale: 1.1 // Tamaño de la imagen de acuerdo a la escala en base al tamaño original
		}))
	});

	pointFeature.setStyle(iconStyle);

	var vectorLayer = new ol.layer.Vector({
		source: new ol.source.Vector({
			features: [pointFeature]
		})
	});

	var mousePosition = new ol.control.MousePosition({
		coordinateFormat: ol.coordinate.createStringXY(4),
		projection: 'EPSG:4326',
		undefinedHTML: '&nbsp;'
	});

	var map = new ol.Map({
		target: 'map-container',
		layers: [osm, vectorLayer],
		view: new ol.View({
			center: ol.proj.transform(initCoord, 'EPSG:4326', 'EPSG:3857'), // Centra el mapa en la Longitud y Latitud dada
			zoom: 6
		})
	});

	map.on('singleclick', function(evt) {
		var LonLat = $('.ol-mouse-position').html().split(", ");
		$("#latitude").val(LonLat[1]);
		$("#longitude").val(LonLat[0]);
		// Modifica la posición actual del marcador al hacer click sobre un punto en el mapa
		iconGeometry.setCoordinates(evt.coordinate);
	});

	map.addControl(mousePosition);

	map.on('loadstart', function() {
		cargando.showPleaseWait();
	});

	map.on('loadend', function() {
		cargando.hidePleaseWait();
	});

	map.updateSize();
</script>

@section('extra_scripts')
	@parent
	<script>
		/*
         * Checkbox init
         */
        var chk = $(".switch");
        chk.bootstrapSwitch({
            onColor: 'success',
            offColor: 'danger',
            onText: '{{ trans('forms.active_yes') }}',
            offText: '{{ trans('forms.active_no') }}',
        });

        $('#postal_code_id').on('click', function() {
        	$.ajax({
        		type: 'GET',
        		cache: false,
        		dataType: 'JSON',
        		url: '{!! url('postal-code/get-data') !!}/' + $(this).val(),
        		success: function(data) {
        			if (data.result) {
        				alert(data.town);
        			}
        		},
        		error: function(jqxhr, textStatus, error) {
        			var err = textStatus + ", " + error + ", " + jqxhr.responseJSON;
        			console.log(err);
        		}
        	})
        	.fail(function() {
        		console.log("error");
        	});
        });
	</script>
@endsection
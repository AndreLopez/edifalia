<div class="row">
	<div class="col-sm-6 form-group{{ $errors->has('contact_name') ? ' has-error' : '' }}">
		<label for="name" class="col-sm-3 control-label">@lang('forms_supplier.contactn_label')</label>
		<div class="col-sm-7">
			<div class="input-group">
		    	{!! Form::text(
		    		'contact_name', (isset($model))?$model->contact_name:null, [
		    			'class' => 'form-control', 'id' => 'contact_name', 
		    			'placeholder' => trans('forms_supplier.contactn_placeholder')
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('contact_name'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('contact_name') }}</strong>
		        </span>
		    </div>
		@endif
	</div>







	<div class="col-sm-6 form-group{{ $errors->has('cif') ? ' has-error' : '' }}">
		<label class="col-sm-3 control-label" for="cif">@lang('forms_supplier.cif_label')</label>
	    <div class="col-sm-7">
	    	<div class="input-group">
	    			{!! Form::text(
		    		'cif', (isset($model))?$model->cif:null, [
		    			'class' => 'form-control', 'id' => 'cif', 
		    			'placeholder' => trans('forms_supplier.cif_placeholder')
		    		]
		    	) !!}

		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
	    </div>
		@if ($errors->has('cif'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('cif') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>



	

<div class="row">
	<div class="col-sm-6 form-group{{ $errors->has('tradename') ? ' has-error' : '' }}">
		<label for="country" class="col-sm-3 control-label">@lang('forms_supplier.tradename_label')</label>
		<div class="col-sm-7">
			<div class="input-group">
	 		{!! Form::text(
		    		'tradename', (isset($model))?$model->tradename:null, [
		    			'class' => 'form-control', 'id' => 'tradename', 
		    			'placeholder' => trans('forms_supplier.tradename_placeholder')
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('tradename'))
			<div class="col-sm-9 col-sm-offset-3	">
		        <span class="help-block">
		            <strong>{{ $errors->first('tradename') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6 form-group{{ $errors->has('business_name') ? ' has-error' : '' }}">
		<label for="business_name" class="col-sm-3 control-label">@lang('forms_supplier.businessn_label')</label>
		<div class="col-sm-7">
			<div class="input-group">
		    	{!! Form::text(
		    		'business_name', (isset($model))?$model->business_name:null, [
		    			'class' => 'form-control', 'id' => 'business_name',
		    			'placeholder' => trans('forms_supplier.businessn_placeholder')
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('business_name'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('business_name') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>


	

<div class="row">
<div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('website') ? ' has-error' : '' }}">
		<label class="col-sm-3 control-label" for="text">@lang('forms_supplier.website_label')</label>
		<div class="col-sm-7">
			<div class="input-group">
		    	{!! Form::text(
		    		'website', (isset($model))?$model->website:null, [
		    			'class' => 'form-control', 'id' => 'website', 
		    			'placeholder' => trans('forms_supplier.website_placeholder')
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('website'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('website') }}</strong>
		        </span>
		    </div>
		@endif
	</div>


<div class="col-sm-6 form-group{{ $errors->has('postal_code_id') ? ' has-error' : '' }}">
		<label for="postal_code_id" class="col-sm-3 control-label">@lang('forms_supplier.postal_code_label')</label>
		<div class="col-sm-7">
			<div class="input-group">
	    		{!! Form::select(
		    		'postal_code_id', $postal_codes, null, [
		    			'id' => 'postal_code_id', 'class' => 'select2', 
		    			'placeholder' => trans('forms_supplier.select_null_option'),
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('postal_code_id'))
			<div class="col-sm-9 col-sm-offset-3	">
		        <span class="help-block">
		            <strong>{{ $errors->first('postal_code_id') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>









	
<div class="row">
<div class="col-sm-6 form-group{{ $errors->has('virtual_office_access') ? ' has-error' : '' }}">
		<label for="platform_accounting" class="col-sm-3 control-label">
			@lang('forms_supplier.virtual_office_access_label')
		</label>
		<div class="col-sm-9">
			{!! Form::checkbox('virtual_office_access', True, False, [
				'data-size' => 'mini', 'class' => 'switch'
			]) !!}
		</div>
		@if ($errors->has('virtual_office_access'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('virtual_office_access') }}</strong>
		        </span>
		    </div>
		@endif
</div>

<div class="col-sm-6 form-group{{ $errors->has('update_data_from_virtual_office') ? ' has-error' : '' }}">
		<label for="platform_accounting" class="col-sm-3 control-label">
			@lang('forms_supplier.update_data_from_virtual_office_label')
		</label>
		<div class="col-sm-9">
			{!! Form::checkbox('update_data_from_virtual_office', True, False, [
				'data-size' => 'mini', 'class' => 'switch'
			]) !!}
		</div>
		@if ($errors->has('update_data_from_virtual_office'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('virtual_office_access') }}</strong>
		        </span>
		    </div>
		@endif
     </div>
</div>


<div class="row">
	<div class="col-sm-6 form-group{{ $errors->has('address') ? ' has-error' : '' }}">
		<label for="address" class="col-sm-3 control-label">@lang('forms_supplier.address_label')</label>
		<div class="col-sm-7">
			{!! Form::textarea('address', null, [
				'class' => 'form-control', 'id' => 'address', 'rows' => '3',
				'placeholder' => trans('forms_supplier.address_placeholder')
			]) !!}
		</div>
		@if ($errors->has('address'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('address') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6 form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
		<label for="notes" class="col-sm-3 control-label">@lang('forms_supplier.notes_label')</label>
		<div class="col-sm-7">
			{!! Form::textarea('notes', null, [
				'class' => 'form-control', 'id' => 'notes', 'rows' => '3',
				'placeholder' => trans('forms_supplier.notes_placeholder')
			]) !!}
		</div>
		@if ($errors->has('notes'))
			<div class="col-sm-9 col-sm-offset-2">
		        <span class="help-block">
		            <strong>{{ $errors->first('notes') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>

<div class="row">
<div class="col-sm-6 form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
		<label for="postal_code_id" class="col-sm-3 control-label">@lang('forms_supplier.user_id_label')</label>
		<div class="col-sm-7">
			<div class="input-group">
	    		{!! Form::select(
		    		'user_id', $users, null, [
		    			'id' => 'user_id', 'class' => 'select2', 
		    			'placeholder' => trans('forms_supplier.select_null_option'),
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('user_id'))
			<div class="col-sm-9 col-sm-offset-3	">
		        <span class="help-block">
		            <strong>{{ $errors->first('user_id') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>

@include('common.bank-account-fields')
@include('common.supplier_activitie-fields')
<div class="row">
	<div class="col-sm-6 form-group{{ $errors->has('reference') ? ' has-error' : '' }}">
		<label for="name" class="col-sm-3 control-label">@lang('forms_contracts.reference_title')</label>
		<div class="col-sm-7">
			<div class="input-group">
		    	{!! Form::text(
		    		'reference', (isset($model))?$model->reference:null, [
		    			'class' => 'form-control', 'id' => 'contact_name', 
		    			'placeholder' => trans('forms_contracts.reference_placeholder')
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('reference'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('reference') }}</strong>
		        </span>
		    </div>
		@endif
	</div>



	<div class="col-sm-6 form-group{{ $errors->has('contract_date') ? ' has-error' : '' }}">
		<label class="col-sm-3 control-label" for="cif">@lang('forms_contracts.contract_date_title')</label>
	    <div class="col-sm-7">
	    	<div class="input-group">
	    			{!! Form::date(
		    		'contract_date', (isset($model))?$model->contract_date:null, [
		    			'class' => 'form-control', 'id' => 'contract_date'
		    		]
		    	) !!}

		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
	    </div>
		@if ($errors->has('contract_date'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('contract_date') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>

<div class="row">
	<div class="col-sm-6 form-group{{ $errors->has('duration_type') ? ' has-error' : '' }}">
		<label for="name" class="col-sm-3 control-label">@lang('forms_contracts.duration_type_title')</label>
		<div class="col-sm-7">
			<div class="input-group">
		    	{!! Form::text(
		    		'duration_type', (isset($model))?$model->duration_type:null, [
		    			'class' => 'form-control', 'id' => 'duration_type', 
		    			'placeholder' => trans('forms_contracts.duration_type_placeholder')
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('duration_type'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('duration_type') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6 form-group{{ $errors->has('duration') ? ' has-error' : '' }}">
		<label class="col-sm-3 control-label" for="cif">@lang('forms_contracts.duration_title')</label>
	    <div class="col-sm-7">
	    	<div class="input-group">
	    		{!! Form::text(
		    		'duration', (isset($model))?$model->duration:null, [
		    			'class' => 'form-control', 'id' => 'duration', 
		    			'placeholder' => trans('forms_contracts.duration_placeholder')
		    		]
		    	) !!}

		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
	    </div>
		@if ($errors->has('duration'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('duration') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>

<div class="row">
	<div class="col-sm-6 form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
		<label for="name" class="col-sm-3 control-label">@lang('forms_contracts.amount_title')</label>
		<div class="col-sm-7">
			<div class="input-group">
		    	{!! Form::number(
		    		'amount', (isset($model))?$model->amount:null, [
		    			'class' => 'form-control', 'id' => 'amount', 'step' => 'any',
		    			'placeholder' => trans('forms_contracts.amount_placeholder')
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('amount'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('amount') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6 form-group{{ $errors->has('minimum') ? ' has-error' : '' }}">
		<label for="name" class="col-sm-3 control-label">@lang('forms_contracts.minimum_title')</label>
		<div class="col-sm-7">
			<div class="input-group">
		    	{!! Form::number(
		    		'minimum', (isset($model))?$model->minimum:null, [
		    			'class' => 'form-control', 'id' => 'minimum', 'step' => 'any',
		    			'placeholder' => trans('forms_contracts.minimum_placeholder')
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('minimum'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('minimum') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>

<div class="row">
	<div class="col-sm-6 form-group{{ $errors->has('maximum') ? ' has-error' : '' }}">
		<label for="name" class="col-sm-3 control-label">@lang('forms_contracts.maximum_title')</label>
		<div class="col-sm-7">
			<div class="input-group">
		    	{!! Form::number(
		    		'maximum', (isset($model))?$model->maximum:null, [
		    			'class' => 'form-control', 'id' => 'maximum', 'step' => 'any',
		    			'placeholder' => trans('forms_contracts.maximum_placeholder')
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('maximum'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('maximum') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6 form-group{{ $errors->has('payment_method_id') ? ' has-error' : '' }}">
		<label for="postal_code_id" class="col-sm-3 control-label">@lang('forms_contracts.payment_method_title')</label>
		<div class="col-sm-7">
			<div class="input-group">
	    		{!! Form::select(
		    		'payment_method_id', $payments, null, [
		    			'id' => 'payment_method_id', 'class' => 'select2', 
		    			'placeholder' => trans('forms_supplier.select_null_option'),
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('payment_method_id'))
			<div class="col-sm-9 col-sm-offset-3	">
		        <span class="help-block">
		            <strong>{{ $errors->first('payment_method_id') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>


<div class="row">
	<div class="col-sm-6 form-group{{ $errors->has('community_id') ? ' has-error' : '' }}">
		<label for="postal_code_id" class="col-sm-3 control-label">@lang('forms_contracts.community_id_title')</label>
		<div class="col-sm-7">
			<div class="input-group">
	    		{!! Form::select(
		    		'community_id', $communities, null, [
		    			'id' => 'comunity_id', 'class' => 'select2', 
		    			'placeholder' => trans('forms_supplier.select_null_option'),
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('community_id'))
			<div class="col-sm-9 col-sm-offset-3	">
		        <span class="help-block">
		            <strong>{{ $errors->first('community_id') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6 form-group{{ $errors->has('supplier_id') ? ' has-error' : '' }}">
		<label for="postal_code_id" class="col-sm-3 control-label">@lang('forms_contracts.supplier_id_title')</label>
		<div class="col-sm-7">
			<div class="input-group">
	    		{!! Form::select(
		    		'supplier_id', $suppliers, null, [
		    			'id' => 'supplier_id', 'class' => 'select2', 
		    			'placeholder' => trans('forms_supplier.select_null_option'),
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('supplier_id'))
			<div class="col-sm-9 col-sm-offset-3	">
		        <span class="help-block">
		            <strong>{{ $errors->first('supplier_id') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>


<div class="row">

	<div class="col-sm-6 form-group{{ $errors->has('accounting_account_id') ? ' has-error' : '' }}">
		<label for="postal_code_id" class="col-sm-3 control-label">@lang('forms_contracts.accounting_account')</label>
		<div class="col-sm-7">
			<div class="input-group">
	    		{!! Form::select(
		    		'accounting_account_id', $accounting_accounts, null, [
		    			'id' => 'accounting_account_id', 'class' => 'select2', 
		    			'placeholder' => trans('forms_supplier.select_null_option'),
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('accounting_account_id'))
			<div class="col-sm-9 col-sm-offset-3	">
		        <span class="help-block">
		            <strong>{{ $errors->first('accounting_account_id') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6 form-group">
		<label for="postal_code_id" class="col-sm-3 control-label">@lang('forms_contracts.months_title')</label>
	</div>
</div>

<div class="show">
	<div class="col-sm-6 form-group{{ $errors->has('jan') ? ' has-error' : '' }}">
		<label for="platform_accounting" class="col-sm-3 control-label">
			@lang('forms_contracts.jan_title')
		</label>
		<div class="col-sm-9">
			{!! Form::checkbox('jan', True, False, ['data-size' => 'mini']) !!}
		</div>
		@if ($errors->has('jan'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('jan') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6  form-group{{ $errors->has('feb') ? ' has-error' : '' }}">
		<label for="platform_accounting" class="col-sm-3 control-label">
			@lang('forms_contracts.feb_title')
		</label>
		<div class="col-sm-9">
			{!! Form::checkbox('feb', True, False, ['data-size' => 'mini']) !!}
		</div>
		@if ($errors->has('feb'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('feb') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>


<div class="show">
	<div class="col-sm-6 form-group{{ $errors->has('mar') ? ' has-error' : '' }}">
		<label for="platform_accounting" class="col-sm-3 control-label">
			@lang('forms_contracts.mar_title')
		</label>
		<div class="col-sm-9">
			{!! Form::checkbox('mar', True, False, ['data-size' => 'mini']) !!}
		</div>
		@if ($errors->has('mar'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('mar') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6  form-group{{ $errors->has('apr') ? ' has-error' : '' }}">
		<label for="platform_accounting" class="col-sm-3 control-label">
			@lang('forms_contracts.apr_title')
		</label>
		<div class="col-sm-9">
			{!! Form::checkbox('apr', True, False, ['data-size' => 'mini']) !!}
		</div>
		@if ($errors->has('apr'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('apr') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>
	


<div class="show">
	<div class="col-sm-6 form-group{{ $errors->has('may') ? ' has-error' : '' }}">
		<label for="platform_accounting" class="col-sm-3 control-label">
			@lang('forms_contracts.may_title')
		</label>
		<div class="col-sm-9">
			{!! Form::checkbox('may', True, False, ['data-size' => 'mini']) !!}
		</div>
		@if ($errors->has('may'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('may') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6  form-group{{ $errors->has('jun') ? ' has-error' : '' }}">
		<label for="platform_accounting" class="col-sm-3 control-label">
			@lang('forms_contracts.jun_title')
		</label>
		<div class="col-sm-9">
			{!! Form::checkbox('jun', True, False, ['data-size' => 'mini']) !!}
		</div>
		@if ($errors->has('jun'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('jun') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>
	

<div class="show">
	<div class="col-sm-6 form-group{{ $errors->has('jul') ? ' has-error' : '' }}">
		<label for="platform_accounting" class="col-sm-3 control-label">
			@lang('forms_contracts.jul_title')
		</label>
		<div class="col-sm-9">
			{!! Form::checkbox('jul', True, False, ['data-size' => 'mini']) !!}
		</div>
		@if ($errors->has('jul'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('jul') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6  form-group{{ $errors->has('aug') ? ' has-error' : '' }}">
		<label for="platform_accounting" class="col-sm-3 control-label">
			@lang('forms_contracts.aug_title')
		</label>
		<div class="col-sm-9">
			{!! Form::checkbox('aug', True, False, ['data-size' => 'mini']) !!}
		</div>
		@if ($errors->has('aug'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('aug') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>
	

<div class="show">
	<div class="col-sm-6 form-group{{ $errors->has('sep') ? ' has-error' : '' }}">
		<label for="platform_accounting" class="col-sm-3 control-label">
			@lang('forms_contracts.sep_title')
		</label>
		<div class="col-sm-9">
			{!! Form::checkbox('sep', True, False, ['data-size' => 'mini']) !!}
		</div>
		@if ($errors->has('sep'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('sep') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6  form-group{{ $errors->has('oct') ? ' has-error' : '' }}">
		<label for="platform_accounting" class="col-sm-3 control-label">
			@lang('forms_contracts.aug_title')
		</label>
		<div class="col-sm-9">
			{!! Form::checkbox('oct', True, False, ['data-size' => 'mini']) !!}
		</div>
		@if ($errors->has('oct'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('oct') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>
	

<div class="show">
	<div class="col-sm-6 form-group{{ $errors->has('nov') ? ' has-error' : '' }}">
		<label for="platform_accounting" class="col-sm-3 control-label">
			@lang('forms_contracts.nov_title')
		</label>
		<div class="col-sm-9">
			{!! Form::checkbox('nov', True, False, ['data-size' => 'mini']) !!}
		</div>
		@if ($errors->has('nov'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('nov') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6  form-group{{ $errors->has('dec') ? ' has-error' : '' }}">
		<label for="platform_accounting" class="col-sm-3 control-label">
			@lang('forms_contracts.dec_title')
		</label>
		<div class="col-sm-9">
			{!! Form::checkbox('dec', True, False, ['data-size' => 'mini']) !!}
		</div>
		@if ($errors->has('dec'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('dec') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>

<div class="row">
	<div class="col-sm-6  form-group{{ $errors->has('publish_virtual_office') ? ' has-error' : '' }}">
		<label for="platform_accounting" class="col-sm-3 control-label">
			@lang('forms_contracts.publish_virtual_office_title')
		</label>
		<div class="col-sm-9">
			{!! Form::checkbox('publish_virtual_office', True, False, ['data-size' => 'mini']) !!}
		</div>
		@if ($errors->has('publish_virtual_office'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('publish_virtual_office') }}</strong>
		        </span>
		    </div>
		@endif
	</div>



	<div class="col-sm-6 form-group{{ $errors->has('description') ? ' has-error' : '' }}">
		<label for="address" class="col-sm-3 control-label">@lang('forms_contracts.description_title')</label>
		<div class="col-sm-7">
			{!! Form::textarea('description', null, [
				'class' => 'form-control', 'id' => 'description', 'rows' => '3',
				'placeholder' => trans('forms_contracts.description_placeholder')
			]) !!}
		</div>
		@if ($errors->has('description'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('description') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>

<div class="row">
	<div class="col-sm-6 form-group{{ $errors->has('observations') ? ' has-error' : '' }}">
		<label for="notes" class="col-sm-3 control-label">@lang('forms_contracts.observations_title')</label>
		<div class="col-sm-7">
			{!! Form::textarea('observations', null, [
				'class' => 'form-control', 'id' => 'observations', 'rows' => '3',
				'placeholder' => trans('forms_contracts.observations_placeholder')
			]) !!}
		</div>
		@if ($errors->has('observations'))
			<div class="col-sm-9 col-sm-offset-2">
		        <span class="help-block">
		            <strong>{{ $errors->first('observations') }}</strong>
		        </span>
		    </div>
		@endif
	</div>


	<div class="col-sm-6 form-group{{ $errors->has('payment_method_id') ? ' has-error' : '' }}">
		<label for="postal_code_id" class="col-sm-3 control-label">@lang('forms_contracts.payment_method_title')</label>
		<div class="col-sm-7">
			<div class="input-group">
	    		{!! Form::select(
		    		'payment_method_id', $payments, null, [
		    			'id' => 'payment_method_id', 'class' => 'select2', 
		    			'placeholder' => trans('forms_supplier.select_null_option'),
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('payment_method_id'))
			<div class="col-sm-9 col-sm-offset-3	">
		        <span class="help-block">
		            <strong>{{ $errors->first('payment_method_id') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>

@include('common.bank-account-fields')
@include('common.accounting-account-fields')

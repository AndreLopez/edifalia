@extends('layouts.dashboard')

@if(!empty($data['breadcrumb_link']))
	@section('breadcrumb_link')
		{{ $data['breadcrumb_link'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_icon']))
	@section('breadcrumb_icon')
		{{ $data['breadcrumb_icon'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_title']))
	@section('breadcrumb_title')
		{{ $data['breadcrumb_title'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_subtitle']))
	@section('breadcrumb_subtitle')
		{{ $data['breadcrumb_subtitle'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_map']))
	@section('breadcrumb_map')
		{{ $data['breadcrumb_map'] }}
	@endsection
@endif

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_management')</a></li>
    <li class="active">@lang('dashboard.mnu_quota_receipts')</li>
@endsection

@section('modals')
    @include('backend.partials.modals')
@endsection

@section('content')

	<div class="box box-default">
		<div class="box-header with-border">
			<div class="panel-title inline-block">
				<h5 class="box-title">
					<i class="glyphicon glyphicon-list-alt"></i>&#160;
					{{ $data['panel_title'] }}
				</h5>
			</div>
		</div>
		<div class="box-body">
			<div class="">
				<table class="table table-hover table-striped dt-responsive nowrap display dataTable" width="100%">
					@if(!empty($data['tbody']))
						<thead>
							<tr>
								<th>@lang('receipt_due.table_id')</th>
								<th>@lang('receipt_due.table_concept')</th>
								<th>@lang('receipt_due.table_amount')</th>
								<th>@lang('receipt_due.table_month')</th>
								<th>@lang('receipt_due.table_year')</th>
								<th class="text-center">@lang('common.lst_action_title')</th>
							</tr>
						</thead>
					@endif
					<tbody>
						@forelse($data['tbody'] as $tbody)
							<tr data-id="{{ $tbody['record_id'] }}" 
							data-rundo="{{route('receiptdues.undelete', $tbody['record_id'])}}">

								{!! $tbody['cols'] !!}
								<td class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-center">
									<div class="btn-group btn-group-xs">
	                                	<a data-toggle="tooltip" class="btn btn-info"
	                                   title="@lang('common.lst_act_show')"
	                                   href="{{route('receiptdues.show', $tbody['record_id'])}}">
	                                    <i class="fa fa-eye"></i></a>

	                                    <a data-toggle="tooltip" class="btn btn-info"
	                                   title="@lang('dashboard.buttons.edit')"
	                                   href="{{route('receiptdues.edit', $tbody['record_id'])}}">
	                                    <i class="fa fa-pencil-square-o"></i></a>

		                                <a data-toggle="tooltip" class="btn btn-info delete_record"
	                                   title="@lang('dashboard.buttons.delete')"
	                                   href="{{route($tbody['url_delete'], $tbody['record_id'])}}">
	                                    <i class="fa fa-trash-o"></i></a>
		                                
		                            </div>
								</td>
							</tr>
						@empty
						    <div class="alert alert-warning">
						        <h4><i class="icon fa fa-warning"></i> @lang('dashboard.messages.notice')</h4>
						        @lang('dashboard.messages.no_stored_records')
						    </div>
						@endforelse
					</tbody>
				</table>
			</div>
		</div>
		<div class="box-footer">
			<a data-toggle="tooltip" class="btn btn-primary col-xs-12 col-sm-12 col-md-1 col-lg-1 pull-right"
               title="@lang('common.lst_new_record')"
               href="{{ $data['url_new'] }}">
                <i class="fa fa-plus"></i> @lang('common.lst_new_record')</a>
		</div>
	</div>
@endsection

@section('scripts')
    {{-- Backend scripts  --}}
    {!! Html::script('js/code01.js') !!}
@endsection
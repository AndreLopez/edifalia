@extends('layouts.dashboard')

@if (!empty($data['breadcrumb_link']))
	@section('breadcrumb_link')
		{{ $data['breadcrumb_link'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_icon']))
	@section('breadcrumb_icon')
		{{ $data['breadcrumb_icon'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_title']))
	@section('breadcrumb_title')
		{{ $data['breadcrumb_title'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_subtitle']))
	@section('breadcrumb_subtitle')
		{{ $data['breadcrumb_subtitle'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_map']))
	@section('breadcrumb_map')
		{{ $data['breadcrumb_map'] }}
	@endsection
@endif

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_management')</a></li>
    <li class="active">@lang('dashboard.mnu_quota_receipts')</li>
@endsection

{{-- Modal Receipt due --}}
@section('modals')
    @include('backend.receiptdue.modal')
@endsection

@section('content')

	<div class="">

		{{-- Formulario --}}
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title inline-block">
							<i class="glyphicon glyphicon-edit"></i>&#160;
							{{ $data['panel_title'] }}
						</h3>
						<div class="btn-group btn-group-sm pull-right">
							<a data-toggle="tooltip" class="btn btn-primary" title="@lang('dashboard.buttons.back')" 
							   href="{{ $data['breadcrumb_link'] }}">
							    <i class="fa fa-reply"></i>
							</a>
							@if (isset($receiptdue[0]->community_id))
								<a data-toggle="tooltip" class="btn btn-primary" title="@lang('dashboard.buttons.new')" 
								   href="{{ $data['url_new'] }}">
								    <i class="fa fa-plus"></i>
								</a>
							@endif
						</div>
					</div>
					{!! Form::open(['url' => $data['form_url'], 'method' => $data['form_method'], 'class' => 'form-horizontal', 'autocomplete' => 'off']) !!}
						<div class="box-body">

							<div class="row">

								<div class="col-sm-12 col-md-12 col-lg-12 form-group">
									<label class="col-sm-1 control-label" for="text">Comunidad</label>
									<div class="col-sm-3 {{ $errors->has('community_id') ? 'has-error' : '' }}">
										{{-- input --}}
										{!! Form::select('community_id', $communities, ((isset($receiptdue[0]->community_id) AND $receiptdue[0]->community_id) ? $receiptdue[0]->community_id : null ), ['placeholder' => trans('forms.community_placeholder'), 'class' => 'select2', 'id' => 'community_id']) !!}

										{{-- error msg --}}
										@if ($errors->has('community_id'))
							                <span class="help-block">
							                    <strong>{{ $errors->first('community_id') }}</strong>
							                </span>
							            @endif
									</div>

									<label class="col-sm-2 control-label" for="text">Concepto de facturación</label>
									<div class="col-sm-3 {{ $errors->has('invoicing_concept_id') ? 'has-error' : '' }}">
										{{-- input --}}
										{!! Form::select('invoicing_concept_id', $concept, ((isset($receiptdue[0]->invoicing_concept_id) AND $receiptdue[0]->invoicing_concept_id) ? $receiptdue[0]->invoicing_concept_id : null ), ['placeholder' => trans('forms.invoicing_concept_placeholder'), 'class' => 'select2', 'id' => 'invoicing_concept_id']) !!}
										
										{{-- error msg --}}
										@if ($errors->has('invoicing_concept_id'))
							                <span class="help-block">
							                    <strong>{{ $errors->first('invoicing_concept_id') }}</strong>
							                </span>
							            @endif
									</div>

									<div class="col-sm-3" align="right">
										<button type="button" class="btn btn-primary btn-form" data-toggle="tooltip" title="Último mes de facturación" onclick="getReceipDueLastMonth()">
						                	<i class="fa fa-search"></i> Buscar
						            	</button>
									</div>
								</div>
							</div>

							<br><br><br>
							<table class="table table-striped table-hover">
								<thead>
								<tr>
									<td colspan="4" align="right"><strong>TOTAL ...</strong></td>
									<td width="5%" style="background-color: #eee;">
										{{ Form::text('total_amount', ((isset($receiptdue[0]->total_amount) AND $receiptdue[0]->total_amount) ? $receiptdue[0]->total_amount : null ), [
											'id' => 'receiptdue_total_amount',
											'placeholder' => '0,00',
											'class' => 'tooltip_err_msg',
											'data-toggle' => 'tooltip',
											'title' => $errors->has('total_amount') ? 'Campo requerido' : ''
										]) }}
									</td>
									<td width="5%" id="td_month" style="background-color: #eee; ">
										{{ Form::text('month', ((isset($receiptdue[0]->month) AND $receiptdue[0]->month) ? $receiptdue[0]->month : null ), [
											'id' => 'receiptdue_month',
											'placeholder' => '00',
											'class' => 'tooltip_err_msg',
											'data-toggle' => 'tooltip',
											'title' => $errors->has('month') ? 'Campo requerido' : ''
										]) }}
									</td>
									<td width="5%" id="td_year" style="background-color: #eee; ">
										{{ Form::text('year', ((isset($receiptdue[0]->year) AND $receiptdue[0]->year) ? $receiptdue[0]->year : null ), [
											'id' => 'receiptdue_year',
											'placeholder' => '0000',
											'class' => 'tooltip_err_msg',
											'data-toggle' => 'tooltip',
											'title' => $errors->has('year') ? 'Campo requerido' : ''
										]) }}
									</td>
								</tr>
								<tr>
									<td width="5%"><strong>@lang('receipt_due.table_id')</strong></td>
									<td><strong>@lang('receipt_due.table_bloq')</strong></td>
									<td><strong>@lang('receipt_due.table_property')</strong></td>
									<td><strong>@lang('receipt_due.table_payer')</strong></td>
									<td><strong>@lang('receipt_due.table_amount')</strong></td>
									<td width="5%"><strong>@lang('receipt_due.table_month')</strong></td>
									<td width="5%"><strong>@lang('receipt_due.table_year')</strong></td>
								</tr>
								</thead>
								<tbody id="receiptdue_body">
									@if (!empty($receiptdueItem))
										@foreach ($receiptdueItem as $row)
											<tr>
												<td>{{ $row->id }} <input type="hidden" value="{{ $row->id }}" name="property_id[]"></td>
												<td>{{ $row->building_number }}</td>
												<td>{{ $row->property_number }}</td>
												<td>{{ $row->first_name." ".$row->last_name }}</td>
												<td>{{ $row->amount }} <input type="hidden" value="{{ $row->amount }}" name="amount[]"></td>
												<td>{{ $receiptdue[0]->month }}</td>
												<td>{{ $receiptdue[0]->year }}</td>
											</tr>
										@endforeach
									@else
										<td>-</td>
										<td>-</td>
										<td>-</td>
										<td>-</td>
										<td>-</td>
										<td>-</td>
									@endif
								</tbody>
							</table>
						</div>
						<div class="box-footer">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								@if(isset($data['breadcrumb_link']))
									<button type="button" class="btn btn-default col-xs-12 col-sm-12 col-md-1 col-lg-1 col-md-offset-8 col-lg-offset-8 btn-form" data-toggle="tooltip" 
					            		title="{{ trans('common.btn_cancel_tooltip') }}" 
					            		onclick="location.href='{{ $data['breadcrumb_link'] }}'">
					                	<i class="glyphicon glyphicon-ban-circle"></i> @lang('common.btn_cancel_text')
						            </button>
						        @endif
								<button type="button" class="btn btn-warning btn-form col-xs-12 col-sm-12 col-md-1 col-lg-1 @if(!isset($data['breadcrumb_link'])) col-md-offset-9 col-lg-offset-9 @endif" data-toggle="tooltip" 
										title="{{ trans('common.btn_clear_tooltip') }}" onclick="clearInputs()">
					                <i class="fa fa-eraser"></i> @lang('common.btn_clear_text')
						        </button>
						        @if (isset($data['btn_record_submit']))
						        	<button type="submit" class="btn btn-info btn-form col-xs-12 col-sm-12 col-md-1 col-lg-1" data-toggle="tooltip" 
										title="{{ trans('common.btn_save_tooltip') }}">
					                	<i class="fa fa-save"></i> {{ $data['btn_record_submit'] }}
					            	</button>
						        @endif								
							</div>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>

	</div>

@endsection

@section('extra_scripts')
	<script type="text/javascript">

			$(".select2").select2({ 
				width: '100%',
				theme: "bootstrap"
			});

			/**
			 * Get properties of a community
			 */
			$('#community_id').change(function() {

				if (!isNaN(parseFloat($(this).val()))) {
					// get community
					data = "id=" + $(this).val();

					$.ajax({
		                type: 'GET',
		                url: '{{ url('receiptdues/getProperties') }}',
		                data: data,
		                success: function(response) {
		                   	console.log(response);

		                   	if (response.success == true) {
		                   		// remove readonly
		                   		/*$("#receiptdue_total_amount").removeAttr("readonly");
		                   		$("#receiptdue_month").removeAttr("readonly");
		                   		$("#receiptdue_year").removeAttr("readonly");*/
		                   		// clear table
		                   		$('#receiptdue_body').html('');

		                        $.each(response.data, function (k, item){
									$('#receiptdue_body').append(
										"<tr><td>"+ item.id +"<input type='hidden' name='property_id[]' value='"+ item.id +"'></td><td>"+ item.building_number +"</td><td>"+ item.property_number +"</td><td>"+ item.first_name +" "+ item.last_name +"</td><td class='coefficient'>"+ item.coefficient +"<input type='hidden' name='amount[]' value='"+ item.coefficient +"'></td><td class='receipt_date_month'>-</td><td class='receipt_date_year'>-</td></tr>"
	                            	);
	                       	 	});
	                       	 	
		                    }else{
		                    	// clear table
		                        clearInputs();
		                    }
		                }
		            });
				} else {
					// add readonly
					/*$("#receiptdue_total_amount").attr("readonly", 'true');
					$("#receiptdue_month").attr("readonly", 'true');
					$("#receiptdue_year").attr("readonly", 'true');*/
					// clear table
					clearInputs();
				}

				
			});

			/**
			 * Calculate amount
			 */
			$('#receiptdue_total_amount').focusout(function() {
				var amount = $(this).val();
				if (amount !== "" || !isNaN(parseFloat(amount))) {
					var calcular;

					$('#receiptdue_body > tr').each(function() {

						var value = $(this).find(".coefficient").text();
						$(this).find(".coefficient").html("");

						calcular = (parseInt(amount) * parseInt(value)) / 100;

						// x = (15 * 500) / 100 = 75
						$(this).find(".coefficient").text(calcular);
						$(this).find(".coefficient").append("<input type='hidden' name='amount[]' value='"+ calcular +"'></input>");
					}); 
				} else {
					$(this).find(".coefficient").html();
				}
			});

			/**
			 * Set month
			 */
			$("#receiptdue_month").focusout(function() {
				$(".receipt_date_month").text("");
				$(".receipt_date_month").text($(this).val());
			});

			/**
			 * Set year
			 */
			$("#receiptdue_year").focusout(function() {
				$(".receipt_date_year").text("");
				$(".receipt_date_year").text($(this).val());
			});

			/**
			 * Clear inputs
			 */
			var clearInputs = function() {
                $('.select2-chosen').text("");
                $('#receiptdue_body').html("");
                $("#receiptdue_total_amount").val("");
				$("#receiptdue_month").val("");
				$("#receiptdue_year").val("");
				$('#receiptdue_body').append(
            		"<tr><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td></tr>"
            	);
			}

			/**
			 * Tooltip on error validation
			 */
			$('.tooltip_err_msg').tooltip({placement: 'top', trigger: 'manual'}).tooltip('show');

			/**
			 * Prevent submit
			 */
			$(window).keydown(function(event){
				if(event.keyCode == 13) {
					event.preventDefault();
					return false;
				}
			});

			/**
			 * Get last month receipt due
			 */
			var getReceipDueLastMonth = function() {

				if ($("#invoicing_concept_id").val() == "")
				{
					$("#modal-warning").modal("show");
				} else {
					var id = "id=" + $("#invoicing_concept_id").val();

					$.ajax({
		                type: 'GET',
		                url: '{{ url('receiptdues/getReceipDueLastMonth') }}',
		                data: id,
		                success: function(response) {
		                   	console.log(response);
		                   	$("#modal-receipt").modal("show");
		                   	$("#modal-receipt-result").html("");
		                   	$("#modal-receipt-result").append(
		                		"<tr><td>" + response.data[0].first_name + " " + response.data[0].last_name + "</td><td>" + response.data[0].total_amount + "</td><td>" + response.data[0].month + "</td><td>" + response.data[0].year + "</td></tr>");
		                   	$("#modal-receipt .modal-title").text(response.data[0].concept);
		                }
		            });				
				}	
			}

			$("#yes-receipt").on("click", function(event){
		        var id = "id=" + $("#invoicing_concept_id").val();

				$.ajax({
	                type: 'GET',
	                url: '{{ url('receiptdues/getReceipDueLastMonth') }}',
	                data: id,
	                success: function(response) {
	                   	// console.log(response);
	                   	// clear table
						clearInputs();
						// field inputs
       	                $('#community_id').val(response.data[0].community_id);
       	                $('#invoicing_concept_id').val(response.data[0].invoicing_concept_id);
       	                $('#receiptdue_body').html("");
       	                $("#receiptdue_total_amount").val(response.data[0].total_amount);
       					$("#receiptdue_month").val(response.data[0].month);
       					$("#receiptdue_year").val(response.data[0].year);
       					$.each(response.data, function (k, item) {
       						$('#receiptdue_body').append(
								"<tr><td>"+ item.id +"<input type='hidden' name='property_id[]' value='"+ item.id +"'></td><td>"+ item.building_number +"</td><td>"+ item.property_number +"</td><td>"+ item.first_name +" "+ item.last_name +"</td><td class='coefficient'>"+ item.amount +"<input type='hidden' name='amount[]' value='"+ item.amount +"'></td><td>"+ item.month +"</td><td>"+ item.year +"</td></tr>"
                        	);
       					});
	                }
	            });
		    });

	</script>
@endsection
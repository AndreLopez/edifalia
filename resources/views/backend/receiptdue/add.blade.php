@extends('layouts.dashboard')

@if (!empty($data['breadcrumb_link']))
	@section('breadcrumb_link')
		{{ $data['breadcrumb_link'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_icon']))
	@section('breadcrumb_icon')
		{{ $data['breadcrumb_icon'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_title']))
	@section('breadcrumb_title')
		{{ $data['breadcrumb_title'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_subtitle']))
	@section('breadcrumb_subtitle')
		{{ $data['breadcrumb_subtitle'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_map']))
	@section('breadcrumb_map')
		{{ $data['breadcrumb_map'] }}
	@endsection
@endif

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_management')</a></li>
    <li class="active">@lang('dashboard.mnu_quota_receipts')</li>
@endsection

@section('content')

	<div class="">

		{{-- Formulario --}}
		@include('backend.receiptdue.form', [
			'receiptdue'  	 => $receiptdue,
			'receiptdueItem' => $receiptdueItem,
			'communities' 	 => $communities,
			'concept' 	  	 => $concept,
			'url' 		  	 => '/receiptdues',
			'method' 	  	 => 'POST'])

	</div>

@endsection

@section('extra_scripts')
	<script type="text/javascript">

			$(".select2").select2({ 
				width: '100%',
				theme: "bootstrap"
			});

			/**
			 * Get properties of a community
			 */
			$('#community_id').change(function() {

				if (!isNaN(parseFloat($(this).val()))) {
					// get community
					data = "id=" + $(this).val();

					$.ajax({
		                type: 'GET',
		                url: '{{ url('receiptdues/getProperties') }}',
		                data: data,
		                success: function(response) {
		                   	//console.log(response);

		                   	if (response.success == true) {
		                   		// remove readonly
		                   		$("#receiptdue_total_amount").removeAttr("readonly");
		                   		$("#receiptdue_month").removeAttr("readonly");
		                   		$("#receiptdue_year").removeAttr("readonly");
		                   		// clear table
		                   		$('#receiptdue_body').html('');

		                        $.each(response.data, function (k, item){
									$('#receiptdue_body').append(
										"<tr><td>"+ item.id +"<input type='hidden' name='property_id[]' value='"+ item.id +"'></td><td>"+ item.building_number +"</td><td>"+ item.property_number +"</td><td>"+ item.first_name +" "+ item.last_name +"</td><td class='coefficient'>"+ item.coefficient +"<input type='hidden' name='amount[]' value='"+ item.coefficient +"'></td><td>mes</td><td>año</td></tr>"
	                            	);
	                       	 	});
	                       	 	
		                    }else{
		                    	// clear table
		                        clearInputs();
		                    }
		                }
		            });
				} else {
					// add readonly
					$("#receiptdue_total_amount").attr("readonly", 'true');
					$("#receiptdue_month").attr("readonly", 'true');
					$("#receiptdue_year").attr("readonly", 'true');
					// clear table
					clearInputs();
				}

				
			});

			/**
			 * Calculate amount
			 */
			$('#receiptdue_total_amount').focusout(function() {
				var amount = $(this).val();
				if (amount !== "" || !isNaN(parseFloat(amount))) {
					var calcular;

					$('#receiptdue_body > tr').each(function() {

						var value = $(this).find(".coefficient").text();
						$(this).find(".coefficient").html("");

						calcular = (parseInt(amount) * parseInt(value)) / 100;

						// x = (15 * 500) / 100 = 75
						$(this).find(".coefficient").text(calcular);
						$(this).find(".coefficient").append("<input type='hidden' name='amount[]' value='"+ calcular +"'></input>");
					}); 
				} else {
					$(this).find(".coefficient").html();
				}
				     
			});

			/**
			 * Clear inputs
			 */
			var clearInputs = function() {
                $('.select2-chosen').text("");
                $('#receiptdue_body').html("");
                $("#receiptdue_total_amount").val("");
				$("#receiptdue_month").val("");
				$("#receiptdue_year").val("");
				$('#receiptdue_body').append(
            		"<tr><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td></tr>"
            	);
			}

			/**
			 * Tooltip on error validation
			 */
			$('.tooltip_err_msg').tooltip({placement: 'top', trigger: 'manual'}).tooltip('show');

			/**
			 * Prevent submit
			 */
			$(window).keydown(function(event){
				if(event.keyCode == 13) {
					event.preventDefault();
					return false;
				}
			});

			/**
			 * Get last month receipt due
			 */
			var getReceipDueLastMonth = function() {

				var id = "id=" + $("#invoicing_concept_id").val();

				$.ajax({
	                type: 'GET',
	                url: '{{ url('receiptdues/getReceipDueLastMonth') }}',
	                data: id,
	                success: function(response) {
	                   	console.log(response);

	                   	/*if (response.success == true) {
	                   		// remove readonly
	                   		$("#receiptdue_total_amount").removeAttr("readonly");
	                   		$("#receiptdue_month").removeAttr("readonly");
	                   		$("#receiptdue_year").removeAttr("readonly");
	                   		// clear table
	                   		$('#receiptdue_body').html('');

	                        $.each(response.data, function (k, item){
								$('#receiptdue_body').append(
									"<tr><td>"+ item.id +"<input type='hidden' name='property_id[]' value='"+ item.id +"'></td><td>"+ item.building_number +"</td><td>"+ item.property_number +"</td><td>"+ item.first_name +" "+ item.last_name +"</td><td class='coefficient'>"+ item.coefficient +"<input type='hidden' name='amount[]' value='"+ item.coefficient +"'></td><td>mes</td><td>año</td></tr>"
                            	);
                       	 	});
                       	 	
	                    }else{
	                    	// clear table
	                        clearInputs();
	                    }*/
	                }
	            });				
			}

	</script>
@endsection
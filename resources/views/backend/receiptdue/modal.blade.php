<div class="modal modal-primary" id="modal-receipt">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">{{-- @lang('dashboard.titles.delete_record') --}}</h4>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                        <th>Pagador</th>
                        <th>Monto</th>
                        <th>Mes</th>
                        <th>Año</th>
                    </thead>
                    <tbody id="modal-receipt-result">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left"
                        data-dismiss="modal">
                    Cancelar{{-- @lang('dashboard.buttons.no_delete') --}}
                </button>
                <button type="button" class="btn btn-outline" id="yes-receipt" data-dismiss="modal">
                    Seleccionar{{-- @lang('dashboard.buttons.yes_delete') --}}
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal modal-danger" id="modal-warning">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Ups{{-- @lang('dashboard.titles.delete_record') --}}</h4>
            </div>
            <div class="modal-body" style="text-align: center">
                <h4>Seleccione un concepto de facturación{{-- @lang('dashboard.messages.delete_record') --}}</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline" data-dismiss="modal">
                    Ok{{-- @lang('dashboard.buttons.yes_delete') --}}
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
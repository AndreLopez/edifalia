@extends('layouts.dashboard')

@if(!empty($data['breadcrumb_link']))
	@section('breadcrumb_link')
		{{ $data['breadcrumb_link'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_icon']))
	@section('breadcrumb_icon')
		{{ $data['breadcrumb_icon'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_title']))
	@section('breadcrumb_title')
		{{ $data['breadcrumb_title'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_subtitle']))
	@section('breadcrumb_subtitle')
		{{ $data['breadcrumb_subtitle'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_map']))
	@section('breadcrumb_map')
		{{ $data['breadcrumb_map'] }}
	@endsection
@endif

@section('breadcrumb_map')
    <li>
        <a href="{{ url('dashboard') }}">
            <i class="fa fa-dashboard"></i> @lang('dashboard.home')
        </a>
    </li>
    <li><a href="#">@lang('dashboard.mnu_management')</a></li>
    <li class="active">@lang('dashboard.mnu_quota_receipts')</li>
@endsection

@section('content')

<div class="box box-info">
		<div class="box-header with-border">
			<div class="panel-title inline-block">
				<h5 class="box-title">
					<i class="glyphicon glyphicon-list-alt"></i>&#160;
					{{ $data['panel_title'] }}
				</h5>
				<div class="btn-group btn-group-sm pull-right">
					<a data-toggle="tooltip" class="btn btn-primary" title="@lang('dashboard.buttons.back')" 
					   href="{{ $data['breadcrumb_link'] }}">
					    <i class="fa fa-reply"></i>
					</a>
					<a data-toggle="tooltip" class="btn btn-primary" title="@lang('dashboard.buttons.new')" 
					   href="{{ $data['url_new'] }}">
					    <i class="fa fa-plus"></i>
					</a>
				</div>
			</div>
		</div>
		<div class="box-body">

			<div class="row">

				<div class="col-sm-12 col-md-12 col-lg-12 form-group">
					<label class="col-sm-1 control-label" for="text">Comunidad</label>
					<div class="col-sm-3">{{ $receiptdue[0]->community_name }}
					</div>

					<label class="col-sm-2 control-label" for="text">Concepto de facturación</label>
					<div class="col-sm-3">{{ $receiptdue[0]->invoicing_concept_name }}
					</div>
				</div>
			</div>

			<br><br><br>
			<table class="table table-striped table-hover">
				<thead>
				<tr>
					<td colspan="4" align="right"><strong>TOTAL ...</strong></td>
					<td width="5%" style="background-color: #eee;">{{ $receiptdue[0]->total_amount }}
					</td>
					<td width="5%" id="td_month" style="background-color: #eee; ">{{ $receiptdue[0]->month }}
					</td>
					<td width="5%" id="td_year" style="background-color: #eee; ">{{ $receiptdue[0]->year }}
					</td>
				</tr>
				<tr>
					<td width="5%"><strong>@lang('receipt_due.table_id')</strong></td>
					<td><strong>@lang('receipt_due.table_bloq')</strong></td>
					<td><strong>@lang('receipt_due.table_property')</strong></td>
					<td><strong>@lang('receipt_due.table_payer')</strong></td>
					<td><strong>@lang('receipt_due.table_amount')</strong></td>
					<td width="5%"><strong>@lang('receipt_due.table_month')</strong></td>
					<td width="5%"><strong>@lang('receipt_due.table_year')</strong></td>
				</tr>
				</thead>
				<tbody id="receiptdue_body">
					@foreach ($receiptdueItem as $row)
					<tr>
						<td>{{ $row->id }}</td>
						<td>{{ $row->building_number }}</td>
						<td>{{ $row->property_number }}</td>
						<td>{{ $row->first_name." ".$row->last_name }}</td>
						<td>{{ $row->coefficient }}</td>
						<td>{{ $receiptdue[0]->month }}</td>
						<td>{{ $receiptdue[0]->year }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="box-footer">
			<a data-toggle="tooltip" class="btn btn-primary col-xs-12 col-sm-12 col-md-1 col-lg-1 pull-right"
           title="@lang('common.lst_act_edit')"
           href="{{ route('receiptdues.edit', $receiptdue[0]->id) }}">
            <i class="fa fa-pencil-square-o"></i> @lang('common.lst_act_edit')</a>
		</div>
	</div>
@endsection
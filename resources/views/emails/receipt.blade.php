<h4>@lang('dashboard.titles.issue_receipt_info'): #{{ $issueReceipt_id }}</h4>
<p>@lang('email.attachment').</p>
<br>
<p>@lang('email.kind_regards').</p>
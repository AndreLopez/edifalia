@extends('layouts.dashboard')

@if(!empty($data['breadcrumb_link']))
	@section('breadcrumb_link')
		{{ $data['breadcrumb_link'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_icon']))
	@section('breadcrumb_icon')
		{{ $data['breadcrumb_icon'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_title']))
	@section('breadcrumb_title')
		{{ $data['breadcrumb_title'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_subtitle']))
	@section('breadcrumb_subtitle')
		{{ $data['breadcrumb_subtitle'] }}
	@endsection
@endif

@if(!empty($data['breadcrumb_map']))
	@section('breadcrumb_map')
		{{ $data['breadcrumb_map'] }}
	@endsection
@endif

@section('content')
   
	<div class="box box-default">
		<div class="box-header with-border">
			<div class="panel-title inline-block">
				<h5 class="box-title">
					<i class="glyphicon glyphicon-list-alt"></i>&#160;
					{{ $data['panel_title'] }}
				</h5>
			</div>
		</div>
		<div class="box-body">
			
   <h3 class="profile-username text-center">@if (Auth::check())
											{{ Auth::user()->name }}
										@endif</h3>

    <p class="text-muted text-center">@if (Auth::user()->type==0)
                                          @lang('user-profile.user_administrator')
                                        @endif
                                      
                                    @if (Auth::user()->type==1)
                                          @lang('user-profile.user_employee')
                                        
                                    @elseif(Auth::user()->type==2)   
                                           @lang('user-profile.user_owner')
                                     
                                    @elseif(Auth::user()->type==3)
                                          @lang('user-profile.user_supplier')
                                            
                                    @endif</p>
                              

    <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>@lang('user-profile.email_title')</b> <a class="pull-right">@if (Auth::check())
											{{ Auth::user()->email }}
										@endif</a>
                </li>
                <li class="list-group-item">
                  <b>@lang('user-profile.created_at_title')</b> <a class="pull-right">@if (Auth::check())
											{{ Auth::user()->created_at }}
										@endif</a>
                </li>
                <li class="list-group-item">
                  <b>@lang('user-profile.active_title')</b> <a class="pull-right">@if(Auth::user()->active==1)
                                                                            @lang('user-profile.user_active_yes')
                                                                         @else
                                                                          @lang('user-profile.user_active_no')
                                                                    @endif</a>
                </li>
              </ul>


          
     <div class="box-footer">
			<a data-toggle="tooltip" class="btn btn-primary col-xs-12 col-sm-12 col-md-1 col-lg-1 pull-right"
              href="{{route('user-profile.edit', Auth::user()->id)}}">
                <i class="route ('user-profile.edit')"></i> @lang('user-profile.lst_edit_user')</a>
		</div>


@endsection
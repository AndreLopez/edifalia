@extends('layouts.login')

@section('content')
<div class="login-box">
    <div class="login-logo">
        <a href="{{url('login')}}"><b>Edifalia</b></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">@lang('dashboard.titles.signin')</p>
        @include('backend.partials.messages')
        <form action="{{url('login')}}" method="post">
            {{ csrf_field() }}
            <div class="form-group has-feedback">
                <input name="email" type="email" class="form-control" placeholder="@lang('dashboard.fields.email')">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input name="password" type="password" class="form-control" placeholder="@lang('dashboard.fields.password')">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox" name="remember"> @lang('dashboard.fields.remember')
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">
                        @lang('dashboard.buttons.login')
                    </button>
                </div>
                <!-- /.col -->
            </div>
        </form>
        <a href="{{ url('/password/reset') }}">@lang('passwords.forgot')</a><br>
    </div>
</div>
@endsection

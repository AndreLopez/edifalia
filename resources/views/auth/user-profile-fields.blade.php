<div class="row">
<div class="col-sm-6 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
		<label for="name" class="col-sm-3 control-label">@lang('user-profile.name_title')</label>
		<div class="col-sm-7">
			<div class="input-group">
		    	{!! Form::text(
		    		'name', (isset($model))?$model->email:null, [
		    			'class' => 'form-control', 'id' => 'name','readonly' 
		    			
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('name'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('name') }}</strong>
		        </span>
		    </div>
		@endif
	</div>



<div class="col-sm-6 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
		<label for="name" class="col-sm-3 control-label">@lang('user-profile.email_title')</label>
		<div class="col-sm-7">
			<div class="input-group">
		    	{!! Form::text(
		    		'email', (isset($model))?$model->email:null, [
		    			'class' => 'form-control', 'id' => 'email', 
		    			'placeholder' => trans('user-profile.email_insert')
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('email'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('email') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>


<div class="row">
	<div class="col-sm-6 form-group{{ $errors->has('password') ? ' has-error' : '' }}">
		<label for="name" class="col-sm-3 control-label">@lang('user-profile.password_title')</label>
		<div class="col-sm-7">
			<div class="input-group">
		    	 <input id="password" type="password" class="form-control" name="password" required>
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('password'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('password') }}</strong>
		        </span>
		    </div>
		@endif
	</div>


<div class="col-sm-6 form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
		<label for="name" class="col-sm-3 control-label">@lang('user-profile.confirm_password')</label>
		<div class="col-sm-7">
			<div class="input-group">
		    
                 <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('password_confirmation'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('password_confirmation') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>


{{-- Campos para otras cuentas de tesorería --}}
<hr>
<div class="row">
	<div class="col-sm-12">
		<h4>
			@lang('forms.other_treasury_accounts')
			&#160;<i class="fa fa-plus-circle text-primary" role="button" id="add-other-treasury-account" 
					 title="{{ trans('tooltips.add_accounting_account') }}" data-toggle="tooltip"></i>
		</h4>
	</div>
</div>
@if(!is_null(old('accounting_account_code')))
	@foreach(old('accounting_account_code') as $key => $value)
		<div class="row treasury-account-fields">
			<div class="col-sm-12">
				<div class="col-sm-4 form-group{{ $errors->has('accounting_account_code.'.$key) ? ' has-error' : '' }}">
					<label for="" class="col-sm-12 text-center">
						@lang('forms.accounting_account_label')
					</label>
					<div class="col-sm-12">
						<input type="text" name="accounting_account_code[{{ $key }}]" id="accounting_account_code[{{ $key }}]" 
							   class="form-control input-sm" 
							   placeholder="{{ trans('forms.accounting_account_placeholder') }}" 
							   value="{{ old('accounting_account_code.'.$key) }}">
					</div>
					@if ($errors->has('accounting_account_code.'.$key))
						<div class="col-sm-12">
					        <span class="help-block">
					            <strong>{{ $errors->first('accounting_account_code.'.$key) }}</strong>
					        </span>
					    </div>
					@endif
				</div>
				<div class="col-sm-7 form-group{{ $errors->has('accounting_account_title.'.$key) ? ' has-error' : '' }}">
					<label for="" class="col-sm-12 text-center">
						@lang('forms.account_title_label')
					</label>
					<div class="col-sm-12">
						<div class="input-group">
							<input type="text" name="accounting_account_title[{{ $key }}]" 
								   id="accounting_account_title[{{ $key }}]" 
								   class="form-control input-sm input-accounting-title" 
								   placeholder="{{ trans('forms.account_title_placeholder') }}" 
								   value="{{ old('accounting_account_title.'.$key) }}">
					    	<span class="input-group-addon">
					    		<i class="fa fa-minus input-required remove-accounting-acount @if($key == 0) transparent-font @endif"></i>
					    	</span>
					    </div>
					</div>
					@if ($errors->has('accounting_account_title.'.$key))
						<div class="col-sm-12">
					        <span class="help-block">
					            <strong>{{ $errors->first('accounting_account_title.'.$key) }}</strong>
					        </span>
					    </div>
					@endif
				</div>
			</div>
		</div>
	@endforeach
@else
	<div class="row treasury-account-fields">
		<div class="col-sm-12">
			<div class="col-sm-4 form-group">
				<label for="" class="col-sm-12 text-center">
					@lang('forms.accounting_account_label')
				</label>
				<div class="col-sm-12">
					<input type="text" name="accounting_account_code[]" id="accounting_account_code[]" 
						   class="form-control input-sm" placeholder="{{ trans('forms.accounting_account_placeholder') }}">
				</div>
			</div>
			<div class="col-sm-7 form-group">
				<label for="" class="col-sm-12 text-center">
					@lang('forms.account_title_label')
				</label>
				<div class="col-sm-12">
					<div class="input-group">
						<input type="text" name="accounting_account_title[]" id="accounting_account_title[]" 
							   class="form-control input-sm input-accounting-title" 
							   placeholder="{{ trans('forms.account_title_placeholder') }}">
				    	<span class="input-group-addon">
				    		<i class="fa fa-minus input-required remove-accounting-acount transparent-font"></i>
				    	</span>
				    </div>
				</div>
			</div>
		</div>
	</div>
@endif

@section('extra_scripts')
	@parent
	<script>
		//$(document).ready(function() {
			$("#add-other-treasury-account").on('click', function() {
				var treasury_fields = $('.treasury-account-fields').last();
				treasury_fields.after("<div class='row treasury-account-fields'>" + treasury_fields.html() + "</div>");
				var i = 0;
				$(".remove-accounting-acount").each(function() {
					$(this).on('click', function() {
						$(this).closest('.treasury-account-fields').remove();
					});
					if (i > 0) {
						$(this).removeClass('transparent-font');
						$(this).attr('role', 'button');
					}
					i++;
				});
			});

			$(".remove-accounting-acount").on('click', function() {
				$(this).remove();
			});
		//});
	</script>
@endsection
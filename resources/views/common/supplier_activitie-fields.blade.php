{{-- Campos para detalles de la(s) cuenta(s) bancaria(s) --}}
<hr>
<div class="row">
	<div class="col-sm-12">
		<h4>
			@lang('forms_supplier.supplier_activie')
			&#160;<i class="fa fa-plus-circle text-primary" role="button" id="add-supplier-activitie" 
					 title="{{ trans('tooltips.add_supplier_activitie') }}" data-toggle="tooltip"></i>
		</h4>
	</div>
</div>

<div class="row">
	<div class="col-sm-6 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
		<label for="name" class="col-sm-3 control-label">@lang('common.supplier_activitie_name_label')</label>
		<div class="col-sm-7">
			<div class="input-group">
		    	{!! Form::text(
		    		'name', (isset($model))?$model->name:null, [
		    			'class' => 'form-control', 'id' => 'name', 
		    			'placeholder' => trans('forms_supplier.contactn_placeholder')
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('name'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('name') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	
@extends('layouts.dashboard')

@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
			<div class="info-box" role="button" onclick="location='{{ route('language.index') }}'">
				<span class="info-box-icon bg-red">
					<i class="fa fa-flag-o"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">
						<h4>@lang('panel_info.box_languages_title')</h4>
					</span>
					<span class="info-box-number">
						@lang('common.total_label')
						<span class="badge bg-default">{{ Edifalia\Models\Common\Language::all()->count() }}</span>
					</span>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
			<div class="info-box" role="button" onclick="location='{{ route('country.index') }}'">
				<span class="info-box-icon bg-blue">
					<i class="fa fa-globe"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">
						<h4>@lang('panel_info.box_countries_title')</h4>
					</span>
					<span class="info-box-number">
						@lang('common.total_label')
						<span class="badge bg-default">{{ Edifalia\Models\Common\Country::all()->count() }}</span>
					</span>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
			<div class="info-box" role="button" onclick="location='{{ route('status.index') }}'">
				<span class="info-box-icon bg-yellow">
					<i class="fa fa-bookmark-o"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">
						<h4>@lang('panel_info.box_status_title')</h4>
					</span>
					<span class="info-box-number">
						@lang('common.total_label')
						<span class="badge bg-default">{{ Edifalia\Models\Common\Status::all()->count() }}</span>
					</span>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
			<div class="info-box" role="button" onclick="location='{{ route('payment-method.index') }}'">
				<span class="info-box-icon bg-green">
					<i class="fa fa-money"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">
						<h4>@lang('panel_info.box_payment_methods_title')</h4>
					</span>
					<span class="info-box-number">
						@lang('common.total_label')
						<span class="badge bg-default">{{ Edifalia\Models\Common\PaymentMethod::all()->count() }}</span>
					</span>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
			<div class="info-box" role="button" onclick="location='{{ route('contact-type.index') }}'">
				<span class="info-box-icon bg-navy">
					<i class="fa fa-male"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">
						<h4>@lang('panel_info.box_contact_types_title')</h4>
					</span>
					<span class="info-box-number">
						@lang('common.total_label')
						<span class="badge bg-default">{{ Edifalia\Models\Common\ContactType::all()->count() }}</span>
					</span>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
			<div class="info-box" role="button" onclick="location='{{ route('postal-code.index') }}'">
				<span class="info-box-icon bg-olive">
					<i class="fa fa-map-o"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">
						<h4>@lang('panel_info.box_postal_codes_title')</h4>
					</span>
					<span class="info-box-number">
						@lang('common.total_label')
						<span class="badge bg-default">{{ Edifalia\Models\Common\PostalCode::all()->count() }}</span>
					</span>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
			<div class="info-box" role="button" onclick="location='{{ route('positions.index') }}'">
				<span class="info-box-icon bg-aqua">
					<i class="fa fa-sitemap"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">
						<h4>@lang('panel_info.box_position_title')</h4>
					</span>
					<span class="info-box-number">
						@lang('common.total_label')
						<span class="badge bg-default">{{ Edifalia\Models\Position::all()->count() }}</span>
					</span>
				</div>
			</div>
		</div>
    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
			<div class="info-box" role="button" onclick="location='{{ route('supplier_contact.index') }}'">
				<span class="info-box-icon bg-orange">
					<i class="fa fa-book"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">
						<h4>@lang('panel_info.box_suppliers_contacts_title')</h4>
					</span>
					<span class="info-box-number">
						@lang('common.total_label')
						<span class="badge bg-default">{{ Edifalia\Models\Common\SupplierContacts::all()->count() }}</span>
					</span>
				</div>
			</div>
		</div>

	<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
			<div class="info-box" role="button" onclick="location='{{ route('supplier_activitie.index') }}'">
				<span class="info-box-icon bg-blue">
					<i class="fa fa-list"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">
						<h4>@lang('panel_info.box_supplier_activitie_title')</h4>
					</span>
					<span class="info-box-number">
						@lang('common.total_label')
						<span class="badge bg-default">{{ Edifalia\Models\Common\SupplierActivitie::all()->count() }}</span>
					</span>
				</div>
			</div>
		</div>


		<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
			<div class="info-box" role="button" onclick="location='{{ route('supplier_sector.index') }}'">
				<span class="info-box-icon bg-yellow">
					<i class="fa fa-map"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">
						<h4>@lang('panel_info.box_supplier_sector_title')</h4>
					</span>
					<span class="info-box-number">
						@lang('common.total_label')
						<span class="badge bg-default">{{ Edifalia\Models\Common\SupplierSector::all()->count() }}</span>
					</span>
				</div>
			</div>
		</div>

<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
			<div class="info-box" role="button" onclick="location='{{ route('accounting-account.index') }}'">
				<span class="info-box-icon bg-green">
					<i class="fa  fa-calculator"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">
						<h4>@lang('panel_info.box_accounting_account_title')</h4>
					</span>
					<span class="info-box-number">
						@lang('common.total_label')
						<span class="badge bg-default">{{ Edifalia\Models\Common\AccountingAccount::all()->count() }}</span>
					</span>
				</div>
			</div>
		</div>


		<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
			<div class="info-box" role="button" onclick="location='{{ route('person-contact.index') }}'">
				<span class="info-box-icon bg-green">
					<i class="fa  fa-users"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">
						<h4>@lang('panel_info.box_person_contact_title')</h4>
					</span>
					<span class="info-box-number">
						@lang('common.total_label')
						<span class="badge bg-default">{{ Edifalia\Models\Common\PersonContact::all()->count() }}</span>
					</span>
				</div>
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
			<div class="info-box" role="button" onclick="location='{{ route('currencies.index') }}'">
				<span class="info-box-icon bg-navy">
					<i class="fa fa-dollar"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">
						<h4>@lang('panel_info.box_currency_title')</h4>
					</span>
					<span class="info-box-number">
						@lang('common.total_label')
						<span class="badge bg-default">{{ Edifalia\Models\Currency::all()->count() }}</span>
					</span>
				</div>
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
			<div class="info-box" role="button" onclick="location='{{ route('zones.index') }}'">
				<span class="info-box-icon bg-red">
					<i class="fa fa-map-marker"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">
						<h4>@lang('panel_info.box_zone_title')</h4>
					</span>
					<span class="info-box-number">
						@lang('common.total_label')
						<span class="badge bg-default">{{ Edifalia\Models\Zone::all()->count() }}</span>
					</span>
				</div>
			</div>
		</div>
	</div>
@endsection
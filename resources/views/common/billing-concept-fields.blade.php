<div class="row">
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
			<label class="col-sm-3 control-label" for="name">@lang('forms.name_label')</label>
			<div class="col-sm-8">
				<div class="input-group">
					{!! Form::text(
						'name', (isset($model))?$model->name:null, [
						'class' => 'form-control', 'id' => 'name', 'placeholder' => trans('forms.name_placeholder')
						]
						) !!}
						<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
				</div>
			</div>
			@if ($errors->has('name'))
			<div class="col-sm-8 col-sm-offset-3">
				<span class="help-block">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
			</div>
			@endif
		</div>
		<div class="form-group{{ $errors->has('reduced_title_1') ? ' has-error' : '' }}">
			<label class="col-sm-3 control-label" for="reduced_title_1">@lang('forms.billing_concept_title_1_label')</label>
			<div class="col-sm-8">
				<div class="input-group">
					{!! Form::text(
							'reduced_title_1', (isset($model))?$model->reduced_title_1:null, [
							'class' => 'form-control', 'id' => 'reduced_title_1', 'placeholder' => trans('forms.billing_concept_title_1_placeholder')
							]
							) !!}
							<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
				</div>
			</div>
			@if ($errors->has('reduced_title_1'))
			<div class="col-sm-8 col-sm-offset-3">
				<span class="help-block">
					<strong>{{ $errors->first('reduced_title_1') }}</strong>
				</span>
			</div>
			@endif
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('reduced_title_2') ? ' has-error' : '' }}">
			<label class="col-sm-3 control-label" for="reduced_title_2">@lang('forms.billing_concept_title_2_label')</label>
			<div class="col-sm-8">
				<div class="input-group">
					{!! Form::text(
						'reduced_title_2', (isset($model))?$model->reduced_title_2:null, [
						'class' => 'form-control', 'id' => 'reduced_title_2', 'placeholder' => trans('forms.billing_concept_title_2_placeholder')
						]
						) !!}
						<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
				</div>
			</div>
			@if ($errors->has('reduced_title_2'))
			<div class="col-sm-8 col-sm-offset-3">
				<span class="help-block">
					<strong>{{ $errors->first('reduced_title_2') }}</strong>
				</span>
			</div>
			@endif
		</div>
	</div>
</div>
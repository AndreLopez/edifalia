<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
	<label class="col-sm-3 control-label" for="name">@lang('forms.name_label')</label>
    <div class="col-sm-8">
    	<div class="input-group">
	    	{!! Form::text(
	    		'name', (isset($model))?$model->name:null, [
	    			'class' => 'form-control', 'id' => 'name', 'placeholder' => trans('forms.name_placeholder'),
	    			'maxlength' => '45'
	    		]
	    	) !!}
	    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
	    </div>
    </div>
	@if ($errors->has('name'))
		<div class="col-sm-8 col-sm-offset-3">
	        <span class="help-block">
	            <strong>{{ $errors->first('name') }}</strong>
	        </span>
	    </div>
	@endif
</div>

<div class="form-group{{ $errors->has('acronym') ? ' has-error' : '' }}">
	<label class="col-sm-3 control-label" for="acronym">@lang('forms.acronym_label')</label>
    <div class="col-sm-2">
    	<div class="input-group">
	    	{!! Form::text(
	    		'acronym', (isset($model))?$model->acronym:null, [
	    			'class' => 'form-control', 'id' => 'acronym', 'maxlength'=> '2', 
	    			'placeholder' => trans('forms.acronym_placeholder')
	    		]
	    	) !!}
	    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
    	</div>
    </div>
	@if ($errors->has('acronym'))
		<div class="col-sm-8 col-sm-offset-3">
	        <span class="help-block">
	            <strong>{{ $errors->first('acronym') }}</strong>
	        </span>
	    </div>
	@endif
</div>
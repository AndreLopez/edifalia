<div class="row">
	<div class="col-md-6">

		<div class="form-group{{ $errors->has('community_id') ? ' has-error' : '' }}">
			<label class="col-sm-3 control-label" for="community_id">@lang('forms.community_label')</label>
		    <div class="col-sm-8">
		    	<div class="input-group">
		    	{!!  Form::select('community_id', $communities, ((isset($model->community_id) AND $model->community_id) ? $model->community_id : null ), ['placeholder' => trans('forms.community_placeholder'), 'class' => 'select2']); 
		    	!!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    	</div>
		    </div>
			@if ($errors->has('community_id'))
				<div class="col-sm-8 col-sm-offset-3">
			        <span class="help-block">
			            <strong>{{ $errors->first('community_id') }}</strong>
			        </span>
			    </div>
			@endif
		</div>

		{{--<div class="form-group{{ $errors->has('accounting_account_id') ? ' has-error' : '' }}">
			<label class="col-sm-3 control-label" for="accounting_account_id">@lang('forms.accounting_account_label')</label>
		    <div class="col-sm-8">
		    	<div class="input-group">
		    	{!!  Form::select('accounting_account_id', $accountingAccounts, ((isset($model->accounting_account_id) AND $model->accounting_account_id) ? $model->accounting_account_id : null ), ['placeholder' => trans('forms.accounting_account_placeholder'), 'class' => 'select2']); 
		    	!!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    	</div>
		    </div>
			@if ($errors->has('accounting_account_id'))
				<div class="col-sm-8 col-sm-offset-3">
			        <span class="help-block">
			            <strong>{{ $errors->first('accounting_account_id') }}</strong>
			        </span>
			    </div>
			@endif
		</div>--}}

		<div class="form-group{{ $errors->has('building_number') ? ' has-error' : '' }}">
			<label class="col-sm-3 control-label" for="building_number">@lang('forms.building_number_label')</label>
		    <div class="col-sm-8">
		    	<div class="input-group">
			    	{!! Form::text(
			    		'building_number', (isset($model))?$model->building_number:null, [
			    			'class' => 'form-control', 'id' => 'building_number', 'placeholder' => trans('forms.building_number_placeholder')
			    		]
			    	) !!}
			    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
			    </div>
		    </div>
			@if ($errors->has('building_number'))
				<div class="col-sm-8 col-sm-offset-3">
			        <span class="help-block">
			            <strong>{{ $errors->first('building_number') }}</strong>
			        </span>
			    </div>
			@endif
		</div>

		<div class="form-group{{ $errors->has('registered_property') ? ' has-error' : '' }}">
			<label class="col-sm-3 control-label" for="registered_property">@lang('forms.registered_property_label')</label>
		    <div class="col-sm-8">
		    	<div class="input-group">
			    	{!! Form::text(
			    		'registered_property', (isset($model))?$model->registered_property:null, [
			    			'class' => 'form-control', 'id' => 'registered_property', 'placeholder' => trans('forms.registered_property_placeholder')
			    		]
			    	) !!}
			    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
			    </div>
		    </div>
			@if ($errors->has('registered_property'))
				<div class="col-sm-8 col-sm-offset-3">
			        <span class="help-block">
			            <strong>{{ $errors->first('registered_property') }}</strong>
			        </span>
			    </div>
			@endif
		</div>

		<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
			<label class="col-sm-3 control-label" for="status">@lang('forms.status_label')</label>
		    <div class="col-sm-8">
		    	<div class="input-group">
			    	{!! Form::radio('status', '1', ((isset($model->status) AND
			    	$model->status == 1)),  ['class' => 'iCheck']) !!}Activa
			    	{!! Form::radio('status', '0', ( (isset($model->status) AND
			    	$model->status == 0)),  ['class' => 'iCheck']) !!}Inactiva
			    	<!--<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>-->
			    </div>
		    </div>
			@if ($errors->has('status'))
				<div class="col-sm-8 col-sm-offset-3">
			        <span class="help-block">
			            <strong>{{ $errors->first('status') }}</strong>
			        </span>
			    </div>
			@endif
		</div>

		<div class="form-group{{ $errors->has('group_properties') ? ' has-error' : '' }}">
			<label class="col-sm-3 control-label" for="group_properties">@lang('forms.group_properties_label')</label>
		    <div class="col-sm-8">
		    	<div class="input-group">
			    	{!! Form::radio('group_properties', '1', ( isset($model->group_properties) AND
			    	$model->group_properties == 1),  ['class' => 'iCheck']) !!}Si
			    	{!! Form::radio('group_properties', '0',  ( isset($model->group_properties) AND
			    	$model->group_properties == 0),  ['class' => 'iCheck']) !!}No
			    	<!--<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>-->
			    </div>
		    </div>
			@if ($errors->has('group_properties'))
				<div class="col-sm-8 col-sm-offset-3">
			        <span class="help-block">
			            <strong>{{ $errors->first('group_properties') }}</strong>
			        </span>
			    </div>
			@endif
		</div>
	</div>

	<div class="col-md-6">

		<div class="form-group{{ $errors->has('person_id') ? ' has-error' : '' }}">
			<label class="col-sm-3 control-label" for="person_id">@lang('forms.person_relation_label')</label>
		    <div class="col-sm-8">
		    	<div class="input-group">
		    	{!!  Form::select('person_id', [], ((isset($model->person_id) AND $model->person_id) ? $model->person_id : null ), [
			    		'placeholder' => trans('forms.person_relation_placeholder'), 
			    		'class' => 'select2', 
			    		'id' => 'person-related-to-property'
		    		]
		    		); 
		    	!!}
		    	<span class="input-group-addon">
		    		<i class="fa fa-asterisk input-required"></i>
		    	</span>
		    	</div>
				<a data-toggle="tooltip" class="btn btn-primary" title="Nueva persona" href="#" 
				id="add-new-person">
					<i class="fa fa-plus"></i>
				</a>
		    </div>
			@if ($errors->has('person_id'))
				<div class="col-sm-8 col-sm-offset-3">
			        <span class="help-block">
			            <strong>{{ $errors->first('person_id') }}</strong>
			        </span>
			    </div>
			@endif
		</div>

		<div class="form-group{{ $errors->has('payment_method_id') ? ' has-error' : '' }}">
			<label class="col-sm-3 control-label" for="payment_method_id">@lang('forms.payment_method_label')</label>
		    <div class="col-sm-8">
		    	<div class="input-group">
		    	{!!  Form::select('payment_method_id', $paymentMethods, ((isset($model->payment_method_id) AND $model->payment_method_id) ? $model->payment_method_id : null ), ['placeholder' => trans('forms.payment_method_placeholder'), 'class' => 'select2']); 
		    	!!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    	</div>
		    </div>
			@if ($errors->has('payment_method_id'))
				<div class="col-sm-8 col-sm-offset-3">
			        <span class="help-block">
			            <strong>{{ $errors->first('payment_method_id') }}</strong>
			        </span>
			    </div>
			@endif
		</div>

		<div class="form-group{{ $errors->has('property_number') ? ' has-error' : '' }}">
			<label class="col-sm-3 control-label" for="property_number">@lang('forms.property_number_label')</label>
		    <div class="col-sm-8">
		    	<div class="input-group">
			    	{!! Form::text(
			    		'property_number', (isset($model))?$model->property_number:null, [
			    			'class' => 'form-control', 'id' => 'property_number', 'placeholder' => trans('forms.property_number_placeholder')
			    		]
			    	) !!}
			    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
			    </div>
		    </div>
			@if ($errors->has('property_number'))
				<div class="col-sm-8 col-sm-offset-3">
			        <span class="help-block">
			            <strong>{{ $errors->first('property_number') }}</strong>
			        </span>
			    </div>
			@endif
		</div>

		<div class="form-group{{ $errors->has('property_type_id') ? ' has-error' : '' }}">
			<label class="col-sm-3 control-label" for="property_type_id">@lang('forms.type_label')</label>
		    <div class="col-sm-8">
		    	<div class="input-group">
		    	{!!  Form::select('property_type_id', $propertyTypes, ((isset($model->property_type_id) AND $model->property_type_id) ? $model->property_type_id : null ), ['placeholder' => trans('forms.type_placeholder'), 'class' => 'select2']); 
		    	!!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    	</div>
		    </div>
			@if ($errors->has('property_type_id'))
				<div class="col-sm-8 col-sm-offset-3">
			        <span class="help-block">
			            <strong>{{ $errors->first('property_type_id') }}</strong>
			        </span>
			    </div>
			@endif
		</div>

		<div class="form-group{{ $errors->has('coefficient') ? ' has-error' : '' }}">
			<label class="col-sm-3 control-label" for="coefficient">@lang('forms.coefficient_label')</label>
		    <div class="col-sm-8">
		    	<div class="input-group">
			    	{!! Form::number(
			    		'coefficient', (isset($model)) ? $model->coefficient : 0, [
			    			'class' => 'form-control', 'id' => 'coefficient', 'placeholder' => trans('forms.coefficient_placeholder')
			    		]
			    	) !!}
			    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
			    </div>
		    </div>
			@if ($errors->has('coefficient'))
				<div class="col-sm-8 col-sm-offset-3">
			        <span class="help-block">
			            <strong>{{ $errors->first('coefficient') }}</strong>
			        </span>
			    </div>
			@endif
		</div>

		<div class="form-group{{ $errors->has('group_concepts') ? ' has-error' : '' }}">
			<label class="col-sm-3 control-label" for="group_concepts">@lang('forms.group_concepts_label')</label>
		    <div class="col-sm-8">
		    	<div class="input-group">
			    	{!! Form::radio('group_concepts', '1', (isset($model->group_concepts) AND
			    	$model->group_concepts == 1), ['class' => 'iCheck']) !!}Si
			    	{!! Form::radio('group_concepts', '0',(isset($model->group_concepts) AND 
			    	$model->group_concepts == 0), ['class' => 'iCheck']) !!}No
			    	<!--<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>-->
			    </div>
		    </div>
			@if ($errors->has('group_concepts'))
				<div class="col-sm-8 col-sm-offset-3">
			        <span class="help-block">
			            <strong>{{ $errors->first('group_concepts') }}</strong>
			        </span>
			    </div>
			@endif
		</div>

	</div>

</div>

@include('common.bank-account-fields')
@include('common.accounting-account-fields')


@section('extra_scripts')
	<script type="text/javascript">
		//$(document).ready(function() 
		//{
			$(".select2").select2({ 
				width: '100%',
				theme: "bootstrap"
			});


			//Flat blue color scheme for iCheck
		    $('.iCheck').iCheck({
		      checkboxClass: 'icheckbox_flat-blue',
		      radioClass: 'iradio_flat-blue'
		    });

		    var chk = $("input[type='checkbox']");
            chk.bootstrapSwitch({
            	onColor: 'success',
            	offColor: 'danger',
             	onText: '{{ trans('forms.active_yes') }}',
            	offText: '{{ trans('forms.active_no') }}',
            });

            var addNewFieldsForAccoutingAccount = function()
            {
            	$("#add-other-treasury-account-modal-person").on('click', function() {
            		var treasury_fields = $('.treasury-account-fields-modal-person').last();
            		treasury_fields.after("<div class='row treasury-account-fields-modal-person'>" + treasury_fields.html() + "</div>");
            		var i = 0;
            		$(".remove-accounting-acount-modal-person").each(function() {
            			$(this).on('click', function() {
            				$(this).closest('.treasury-account-fields-modal-person').remove();
            			});
            			if (i > 0) {
            				$(this).removeClass('transparent-font');
            				$(this).attr('role', 'button');
            			}
            			i++;
            		});
            	});

            	$(".remove-accounting-acount-modal-person").on('click', function() {
            		$(this).remove();
            	});
            }

            var addNewFieldsForBankAccouting = function(){
            	$("#add-bank-account-modal-person").on('click', function() {
            		var bank_fields = $('.bank-account-fields-modal-person').last();
            		bank_fields.after("<div class='row bank-account-fields-modal-person'>" + bank_fields.html() + "</div>");
            		var i = 0;
            		$(".remove-bank-acount-modal-person").each(function() {
            			$(this).on('click', function() {
            				$(this).closest('.bank-account-fields-modal-person').remove();
            			});
            			if (i > 0) {
            				$(this).removeClass('transparent-font');
            				$(this).attr('role', 'button');
            			}
            			i++;
            		});
            	});

            	$(".remove-bank-acount-modal-person").on('click', function() {
            		$(this).remove();
            	});
            }

			var listAllPersonsForProperties = function() {
	            $.ajax({
	                type: 'GET',
	                url: '{{ route('api.list-persons-for-properties') }}',
	                dataType:'json',
	                success: function(response) {
	                   	//console.log(response);
	                	var person_id= "{{ isset($person_id) ? $person_id : 0 }}";
	    				console.log("person_id:",person_id);
	                    if (response.success == true) {
                        	jQuery('#person-related-to-property').html('');
	                        jQuery('#person-related-to-property').append('<option value=\"\">{{ trans('forms.person_relation_placeholder') }}</option>');
	                        $.each(response.items, function (k, item){
                            	$('#person-related-to-property').append('<option value=\"'+item.id+'\">'+item.first_name+" "+ item.last_name+'</option>');
                       	 	});
                       	 	if(person_id){
	    						jQuery('#person-related-to-property').val(person_id).trigger("change");
                       	 	}
	                    }else{
	                        jQuery('#person-related-to-property').html('');
	                        jQuery('#person-related-to-property').append('<option value=\"\"></option>');
	                    }
	                }
	            });
	    	}

	    	var addValidationRulesForms = function () 
	    	{

	    		var rules = {
	    			first_name:{
	                    required:true,
	                    rangelength: [1, 75]
                	},
                	last_name:{
	                    required:true,
	                    rangelength: [1, 75]
                	},
                	address:{
                		required: true
                	},
                	dni:{
                		required: true,
                		rangelength: [1, 10]
                	},
                	reference:{
                		required: true,
                		rangelength: [1, 12]
                	},
                	strength_language:{
                		required: true,
                		rangelength: [1,1]
                	},
                	postal_code_id:{
                		required: true,
                		integer: true
                	},
                	language_id:{
                		required: true,
                		integer: true
                	},
                	payment_method_id:{
                		required: true,
                		integer: true
                	},
                	user_id:{
                		required:true,
                		integer: true
                	},
                	'accounting_account_code[]':{
                		required: true,
                		rangelength:[1,15]
                	},
                	'accounting_account_title[]':{
                		required: true,
                		rangelength:[1,45]
                	},
                	'bank_account_accounting_code[]':{
                		required: true,
                		rangelength:[1,15]
                	},
                	'bank_account_iban[]':{
                		required: true,
                		rangelength:[1,4]
                	},
                	'bank_account_code_a[]':{
                		required: true,
                		rangelength:[1,4]
                	},
                	'bank_account_code_b[]':{
                		required: true,
                		rangelength:[1,4]
                	},
                	'bank_account_code_c[]':{
                		required: true,
                		rangelength:[1,4]
                	},
                	'bank_account_code_d[]':{
                		required: true,
                		rangelength:[1,4]
                	},
                	'bank_account_code_e[]':{
                		required: true,
                		rangelength:[1,4]
                	},
                	'bank_account_sufix[]':{
                		required: true,
                		rangelength:[1,4]
                	},
                	'bank_account_bic[]':{
                		required: true,
                		rangelength:[1,4]
                	}
	    		};

	    		var messages = {
	    			first_name:{
                    	required:'Este campo es obligatorio.',
                    	rangelength: 'Por favor ingrese entre [1, 75] caracteres'
                	},
                	last_name:{
	                    required:'Este campo es obligatorio.',
	                    rangelength: 'Por favor ingrese entre [1, 75] caracteres'
                	},
                	address:{
                		required:'Este campo es obligatorio.',
                	},
                	dni:{
                		required:'Este campo es obligatorio.',
                	},
                	reference:{
                		required:'Este campo es obligatorio.',
                	},
                	strength_language:{
                		required:'Este campo es obligatorio.',
                	},
                	message_email_only:{
                		integer: 'El valor debe ser un numero entero',
                	},
                	group_properties:{
                		integer: 'El valor debe ser un numero entero',
                	},
                	group_invoice_concepts:{
                		integer: 'El valor debe ser un numero entero',
                	},
                	dont_get_email:{
                		integer: 'El valor debe ser un numero entero',
                	},
                	can_update_own_data:{
                		integer: 'El valor debe ser un numero entero',
                	},
                	exclusiveness: {
                		integer: 'El valor debe ser un numero entero',
                	},
                	virtual_office_access: {
                		integer: 'El valor debe ser un numero entero',
                	},
                	update_data_from_virtual_office:{
                		integer: 'El valor debe ser un numero entero',
                	},
                	postal_code_id:{
                		required:'Este campo es obligatorio.',
                		integer: 'El valor debe ser un numero entero'
                	},
                	language_id:{
                		required:'Este campo es obligatorio.',
                		integer: 'El valor debe ser un numero entero'
                	},
                	payment_method_id:{
                		required:'Este campo es obligatorio.',
                		integer: 'El valor debe ser un numero entero'
                	},
                	user_id:{
                		required:'Este campo es obligatorio.',
                		integer: 'El valor debe ser un numero entero'
                	},
                	'accounting_account_code[]':{
                		required:'Este campo es obligatorio.',
                		rangelength: 'Por favor ingrese entre [1, 15] caracteres'
                	},
                	'accounting_account_title[]':{
                		required:'Este campo es obligatorio.',
                		rangelength: 'Por favor ingrese entre [1, 45] caracteres'
                	},
                	'bank_account_accounting_code[]':{
                		required:'Este campo es obligatorio.',
                		rangelength: 'Por favor ingrese entre [1, 15] caracteres'
                	},
                	'bank_account_iban[]':{
                		required:'Este campo es obligatorio.',
                		rangelength: 'Por favor ingrese entre [1, 4] caracteres'
                	},
                	'bank_account_code_a[]':{
                		required:'Este campo es obligatorio.',
                		rangelength: 'Por favor ingrese entre [1, 4] caracteres'
                	},
                	'bank_account_code_b[]':{
                		required:'Este campo es obligatorio.',
                		rangelength: 'Por favor ingrese entre [1, 4] caracteres'
                	},
                	'bank_account_code_c[]':{
                		required:'Este campo es obligatorio.',
                		rangelength: 'Por favor ingrese entre [1, 4] caracteres'
                	},
                	'bank_account_code_d[]':{
                		required:'Este campo es obligatorio.',
                		rangelength: 'Por favor ingrese entre [1, 4] caracteres'
                	},
                	'bank_account_code_e[]':{
                		required:'Este campo es obligatorio.',
                		rangelength: 'Por favor ingrese entre [1, 4] caracteres'
                	},
                	'bank_account_sufix[]':{
                		required:'Este campo es obligatorio.',
                		rangelength: 'Por favor ingrese entre [1, 4] caracteres'
                	},
                	'bank_account_bic[]':{
                		required:'Este campo es obligatorio.',
                		rangelength: 'Por favor ingrese entre [1, 4] caracteres'
                	}
	    		};

	    		$('#person-form-modal').validate({
	    			rules,
	    			messages,
	    			highlight:function(element){
		                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		            },
		            unhighlight:function(element){
		                $(element).closest('.form-group').removeClass('has-error');
		            },
		            success:function(element){
		                element.addClass('valid').closest('.form-group').removeClass('has-error').addClass('has-success');
		            }
	    		});
	    	}

	    	var handleBootboxNewPerson = function()
	    	{
	    		addValidationRulesForms();
	    		// Mostrar formulario para agregar nuevo jugador
		        $('#add-new-person').on('click', function(e)
		        {
		        	e.preventDefault();
			    	$("#person-form-modal").trigger("reset");
			    	bootbox
			        	.dialog(
			            {
			            	size: "large",
			            	message: $('#person-form-div'),
			                buttons:
			                {
			                	success:
			                	{
			                    	label: "Guardar",
			                        className: "btn-primary",
			                        callback: function ()
			                        {
			                        	var forData = $('#person-form-modal').serialize();
			                        	var url = $('#person-form-modal').attr('action');

			                        	if($('#person-form-modal').valid()) 
			                        	{
			                        		$.post(url, forData, function(response) {
										  		//console.log(response);
										  		if(response.success) 
										  		{
										  			bootbox.dialog({
                                                    	message:"Datos agregados correctamente!",
                                                        title: "Éxito",
                                                        buttons: {
                                                            success: {
                                                                label: "Success!",
                                                                className: "btn-success",
                                                                callback: function () {
                                                                	listAllPersonsForProperties();
										  							jQuery('#person-related-to-property').val(response.person.id).trigger("change");
										  							$("#person-form-modal").trigger("reset");
                                                                }
                                                            }
                                                        }
                                                    });
										  		}
											}).fail(function(response) {

												var errors = $.parseJSON(response.responseText);
												var out = '<ul>';
    											for (var fieldAttr in errors) {
								                    var errorMgs = errors[fieldAttr];
								                    for (var msg in errorMgs) {
								                        out += "<li>" +errorMgs[msg]+ "</li>";
								                    }
								                }
								                out += "</ul>";

												bootbox.dialog({
                                                	message: out,
                                                    title: "Error",
                                                   	buttons: {
                                                    	danger: {
                                                        	label: "Danger!",
                                                            className: "btn-danger",
                                                            callback: function () {
                                                           		return true;
                                                            }
                                                        }
                                                    }
                                                });
  											});
			                        	}else{
			                        		return false;
			                        	}
		                                return false;
			                        }
			                    }
			               	},
			                show: false // We will show it manually later
			            })
			            .on('shown.bs.modal', function() {
			            	$('#person-form-div').show();  // Show the form
			            })
				        .on('hide.bs.modal', function(e) {
				        	$('#person-form-div').hide().appendTo('#new-person-form');
				        })
			            .modal('show');
		        });
	    	}

	    	listAllPersonsForProperties();
	    	handleBootboxNewPerson();
	    	addNewFieldsForAccoutingAccount();
	    	addNewFieldsForBankAccouting();
		//});
	</script>
@endsection
<table class="table table-hover">
	<thead>
		<tr class="info">
			<th>@lang('forms.code_label')</th>
			<th>@lang('forms.country_label')</th>
			<th>@lang('forms.town_label')</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>{{ $postal_code->code }}</td>
			<td>{{ $postal_code->country->name }}</td>
			<td>{{ $postal_code->town }}</td>
		</tr>
	</tbody>
</table>
<table class="table table-hover">
	<thead>
		<tr class="info">
			<th>@lang('forms.province_label')</th>
			<th>@lang('forms.region_label')</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>{{ $postal_code->province }}</td>
			<td>{{ $postal_code->region }}</td>
		</tr>
	</tbody>
</table>
<section class="content" id="person-form-div">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">{{ trans('person.new_title') }}</h3>
				</div>

				{{ Form::open(['route' => 'api.person.store','class'=>'form-horizontal','role'=>'form',
				'method' => 'POST','files' => true, 'id'=> 'person-form-modal']) }}
					<div class="box-body">
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="text">
										@lang('forms_persons.first_name_title')
									</label>
									<div class="col-sm-8 controls">
										<div class="input-group">
											{!! Form::text(
												'first_name', null, [
												'class' => 'form-control', 'id' => 'first_name', 
												'placeholder' => trans('forms_persons.first_name_placeholder')
												]
												) 
											!!}
											<span class="input-group-addon">
												<i class="fa fa-asterisk input-required"></i>
											</span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="text">
										@lang('forms_persons.last_name_title')
									</label>
									<div class="col-sm-8 controls">
										<div class="input-group">
											{!! Form::text('last_name', null, [
												'class' => 'form-control', 'id' => 'last_name', 
												'placeholder' => trans('forms_persons.last_name_placeholder')
												]
												) 
											!!}
											<span class="input-group-addon">
												<i class="fa fa-asterisk input-required"></i>
											</span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="text">
										@lang('forms_persons.address_placeholder')
									</label>
									<div class="col-sm-8 controls">
										<div class="input-group">
											  {!! Form::textarea('address', null, [
												'class' => 'form-control', 'id' => 'address', 'rows' => '2',
												'placeholder' => trans('forms_persons.address_placeholder')
											]) !!}
											<span class="input-group-addon">
												<i class="fa fa-asterisk input-required"></i>
											</span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="text">
										@lang('forms_persons.dni_title')
									</label>
									<div class="col-sm-8 controls">
										<div class="input-group">
											{!! Form::text('dni', null, [
												'class' => 'form-control', 'id' => 'dni', 
												'placeholder' => trans('forms_persons.dni_placeholder')
												]
												) !!}
												<span class="input-group-addon">
													<i class="fa fa-asterisk input-required"></i>
												</span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="text">
										@lang('forms_persons.reference_title')
									</label>
									<div class="col-sm-8 controls">
										<div class="input-group">
											{!! Form::text('reference', null, [
												'class' => 'form-control', 'id' => 'reference', 
												'placeholder' => trans('forms_persons.reference_placeholder')
												]
												) !!}
												<span class="input-group-addon">
													<i class="fa fa-asterisk input-required"></i>
												</span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="text">
										@lang('forms_persons.strength_language_title')
									</label>
									<div class="col-sm-8 controls">
										<div class="input-group">
											{!! Form::text('strength_language', null, [
												'class' => 'form-control', 'id' => 'strength_language', 
												'placeholder' => trans('forms_persons.strength_language_placeholder')
												]
												) !!}
												<span class="input-group-addon">
													<i class="fa fa-asterisk input-required"></i>
												</span>
										</div>
									</div>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="text">
										@lang('forms_persons.notes_title')
									</label>
									<div class="col-sm-8 controls">
										<div class="input-group">
											{!! Form::textarea('notes', null, [
												'class' => 'form-control', 'id' => 'notes', 'rows' => '2',
												'placeholder' => trans('forms_persons.notes_placeholder')
											]) !!}
												<span class="input-group-addon">
													<i class="fa fa-asterisk input-required"></i>
												</span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="text">
										@lang('forms.postal_code_label')
									</label>
									<div class="col-sm-8 controls">
										<div class="input-group">
											{!! Form::select(
													'postal_code_id', 
													isset($postalCodes) ? $postalCodes : [], null, 
													[
														'id' => 'postal_code_id', 'class' => 'select2', 
														'placeholder' => trans('forms_supplier.select_null_option'),
													]
												) 
											!!}
												<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="text">
										@lang('forms_contracts.payment_method_title')
									</label>
									<div class="col-sm-8 controls">
										<div class="input-group">
											{!! Form::select(
													'payment_method_id', 
													isset($paymentMethods) ? $paymentMethods : [], null, 
													[
														'id' => 'payment_method_id', 'class' => 'select2', 
														'placeholder' => trans('forms_supplier.select_null_option'),
													]
												) 
											!!}
												<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="text">
										@lang('forms_persons.user_id_title')
									</label>
									<div class="col-sm-8 controls">
										<div class="input-group">
											{!! Form::select(
													'user_id', 
													isset($users) ? $users : [], null, 
													[
														'id' => 'user_id', 'class' => 'select2', 
														'placeholder' => trans('forms_supplier.select_null_option'),
													]
												) 
											!!}
												<span class="input-group-addon">
													<i class="fa fa-asterisk input-required"></i>
												</span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="text">
										@lang('forms_persons.language_title')
									</label>
									<div class="col-sm-8 controls">
										<div class="input-group">
											{!! Form::select(
													'language_id', 
													isset($languages) ? $languages : [], null, 
													[
														'id' => 'language_id', 'class' => 'select2', 
														'placeholder' => trans('forms_supplier.select_null_option'),
													]
												) 
											!!}
												<span class="input-group-addon">
													<i class="fa fa-asterisk input-required"></i>
												</span>
										</div>
									</div>
								</div>
							</div>
						</div>

						<hr>
						<div class="row">
							<div class="col-sm-12">
								<h4>
									@lang('forms.other_treasury_accounts')
									&#160;<i class="fa fa-plus-circle text-primary" role="button" id="add-other-treasury-account-modal-person" 
									title="{{ trans('tooltips.add_accounting_account') }}" data-toggle="tooltip"></i>
								</h4>
							</div>
						</div>

						<div class="row treasury-account-fields-modal-person">
							<div class="col-sm-12">
								<div class="col-sm-4 form-group">
									<label for="" class="col-sm-12 text-center">
										@lang('forms.accounting_account_label')
									</label>

									<div class="col-sm-12">
										<input type="text" name="accounting_account_code[]" id="accounting_account_code[]" 
										class="form-control input-sm" placeholder="{{ trans('forms.accounting_account_placeholder') }}">
									</div>
								</div>
								<div class="col-sm-7 form-group">
									<label for="" class="col-sm-12 text-center">
										@lang('forms.account_title_label')
									</label>
									<div class="col-sm-12">
										<div class="input-group">
											<input type="text" name="accounting_account_title[]" id="accounting_account_title[]" 
											class="form-control input-sm input-accounting-title" 
											placeholder="{{ trans('forms.account_title_placeholder') }}">

											<span class="input-group-addon">
												<i class="fa fa-minus input-required remove-accounting-acount-modal-person transparent-font"></i>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>

						<hr>
						<div class="row">
							<div class="col-sm-12">
								<h4>
									@lang('forms.bank_accounts_label')
									&#160;<i class="fa fa-plus-circle text-primary" role="button" id="add-bank-account-modal-person" 
									title="{{ trans('tooltips.add_bank_account') }}" data-toggle="tooltip"></i>
								</h4>
							</div>
						</div>

					

						<div class="row bank-account-fields-modal-person">
							<div class="col-sm-12">
								<div class="col-sm-3 form-group">

									<label for="" class="col-sm-12 text-center">
										@lang('forms.accounting_account_label')
									</label>

									<div class="col-sm-10">

										<input type="text" name="bank_account_accounting_code[]" 
										id="bank_account_accounting_code[]" 
										class="form-control input-sm" 
										placeholder="{{trans('forms.accounting_account_placeholder')}}">

									</div>

								</div>
								<div class="col-sm-8 form-group">
									<label for="" class="col-sm-12 text-center">
										@lang('forms.bank_account_sepa_label')
									</label>
									<div class="col-sm-2">

										<input type="text" name="bank_account_iban[]" id="bank_account_iban[]" 
										class="form-control input-sm" 
										maxlength="4" placeholder="{{trans('forms.iban_placeholder')}}">

									</div>
									<div class="col-sm-2">
										<input type="text" name="bank_account_code_a[]" id="bank_account_code_a[]" 
										maxlength="4" class="form-control input-sm">
									</div>
									<div class="col-sm-2">
										<input type="text" name="bank_account_code_b[]" id="bank_account_code_a[]" 
							   maxlength="4" class="form-control input-sm">
									</div>
									<div class="col-sm-2">
										<input type="text" name="bank_account_code_c[]" id="bank_account_code_a[]" 
							   maxlength="4" class="form-control input-sm">
									</div>
									<div class="col-sm-2">
										<input type="text" name="bank_account_code_d[]" id="bank_account_code_a[]" 
							   maxlength="4" class="form-control input-sm">
									</div>
									<div class="col-sm-2">
										<input type="text" name="bank_account_code_e[]" id="bank_account_code_a[]" 
							   maxlength="4" class="form-control input-sm">
									</div>
								</div>
								<div class="col-sm-4">
									<div class="col-sm-6 form-group">
										<label for="" class="col-sm-12 text-center">
											@lang('forms.sufix_label')
										</label>
										<div class="col-sm-12">
											<input type="text" name="bank_account_sufix[]" id="bank_account_sufix[]" 
								   maxlength="4" class="form-control input-sm">
										</div>
									</div>
									<div class="col-sm-6 form-group">
										<label for="" class="col-sm-12 text-center">
											@lang('forms.bic_label')
										</label>
										<div class="input-group">
											<input type="text" name="bank_account_bic[]" id="bank_account_bic[]" maxlength="4" 
								   class="form-control input-sm">
											<span class="input-group-addon">
												<i class="fa fa-minus input-required remove-bank-acount-modal-person transparent-font"></i>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-6 form-group">
								<div class="checkbox">
									<label>
										{!! Form::checkbox('message_email_only', True, False, ['data-size' => 'mini']) !!}
										@lang('forms_persons.message_email_only_title')
									</label>
								</div>
							</div>
							<div class="col-sm-6 form-group">
								<div class="checkbox">
									<label>
										{!! Form::checkbox('group_properties', True, False, ['data-size' => 'mini']) !!}
										@lang('forms_persons.group_properties_title')
									</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6 form-group">
								<div class="checkbox">
									<label>
										{!! Form::checkbox('group_invoice_concepts', True, False, ['data-size' => 'mini']) !!}
										@lang('forms_persons.group_invoice_concepts_title')
									</label>
								</div>
							</div>
							<div class="col-sm-6 form-group">
								<div class="checkbox">
									<label>
										{!! Form::checkbox('dont_get_email', True, False, ['data-size' => 'mini']) !!}
										@lang('forms_persons.dont_get_email_title')
									</label>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-6 form-group">
								<div class="checkbox">
									<label>
										{!! Form::checkbox('exclusiveness', True, False, ['data-size' => 'mini']) !!}
										@lang('forms_persons.exlusiveness_title')
									</label>
								</div>
							</div>
							<div class="col-sm-6 form-group">
								<div class="checkbox">
									<label>
										{!! Form::checkbox('can_update_own_data', True, False, ['data-size' => 'mini']) !!}
										@lang('forms_persons.can_update_own_data_title')
									</label>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-6 form-group">
								<div class="checkbox">
									<label>
										{!! Form::checkbox('virtual_office_access', True, False, ['data-size' => 'mini']) !!}
										@lang('forms_persons.virtual_office_access_title')
									</label>
								</div>
							</div>
							<div class="col-sm-6 form-group">
								<div class="checkbox">
									<label>
										{!! Form::checkbox('update_data_from_virtual_office', True, False, ['data-size' => 'mini']) !!}
										@lang('forms_persons.update_data_from_virtual_office_title')
									</label>
								</div>
							</div>
						</div>
					</div>
				{{ Form::close() }}
			</div>
			<!-- /.box -->
		</div>
	</div>
</section>
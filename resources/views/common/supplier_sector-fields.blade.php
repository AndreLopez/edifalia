<div class="row">
	<div class="col-sm-6 form-group{{ $errors->has('supplier_id') ? ' has-error' : '' }}">
		<label for="name" class="col-sm-3 control-label">@lang('common.supplier_sector_id')</label>
		<div class="col-sm-7">
			<div class="input-group">
		    	{!! Form::select(
		    		'supplier_id', $suppliers, null, [
		    			'id' => 'supplier_id', 'class' => 'select2', 
		    			'placeholder' => trans('common.supplier_sector_sector_id'),
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('supplier_id'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('supplier_id') }}</strong>
		        </span>
		    </div>
		@endif
	</div>




<div class="row">
	<div class="col-sm-6 form-group{{ $errors->has('sector_id') ? ' has-error' : '' }}">
		<label for="name" class="col-sm-3 control-label">@lang('common.supplier_sector_sector_id')</label>
		<div class="col-sm-7">
			<div class="input-group">
		   	{!! Form::select(
		    		'sector_id', $suppliers_activities, null, [
		    			'id' => 'sector_id', 'class' => 'select2', 
		    			'placeholder' => trans('forms_supplier.select_null_option'),
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('sector_id'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('sector_id') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

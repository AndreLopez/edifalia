<div class="row">
	<div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('iban') ? ' has-error' : '' }}">
		<label class="col-sm-3 control-label" for="text">@lang('forms_supplier.iban_label')</label>
		<div class="col-sm-5">
			<div class="input-group">
		    	{!! Form::text(
		    		'iban', (isset($model))?$model->iban:null, [
		    			'class' => 'form-control', 'id' => 'iban', 
		    			'placeholder' => trans('forms_supplier.iban_placeholder')
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('iban'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('iban') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
<div class="col-sm-6 form-group{{ $errors->has('number') ? ' has-error' : '' }}">
		<label class="col-sm-3 control-label" for="status_id">@lang('forms_supplier.number_label')</label>
	    <div class="col-sm-9">
	    	<div class="input-group">
	    	{!! Form::text(
		    		'number', (isset($model))?$model->number:null, [
		    			'class' => 'form-control', 'id' => 'number', 
		    			'placeholder' => trans('forms_supplier.number_placeholder')
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
	    </div>
		@if ($errors->has('number'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('number') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>

<div class="row">
	<div class="col-sm-6 form-group{{ $errors->has('sufix') ? ' has-error' : '' }}">
		<label for="name" class="col-sm-3 control-label">@lang('forms_supplier.sufix_label')</label>
		<div class="col-sm-9">
			<div class="input-group">
		    	{!! Form::text(
		    		'sufix', (isset($model))?$model->sufix:null, [
		    			'class' => 'form-control', 'id' => 'sufix', 
		    			'placeholder' => trans('forms_supplier.sufix_placeholder')
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('sufix'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('sufix') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6 form-group{{ $errors->has('bic') ? ' has-error' : '' }}">
		<label for="nif" class="col-sm-3 control-label">@lang('forms_supplier.bic_label')</label>
		<div class="col-sm-9">
			<div class="input-group">
		    	{!! Form::text(
		    		'bic', (isset($model))?$model->bic:null, [
		    			'class' => 'form-control', 'id' => 'bic',
		    			'placeholder' => trans('forms_supplier.bic_placeholder')
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('bic'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('bic') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>
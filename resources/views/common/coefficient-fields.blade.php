<div class="row">
	<div class="col-sm-6">
		<div class="form-group{{ $errors->has('community_id') ? ' has-error' : '' }}">
			<label class="col-sm-3 control-label" for="community_id">@lang('forms.community_select_label')</label>
		    <div class="col-sm-8">
		    	<div class="input-group">
			    	{!! Form::text(
			    		'community_name', $community->name, [
			    			'class' => 'form-control', 'id' => 'community_name',
			    			'readonly' => 'readonly'
			    		]
			    	) !!}
			    	{!! Form::hidden('community_id', $community->id) !!}
		    	</div>
		    </div>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group{{ $errors->has('coefficent_total') ? ' has-error' : '' }}">
			<label class="col-sm-3 control-label" for="coefficent_total">@lang('forms.coefficient_total_label')</label>
		    <div class="col-sm-4">
		    	<div class="input-group">
			    	{!! Form::number(
			    		'coefficent_total', $coefficient_total, [
			    			'class' => 'form-control', 'id' => 'coefficent_total', 'min' => '0', 
			    			'max' => '100', 'placeholder' => '0', 'step' => '0.01'
			    		]
			    	) !!}
			    	<span class="input-group-addon"><i class="fa fa-percent"></i></span>
			    	<span class="input-group-addon">
			    		<i id="calc_coefficient" class="fa fa-calculator" role="button" data-toggle="tooltip" 
			    		   title="@lang('forms.coefficient_calc_tooltip')"></i>
			    	</span>
		    	</div>
		    </div>
			@if ($errors->has('coefficent_total'))
				<div class="col-sm-8 col-sm-offset-3">
			        <span class="help-block">
			            <strong>{{ $errors->first('coefficent_total') }}</strong>
			        </span>
			    </div>
			@endif
		</div>
	</div>
</div>
<hr>
<div class="col-sm-12"><h4>@lang('forms.properties_label')</h4></div>
<div class="col-sm-12">
	<div class="row form-group">
		<div class="col-sm-8">
			<label class="control-label">@lang('forms.property_label')</label>
		</div>
		<div class="col-sm-4">
			<label class="control-label">@lang('forms.coefficient_individual')</label>
		</div>
	</div>

	@foreach ($properties as $property)
		<div class="row form-group{{ $errors->has('coefficient_property.'.$property->id) ? ' has-error' : '' }}">
			<div class="col-sm-8">{{ $property->property_number }}</div>
			<div class="col-sm-2">
				<div class="input-group">
			    	<input type="number" name="coefficient_propiedad[{{$property->id}}]" 
			    		   id="coefficient_propiedad[{{$property->id}}]" 
			    		   class="form-control input-sm coefficient_individual" 
			    		   placeholder="0" min="0" max="100", step="0.01" value="{{$property->coefficient}}">
			    	<span class="input-group-addon"><i class="fa fa-percent"></i></span>
			    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    	</div>
			    @if ($errors->has('coefficent_propiedad.'.$property->id))
					<div class="col-sm-12">
				        <span class="help-block">
				            <strong>{{ $errors->first('coefficent_propiedad.'.$property->id) }}</strong>
				        </span>
				    </div>
				@endif
			</div>
		</div>
	@endforeach
		
</div>

@section('extra_scripts')
	@parent
	<script>
		$(document).ready(function() {
			$("#calc_coefficient").on('click', function() {
				var $count_properties = $('.coefficient_individual').length, 
					$total_coefficient = $('#coefficent_total').val(), 
					$coefficient = $total_coefficient / $count_properties;

				$('.coefficient_individual').each(function() {
					$(this).val($coefficient);
				});
			});

			$('.coefficient_individual').on('change', function() {
				var total = 0;
				$('.coefficient_individual').each(function() {
					total += parseFloat($(this).val());
				});
				$("#coefficent_total").val(parseFloat(total));
			});
		});
	</script>
@endsection
{{-- Campos para detalles de la(s) cuenta(s) bancaria(s) --}}
<hr>
<div class="row">
	<div class="col-sm-12">
		<h4>
			@lang('forms.bank_accounts_label')
			&#160;<button id="btnadd" class="btn btn-primary">Agregar Nuevo</button>
		</h4>
	</div>
</div>
@if(!is_null(old('bank_account_accounting_code')))
	@foreach(old('bank_account_accounting_code') as $key => $value)
		<div class="row bank-account-fields">
			<div class="col-sm-12">
				<div class="col-sm-3 form-group{{ $errors->has('bank_account_accounting_code.'.$key) ? ' has-error' : '' }}">
					<label for="" class="col-sm-12 text-center">
						@lang('forms.accounting_account_label')
					</label>
					<div class="col-sm-12">
						<input type="text" name="bank_account_accounting_code[{{$key}}]" 
							   id="bank_account_accounting_code[{{$key}}]" 
							   class="form-control input-sm" 
							   placeholder="{{trans('forms.accounting_account_placeholder')}}">
					</div>
				</div>
				<div class="col-sm-6 form-group{{ ($errors->has('bank_account_iban.'.$key) || $errors->has('bank_account_code_a.'.$key) || $errors->has('bank_account_code_b.'.$key) || $errors->has('bank_account_code_c.'.$key) || $errors->has('bank_account_code_d.'.$key) || $errors->has('bank_account_code_e.'.$key)) ? ' has-error' : '' }}">
					<label for="" class="col-sm-12 text-center">
						@lang('forms.bank_account_sepa_label')
					</label>
					<div class="col-sm-2">
						<input type="text" name="bank_account_iban[{{$key}}]" id="bank_account_iban[{{$key}}]" 
							   class="form-control input-sm" 
							   maxlength="4" placeholder="{{trans('forms.iban_placeholder')}}">
					</div>
					<div class="col-sm-2">
						<input type="text" name="bank_account_code_a[{{$key}}]" id="bank_account_code_a[{{$key}}]" 
							   maxlength="4" class="form-control input-sm">
					</div>
					<div class="col-sm-2">
						<input type="text" name="bank_account_code_b[{{$key}}]" id="bank_account_code_b[{{$key}}]" 
							   maxlength="4" class="form-control input-sm">
					</div>
					<div class="col-sm-2">
						<input type="text" name="bank_account_code_c[{{$key}}]" id="bank_account_code_c[{{$key}}]" 
							   maxlength="4" class="form-control input-sm">
					</div>
					<div class="col-sm-2">
						<input type="text" name="bank_account_code_d[{{$key}}]" id="bank_account_code_d[{{$key}}]" 
							   maxlength="4" class="form-control input-sm">
					</div>
					<div class="col-sm-2">
						<input type="text" name="bank_account_code_e[{{$key}}]" id="bank_account_code_e[{{$key}}]" 
							   maxlength="4" class="form-control input-sm">
					</div>
				</div>
				<div class="col-sm-3">
					<div class="col-sm-6 form-group{{ $errors->has('bank_account_sufix.'.$key) ? ' has-error' : '' }}">
						<label for="" class="col-sm-12 text-center">
							@lang('forms.sufix_label')
						</label>
						<div class="col-sm-12">
							<input type="text" name="bank_account_sufix[{{$key}}]" 
								   id="bank_account_sufix[{{$key}}]" 
								   maxlength="4" class="form-control input-sm">
						</div>
					</div>
					<div class="col-sm-6 form-group{{ $errors->has('bank_account_bic.'.$key) ? ' has-error' : '' }}">
						<label for="" class="col-sm-12 text-center">
							@lang('forms.bic_label')
						</label>
						<div class="input-group">
							<input type="text" name="bank_account_bic[{{$key}}]" 
								   id="bank_account_bic[{{$key}}]" maxlength="4" 
								   class="form-control input-sm">
					    	<span class="input-group-addon">
					    		<i class="fa fa-minus input-required remove-bank-acount @if($key == 0) transparent-font @endif"></i>
					    	</span>
					    </div>
					</div>
				</div>
			</div>
		</div>
	@endforeach
@else
	<div class="row bank-account-fields">
		<div class="col-sm-12">
				<div class="col-sm-3 form-group">
					<label for="" class="col-sm-12 text-center">
						@lang('forms.accounting_account_label')
					</label>
					<div class="col-sm-12">
						<input type="text" name="bank_account_accounting_code[]" 
							   id="bank_account_accounting_code[]" 
							   class="form-control input-sm" 
							   placeholder="{{trans('forms.accounting_account_placeholder')}}">
					</div>
				</div>
				<div class="col-sm-6 form-group">
					<label for="" class="col-sm-12 text-center">
						@lang('forms.bank_account_sepa_label')
					</label>
					<div class="col-sm-2">
						<input type="text" name="bank_account_iban[]" id="bank_account_iban[]" 
							   class="form-control input-sm" 
							   maxlength="4" placeholder="{{trans('forms.iban_placeholder')}}">
					</div>
					<div class="col-sm-2">
						<input type="text" name="bank_account_code_a[]" id="bank_account_code_a[]" 
							   maxlength="4" class="form-control input-sm">
					</div>
					<div class="col-sm-2">
						<input type="text" name="bank_account_code_b[]" id="bank_account_code_b[]" 
							   maxlength="4" class="form-control input-sm">
					</div>
					<div class="col-sm-2">
						<input type="text" name="bank_account_code_c[]" id="bank_account_code_c[]" 
							   maxlength="4" class="form-control input-sm">
					</div>
					<div class="col-sm-2">
						<input type="text" name="bank_account_code_d[]" id="bank_account_code_d[]" 
							   maxlength="4" class="form-control input-sm">
					</div>
					<div class="col-sm-2">
						<input type="text" name="bank_account_code_e[]" id="bank_account_code_e[]" 
							   maxlength="4" class="form-control input-sm">
					</div>
				</div>
				<div class="col-sm-3">
					<div class="col-sm-6 form-group">
						<label for="" class="col-sm-12 text-center">
							@lang('forms.sufix_label')
						</label>
						<div class="col-sm-12">
							<input type="text" name="bank_account_sufix[]" id="bank_account_sufix[]" 
								   maxlength="4" class="form-control input-sm">
						</div>
					</div>
					<div class="col-sm-6 form-group">
						<label for="" class="col-sm-12 text-center">
							@lang('forms.bic_label')
						</label>
						<div class="input-group">
							<input type="text" name="bank_account_bic[]" id="bank_account_bic[]" maxlength="4" 
								   class="form-control input-sm">
					    	<span class="input-group-addon">
					    		<i class="fa fa-minus input-required remove-bank-acount transparent-font"></i>
					    	</span>
					    </div>
					</div>
				</div>
		</div>
	</div>
@endif

@section('extra_scripts')
	@parent
	<script>
		//$(document).ready(function() {
			$("#add-bank-account").on('click', function() {
				var bank_fields = $('.bank-account-fields').last();
				bank_fields.after("<div class='row bank-account-fields'>" + bank_fields.html() + "</div>");
				var i = 0;
				$(".remove-bank-acount").each(function() {
					$(this).on('click', function() {
						$(this).closest('.bank-account-fields').remove();
					});
					if (i > 0) {
						$(this).removeClass('transparent-font');
						$(this).attr('role', 'button');
					}
					i++;
				});
			});

			$(".remove-bank-acount").on('click', function() {
				$(this).remove();
			});
		//});
	</script>
@endsection
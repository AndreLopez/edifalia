<div class="row">
	<div class="col-sm-6 form-group{{ $errors->has('contact') ? ' has-error' : '' }}">
		<label for="name" class="col-sm-3 control-label">@lang('common.contact_name_label')</label>
		<div class="col-sm-7">
			<div class="input-group">
		    	{!! Form::text(
		    		'contact', (isset($model))?$model->contact:null, [
		    			'class' => 'form-control', 'id' => 'contact', 
		    			'placeholder' => trans('common.contact_name_placeholder')
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('contact'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('contact') }}</strong>
		        </span>
		    </div>
		@endif
	</div>


<div class="col-sm-6 form-group{{ $errors->has('contact_type_id') ? ' has-error' : '' }}">
		<label for="contact_type_id" class="col-sm-3 control-label">@lang('common.contact_type_label')</label>
		<div class="col-sm-7">
			<div class="input-group">
	    		{!! Form::select(
		    		'contact_type_id', $contacts_type, null, [
		    			'id' => 'contact_type_id', 'class' => 'select2', 
		    			'placeholder' => trans('forms_supplier.select_null_option'),
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('contact_type_id'))
			<div class="col-sm-9 col-sm-offset-3	">
		        <span class="help-block">
		            <strong>{{ $errors->first('contact_type_id') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>

<div class="row">
	<div class="col-sm-6 form-group{{ $errors->has('supplier_id') ? ' has-error' : '' }}">
		<label for="name" class="col-sm-3 control-label">@lang('common.supplier_id_label')</label>
		<div class="col-sm-7">
			<div class="input-group">
		   	{!! Form::select(
		    		'supplier_id', $suppliers, null, [
		    			'id' => 'supplier_id', 'class' => 'select2', 
		    			'placeholder' => trans('forms_supplier.select_null_option'),
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('supplier_id'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('supplier_id') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

<div class="col-sm-6 form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
		<label for="notes" class="col-sm-3 control-label">@lang('common.notes_label')</label>
		<div class="col-sm-7">
			{!! Form::textarea('notes', null, [
				'class' => 'form-control', 'id' => 'notes', 'rows' => '3',
				'placeholder' => trans('forms_supplier.notes_placeholder')
			]) !!}
		</div>
		@if ($errors->has('notes'))
			<div class="col-sm-9 col-sm-offset-2">
		        <span class="help-block">
		            <strong>{{ $errors->first('notes') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>

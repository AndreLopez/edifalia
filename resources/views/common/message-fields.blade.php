@include('common.filter-communities-fields')

<hr>
<div class="row">
	<div class="col-sm-12">
		<h4>
			Campos para enviar email.
		</h4>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('priority') ? ' has-error' : '' }}">
			<label class="col-sm-3 control-label" for="priority">@lang('forms.community_label')</label>
			<div class="col-sm-8">
				<div class="input-group">
					{!!  Form::select('priority', $priorities, ((isset($model->priority) AND $model->priority) ? $model->priority : null ), ['placeholder' => 'Prioridad', 'class' => 'select2']); 
					!!}
					<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
				</div>
			</div>
			@if ($errors->has('priority'))
			<div class="col-sm-8 col-sm-offset-3">
				<span class="help-block">
					<strong>{{ $errors->first('priority') }}</strong>
				</span>
			</div>
			@endif
		</div>
		<div class="form-group{{ $errors->has('users_id') ? ' has-error' : '' }}">
			<label class="col-sm-3 control-label" for="from">
				Direcciones emails
			</label>
			<div class="col-sm-8">
				<div class="input-group">
					{!!  Form::select('users_id[]', $users, ((isset($model->users_id) AND $model->users_id) ? $model->users_id : null ), ['placeholder' => 'Direcciones email', 'class' => 'select2', 'multiple' => true]); 
					!!}
					<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
				</div>
			</div>
			@if ($errors->has('users_id'))
			<div class="col-sm-8 col-sm-offset-3">
				<span class="help-block">
					<strong>{{ $errors->first('users_id') }}</strong>
				</span>
			</div>
			@endif
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
			<label class="col-sm-3 control-label" for="subject">Asunto</label>
			<div class="col-sm-8">
				<div class="input-group">
					{!! Form::text(
						'subject', (isset($model))?$model->subject:null, [
						'class' => 'form-control', 'id' => 'subject', 'placeholder' => 'Asuntos'
						]
						) !!}
						<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
					</div>
				</div>
				@if ($errors->has('subject'))
				<div class="col-sm-8 col-sm-offset-3">
					<span class="help-block">
						<strong>{{ $errors->first('subject') }}</strong>
					</span>
				</div>
				@endif
		</div>
		<div class="form-group{{ $errors->has('from') ? ' has-error' : '' }}">
			<label class="col-sm-3 control-label" for="from">
				Enviar desde
			</label>
			<div class="col-sm-8">
				<div class="input-group">
					{!!  Form::select('from', $users, ((isset($model->from) AND $model->from) ? $model->from : null ), ['placeholder' => 'Enviar desde', 'class' => 'select2']); 
					!!}
					<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
				</div>
			</div>
			@if ($errors->has('from'))
			<div class="col-sm-8 col-sm-offset-3">
				<span class="help-block">
					<strong>{{ $errors->first('from') }}</strong>
				</span>
			</div>
			@endif
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
			<label class="col-sm-3 control-label" for="body">Contenido</label>
			<div class="col-sm-9">
				<div class="input-group">
					{!! Form::textarea('body', null, [
						'class' => 'form-control wysihtml5-textarea', 'id' => 'address',
						'placeholder' => 'Contenido'
						]) 
					!!}
					{{--<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>--}}
				</div>
			</div>
			@if ($errors->has('body'))
				<div class="col-sm-8 col-sm-offset-3">
					<span class="help-block">
						<strong>{{ $errors->first('body') }}</strong>
					</span>
				</div>
			@endif
		</div>
	</div>
</div>

<hr>
<div class="row">
	<div class="col-sm-12">
		<h4>
			Seleccionar ficheros para enviar en este email.
			&#160;<i class="fa fa-plus-circle text-primary" role="button" 
			id="add-other-input-file" 
					 title="ficheros para enviar en este email" data-toggle="tooltip"></i>
		</h4>
	</div>
</div>

<div class="row email-input-files">
	<div class="col-sm-12">
		<div class="input-group">
			{{ Form::file('files[]', ['class' => 'file-upload']) }}
			<span class="input-group-addon">
				<i class="fa fa-minus input-required remove-email-input-files transparent-font"></i>
			</span>
		</div>
	</div>
</div>

@section('extra_scripts')
	<script type="text/javascript">
		$("#address").css({'height':'300px', 'width':'100%'});

		$("#add-other-input-file").on('click', function() {
			var inputFile = $('.email-input-files').last();
			inputFile.after("<div class='row email-input-files'>"+ inputFile.html() +"</div>");
			var i = 0;
			$(".remove-email-input-files").each(function() {
				$(this).on('click', function() {
					$(this).closest('.email-input-files').remove();
				});
				if (i > 0) {
					$(this).removeClass('transparent-font');
					$(this).attr('role', 'button');
				}
				i++;
			});
			$(".remove-email-input-files").on('click', function() {
				$(this).remove();
			});
		});
	</script>
@endsection
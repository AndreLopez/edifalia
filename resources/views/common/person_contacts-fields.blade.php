<div class="row">
	<div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('contact') ? ' has-error' : '' }}">
		<label class="col-sm-3 control-label" for="text">@lang('common.person_contact_contact_title')</label>
		<div class="col-sm-5">
			<div class="input-group">
		    	{!! Form::text(
		    		'contact', (isset($model))?$model->contact:null, [
		    			'class' => 'form-control', 'id' => 'contact', 
		    			'placeholder' => trans('common.person_contact_contact_placeholder')
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('contact'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('contact') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6 col-md-6 col-lg-6 form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
		<label class="col-sm-3 control-label" for="text">@lang('common.person_contact_notes_title')</label>
		<div class="col-sm-5">
			<div class="input-group">
		  {!! Form::textarea('notes', null, [
				'class' => 'form-control', 'id' => 'notes', 'rows' => '2',
				'placeholder' => trans('common.person_contact_notes_placeholder')
			]) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('notes'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('notes') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
	</div>

	<div class="row">
	<div class="col-sm-6 form-group{{ $errors->has('contact_type_id') ? ' has-error' : '' }}">
		<label for="postal_code_id" class="col-sm-3 control-label">@lang('common.contact_type_title')</label>
		<div class="col-sm-5">
			<div class="input-group">
	    		{!! Form::select(
		    		'contact_type_id', $contacts_type, null, [
		    			'id' => 'contact_type_id', 'class' => 'select2', 
		    			'placeholder' => trans('forms.select_null_option'),
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('contact_type_id'))
			<div class="col-sm-9 col-sm-offset-3	">
		        <span class="help-block">
		            <strong>{{ $errors->first('contact_type_id') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-sm-6 form-group{{ $errors->has('person_id') ? ' has-error' : '' }}">
		<label for="postal_code_id" class="col-sm-3 control-label">@lang('common.person_title')</label>
		<div class="col-sm-5">
			<div class="input-group">
	    		{!! Form::select(
		    		'person_id', $persons, null, [
		    			'id' => 'person_id', 'class' => 'select2', 
		    			'placeholder' => trans('forms.select_null_option'),
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
		</div>
		@if ($errors->has('person_id'))
			<div class="col-sm-9 col-sm-offset-3	">
		        <span class="help-block">
		            <strong>{{ $errors->first('person_id') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
	</div>

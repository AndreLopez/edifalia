<div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
	<label class="col-sm-3 control-label" for="type">@lang('forms.type_label')</label>
    <div class="col-sm-8">
    	<div class="input-group">
	    	{!! Form::text(
	    		'type', (isset($model))?$model->type:null, [
	    			'class' => 'form-control', 'id' => 'type', 'placeholder' => trans('forms.type_placeholder')
	    		]
	    	) !!}
	    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
	    </div>
    </div>
	@if ($errors->has('type'))
		<div class="col-sm-8 col-sm-offset-3">
	        <span class="help-block">
	            <strong>{{ $errors->first('type') }}</strong>
	        </span>
	    </div>
	@endif
</div>
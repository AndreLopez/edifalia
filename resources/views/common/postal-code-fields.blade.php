<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 form-group{{ $errors->has('code') ? ' has-error' : '' }}">
		<label class="col-sm-3 control-label" for="code">@lang('forms.code_label')</label>
	    <div class="col-sm-5">
	    	<div class="input-group">
		    	{!! Form::text(
		    		'code', (isset($model))?$model->code:null, [
		    			'class' => 'form-control', 'id' => 'code', 'placeholder' => trans('forms.code_placeholder')
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
	    </div>
		@if ($errors->has('code'))
			<div class="col-sm-8 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('code') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 form-group{{ $errors->has('country_id') ? ' has-error' : '' }}">
		<label class="col-sm-3 control-label" for="country_id">@lang('forms.country_label')</label>
	    <div class="col-sm-9">
	    	<div class="input-group">
	    		{!! Form::select(
		    		'country_id', $countries, null, [
		    			'id' => 'country_id', 'class' => 'select2', 
		    			'placeholder' => trans('forms.select_null_option'),
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
	    </div>
		@if ($errors->has('country_id'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('country_id') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 form-group{{ $errors->has('town') ? ' has-error' : '' }}">
		<label class="col-sm-3 control-label" for="town">@lang('forms.town_label')</label>
	    <div class="col-sm-9">
	    	<div class="input-group">
		    	{!! Form::text(
		    		'town', (isset($model))?$model->town:null, [
		    			'class' => 'form-control', 'id' => 'town', 'placeholder' => trans('forms.town_placeholder')
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
	    </div>
		@if ($errors->has('town'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('town') }}</strong>
		        </span>
		    </div>
		@endif
	</div>

	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 form-group{{ $errors->has('province') ? ' has-error' : '' }}">
		<label class="col-sm-3 control-label" for="province">@lang('forms.province_label')</label>
	    <div class="col-sm-9">
	    	<div class="input-group">
		    	{!! Form::text(
		    		'province', (isset($model))?$model->province:null, [
		    			'class' => 'form-control', 'id' => 'province', 'placeholder' => trans('forms.province_placeholder')
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
	    </div>
		@if ($errors->has('province'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('province') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 form-group{{ $errors->has('region') ? ' has-error' : '' }}">
		<label class="col-sm-3 control-label" for="region">@lang('forms.region_label')</label>
	    <div class="col-sm-9">
	    	<div class="input-group">
		    	{!! Form::text(
		    		'region', (isset($model))?$model->region:null, [
		    			'class' => 'form-control', 'id' => 'region', 'placeholder' => trans('forms.region_placeholder')
		    		]
		    	) !!}
		    	<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
		    </div>
	    </div>
		@if ($errors->has('region'))
			<div class="col-sm-9 col-sm-offset-3">
		        <span class="help-block">
		            <strong>{{ $errors->first('region') }}</strong>
		        </span>
		    </div>
		@endif
	</div>
</div>
<hr>
<div class="row">
	<div class="col-sm-12">
		<h4>
			Campos para filtrar comunidades
		</h4>
	</div>
</div>

<div class="row">	
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('invoicing_concept_ids') ? ' has-error' : '' }}">
			<label class="col-sm-3 control-label" for="invoicing_concept_ids">
				Conceptos de facturación
			</label>
			<div class="col-sm-8">
				<div class="input-group">
					{!!  Form::select('invoicing_concept_ids[]', $invoicingConcepts, ((isset($model->invoicing_concept_ids) AND $model->invoicing_concept_ids) ? $model->invoicing_concept_ids : null ), ['placeholder' => 'Conceptos de facturación', 'class' => 'select2', 'id' => 'email-concepts','multiple' => true]); 
					!!}
					<span class="input-group-addon">
						<i class="fa fa-asterisk input-required"></i>
					</span>
				</div>
			</div>
			@if ($errors->has('invoicing_concept_ids'))
			<div class="col-sm-8 col-sm-offset-3">
				<span class="help-block">
					<strong>{{ $errors->first('invoicing_concept_ids') }}</strong>
				</span>
			</div>
			@endif
		</div>
		<div class="form-group{{ $errors->has('people_without_property') ? ' has-error' : '' }}">
			<label for="platform_accounting" class="col-sm-3 control-label">
				Incluir personas sin propiedad
			</label>
			<div class="col-sm-8">
				{!! Form::checkbox('people_without_property', True, False, 
				['data-size' => 'mini', 'id' => 'email-people-without-property']) 
				!!}
			</div>
			@if ($errors->has('people_without_property'))
			<div class="col-sm-8 col-sm-offset-3">
				<span class="help-block">
					<strong>{{ $errors->first('people_without_property') }}</strong>
				</span>
			</div>
			@endif
		</div>

		<div class="form-group{{ $errors->has('building_number') ? ' has-error' : '' }}">
			<label class="col-sm-3 control-label" for="building_number">@lang('forms.building_number_label')</label>
			<div class="col-sm-8">
				<div class="input-group">
					{!! Form::text(
						'building_number', (isset($model))?$model->building_number:null, [
						'class' => 'form-control', 'id' => 'building_number', 
						'placeholder' => trans('forms.building_number_placeholder'),
						'id' => 'email-building-number'
						]
						) !!}
						<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
					</div>
				</div>
				@if ($errors->has('building_number'))
				<div class="col-sm-8 col-sm-offset-3">
					<span class="help-block">
						<strong>{{ $errors->first('building_number') }}</strong>
					</span>
				</div>
				@endif
			</div>
		</div>

		<div class="col-md-6">

			<div class="form-group{{ $errors->has('language_ids') ? ' has-error' : '' }}">
				<label class="col-sm-3 control-label" for="language_ids">
					Idiomas
				</label>
				<div class="col-sm-8">
					<div class="input-group">
						{!!  Form::select('language_ids[]', $languages, ((isset($model->language_ids) AND $model->language_ids) ? $model->language_ids : null ), [
							'placeholder' => 'Idiomas', 
							'class' => 'select2', 
							'id' => 'email-languages',
							'multiple' => true]); 
						!!}
						<span class="input-group-addon">
							<i class="fa fa-asterisk input-required"></i>
						</span>
					</div>
				</div>
				@if ($errors->has('language_ids'))
				<div class="col-sm-8 col-sm-offset-3">
					<span class="help-block">
						<strong>{{ $errors->first('language_ids') }}</strong>
					</span>
				</div>
				@endif
			</div>

			<div class="form-group{{ $errors->has('message_email_only') ? ' has-error' : '' }}">
				<label for="platform_accounting" class="col-sm-3 control-label">
					@lang('forms_persons.message_email_only_title')
				</label>
				<div class="col-sm-8">
					{!! Form::checkbox('message_email_only', True, False, 
					['data-size' => 'mini', 'id' => 'message-email-only']) 
					!!}
				</div>
				@if ($errors->has('message_email_only'))
				<div class="col-sm-8 col-sm-offset-3">
					<span class="help-block">
						<strong>{{ $errors->first('message_email_only') }}</strong>
					</span>
				</div>
				@endif
			</div>

		</div>

		<div class="row">
			<div class="col-sm-10">
				<div class="form-group{{ $errors->has('community_ids') ? ' has-error' : '' }}">
					<label class="col-sm-3 control-label" for="community_ids">@lang('forms.community_label')</label>
					<div class="col-sm-8">
						<div class="input-group">
							{!!  Form::select('community_ids[]', $communities, ((isset($model->community_ids) AND $model->community_ids) ? $model->community_ids : null ), [
								'placeholder' => trans('forms.community_placeholder'), 
								'class' => 'select2', 
								'id' => 'emails-for-communities',
								'multiple' => true
								]); 
							!!}
							<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
						</div>
					</div>
					@if ($errors->has('community_ids'))
					<div class="col-sm-8 col-sm-offset-3">
						<span class="help-block">
							<strong>{{ $errors->first('community_ids') }}</strong>
						</span>
					</div>
					@endif
				</div>
			</div>
		</div>
</div>

	

@section('extra_scripts')
@parent
<script type="text/javascript">

	var fieldsToFilter = {

		invoicing_concept_ids: '',
		people_without_property: '',
		building_number: '',
		languages: '',
		message_email_only: '',

	};


	var loadInputs = function () {
		fieldsToFilter.invoicing_concept_ids = $("#email-concepts").val();
		fieldsToFilter.building_number = $("#email-building-number").val();
		fieldsToFilter.languages = $("#email-languages").val();
		fieldsToFilter.people_without_property = $("#email-people-without-property").is(':checked') ? 1 : "";
		fieldsToFilter.message_email_only = $("#message-email-only").is(':checked') ? 1 : "";
	}


	var getInvoicingConceptIds = function() {
		$("#email-concepts").change(function() {
			//fieldsToFilter.invoicing_concept_ids = $(this).val();
			loadInputs();
			console.log(fieldsToFilter);
		});
	}

	var getLanguageIds = function() {
		$("#email-languages").change(function() {
			 //fieldsToFilter.language_ids = $(this).val();
			loadInputs();
			console.log(fieldsToFilter);
		});
	}

	var getPeopleWithoutProperty = function() {

		$("#email-people-without-property").change(function() {
			//fieldsToFilter.people_without_property = $(this).val();
			loadInputs();
			console.log(fieldsToFilter);
		});
	}

	var getMessageEmailOnly = function() {

		$("#message-email-only").change(function() {
			//fieldsToFilter.message_email_only = $(this).val();
			loadInputs();
			console.log(fieldsToFilter);
		});
	}


	var getBuildingNumber = function (){
		$("#email-building-number").keyup(function() {
			//fieldsToFilter.building_number = $(this).val();
			loadInputs();
			console.log(fieldsToFilter);
		});
	}


	var filterCommunities = function() {
		$.ajax({
	    	type: 'GET',
	    	url: '{{ route('api.filter.communities') }}',
	    	dataType:'json',
	    	data: fieldsToFilter,
	   		success: function(response) {
	        	console.log(response);
	        	if (response.success == true) 
	        	{
                	jQuery('#emails-for-communities').html('');
	            	jQuery('#emails-for-communities').append('<option value=\"\">Comunidades</option>');
	              	$.each(response.communities, function (index, value){
                    	$('#emails-for-communities').append('<option value=\"'+index+'\">'+value+'</option>');
                	});
	         	}else{
	        		jQuery('#emails-for-communities').html('');
	          		jQuery('#emails-for-communities').append('<option value=\"\"></option>');
	            }
	        }
	    });
	}

	getMessageEmailOnly();
	getPeopleWithoutProperty();
	getInvoicingConceptIds();
	getBuildingNumber();
	getLanguageIds();
	filterCommunities();
</script>
@endsection
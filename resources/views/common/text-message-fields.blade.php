@include('common.filter-communities-fields')

<hr>
<div class="row">
	<div class="col-sm-12">
		<h4>
			Campos para enviar sms.
		</h4>
	</div>
</div>

<div class="row">
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
			<label class="col-sm-3 control-label" for="phone_number">Otros móviles SMS</label>
			<div class="col-sm-8">
				<div class="input-group">
					{!! Form::text(
						'phone_number', (isset($model))?$model->phone_number:null, [
						'class' => 'form-control', 'id' => 'phone_number', 'placeholder' => 'Asuntos'
						]
						) !!}
						<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>
					</div>
				</div>
				@if ($errors->has('phone_number'))
				<div class="col-sm-8 col-sm-offset-3">
					<span class="help-block">
						<strong>{{ $errors->first('phone_number') }}</strong>
					</span>
				</div>
				@endif
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
			<label class="col-sm-3 control-label" for="body">Contenido</label>
			<div class="col-sm-9">
				<div class="input-group">
					{!! Form::textarea('body', null, [
						'class' => 'form-control', 'id' => 'address',
						'placeholder' => 'Contenido'
						]) 
					!!}
					{{--<span class="input-group-addon"><i class="fa fa-asterisk input-required"></i></span>--}}
				</div>
			</div>
			@if ($errors->has('body'))
				<div class="col-sm-8 col-sm-offset-3">
					<span class="help-block">
						<strong>{{ $errors->first('body') }}</strong>
					</span>
				</div>
			@endif
		</div>
	</div>
</div>
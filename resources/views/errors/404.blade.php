@extends('layouts.login')

@section('title')
    @lang('dashboard.titles.not_found')
@endsection

@section('content')
    <section class="content">
        <div class="error-page">
            <h2 class="headline text-yellow">404</h2><br>
            <div class="error-content">
                <h3><i class="fa fa-warning text-yellow"></i>
                    ¡Error! @lang('dashboard.titles.not_found').
                </h3>
                <p style="text-align: center">
                    <a class="btn btn-warning btn-lg" title="@lang('dashboard.buttons.home')"
                       href="{{url('/')}}"><i class="fa fa-home"></i> @lang('dashboard.buttons.home')</a>
                </p>
            </div>
            <!-- /.error-content -->
        </div>
        <!-- /.error-page -->
    </section>
@endsection

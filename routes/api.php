<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');


Route::group(['namespace' => 'Backend'], function()
{
	/**
	 * Route for filter list persons for properties
	 */
	Route::get('listPersonsForProperties',[
			'as' => 'api.list-persons-for-properties',
			'uses' => 'PersonsController@selectList'
	]);

	/**
	 * Route for filter communities
	 */
	Route::get('filter-communities',[
			'as' => 'api.filter.communities',
			'uses' => 'CommunitiesController@filter'
	]);

	Route::post('ApiStore', [
        'as' => 'api.person.store',
        'uses' => 'PersonsController@ApiStore',
    ]);
});

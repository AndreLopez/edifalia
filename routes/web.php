<?php
// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
/*Route::get('register', 'Auth\RegisterController@showRegistrationForm');
Route::post('register', 'Auth\RegisterController@register');*/

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('/', 'HomeController@index');

/**
 * Rutas para módulo Datos
 */
Route::group(['namespace' => 'Backend', 'middleware' => ['auth', 'user_access:access_data', 'user_connection']], function(){
    /**
     * Rutas de comunidades
     */
    Route::resource('community', 'CommunitiesController');

    /**
     * Rutas de propiedades
     */
    Route::resource('property', 'PropertiesController', ['except' => ['show']]);

    /**
     * Ruta person
     */
    Route::resource('person', 'PersonsController');

    /**
     * Rutas de Proveedores
     */
    Route::resource('supplier', 'SuppliersController');

    /**
     * Ruta para la gestión de coeficientes
     */
    Route::resource('coefficient', 'CoefficientController', [
        'except' => ['create', 'store', 'show', 'destroy']
    ]);

    /**
     * Ruta de Contratos
     */
    Route::resource('contract', 'ContractsController');

    /**
     * Ruta de códigos postales
     */
    Route::resource('postal-code', 'PostalCodesController');
    Route::get('postal-code/get-data/{id}', 'PostalCodesController@getData');
});

/**
 * Rutas para módulo Gestión
 */
Route::group(['namespace' => 'Backend', 'middleware' => ['auth', 'user_access:access_management', 'user_connection']], function(){
    /**
     * Ruta de documents
     */
    Route::post('pages/undo/{id}', [
        'uses' => 'PagesController@postUndelete',
        'as' => 'pages.undelete'
    ]);
    Route::resource('pages', 'PagesController', ['except' => ['show']]);

    Route::post('tags/undo/{id}', [
        'uses' => 'TagsController@postUndelete',
        'as' => 'tags.undelete'
    ]);
    Route::resource('tags', 'TagsController', ['except' => ['show']]);

    Route::post('documents/undo/{id}', [
        'uses' => 'DocumentsController@postUndelete',
        'as' => 'documents.undelete'
    ]);
    Route::resource('documents', 'DocumentsController');

    /**
     * Ruta de templates
     */
    Route::post('models/undo/{id}', [
        'uses' => 'ModelsController@postUndelete',
        'as' => 'models.undelete'
    ]);
    Route::resource('models', 'ModelsController', ['except' => ['show']]);

    Route::post('templates/undo/{id}', [
        'uses' => 'TemplatesController@postUndelete',
        'as' => 'templates.undelete'
    ]);
    Route::resource('templates', 'TemplatesController', ['except' => ['show']]);

    /**
     * Ruta de incidences
     */
    Route::post('communicator-types/undo/{id}', [
        'uses' => 'CommunicatorTypesController@postUndelete',
        'as' => 'communicator-types.undelete'
    ]);
    Route::resource('communicator-types', 'CommunicatorTypesController', ['except' => ['show']]);

    Route::post('receptions/undo/{id}', [
        'uses' => 'ReceptionsController@postUndelete',
        'as' => 'receptions.undelete'
    ]);
    Route::resource('receptions', 'ReceptionsController', ['except' => ['show']]);

    Route::post('reasons/undo/{id}', [
        'uses' => 'ReasonsController@postUndelete',
        'as' => 'reasons.undelete'
    ]);
    Route::resource('reasons', 'ReasonsController', ['except' => ['show']]);

    Route::post('sectors/undo/{id}', [
        'uses' => 'SectorsController@postUndelete',
        'as' => 'sectors.undelete'
    ]);
    Route::resource('sectors', 'SectorsController', ['except' => ['show']]);

    Route::get('incidences/communities', [
        'uses' => 'IncidencesController@getCommunities',
        'as' => 'incidences.communities'
    ]);

    Route::post('incidences/undo/{id}', [
        'uses' => 'IncidencesController@postUndelete',
        'as' => 'incidences.undelete'
    ]);
    Route::resource('incidences', 'IncidencesController');

    /**
     * Ruta de presupuestos
     */
    Route::post('invoicing-concepts/undo/{id}', [
        'uses' => 'InvoicingConceptsController@postUndelete',
        'as' => 'invoicing-concepts.undelete'
    ]);
    Route::resource('invoicing-concepts', 'InvoicingConceptsController', ['except' => ['show']]);

    Route::get('budgets/community-properties', [
        'uses' => 'BudgetsController@getCommunityProperties',
        'as' => 'budgets.properties'
    ]);
    Route::get('budgets/data', [
        'uses' => 'BudgetsController@getBudgetData',
        'as' => 'budgets.data'
    ]);
    Route::post('budgets/undo/{id}', [
        'uses' => 'BudgetsController@postUndelete',
        'as' => 'budgets.undelete'
    ]);
    Route::resource('budgets', 'BudgetsController', ['except' => ['show']]);

    /**
     * Ruta de readings
     */
    Route::post('rates/undo/{id}', [
        'uses' => 'RatesController@postUndelete',
        'as' => 'rates.undelete'
    ]);
    Route::resource('rates', 'RatesController', ['except' => ['show']]);

    Route::get('readings/data', [
        'uses' => 'ReadingsController@getReadingData',
        'as' => 'readings.data'
    ]);
    Route::get('readings/community-properties', [
        'uses' => 'ReadingsController@getCommunityProperties',
        'as' => 'readings.properties'
    ]);
    Route::post('readings/undo/{id}', [
        'uses' => 'ReadingsController@postUndelete',
        'as' => 'readings.undelete'
    ]);
    Route::resource('readings', 'ReadingsController', ['except' => ['show']]);

    /**
     * Ruta de Cuotas de recibos
     */
    Route::get('receiptdues/getProperties', 'ReceiptDuesController@getProperties');
    Route::get('receiptdues/getReceipDueLastMonth', 'ReceiptDuesController@getReceipDueLastMonth');
    Route::post('receiptdues/postUndelete/{id}', [
        'uses' => 'ReceiptDuesController@postUndelete',
        'as' => 'receiptdues.undelete'
    ]);
    Route::resource('receiptdues', 'ReceiptDuesController');

    /**
     * Ruta para emision de recibos
     */
    Route::post('issue-receipts/undo/{id}', [
        'uses' => 'IssueReceiptsController@postUndelete',
        'as' => 'issue-receipts.undelete'
    ]);
    Route::resource('issue-receipts', 'IssueReceiptsController', ['except' => ['edit']]);

    /**
     * Ruta para recibos a bancos
     */
    Route::post('receipts-banks/undo/{id}', [
        'uses' => 'ReceiptsToBanksController@postUndelete',
        'as' => 'receipts-banks.undelete'
    ]);
    Route::resource('receipts-banks', 'ReceiptsToBanksController', ['except' => ['edit']]);

    /**
     * Ruta para cobros
     */
    Route::get('charges/receipts-and-properties', [
        'uses' => 'ChargesController@getReceiptsAndProperties',
        'as' => 'charges.receipts_and_properties'
    ]);
    Route::post('charges/undo/{id}', [
        'uses' => 'ChargesController@postUndelete',
        'as' => 'charges.undelete'
    ]);
    Route::resource('charges', 'ChargesController', ['except' => ['show']]);
});

/**
 * Rutas para módulo Correo
 */
Route::group(['namespace' => 'Common', 'middleware' => ['auth', 'user_access:access_mail', 'user_connection']], function(){

    Route::resource('message', 'MessagesController', ['except' => ['show', 'index', 'update', 'edit', 'destroy']]);
    Route::get('received-messages', [
        'uses' => 'MessagesController@received',
        'as'   => 'received.messages'
    ]);
    Route::get('sent-messages', [
        'uses' => 'MessagesController@sent',
        'as'   => 'sent.messages'
    ]);
    Route::get('show-sent-messages/{id}', [
        'uses' => 'MessagesController@showSent',
        'as'   => 'show.sent.messages'
    ]);
    Route::get('show-received-messages/{id}', [
        'uses' => 'MessagesController@showReceived',
        'as'   => 'show.received.messages'
    ]);
    Route::get('delete-received-message/{id}', [
        'uses' => 'MessagesController@destroyReceived',
        'as'   => 'delete.received.message'
    ]);
    Route::get('delete-sent-message/{id}', [
        'uses' => 'MessagesController@destroySent',
        'as'   => 'delete.sent.message'
    ]);
    Route::get('download-message-file/{fileName}', [
        'uses' => 'MessagesController@download',
        'as'   => 'download.message.file'
    ]);

    /**
     * Text message
     */
    Route::get('text-messages/create', [
        'uses' => 'TextMessageController@create',
        'as'   => 'text.messages.create'
    ]);

    Route::post('text-messages/store', [
        'uses' => 'TextMessageController@store',
        'as'   => 'text.messages.store'
    ]);

    Route::get('sent-text-messages', [
        'uses' => 'TextMessageController@sent',
        'as'   => 'sent.text.messages'
    ]);
});

/**
 * Rutas para módulo Informes
 */
Route::group(['namespace' => 'Backend', 'middleware' => ['auth', 'user_access:access_reports', 'user_connection']], function(){
    Route::group(['prefix' => 'report'], function () {
        // Dossier
        Route::get('/dossier', ['as' => 'report.dossier', 'uses' => 'ReportController@dossier']);
        Route::get('/view-report-dossier', ['as' => 'report.viewdossier', 'uses' => 'ReportController@dossierViewReport']);
        Route::get('/export-dossier-pdf', ['as' => 'report.dossier-pdf', 'uses' => 'ReportController@dossierExportPdf']);
        Route::get('/export-dossier-excel', ['as' => 'report.dossier-excel', 'uses' => 'ReportController@dossierExportExcel']);

        // Liquidation Cons.
        Route::get('/liquidate-cons', ['as' => 'report.liquidate-cons', 'uses' => 'ReportController@liquidate']);
        Route::get('/liquidate-pdf', ['as' => 'liquidate.export-pdf', 'uses' => 'ReportController@liquidateExportPdf']);
        Route::get('/liquidate-excel', ['as' => 'liquidate.export-excel', 'uses' => 'ReportController@liquidateExportExcel']);

        // Liquidation
        Route::get('/liquidation', ['as' => 'report.liquidation', 'uses' => 'ReportController@liquidation']);
        Route::get('/liquidation/export-pdf/{id?}', ['as' => 'liquidation.export-pdf', 'uses' => 'ReportController@liquidationExportPdf']);

        // Spending
        Route::get('/spending', ['as' => 'report.spending', 'uses' => 'ReportController@spending']);
        Route::get('/spending/export-pdf', ['as' => 'spending.export-pdf', 'uses' => 'ReportController@spendingExportPdf']);

        // Meeting
        Route::group(['prefix' => 'meeting'], function () {
            Route::get('/list{format?}', ['as' => 'report.meeting', 'uses' => 'ReportController@meeting']);
            Route::get('/show/{id}', ['as' => 'meeting.show', 'uses' => 'ReportController@meetingShow']);
            Route::get('/create', ['as' => 'meeting.create', 'uses' => 'ReportController@meetingCreate']);
            Route::post('/load', ['as' => 'meeting.load', 'uses' => 'ReportController@meetingLoad']);
            Route::get('/view-assistant', ['as' => 'meeting.assistant', 'uses' => 'ReportController@meetingAssistant']);
            Route::get('/export-meeting-pdf/{id?}', ['as' => 'meeting.export-pdf', 'uses' => 'ReportController@meetingExportPdf']);
            Route::get('/export-meeting-excel/{id?}', ['as' => 'meeting.export-excel', 'uses' => 'ReportController@meetingExportExcel']);
            Route::get('/remove', ['as' => 'meeting.remove', 'uses' => 'ReportController@meetingRemove']);
        });

        // Meeting
        Route::group(['prefix' => 'economic'], function () {
            Route::get('/for-group-account', ['as' => 'economic.account', 'uses' => 'ReportEconomicController@forAccount']);
            Route::get('/for-group-account-excel', ['as' => 'economic.account-excel', 'uses' => 'ReportEconomicController@forAccountExcel']);

            Route::get('/for-movement-detail', ['as' => 'economic.movement', 'uses' => 'ReportEconomicController@forMovement']);
            Route::get('/for-movement-detail-excel', ['as' => 'economic.movement-excel', 'uses' => 'ReportEconomicController@forMovementExcel']);

            Route::get('/for-clasification-group', ['as' => 'economic.clasification-group', 'uses' => 'ReportEconomicController@forClasification']);
            Route::get('/for-clasification-group-excel', ['as' => 'economic.clasification-group-excel', 'uses' => 'ReportEconomicController@forClasificationExcel']);
            
            Route::get('/for-month', ['as' => 'economic.month', 'uses' => 'ReportEconomicController@forMonth']);
            Route::get('/for-month-pdf', ['as' => 'economic.month-pdf', 'uses' => 'ReportEconomicController@forMonthPdf']);
            Route::get('/for-month-excel', ['as' => 'economic.month-excel', 'uses' => 'ReportEconomicController@forMonthExcel']);

            Route::get('/for-property', ['as' => 'economic.property', 'uses' => 'ReportEconomicController@forProperty']);
            Route::get('/for-property-excel', ['as' => 'economic.property-excel', 'uses' => 'ReportEconomicController@forPropertyExcel']);
            
            Route::get('/for-total', ['as' => 'economic.total', 'uses' => 'ReportEconomicController@forTotal']);
            Route::get('/for-total-excel', ['as' => 'economic.total-excel', 'uses' => 'ReportEconomicController@forTotalExcel']);

            Route::get('/for-supplier-payment', ['as' => 'economic.payment-supplier', 'uses' => 'ReportEconomicController@forSupplierPayment']);
            Route::get('/for-supplier-payment-excel', ['as' => 'economic.payment-supplier-excel', 'uses' => 'ReportEconomicController@forSupplierPaymentExcel']);

            Route::get('/for-supplier-pending', ['as' => 'economic.supplier-pending', 'uses' => 'ReportEconomicController@forSupplierPending']);
            Route::get('/for-supplier-pending-excel', ['as' => 'economic.supplier-pending-excel', 'uses' => 'ReportEconomicController@forSupplierPendingExcel']);
            
            Route::get('/receipt', ['as' => 'economic.receipt', 'uses' => 'ReportEconomicController@forReceipt']);            
            Route::get('/receipt-export-pdf', ['as' => 'economic.receipt-pdf', 'uses' => 'ReportEconomicController@forReceiptExportPdf']);            
            Route::get('/receipt-export-excel', ['as' => 'economic.receipt-excel', 'uses' => 'ReportEconomicController@forReceiptExportExcel']);
        });

        // Data Generica Para Reportes
        Route::group(['prefix' => 'generic-data'], function () {
            Route::get('/community', ['as' => 'gereric.community', 'uses' => 'GenericDataController@getCommunity']);
            Route::get('/get-property-for-community', ['as' => 'generic-property', 'uses' => 'GenericDataController@getPropertyForCommunity']);
            Route::post('/get-community-for-id', ['as' => 'generic-property', 'uses' => 'GenericDataController@getCommunityForId']);
            Route::get('/get-meeting-for-community', ['as' => 'generic-meeting', 'uses' => 'GenericDataController@getMeetingForCommunity']);
            Route::get('/get-budgets-for-community', ['as' => 'generic-budget', 'uses' => 'GenericDataController@getBudgetForCommunity']);
            Route::get('/get-reading-for-community', ['as' => 'generic-reading', 'uses' => 'GenericDataController@getReadingForCommunity']);
            Route::get('/get-meeting-control', ['as' => 'generic-meeting-control', 'uses' => 'GenericDataController@getMeetingControl']);
            Route::get('/get-reading-for-id', ['as' => 'generic-reading-id', 'uses' => 'GenericDataController@getReadingForId']);
            Route::get('/get-months', ['as' => 'generic.get-months', 'uses' => 'GenericDataController@getMonths']);
        });

        Route::group(['prefix' => 'control'], function () {
            Route::get('/list{format?}', ['as' => 'report.controls', 'uses' => 'ReportController@controlIndex']);            
            Route::get('/create', ['as' => 'control.create', 'uses' => 'ReportController@controlCreate']);            
            Route::post('/create-post', ['as' => 'control.create', 'uses' => 'ReportController@controlCreate']);
            Route::get('/remove', ['as' => 'control.remove', 'uses' => 'ReportController@controlRemove']);
        });        
    });
});

/**
 * Rutas para módulo Contabilidad
 */
Route::group(['namespace' => 'Backend', 'middleware' => ['auth', 'user_access:access_accounting', 'user_connection']], function(){

});

/**
 * Rutas solo para administradores
 */
Route::group(['namespace' => 'Backend', 'middleware' => ['auth', 'admin', 'user_connection']], function() {
    /**
     * Ruta de Usuario principal
     */
    Route::get('main-users/zones', [
        'uses' => 'MainUsersController@getZones',
        'as' => 'main_users.zones'
    ]);
    Route::resource('main-users', 'MainUsersController');

    /**
     * Ruta de offices
    */
    Route::get('offices/communities', [
        'uses' => 'OfficesController@getCommunities',
        'as' => 'offices.communities'
    ]);
    Route::post('offices/undo/{id}', [
        'uses' => 'OfficesController@postUndelete',
        'as' => 'offices.undelete'
    ]);
    Route::resource('offices', 'OfficesController');

     /**
     * Ruta de employees (Gestión acceso usuarios)
    */
    Route::post('positions/undo/{id}', [
        'uses' => 'PositionsController@postUndelete',
        'as' => 'positions.undelete'
    ]);
    Route::resource('positions', 'PositionsController', ['except' => ['show']]);

    Route::post('employees/status/{id}', [
        'uses' => 'EmployeesController@postSaveStatus',
        'as' => 'employees.save_status'
    ]);
    Route::post('employees/undo/{id}', [
        'uses' => 'EmployeesController@postUndelete',
        'as' => 'employees.undelete'
    ]);
    Route::resource('employees', 'EmployeesController');

    /**
     * Ruta de backups
     */
    Route::get('backups', [
        'uses' => 'BackupsController@getIndex',
        'as'   => 'backups.index'
    ]);
    Route::post('backups', [
        'uses' => 'BackupsController@postExport',
        'as'   => 'backups.export'
    ]);

    /**
     * Ruta de acceso a intranet
    */
    Route::get('intranet', [
        'uses' => 'UsersConnectionsController@getIntranetAccess',
        'as'   => 'connections.intranet'
    ]);

    /**
    * Ruta de usuarios conectados
    */
    Route::get('connections', [
        'uses' => 'UsersConnectionsController@getUsersOnline',
        'as'   => 'connections.online'
    ]);

    /**
     * Ruta de action-history
    */
    Route::get('action-history', [
        'uses' => 'ActionHistoryController@getHistory',
        'as'   => 'history.all'
    ]);

    /**
     *  Ruta de currencies
     */
    Route::post('currencies/undo/{id}', [
        'uses' => 'CurrenciesController@postUndelete',
        'as' => 'currencies.undelete'
    ]);
    Route::resource('currencies', 'CurrenciesController', ['except' => ['show']]);

    /**
     *  Ruta de zones
     */
    Route::post('zones/undo/{id}', [
        'uses' => 'ZonesController@postUndelete',
        'as' => 'zones.undelete'
    ]);
    Route::resource('zones', 'ZonesController', ['except' => ['show']]);
});

/**
 * Rutas para todos los usuarios
 */
Route::group(['namespace' => 'Backend', 'middleware' => ['auth', 'user_connection']], function() {

    /**
     * Ruta Perfil de Usuario
     */
    Route::resource('user-profile', 'UsersProfilesController');
});

/**
 * Rutas comúnes
 */
Route::group(['namespace' => 'Common', 'middleware' => ['auth', 'user_connection']], function() {
    Route::resource('language', 'LanguagesController', ['except' => ['show']]);
    Route::resource('country', 'CountriesController', ['except' => ['show']]);
    Route::resource('status', 'StatusController', ['except' => ['show']]);
    Route::resource('payment-method', 'PaymentMethodsController', ['except' => ['show']]);
    Route::resource('contact-type', 'ContactTypesController', ['except' => ['show']]);
    Route::resource('supplier_contact', 'SuppliersContactsController', ['except' => ['show']]);
    Route::resource('supplier_activitie', 'SupplierActivitiesController', ['except' => ['show']]);
    Route::resource('supplier_sector', 'SuppliersSectorsController', ['except' => ['show']]);
    Route::resource('accounting-account', 'AccountingAccountsController', ['except' => ['show']]);
    Route::resource('propertyType', 'PropertyTypesController', ['except' => ['show']]);
    Route::resource('person-contact', 'PersonsContactsController', ['except' => ['show']]);
});

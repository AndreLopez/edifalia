# Edifalia

Sistema para la gestión de condominios y comunidades

## Requerimientos

Los requerimientos mínimos para el correcto funcionamiento de la aplicación son:

>>>
* PHP >= 5.6.4
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension
* Composer
* Laravel >= 5.3
>>>

## Instalación

Para la gestión de dependencias, la aplicación utiliza composer el cual debe estar instalado en el servidor en el que se coloque la aplicación. Una guía de como instalarlo se encuentra [aquí](https://getcomposer.org/doc/00-intro.md).

Una vez instalado y configurado el composer, se debe descargar el proyecto del repositorio de la siguiente forma:

>>>
git clone https://gitlab.com/proyectosismcenter/afinon.git
>>>

Esto descargara la estructura base de la aplicación.

Posteriormente se debe actualizar las dependencias de la herramienta para su correcto funcionamiento, para ello se debe ejecutar (dentro del directorio de la aplicación) el siguiente comando:

>>>
composer update
>>>

Esto desargará todas las dependencias requeridas por la aplicación y generará todos los archivos de configuración correspondientes.

El siguiente paso es crear el archivo ```.env``` que contendra la configuración de acceso a la aplicación, base de datos, correo electrónico, etc. Para esto copiamos el archivo ```.env.example``` con el nombre ```.env``` y se modifican los valores de las variables de configuración de acuerdo al entorno de instalación de la aplicación.

Por defecto, cuando se descarga e instalan las dependencias del sistema, la clave única del sistema se encuentra vacía por lo que es necesario generarla, para lo cual se ejecuta el comando

>>>
php artisan key:generate
>>>

El siguiente paso a realizar es la migración de la estructura de la base de datos, para lo cual se debe haber creado en el gestor de base de datos correspondiente y se requiere su configuración en el archivo ```.env```.

La migración de la estructura de la base de datos se realiza con el comando

>>>
php artisan migrate
>>>

y los datos básicos iniciales se registran con el comando

>>>
php artisan db:seed --class:```<NOMBRE_CLASE>```
>>>

Donde ```<NOMBRE_CLASE>``` es el nombre de la clase que contiene las instrucciones para el registro de información inicial en la aplicación, estas se encuentran dentro de la carpeta ```database/seeds```.
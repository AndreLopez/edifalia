$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function errorMessage(message){
        // oculto mensaje de espera
        $("#pleaseWait").modal("hide");
        // se muestra mensaje de error
        toastr.error(message);
    }

    $('table').on('click', 'a.delete_record', function(eventLink){
        eventLink.preventDefault();
        $("#form-delete").attr("action", $(this).attr('href'));
        $("#modal-delete").modal("show");
    });

    $('#yes-delete').on("click", function(){
        var route = $("#form-delete").attr("action");
        var type = $("#form-delete").attr("method");
        var data = $("#form-delete").serialize();
        $("#pleaseWait").modal("show");
        $.ajax({
            type: type,
            url: route,
            data: data,
            success: function(data)
            {
                if (data.data) {
                    var routeUndo;
                    $("table tr").each(function(){
                        if($(this).data("id") == data.id){
                            routeUndo = $(this).data("rundo");
                            $(this).fadeOut();
                        }
                    });
                    // oculto mensaje de espera
                    $("#pleaseWait").modal("hide");
                    // se muestra mensaje de exito
                    toastr.success(data.message+"<br><button type='button' class='btn btn-outline clear' id='undelete'>"+data.undo+"</button><form id='form-undelete' action='' method='POST'></form>");
                    // coloco la ruta para deshacer en el action del form undelete
                    $("#form-undelete").attr("action", routeUndo);
                    // limpio el action del formulario del modal
                    $("#form-delete").attr("action", "");
                }
                else {
                    errorMessage(data.message);
                }
            },
            error: function(message){
                errorMessage("¡ Error !");
            }
        });
    });

    $("body").on("click", "button#undelete", function(event){
        event.preventDefault();
        var route = $("#form-undelete").attr("action");
        var type = $("#form-undelete").attr("method");
        var data = $("#form-undelete").serialize();
        $("#pleaseWait").modal("show");
        $.ajax({
            type: type,
            url: route,
            data: data,
            success: function(data)
            {
                if (data.data){
                    $("#pleaseWait").modal("hide");
                    $("#form-undelete").attr("action", "");
                    // se busca la fila para mostrarla
                    $("tbody tr, div.row #box").each(function(){
                        if($(this).data("id") == data.id){
                            $(this).fadeIn();
                        }
                    });
                }
                else {
                    errorMessage(data.message);
                }
            },
            error: function(data){
                errorMessage("¡ Error !");
            }
        });
    });

    $("table div.checkbox").on("click", "input#ckbox", function(){
        $("#pleaseWait").modal("show");
        var status = ($(this).is(':checked'))?1:0;
        $.post($(this).data('route'), { status:status }, function(data) {
            if(data.data) {
                $("#pleaseWait").modal("hide");
                toastr.success(data.message);
            }
            else {
                errorMessage(data.message);
            }
        });
    });
});
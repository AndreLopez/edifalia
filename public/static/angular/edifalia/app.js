'use strict';

/**
 * @ngdoc overview
 * @name edifaliaApp
 * @Modulo de Edifalia
 * # edifaliaApp
 * @author Máximo Sojo <maxtoan in atechnologies>
 * Main module of the application.
 */
angular.module('edifaliaApp', ['ngSanitize', 'masterService','ui.bootstrap', 'ngTable', 'ngToast'])

.config(['$interpolateProvider', function ($interpolateProvider) {
      $interpolateProvider.startSymbol('{$');
      $interpolateProvider.endSymbol('$}');
}])

.filter('myHashKey',function($filter){
    return function(dateString){
    if (typeof dateString != 'undefined') {      
        // console.log(dateString);
        var d = new Date();
        var numero = d.getTime();
        numero = numero+'_'+dateString.id;
        return numero;
    };
  };
})

.controller('DossierController',function($scope, $rootScope, $http, httpMethod, $uibModal){
    var vm = this;
    var array = [];
    var arrayExclude = [];
    vm.property = {};//Data de propiedad
    vm.model = {};//Base para diferentes modelos
    $rootScope.dataPreset = {};
    vm.model.include = {
        dni: false,
        email: false,
        phone: false,
        notData: false
    };   
    
    httpMethod.getData(Router.route('gereric.community')).then(function(res){
    	vm.community = res.data;
    });

    vm.selectProperty = function(){
    	array = [];
    	arrayExclude = [];
    	vm.property = [];
    	if (vm.model.community) {
	    	httpMethod.getData('generic-data/get-property-for-community?id='+vm.model.community.id).then(function(res){
	    		angular.forEach(res.data, function(value, key) {
	    			array.push({
	    				id: value.id,
	    				property_number: value.property_number
	    			});
		    	});
		    	vm.property.available = array;
		    });
    	}else{
    		vm.property = null;
    	}
    };
    
    vm.notData = function(){
        vm.model.include.dni = false;
        vm.model.include.email = false;
        vm.model.include.phone = false;
    };

    vm.orderBy = function(orderBy){
        $rootScope.orderBy = orderBy;
    };

    vm.propertyAvailable = function(){
    	array.push(arrayExclude[vm.model.propertyExclude[0]]);
    	arrayExclude.splice(vm.model.propertyExclude[0], 1);
    	vm.property.available = array;
    	$('option#exclude_'+vm.model.propertyExclude[0]).remove();
    };

    vm.propertyExclude = function(){
        arrayExclude.push(array[vm.model.property[0]]);
    	array.splice(vm.model.property[0], 1);
    	vm.property.exclude = arrayExclude;
    	$('option#available_'+vm.model.property[0]).remove();
    };

    vm.generateReport = function(){
    	$rootScope.dataPreset = { propertyAvailble: vm.property.available, include: vm.model.include , title: vm.model.title};
        // console.log(vm.model.include);
	    var modalInstance = $uibModal.open({
	      animation: vm.animationsEnabled,
	      templateUrl: 'view-report-dossier',
	      controller: 'DossierViewController as dossierView',
	      size: 400
	    });
    };
})

.controller('DossierViewController', function ($scope, $rootScope, httpMethod, $uibModal, $uibModalInstance){
	var	vm = this;
    vm.include = {
        dni:$rootScope.dataPreset['include'].dni,
        email:$rootScope.dataPreset['include'].email,
        phone:$rootScope.dataPreset['include'].phone
    };
    

    httpMethod.postData('generic-data/get-community-for-id', $rootScope.dataPreset).then(function(res){
        vm.resultReport = res.data;
    });
    
    vm.exportDossier = function(url){
        var data = JSON.stringify({list: vm.resultReport, title: $rootScope.dataPreset.title, include: vm.include});
        document.location.href = 'export-dossier-pdf?data='+data;
    };

    vm.exportDossierExcel = function(){
        var data = JSON.stringify({list: vm.resultReport, title: $rootScope.dataPreset.title, include: vm.include});
        document.location.href = 'export-dossier-excel?data='+data;
    };

    $scope.propertyName = '-'+$rootScope.orderBy;
    // $scope.propertyName = '-dni';
    $scope.reverse = true;
    $scope.friends = vm.resultReport;

    $scope.sortBy = function(propertyName) {
        $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
        $scope.propertyName = propertyName;
    };

	vm.cancel = function () {        
      $uibModalInstance.close();      
    };
})

.controller('MeetingController', function ($scope, $rootScope, httpMethod, NgTableParams, messageFlash, ngToast){
    var vm = this;
    vm.itemMeeting = 0;
    // ngToast.create('a toast message...');
    // console.log(window.location.origin);
    vm.printPdf = function(){
        if (vm.itemMeeting) {            
            document.location.href = 'export-meeting-pdf/'+vm.itemMeeting;
        }else{
            alert('Debe seleccionar un item');
        };
    };

    vm.printExcel = function(){
        if (vm.itemMeeting) {            
            document.location.href = 'export-meeting-excel/'+vm.itemMeeting;
        }else{
            alert('Debe seleccionar un item');
        };
    };

    $rootScope.$on('loadMeeting',function(){
        httpMethod.getData('listjson').then(function(response){
            vm.inData = response.data;
            vm.tableParams = new NgTableParams({}, { dataset: response.data});
        });
    });
    $rootScope.$broadcast('loadMeeting');

    vm.remove = function(row){
        var result = confirm('Desea eliminar el registro?');
        if (result) {
            httpMethod.getData('remove?id='+row.id).then(function(res){
                if (res.data) {
                    $rootScope.$broadcast('loadMeeting');
                }
            });            
        }
    };

    vm.selectItem = function(row){
        vm.itemMeeting = row.id;        
    };
})

.controller('MeetingCreateController', function ($scope, $rootScope, httpMethod, $uibModal){
    var vm = this;
    $rootScope.personsData = {};
    vm.model = {};
    vm.setAssistant = [];
    vm.modelVoting = {};

    vm.detailAssistant = {
        suma: 0,
        perAssistante: 0,
        perRepre: 0,
        total: 0
    };

    vm.assistant = {
        present: null,
        represent: null,
        total: null
    };

    httpMethod.getData(Router.route('gereric.community')).then(function(res){
        vm.community = res.data;
    }); 
    
    httpMethod.getData(Router.route('generic-meeting-control')).then(function(res){
        vm.meetingControl = res.data;
    });

    vm.typeMeeting = function(type){
        // console.log(type);
    };

    vm.selectMeeting = function(){
        vm.dataAssistant = {};
        if (typeof vm.model.community != 'undefined') {
            httpMethod.getData(Router.route('generic-meeting')+'?id='+vm.model.community.id).then(function(res){
                vm.meetingData = res.data;
            });            
        }
    };

    vm.actionAssistant = function(idAssistant, action){
        var data = {id: idAssistant, action: action};

        if (action == 1) {
            vm.assistant.present = vm.assistant.present + 1;
        }else{
            vm.assistant.represent = vm.assistant.represent + 1;
        }
        
        vm.setAssistant.push(data);        
        vm.detailAssistant.suma = vm.setAssistant.length;
        vm.detailAssistant.perAssistant = (vm.assistant.present / vm.detailAssistant.suma) * 100;
        vm.detailAssistant.perRepre = (vm.assistant.represent / vm.detailAssistant.suma) * 100;
        vm.detailAssistant.total = vm.detailAssistant.perAssistant + vm.detailAssistant.perRepre;

    };

    vm.generateMeeting = function(){
        var data = {
            community: vm.model.community,
            meeting: vm.model.formMeeting,
            assistant: vm.setAssistant,
            voting: vm.modelVoting
        };
        httpMethod.postData(Router.route('meeting.load'), data).then(function(res){
            vm.showMessage = true;
            vm.message = res.data;
            vm.model = {};
            vm.setAssistant = [];
            vm.modelVoting = {};
            vm.dataAssistant = {};
        });
    };
    
    vm.trashAssistant = function(index){
        console.log(index);
    };

    // Asistentes
    $scope.$on("updateListAssistant",function(){
        vm.dataAssistant = $rootScope.personsData;
    });

    vm.searchAssistant = function(){
        $rootScope.dataPreset = vm.model.community.id;
        var modalInstance = $uibModal.open({
          animation: vm.animationsEnabled,
          templateUrl: Router.route('meeting.assistant'),
          controller: 'AssistantController as assistant',
          size: 400
        });
    };

    vm.formVoting = [];
    vm.modelVoting = {};

    vm.count = 0;

    vm.addVoting = function(){
        // console.log(vm.dataAssistant);
        vm.count = vm.count + 1;
        var item = {
                id: 1,
                name: vm.count,
                placeholder: 'Ingrese Votación',
                child: vm.dataAssistant
            }            
        ;
        vm.formVoting.push(item);
    };

    vm.testing = function(){
        console.log(vm.modelVoting);
    };
})

.controller('AssistantController', function ($scope, $rootScope, httpMethod, $uibModalInstance){
    var vm = this;
    vm.persons = [];
    var value = 1;

    httpMethod.getData(Router.route('generic-property')+'?id='+$rootScope.dataPreset).then(function(res){
        vm.property = res.data;
    });

    vm.selectPerson = function(row){
        var checkPerson = angular.element('#person_'+row.id).val();
        if (checkPerson == 0) {
            angular.element('#person_'+row.id).val(value);
            vm.persons.push(row);
        }else{
            angular.element('#person_'+row.id).val(0);
        }
    };

    vm.select = function(){
        $rootScope.personsData = vm.persons;
        $rootScope.$broadcast('updateListAssistant');
        $uibModalInstance.close();
    };

    vm.cancel = function () {        
      $uibModalInstance.close();
    };
})

.controller('LiquidationController', function($scope, httpMethod){
    var vm = this;
    vm.model = {};
    
    httpMethod.getData('generic-data/community').then(function(res){
        vm.community = res.data;
    });  

    vm.generateReport = function(){
        if (vm.model.community) {
            document.location.href = 'liquidation/export-pdf/'+vm.model.community.id;            
        };
    };

    vm.selectProperty = function(){
        // console.log(vm.model.community);        
    };
})

.controller('SpendingController', function($scope, httpMethod){
    var vm = this;
    vm.model = {};

    httpMethod.getData('generic-data/community').then(function(res){
        vm.community = res.data;
    });  

    vm.selectProperty = function(){
        if (vm.model.community) {
            httpMethod.getData('generic-data/get-budgets-for-community?id='+vm.model.community.id).then(function(res){
                vm.budgets = res.data;
            });            
        }        
    };

    vm.generatePdf = function(){
        var data = JSON.stringify({
            community: vm.model.community,
            budget : vm.model.budget
        });

        document.location.href = 'spending/export-pdf?data='+data;
    }
})

.controller('LiquidateConsController', function($scope, httpMethod){
    var vm = this;
    vm.model = {};

    httpMethod.getData('generic-data/community').then(function(res){
        vm.community = res.data;
    });  

    vm.selectCommunity = function(){
        if (vm.model.community) {
            httpMethod.getData('generic-data/get-reading-for-community?id='+vm.model.community.id).then(function(res){
                vm.readingData = res.data;
            });
        }else{
            vm.readinsData = null;
        }
    };

    vm.selectReading = function(){
        vm.model.consumption = 0;
        vm.model.payment = 0;
        if (vm.model.reading) {
            httpMethod.getData('generic-data/get-reading-for-id?id='+vm.model.reading.id).then(function(res){
                vm.propertyReadings = res.data.property_reading;
                angular.forEach(res.data.property_reading, function(value, key) {
                    vm.model.consumption = vm.model.consumption + parseFloat(value.consumption);                    
                });
                angular.forEach(res.data.reading, function(value, key) {
                    vm.reading = value;                
                });
            });
        }else{
            vm.propertyReadings = {};
            vm.reading = {};
            vm.model.consumption = 0;
            vm.model.payment = 0;
        };
    };

    vm.generatePdf = function(){
        var data = JSON.stringify({
            community: vm.model.community,
            reading : vm.model.reading,
            dataReading: vm.reading,
            readings: vm.propertyReadings,
            consumption: vm.model.consumption
        });
        document.location.href = 'liquidate-pdf?data='+data;
    };

    vm.generateExcel = function(){
        var data = JSON.stringify({
            community: vm.model.community,
            reading : vm.model.reading,
            dataReading: vm.reading,
            readings: vm.propertyReadings,
            consumption: vm.model.consumption
        });
        document.location.href = 'liquidate-excel?data='+data;
    };

    vm.reset = function(){
        vm.model = {};
        vm.reading = {};
        vm.propertyReadings = {};
        vm.model.consumption = {};
    };
})

.controller('MeetingControlController', function ($scope, $rootScope, httpMethod, NgTableParams, $uibModal){
    var vm = this;

    $rootScope.$on('loadMeetingControl',function(){
        httpMethod.getData('listjson').then(function(response){
            vm.tableParams = new NgTableParams({}, { dataset: response.data});
        });
    });
    $rootScope.$broadcast('loadMeetingControl');

    vm.create = function(){
        var modalInstance = $uibModal.open({
          animation: vm.animationsEnabled,
          templateUrl: 'create',
          controller: 'MeetingControlFormController as control',
          size: 400
        });
    };

    vm.remove = function(row){
        var result = confirm('Desea eliminar el registro?');
        if (result) {
            httpMethod.getData('remove?id='+row.id).then(function(res){
                if (res.data) {
                    $rootScope.$broadcast('loadMeetingControl');
                }
            });            
        }
    };
})

.controller('MeetingControlFormController', function ($scope, $rootScope, httpMethod, $uibModalInstance){
    var vm = this;
    vm.modelForm = {};

    vm.create = function(){
        var data = vm.modelForm;
        httpMethod.postData('create-post', data).then(function(res){
            if (res.data) {
                $uibModalInstance.close();
                $rootScope.$broadcast('loadMeetingControl');
            };
        });
    };

    vm.cancel = function () {        
      $uibModalInstance.close();
    };
})

.controller('ReceiptReportController', function ($scope, httpMethod){
    var vm = this;
    vm.model = {};

    httpMethod.getData(Router.route('gereric.community')).then(function(res){
        vm.community = res.data;
    });
    
    vm.selectProperty = function(){
        var url = Router.route('economic.receipt-pdf');
        vm.model.idCommunity = vm.model.community.id;
    };

    vm.generateReportPdf = function(){
        var data = JSON.stringify({
            idCommunity: vm.model.idCommunity,
            dateStart: vm.model.dateStart,
            dateEnd: vm.model.dateEnd            
        });
        
        document.location.href = Router.route('economic.receipt-pdf')+'?data='+data;
    };

    vm.generateReportExcel = function(){
        var data = JSON.stringify({
            idCommunity: vm.model.idCommunity,
            dateStart: vm.model.dateStart,
            dateEnd: vm.model.dateEnd            
        });
        
        document.location.href = Router.route('economic.receipt-excel')+'?data='+data;
    };
})

.controller('EconomicMonthController', function($scope, httpMethod){
    var vm = this;
    vm.model = {};
    
    httpMethod.getData(Router.route('gereric.community')).then(function(res){
        vm.community = res.data;
    });

    httpMethod.getData(Router.route('generic.get-months')).then(function(res){
        vm.months = res.data;
    });

    vm.generateReportPdf = function(){
        document.location.href = Router.route('economic.month-pdf');
    };

    vm.generateReportExcel = function(){
        document.location.href = Router.route('economic.month-excel');
    }
})

.controller('EconomicPropertyController', function($scope, httpMethod){
    var vm = this;
    vm.model = {};
    
    httpMethod.getData(Router.route('gereric.community')).then(function(res){
        vm.community = res.data;
    });

    vm.generateReportPdf = function(){
        console.log(vm.model.community);
        // var data = {
        //     idCommunity: vm.model.community.id
        // };
        document.location.href = Router.route('economic.property-excel')+'?idCommunity='+vm.model.community.id;
    };    
})

.controller('EconomicAccountController', function($scope, httpMethod){
  var vm = this;
    vm.model = {};
    
    httpMethod.getData(Router.route('gereric.community')).then(function(res){
        vm.community = res.data;
    });

    // vm.generateReportPdf = function(){
    //     document.location.href = Router.route('economic.month-pdf');
    // };

    vm.generateReportExcel = function(){
        document.location.href = Router.route('economic.account-excel');
    }  
})

.controller('EconomicMovementController', function($scope, httpMethod){
  var vm = this;
    vm.model = {
        includeAll:  null,
        months: null
    };
    
    httpMethod.getData(Router.route('gereric.community')).then(function(res){
        vm.community = res.data;
    });

    httpMethod.getData(Router.route('generic.get-months')).then(function(res){
        vm.months = res.data;
    });

    vm.generateReportExcel = function(){
        var parameters = {
            idCommunity: vm.model.community.id,
            includeAll: vm.model.includeAll,
            month: vm.model.months

        };
        var data = JSON.stringify(parameters);
        document.location.href = Router.route('economic.movement-excel')+'?_dc='+data;
    }  
})

.controller('EconomicClasificationController', function($scope, httpMethod){
  var vm = this;
    vm.model = {};
    
    httpMethod.getData(Router.route('gereric.community')).then(function(res){
        vm.community = res.data;
    });

    vm.generateReportExcel = function(){
        document.location.href = Router.route('economic.clasification-group-excel');
    }  
})

.controller('EconomicTotalController', function($scope, httpMethod){
  var vm = this;
    vm.model = {};
    
    httpMethod.getData(Router.route('gereric.community')).then(function(res){
        vm.community = res.data;
    });

    vm.generateReportExcel = function(){
        document.location.href = Router.route('economic.total-excel');
    }  
})

.controller('EconomicSupplierPaymentController', function($scope, httpMethod){
  var vm = this;
    vm.model = {};
    
    httpMethod.getData(Router.route('gereric.community')).then(function(res){
        vm.community = res.data;
    });

    vm.generateReportExcel = function(){
        document.location.href = Router.route('economic.payment-supplier-excel');
    }  
})

.controller('EconomicSupplierPendingController', function($scope, httpMethod){
  var vm = this;
    vm.model = {};
    
    httpMethod.getData(Router.route('gereric.community')).then(function(res){
        vm.community = res.data;
    });

    vm.generateReportExcel = function(){
        document.location.href = Router.route('economic.supplier-pending-excel');
    }  
})
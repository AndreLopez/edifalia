'use strict';
angular.module('masterService', [])

.factory('messageFlash', function (ngToast){
  return {
    success: function(message){
        if(message === undefined){
            message = null;
        }
        ngToast.create({
        className: 'success',
        content: message
      });
    },
    info: function(message){
        if(message === undefined){
            message = null;
        }
      ngToast.create({
        className: 'info',
        content: '<div class="alert alert-info alert-dismissible message-flash">'+
                      '<h4><i class="icon fa fa-info"></i> Mensaje!</h4>'+
                      message+
                  '</div>'
      });
    },
    warning: function(message){
        if(message === undefined){
            message = null;
        }
      ngToast.create({
        className: 'warning',
        content: '<div class="alert alert-warning alert-dismissible message-flash">'+
                      '<h4><i class="icon fa fa-warning"></i> Mensaje!</h4>'+
                      message+
                  '</div>'
      });
    },
    danger: function(message){
        if(message === undefined){
            message = null;
        }
      ngToast.create({
        className: 'danger',
        content: '<div class="alert alert-danger alert-dismissible message-flash">'+
                      '<h4><i class="icon fa fa-ban"></i> Mensaje!</h4>'+
                      message+
                  '</div>'
      });
    }
  }
})

.service('httpMethod',['$http', '$q', function ($http, $q){
    this.getData = function (route){
        var deferred = $q.defer();
        return $http.get(route)
            .success(function (data) {
              deferred.resolve(data);
            })
            .error(function(err) {
                deferred.reject(err)
            });
        return deferred.promise;
    };

    this.postData = function(route, data){
        return $http({
          method: 'POST',
          url: route,
          data: data,
          headers: {'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest'}
        }).success(function (data) {
          return true;
        }).error(function (data, status, headers, config) {            
          return false;
        });
    };
}])
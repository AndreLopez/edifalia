<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PropertyTypesTableSeeder::class);
        $this->call(AccountingAccountTableSeeder::class);
        $this->call(PaymentMethodsTableSeeder::class);
        //$this->call(PersonsTableSeeder::class);
        $this->call(CommunitiesTableSeeder::class);
        $this->call(PostarCodesTableSeeder::class);
        $this->call(LanaguagesTableSeeder::class);
        $this->call(InvoicingConceptsTableSeeder::class);
        $this->call(PropertiesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
    }
}

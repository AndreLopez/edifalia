<?php

use Illuminate\Database\Seeder;
use Edifalia\Models\Common\Property;

class PropertiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	\Eloquent::unguard();
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Property::truncate();

        factory(Property::class)->create([
            'building_number' => '1234', 
            'property_number' => '1111', 
            'registered_property' => '1234', 
            'property_type_id' => 1,
            'status' => 1,
            'coefficient' => 0,
            'group_properties' => 1,
            'group_concepts' => 1,
            'person_id' => 1,
            'payment_method_id' => 1,
            'community_id' => 1
        ]);

        factory(Property::class)->create([
            'building_number' => '2222', 
            'property_number' => '2222', 
            'registered_property' => '2222', 
            'property_type_id' => 1,
            'status' => 1,
            'coefficient' => 0,
            'group_properties' => 1,
            'group_concepts' => 1,
            'person_id' => 2,
            'payment_method_id' => 1,
            'community_id' => 2
        ]);

        factory(Property::class)->create([
            'building_number' => '3333', 
            'property_number' => '3333', 
            'registered_property' => '3333', 
            'property_type_id' => 1,
            'status' => 1,
            'coefficient' => 0,
            'group_properties' => 1,
            'group_concepts' => 1,
            'person_id' => 3,
            'payment_method_id' => 1,
            'community_id' => 3
        ]);

        factory(Property::class)->create([
            'building_number' => '4444', 
            'property_number' => '4444', 
            'registered_property' => '4444', 
            'property_type_id' => 1,
            'status' => 1,
            'coefficient' => 0,
            'group_properties' => 1,
            'group_concepts' => 1,
            'person_id' => 4,
            'payment_method_id' => 1,
            'community_id' => 4
        ]);

        factory(Property::class)->create([
            'building_number' => '5555', 
            'property_number' => '5555', 
            'registered_property' => '5555', 
            'property_type_id' => 1,
            'status' => 1,
            'coefficient' => 0,
            'group_properties' => 1,
            'group_concepts' => 1,
            'person_id' => 5,
            'payment_method_id' => 1,
            'community_id' => 1
        ]);

        factory(Property::class)->create([
            'building_number' => '6', 
            'property_number' => '6', 
            'registered_property' => '6', 
            'property_type_id' => 1,
            'status' => 1,
            'coefficient' => 0,
            'group_properties' => 1,
            'group_concepts' => 1,
            'person_id' => 6,
            'payment_method_id' => 1,
            'community_id' => 2
        ]);

        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}

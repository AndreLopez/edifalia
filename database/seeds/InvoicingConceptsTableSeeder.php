<?php

use Illuminate\Database\Seeder;

use Edifalia\Models\InvoicingConcept;

class InvoicingConceptsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$concepts = [
    		"Agua", 
    		"Calefacción", 
    		"Cuota del seguro", 
    		"Cambio de contadores de agua"
    	];

    	foreach ($concepts as $key => $concept) {
    		InvoicingConcept::updateOrCreate(['name' => $concept], []);
    	}
    }
}

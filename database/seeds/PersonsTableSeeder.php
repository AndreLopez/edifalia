<?php

use Illuminate\Database\Seeder;
use Edifalia\Models\Backend\Person;
class PersonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	\Eloquent::unguard();
    	\DB::statement('SET FOREIGN_KEY_CHECKS=0;');
    	//Person::truncate();

    	\DB::table('persons')->insert([
    	'first_name' => 'Alvaro',
    	'last_name' => 'Padilla',
    	'address' => 'Las Delicias',
    	'dni' => '19234099',
    	'reference' => 'Ninguna',
    	'strength_language' => 'L',
    	'message_email_only' => 1,
    	'group_properties' => 1,
    	'group_invoice_concepts' => 1,
    	'dont_get_email' => 1,
    	'can_update_own_data' => 1,
    	'exclusiveness' => 1,
    	'virtual_office_access' => 1,
    	'update_data_from_virtual_office' => 1,
    	'notes' => 'Text 1',
    	'postal_code_id' => 1,
    	'language_id' => 1,
    	'payment_method_id' => 1,
    	'user_id' => 1
    	]);

    	\DB::table('persons')->insert([
    	'first_name' => 'Eduardo',
    	'last_name' => 'Frechon',
    	'address' => 'La Concordia',
    	'dni' => '19975245',
    	'reference' => 'Ninguna',
    	'strength_language' => 'L',
    	'message_email_only' => 1,
    	'group_properties' => 1,
    	'group_invoice_concepts' => 1,
    	'dont_get_email' => 1,
    	'can_update_own_data' => 1,
    	'exclusiveness' => 1,
    	'virtual_office_access' => 1,
    	'update_data_from_virtual_office' => 1,
    	'notes' => 'Text 1',
    	'postal_code_id' => 1,
    	'language_id' => 2,
    	'payment_method_id' => 1,
    	'user_id' => 2
    	]);

    	\DB::table('persons')->insert([
    	'first_name' => 'Lenyn',
    	'last_name' => 'Alcantara',
    	'address' => 'Unidad Vecinal',
    	'dni' => '18990567',
    	'reference' => 'Ninguna',
    	'strength_language' => 'L',
    	'message_email_only' => 1,
    	'group_properties' => 1,
    	'group_invoice_concepts' => 1,
    	'dont_get_email' => 1,
    	'can_update_own_data' => 1,
    	'exclusiveness' => 1,
    	'virtual_office_access' => 1,
    	'update_data_from_virtual_office' => 1,
    	'notes' => 'Text 1',
    	'postal_code_id' => 1,
    	'language_id' => 1,
    	'payment_method_id' => 1,
    	'user_id' => 3
    	]);

    	\DB::table('persons')->insert([
    	'first_name' => 'Giancarlo',
    	'last_name' => 'Vargas',
    	'address' => 'Palo Gordo',
    	'dni' => '19234100',
    	'reference' => 'Ninguna',
    	'strength_language' => 'L',
    	'message_email_only' => 1,
    	'group_properties' => 1,
    	'group_invoice_concepts' => 1,
    	'dont_get_email' => 1,
    	'can_update_own_data' => 1,
    	'exclusiveness' => 1,
    	'virtual_office_access' => 1,
    	'update_data_from_virtual_office' => 1,
    	'notes' => 'Text 1',
    	'postal_code_id' => 1,
    	'language_id' => 2,
    	'payment_method_id' => 1,
    	'user_id' => 4
    	]);

    	\DB::table('persons')->insert([
    	'first_name' => 'Irali',
    	'last_name' => 'Bustamante',
    	'address' => 'Las Delicias',
    	'dni' => '1234567',
    	'reference' => 'Ninguna',
    	'strength_language' => 'L',
    	'message_email_only' => 1,
    	'group_properties' => 1,
    	'group_invoice_concepts' => 1,
    	'dont_get_email' => 1,
    	'can_update_own_data' => 1,
    	'exclusiveness' => 1,
    	'virtual_office_access' => 1,
    	'update_data_from_virtual_office' => 1,
    	'notes' => 'Text 1',
    	'postal_code_id' => 1,
    	'language_id' => 1,
    	'payment_method_id' => 1,
    	'user_id' => 5
    	]);

    	\DB::table('persons')->insert([
    	'first_name' => 'Andres',
    	'last_name' => 'Noguera',
    	'address' => 'Quinimari',
    	'dni' => '1235689',
    	'reference' => 'Ninguna',
    	'strength_language' => 'L',
    	'message_email_only' => 1,
    	'group_properties' => 1,
    	'group_invoice_concepts' => 1,
    	'dont_get_email' => 1,
    	'can_update_own_data' => 1,
    	'exclusiveness' => 1,
    	'virtual_office_access' => 1,
    	'update_data_from_virtual_office' => 1,
    	'notes' => 'Text 1',
    	'postal_code_id' => 1,
    	'language_id' => 2,
    	'payment_method_id' => 1,
    	'user_id' => 6
    	]);

    	\DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}

<?php

use Illuminate\Database\Seeder;
use Edifalia\Models\Common\PaymentMethod;

class PaymentMethodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	\Eloquent::unguard();

    	$types = [
	    	'Efectivo ', 
	    	'Transferencia',
	    	'Cheque',
	    	'Deposito'
    	];

    	for ($i=0; $i < 4; $i++) 
    	{ 
    		$paymentMethod = [
	    		'type' => $types[$i]
    		]; 
    		PaymentMethod::updateOrCreate($paymentMethod, []);
    	}
    }
}

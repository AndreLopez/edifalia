<?php

use Illuminate\Database\Seeder;
use Edifalia\Models\Common\Language;

class LanaguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lanaguages = ['Ingles', 'Español'];
        $acronimo = ['en', 'es'];

        for ($i=0; $i <2 ; $i++) 
        { 
        	$item = [
        		'name' => $lanaguages[$i],
        		'acronym' => $acronimo[$i]
        	];
        	Language::updateOrCreate($item, []);
        }
    }
}

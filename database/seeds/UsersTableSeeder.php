<?php

use Illuminate\Database\Seeder;
use Edifalia\User;
use Edifalia\Models\Currency;
use Edifalia\Models\NumberFormat;
use Edifalia\Models\DateFormat;
use Edifalia\Models\Continent;
use Edifalia\Models\Zone;
use Edifalia\Models\MainUser;
use Edifalia\Models\Common\Country;
use Edifalia\Models\Common\PostalCode;
use Edifalia\Models\Common\Language;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Admin
        $user = User::updateOrCreate(
            ['email' => 'admin@admin.com'],
            [
                'type' => 0, 'name' => 'Admin', 'password' => '123456', 
                'remember_token' => str_random(10)
            ]
        );
        
        $currency = Currency::updateOrCreate(['name' => 'Euro'], ['Symbol' => '€']);

        $numberFormat = NumberFormat::updateOrCreate(['format' => '1.500,75'], []);

        $dateFormat = DateFormat::updateOrCreate(['format' => 'DD/MM/YY'], []);

        $continent = Continent::updateOrCreate(['name' => 'Europeo'], []);

        $zone = Zone::updateOrCreate(['name' => 'España'], ['continent_id' => $continent->id]);

        $country = Country::updateOrCreate(['name' => 'España'], []);

        $postalCode = PostalCode::updateOrCreate(
            ['code' => '08'],
            ['town' => 'Madrid', 'province' => 'Madrid', 'region' => 'Madrid', 'country_id' => $country->id]
        );

        $language = Language::updateOrCreate(['name' => 'Español'], ['acronym' => 'es']);

        $mainUser = MainUser::updateOrCreate(
            ['user_id' => $user->id],
            [
                'company' => 'Empresa C.A.',
                'first_name' => 'Principal',
                'last_name' => 'Principal',
                'dni' => 12345678,
                'address' => 'Av. principal',
                'postal_code_id' => $postalCode->id,
                'language_id' => $language->id,
                'currency_id' => $currency->id,
                'number_format_id' => $numberFormat->id,
                'date_format_id' => $dateFormat->id,
                'continent_id' => $continent->id,
                'zone_id' => $zone->id,
            ]
        );

        // Employee 1
        /*factory(\Edifalia\User::class)->create([
            'type' => 1,
            'name' => 'Employee1',
            'email' => 'employee1@employee1.com',
            'password' => '123456',
            'remember_token' => str_random(10),
        ]);*/

        // Employee 2
        /*factory(\Edifalia\User::class)->create([
            'type' => 1,
            'name' => 'Employee2',
            'email' => 'employee2@employee2.com',
            'password' => '123456',
            'remember_token' => str_random(10),
        ]);

        // Employee 3
        factory(\Edifalia\User::class)->create([
            'type' => 1,
            'name' => 'Employee3',
            'email' => 'employee3@employee3.com',
            'password' => '123456',
            'remember_token' => str_random(10),
        ]);

          // Employee 4
        factory(\Edifalia\User::class)->create([
            'type' => 1,
            'name' => 'Employee4',
            'email' => 'employee4@employee4.com',
            'password' => '123456',
            'remember_token' => str_random(10),
        ]);

        // Employee 5
        factory(\Edifalia\User::class)->create([
            'type' => 1,
            'name' => 'Employee5',
            'email' => 'employee5@employee5.com',
            'password' => '123456',
            'remember_token' => str_random(10),
        ]);*/

        // history
        /*\DB::table('revisions')->where('id', 1)->update(['user_id' => 1]);
        \DB::table('revisions')->where('id', 2)->update(['user_id' => 2]);
        \DB::table('revisions')->where('id', 3)->update(['user_id' => 3]);
        \DB::table('revisions')->where('id', 4)->update(['user_id' => 4]);
        \DB::table('revisions')->where('id', 4)->update(['user_id' => 5]);
        \DB::table('revisions')->where('id', 4)->update(['user_id' => 6]);*/
    }
}

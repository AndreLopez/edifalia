<?php

use Illuminate\Database\Seeder;
use Edifalia\Models\Common\PropertyType;

class PropertyTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	\Eloquent::unguard();
        //PropertyType::truncate();

        $propertyTypesName = [ 'Finca', 'Hacienda', 'Terreno','Casa',
        	'Apartamento', 'Local'
        ];

        $length = count($propertyTypesName);

        for ($i=0; $i < $length; $i++) { 
        	$propertyType = ['name' => $propertyTypesName[$i]];
        	PropertyType::create($propertyType);
        }
    }
}

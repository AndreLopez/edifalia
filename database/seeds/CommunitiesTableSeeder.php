<?php

use Illuminate\Database\Seeder;
use Edifalia\Models\Backend\Community;

class CommunitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	\Eloquent::unguard();
    	\DB::statement('SET FOREIGN_KEY_CHECKS=0;');
    	Community::truncate();

    	$names = ['Murachi', 'Pablo Sexto', 'Palo Alto', 'Los llanos'];

    	$addresses = [
    		'CALLE AGUSTIN LARA NO. 69-B', 
    		'AV. INDEPENDENCIA NO. 241',
    		'CARRETERA A LOMA ALTA S/N.',
    		'CALLE OCAMPO NO 14	'
    	];

    	$nifs = ['68370', '68310', '68311', '68325'];

    	$years = ['1988', '1988', '2001', '2014'];

    	for ($i=0; $i < 4; $i++) 
    	{ 
    		$community = [
	    		'code' => $i, 
		        'name' => $names[$i], 
		        'address' => $addresses[$i], 
		        'nif' => $nifs[$i], 
		        'structure_year' => $years[$i], 
		        'elevator_numbers' => rand(1, 20), 
		        'platform_accounting' => rand(0, 1),
		        'longitude' => 1.5,
		        'latitude' => 1.8,
		        'notes' => 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...', 
		        'status_id' => 1, 
		        'postal_code_id' => 1, 
		        'president_id' => 1, 
		        'office_id' => null,

    		]; 
    		Community::create($community);
    	}
    	
    	\DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}

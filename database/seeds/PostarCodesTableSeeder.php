<?php

use Illuminate\Database\Seeder;
use Edifalia\Models\Common\PostalCode;
class PostarCodesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	\Eloquent::unguard();
    	\DB::statement('SET FOREIGN_KEY_CHECKS=0;');
    	PostalCode::truncate();

    	$towns = ['San Pedro Del Rio', 'Tariba'];
    	$codes = ['5001', '5002'];
    	$provinces = ['Táchira', 'Apure'];
    	$regions = ['Los Andes', 'Los Llanos'];

    	for ($i=0; $i < 2 ; $i++) 
    	{ 
    		$item = [
    			'code' => $codes[$i], 
    			'town' => $towns[$i], 
    			'province' => $provinces[$i], 
    			'region' => $regions[$i],
    			'country_id' => 1
    		]; 
    		PostalCode::create($item);
    	}

    	\DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}

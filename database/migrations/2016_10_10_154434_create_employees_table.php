<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('first_name', 100);
            $table->string('last_name', 100);
            $table->string('dni', 10)->unique();
            $table->string('department')->nullable();
            $table->string('phone')->nullable();
            $table->string('extension')->nullable();
            $table->integer('position_id')->unsigned()->nullable();
            $table->integer('office_id')->unsigned();
            $table->boolean('access_data')->default(false);
            $table->boolean('access_management')->default(false);
            $table->boolean('access_mail')->default(false);
            $table->boolean('access_incidents')->default(false);
            $table->boolean('access_reports')->default(false);
            $table->boolean('access_accounting')->default(false);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('position_id')->references('id')->on('positions')
                ->onDelete('cascade');
            $table->foreign('office_id')->references('id')->on('offices')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPersonIdToPersonsContacts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        if (Schema::hasTable('persons_contacts') && !Schema::hasColumn('persons_contacts', 'person_id')) {
            Schema::table('persons_contacts', function (Blueprint $table) {
                $table->integer('person_id')->unsigned()->after('contact_type_id');
                $table->foreign('person_id')
                        ->references('id')
                      ->on('persons')
                      ->onDelete('restrict')
                      ->onUpdate('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('persons_contacts', function (Blueprint $table) {
            $table->dropForeign('persons_contacts_person_id_foreign');
            $table->dropColumn('person_id');
        });
    }
}

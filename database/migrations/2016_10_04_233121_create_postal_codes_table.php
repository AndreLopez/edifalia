<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostalCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('postal_codes')) {
            Schema::create('postal_codes', function (Blueprint $table) {
                $table->increments('id')->comment('unique number for the record');
                $table->string('code', 45)->comment('postal_code');
                $table->string('town', 45)->comment('poblation name');
                $table->string('province', 45)->comment('province name');
                $table->string('region', 45)->comment('region name');
                $table->integer('country_id')->unsigned();
                $table->foreign('country_id')->references('id')
                      ->on('countries')->onDelete('restrict')->onUpdate('cascade');
                $table->timestamps();
                $table->softDeletes()->comment('timestamp in that record has deleted');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postal_codes');
    }
}

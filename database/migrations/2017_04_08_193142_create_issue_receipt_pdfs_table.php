<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIssueReceiptPdfsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('issue_receipt_pdfs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('issue_receipt_id')->unsigned();
            $table->string('pdf_name');
            $table->timestamps();
            $table->foreign('issue_receipt_id')->references('id')->on('issue_receipts')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('issue_receipt_pdfs');
    }
}

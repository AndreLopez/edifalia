<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVirtualOfficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('virtual_offices')) {
            Schema::create('virtual_offices', function (Blueprint $table) {
                $table->increments('id')->comment('unique number for the record');
                $table->text('image_url')->nullable()->comment('Image or logo for the virtual office');
                $table->string('image_title')->nullable()->comment('Image title');
                $table->text('header_url')->nullable()->comment('Header image for the virtual office');
                $table->string('header_title')->nullable()->comment('Header title');
                $table->boolean('publish_bank_account')->default(false)
                      ->comment('Publish the biggest bank accounts');
                $table->boolean('show_receipts_owner')->default(false)->comment('Show your receipts to owners');
                $table->boolean('owners_viewed')->default(false)->comment('Owners may be viewed between them');
                $table->boolean('owners_contact')->default(false)->comment('Owners can contact between them');
                $table->boolean('show_personalized_info')->default(false)
                      ->comment('I want to show personalized information to owners');
                $table->timestamps();
                $table->softDeletes()->comment('timestamp in that record has deleted');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('virtual_offices');
    }
}

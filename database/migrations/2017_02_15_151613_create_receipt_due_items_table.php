<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptDueItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('receipt_due_items')) {
            Schema::create('receipt_due_items', function(Blueprint $table) {
                $table->increments('id');
                $table->integer('receipt_due_id')->unsigned();
                $table->integer('property_id')->unsigned();
                $table->float('amount');
                $table->timestamps();

                $table->foreign('receipt_due_id')->references('id')->on('receipt_dues');
                $table->foreign('property_id')->references('id')->on('properties');
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipt_due_items');
    }
}
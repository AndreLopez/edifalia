<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPersonIdToDocumentPerson extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('document_person') && !Schema::hasColumn('document_person', 'person_id')) {
            Schema::table('document_person', function (Blueprint $table) {
            
                $table->integer('person_id')
                    ->unsigned()
                    ->after('document_id');
                $table->foreign('person_id')
                    ->references('id')
                    ->on('persons')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('document_person', function (Blueprint $table) {
            $table->dropForeign('document_person_person_id_foreign');
            $table->dropColumn('person_id');
        });
    }
}

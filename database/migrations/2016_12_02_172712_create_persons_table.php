
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('persons')) {
            Schema::create('persons', function (Blueprint $table) {
                $table->increments('id')->comment('unique number for the record');
                $table->string('first_name', 75)->comment('person first name');
                $table->string('last_name', 75)->comment('person last name');
                $table->text('address')->comment('person address');
                $table->string('dni', 10)->unique()->comment('DNI or Passport number');
                $table->string('reference', 12)->nullable()->comment('reference code');
                $table->string('strength_language',1)->comment('Strength language, Basic, Normal, Fluid');
                $table->boolean('message_email_only')->default(False)->comment('Message email only');
                $table->boolean('group_properties')->default(False)
                      ->comment('In issuing receipts, grouping the properties of the same community');
                $table->boolean('group_invoice_concepts')->default(False)
                      ->comment('In issuing receipts, group invoice concepts of the same property');
                $table->boolean('dont_get_email')->default(False)
                      ->comment('Do not want to receive email');
                $table->boolean('can_update_own_data')->default(False)->comment('Can update own data');
                $table->boolean('exclusiveness')->default(False)->comment('Exclusiveness');
                $table->boolean('virtual_office_access')->default(False)->comment('Virtual Office access');
                $table->boolean('update_data_from_virtual_office')->default(False)
                      ->comment('Update own data from Virtual Office');
                $table->text('notes')->nullable()->comment('Notes or observations');

                $table->integer('postal_code_id')->unsigned();
                $table->foreign('postal_code_id')->references('id')
                      ->on('postal_codes')->onDelete('restrict')->onUpdate('cascade');
                $table->integer('language_id')->unsigned();
                $table->foreign('language_id')->references('id')
                      ->on('languages')->onDelete('restrict')->onUpdate('cascade');
                $table->integer('payment_method_id')->unsigned();
                $table->foreign('payment_method_id')->references('id')
                      ->on('payment_methods')->onDelete('restrict')->onUpdate('cascade');
                $table->integer('user_id')->unsigned()->nullable();
                $table->foreign('user_id')->references('id')
                      ->on('users')->onDelete('restrict')->onUpdate('cascade');
                $table->timestamps();
                $table->softDeletes()->comment('timestamp in that record has deleted');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('persons');
    }
}

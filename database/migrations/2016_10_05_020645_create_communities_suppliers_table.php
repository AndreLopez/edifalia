<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunitiesSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('communities_suppliers')) {
            Schema::create('communities_suppliers', function (Blueprint $table) {
                $table->increments('id')->comment('unique number for the record');
                $table->text('comment')->nullable()->comment('Notes or comments');
                
                $table->integer('community_id')->unsigned();
                $table->foreign('community_id')->references('id')
                      ->on('communities')->onDelete('restrict')->onUpdate('cascade');
                $table->integer('supplier_id')->unsigned();
                $table->foreign('supplier_id')->references('id')
                      ->on('suppliers')->onDelete('restrict')->onUpdate('cascade');
                $table->timestamps();
                $table->softDeletes()->comment('timestamp in that record has deleted');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('communities_suppliers');
    }
}

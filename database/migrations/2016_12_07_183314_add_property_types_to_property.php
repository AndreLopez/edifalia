<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPropertyTypesToProperty extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('properties')) {
            Schema::table('properties', function (Blueprint $table) {
                $table->dropColumn('type');
                $table->integer('property_type_id')->unsigned()->after('registered_property');
                $table->foreign('property_type_id')
                    ->references('id')
                    ->on('property_types')
                    ->onDelete('restrict')
                    ->onUpdate('cascade');
            });
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->dropForeign('properties_property_type_id_foreign');
            $table->dropColumn('property_type_id');
        });
    }
}

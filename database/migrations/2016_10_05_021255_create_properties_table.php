<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('properties')) {
            Schema::create('properties', function (Blueprint $table) {
                $table->increments('id')->comment('unique number for the record');
                $table->string('building_number', 4)->comment('Block number or building number');
                $table->string('property_number', 6)->comment('Property number');
                $table->string('registered_property', 45)->nullable()->comment('Registered property data');
                $table->string('type', 1)->comment('Property type, floor, local, etc...');
                $table->boolean('status')->default(true)->comment('Property status, active or inactive');
                $table->float('coefficient', 5, 2)->nullable()->default(0)->comment('Coefficient percent');
                $table->boolean('group_properties')->default(false)
                      ->comment('To issue receipts, grouping the properties of the same community');
                $table->boolean('group_concepts')->default(false)
                      ->comment('To issue receipts, group billing concepts of the same property');
                $table->string('person_relation')->comment('Person relation with property, owner, occupant, etc');

                $table->timestamps();
                $table->softDeletes()->comment('timestamp in that record has deleted');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}

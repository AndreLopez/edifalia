<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBudgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('budgets')){
            Schema::create('budgets', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->string('title');
                $table->integer('community_id')->unsigned();
                $table->integer('invoicing_concept_id')->unsigned();
                $table->integer('periodicity'); // 1,2,3,4
                $table->integer('rounding');
                $table->integer('duration');
                $table->integer('month');
                $table->integer('year');
                $table->timestamps();
                $table->softDeletes();
                $table->foreign('user_id')->references('id')->on('users')
                    ->onDelete('cascade');
                $table->foreign('community_id')->references('id')->on('communities')
                    ->onDelete('cascade');
                $table->foreign('invoicing_concept_id')->references('id')->on('invoicing_concepts')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('budgets');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIssueReceiptIdFieldToReceiptDues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('receipt_dues', 'issue_receipt_id')){
            Schema::table('receipt_dues', function (Blueprint $table) {
                $table->integer('issue_receipt_id')->after('year')->unsigned()->nullable();
                $table->foreign('issue_receipt_id')->references('id')->on('issue_receipts')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('receipt_dues', function (Blueprint $table) {
            $table->dropForeign('receipt_dues_issue_receipt_id_foreign');
            $table->dropColumn('issue_receipt_id');
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('suppliers')) {
            Schema::create('suppliers', function (Blueprint $table) {
                $table->increments('id')->comment('unique number for the record');
                $table->string('cif', 15)->comment('CIF number');
                $table->string('business_name', 45)->unique()->comment('business name');
                $table->string('tradename', 45)->unique()->comment('Trade name or comercial name');
                $table->text('address')->nullable()->comment('supplier address');
                $table->string('website', 75)->nullable()->comment('supplier url website');
                $table->string('contact_name')->nullable()->comment('contact name with supplier');
                $table->boolean('update_data_from_virtual_office')->default(False)
                      ->comment('Update own data from Virtual Office');
                $table->boolean('virtual_office_access')->default(False)
                      ->comment('Have access to Virtual Office');
                $table->text('notes')->nullable()->comment('Notes or observations');

                $table->integer('postal_code_id')->unsigned();
                $table->foreign('postal_code_id')->references('id')
                      ->on('postal_codes')->onDelete('restrict')->onUpdate('cascade');
                $table->integer('bank_account_id')->unsigned();
                $table->foreign('bank_account_id')->references('id')
                      ->on('bank_accounts')->onDelete('restrict')->onUpdate('cascade');
                $table->integer('user_id')->unsigned()->nullable();
                $table->foreign('user_id')->references('id')
                      ->on('users')->onDelete('restrict')->onUpdate('cascade');
                $table->timestamps();
                $table->softDeletes()->comment('timestamp in that record has deleted');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}

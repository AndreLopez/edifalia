<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMainUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('main_users')){
            Schema::create('main_users', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->string('company')->nullable();
                $table->string('first_name', 100)->nullable();
                $table->string('last_name', 100)->nullable();
                $table->string('dni', 10)->unique()->nullable();
                $table->string('address')->nullable();
                $table->integer('postal_code_id')->unsigned()->nullable();
                $table->string('phone')->nullable();
                $table->string('fax')->nullable();
                $table->text('signature')->nullable();
                $table->enum('ip_list_option', [1,2])->default(1);
                $table->string('collegiate_number')->nullable();
                $table->string('aaff_program')->nullable();
                $table->string('aaff_email')->nullable();
                $table->text('letterhead')->nullable();

                $table->integer('language_id')->unsigned();
                $table->integer('currency_id')->unsigned();
                $table->integer('number_format_id')->unsigned();
                $table->integer('date_format_id')->unsigned();
                $table->integer('continent_id')->unsigned();
                $table->integer('zone_id')->unsigned();

                $table->timestamps();
                $table->softDeletes();

                $table->foreign('user_id')->references('id')->on('users')
                    ->onDelete('cascade');
                $table->foreign('postal_code_id')->references('id')->on('postal_codes')
                    ->onDelete('cascade');
                $table->foreign('language_id')->references('id')->on('languages')
                    ->onDelete('cascade');
                $table->foreign('currency_id')->references('id')->on('currencies')
                    ->onDelete('cascade');
                $table->foreign('number_format_id')->references('id')->on('number_formats')
                    ->onDelete('cascade');
                $table->foreign('date_format_id')->references('id')->on('date_formats')
                    ->onDelete('cascade');
                $table->foreign('continent_id')->references('id')->on('continents')
                    ->onDelete('cascade');
                $table->foreign('zone_id')->references('id')->on('zones')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('main_users');
    }
}

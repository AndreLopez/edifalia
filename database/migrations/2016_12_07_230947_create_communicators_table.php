<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunicatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('communicators')) {
            Schema::create('communicators', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('type_id')->unsigned();
                $table->string('name')->nullable();
                $table->string('last_name')->nullable();
                $table->text('address')->nullable();
                $table->string('email')->nullable();
                $table->string('phone')->nullable();
                $table->string('other_phone')->nullable();
                $table->timestamps();
                $table->softDeletes();
                $table->foreign('type_id')->references('id')->on('communicator_types')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('communicators');
    }
}

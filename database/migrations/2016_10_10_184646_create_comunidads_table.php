<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComunidadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comunidads', function (Blueprint $table) {
             $table->increments('id');
            $table->string('code',4);
            $table->string('name',75);
            $table->text('address');
            $table->string('nif',15);
            $table->string('structure_year',4);
            $table->integer('elevator_numbers',false,true)->lenght(11);
            $table->tinyInteger('platform_accounting',false,true)->lenght(1);
            $table->double('longitude',15,8);
            $table->double('latitude',15,8);
            $table->text('notes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comunidads');
    }
}

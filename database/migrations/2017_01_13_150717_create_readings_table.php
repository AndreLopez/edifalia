<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReadingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('readings')){
            Schema::create('readings', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->string('title');
                $table->integer('community_id')->unsigned();
                $table->integer('invoicing_concept_id')->unsigned();
                $table->integer('rate_id')->unsigned();
                $table->integer('periodicity'); // 1,2,3,4
                $table->integer('month');
                $table->integer('year');
                $table->text('concept_receipt');
                $table->date('date');
                $table->timestamps();
                $table->softDeletes();
                $table->foreign('user_id')->references('id')->on('users')
                    ->onDelete('cascade');
                $table->foreign('community_id')->references('id')->on('communities')
                    ->onDelete('cascade');
                $table->foreign('invoicing_concept_id')->references('id')->on('invoicing_concepts')
                    ->onDelete('cascade');
                $table->foreign('rate_id')->references('id')->on('invoicing_concepts')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('readings');
    }
}

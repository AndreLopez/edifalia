<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMeetingControl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('meeting_controls')) {
            Schema::create('meeting_controls', function (Blueprint $table) {
                $table->increments('id');
                $table->string('title');
                $table->integer('person_id')->unsigned();
                $table->foreign('person_id')->references('id')->on('persons')->onDelete('cascade');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meeting_controls');        
    }
}

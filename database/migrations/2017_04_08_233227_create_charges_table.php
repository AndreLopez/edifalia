<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('charges')) {
            Schema::create('charges', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->integer('community_id')->unsigned();
                $table->integer('property_id')->unsigned();
                $table->integer('issue_receipt_id')->unsigned();
                //$table->double('amount', 7, 2);
                $table->double('pending', 7, 2);
                $table->integer('charge_type');
                $table->timestamps();
                $table->softDeletes();

                $table->foreign('user_id')->references('id')->on('users')
                    ->onDelete('cascade');
                $table->foreign('community_id')->references('id')->on('communities')
                    ->onDelete('cascade');
                $table->foreign('property_id')->references('id')->on('properties')
                    ->onDelete('cascade');
                $table->foreign('issue_receipt_id')->references('id')->on('issue_receipts')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('charges');
    }
}

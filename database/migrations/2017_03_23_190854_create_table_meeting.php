<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMeeting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('meetings')) {
            Schema::create('meetings', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('type')->nullable(); // 0,1,2, or 3
                $table->string('title');
                $table->string('date_in');
                $table->text('place');
                $table->text('order_day');
                $table->text('minute');
                $table->string('time_one');
                $table->string('time_two');
                $table->string('time_end');
                $table->integer('community_id')->unsigned();
                $table->foreign('community_id')->references('id')->on('communities');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meetings');
    }
}

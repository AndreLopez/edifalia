<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('communities')) {
            Schema::create('communities', function (Blueprint $table) {
                $table->increments('id')->comment('unique number for the record');
                $table->string('code', 4)->unique()->comment('community code');
                $table->string('name', 75)->comment('community name');
                $table->text('address')->comment('community address');
                $table->string('nif', 15)->unique()->comment('identification fiscal number');
                $table->string('structure_year', 4)->comment('construction year');
                $table->integer('elevator_numbers')->nullable()->comment('elevator numbers if exists');
                $table->boolean('platform_accounting')->default(false)->comment('accounting with platform');
                $table->double('longitude', 15, 8)->nullable()->comment('longitude geographic coordinate');
                $table->double('latitude', 15, 8)->nullable()->comment('latitude geographic coordinate');
                $table->text('notes')->nullable()->comment('Notes or Observations');

                $table->integer('status_id')->unsigned();
                $table->foreign('status_id')->references('id')
                      ->on('status')->onDelete('restrict')->onUpdate('cascade');
                $table->integer('postal_code_id')->unsigned();
                $table->foreign('postal_code_id')->references('id')
                      ->on('postal_codes')->onDelete('restrict')->onUpdate('cascade');
                $table->integer('office_id')->unsigned()->nullable();
                $table->foreign('office_id')->references('id')->on('offices')->onDelete('cascade');

                $table->timestamps();
                $table->softDeletes()->comment('timestamp in that record has deleted');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('communities');
    }
}

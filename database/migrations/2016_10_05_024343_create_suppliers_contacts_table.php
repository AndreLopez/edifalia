<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliersContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('supplier_contacts')) {
            Schema::create('supplier_contacts', function (Blueprint $table) {
                $table->increments('id')->comment('unique number for the record');
                $table->string('contact', 75)->comment('Contact name');
                $table->text('notes')->nullable()->comment('Notes or Observations');
                
                $table->integer('contact_type_id')->unsigned();
                $table->foreign('contact_type_id')->references('id')
                      ->on('contact_types')->onDelete('restrict')->onUpdate('cascade');
                $table->integer('supplier_id')->unsigned();
                $table->foreign('supplier_id')->references('id')
                      ->on('suppliers')->onDelete('restrict')->onUpdate('cascade');
                $table->timestamps();
                $table->softDeletes()->comment('timestamp in that record has deleted');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_contacts');
    }
}

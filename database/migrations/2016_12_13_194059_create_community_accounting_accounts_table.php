<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunityAccountingAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('community_accounting_accounts')) {
            Schema::create('community_accounting_accounts', function (Blueprint $table) {
                $table->integer('accounting_account_id')->unsigned();
                $table->foreign('accounting_account_id')->references('id')
                      ->on('accounting_accounts')->onDelete('restrict')->onUpdate('cascade');
                $table->integer('community_id')->unsigned();
                $table->foreign('community_id')->references('id')
                      ->on('communities')->onDelete('restrict')->onUpdate('cascade');
                $table->timestamps();
                $table->softDeletes()->comment('timestamp in that record has deleted');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('community_accounting_accounts');
    }
}

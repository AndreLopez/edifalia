<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMeetingVotingDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('meeting_voting_details')) {
            Schema::create('meeting_voting_details', function (Blueprint $table) {
                $table->increments('id');
                $table->boolean('voting')->default(false);
                $table->integer('voting_id')->unsigned();
                $table->foreign('voting_id')->references('id')->on('meeting_votings');
                $table->integer('property_id')->unsigned();
                $table->foreign('property_id')->references('id')->on('properties');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meeting_voting_details');
    }
}

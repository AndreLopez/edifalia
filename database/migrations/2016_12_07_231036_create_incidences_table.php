<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncidencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('incidences')) {
            Schema::create('incidences', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->integer('office_id')->unsigned();
                $table->integer('community_id')->unsigned();
                $table->integer('communicator_id')->unsigned()->nullable();
                $table->integer('priority');
                $table->integer('reception_id')->unsigned();
                $table->integer('reason_id')->unsigned();
                $table->integer('controls');
                $table->boolean('publish')->default(false);
                $table->boolean('conclude')->default(false);
                $table->string('title');
                $table->text('details')->nullable();
                $table->integer('send')->nullable();
                $table->integer('sector_id')->unsigned();
                $table->integer('supplier_id')->unsigned();
                $table->text('other_annotations')->nullable();
                $table->timestamps();
                $table->softDeletes();
                $table->foreign('user_id')->references('id')->on('users')
                    ->onDelete('cascade');
                $table->foreign('office_id')->references('id')->on('offices')
                    ->onDelete('cascade');
                $table->foreign('community_id')->references('id')->on('communities')
                    ->onDelete('cascade');
                $table->foreign('communicator_id')->references('id')->on('communicators')
                    ->onDelete('cascade');
                $table->foreign('reception_id')->references('id')->on('receptions')
                    ->onDelete('cascade');
                $table->foreign('reason_id')->references('id')->on('reasons')
                    ->onDelete('cascade');
                $table->foreign('sector_id')->references('id')->on('sectors')
                    ->onDelete('cascade');
                $table->foreign('supplier_id')->references('id')->on('suppliers')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incidences');
    }
}

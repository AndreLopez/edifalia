<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMeetingAssistant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('meeting_assistants')) {
            Schema::create('meeting_assistants', function (Blueprint $table) {
                $table->increments('id');
                $table->boolean('presented')->default(false);
                $table->integer('meeting_id')->unsigned();
                $table->foreign('meeting_id')->references('id')->on('meetings');
                $table->integer('property_id')->unsigned();
                $table->foreign('property_id')->references('id')->on('properties');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meeting_assistants');
    }
}

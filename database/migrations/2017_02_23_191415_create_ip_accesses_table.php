<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIpAccessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('ip_accesses')) {
            Schema::create('ip_accesses', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('main_user_id')->unsigned();
                $table->string('ip', 50)->unique();
                $table->timestamps();
                $table->foreign('main_user_id')->references('id')->on('main_users')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ip_accesses');
    }
}

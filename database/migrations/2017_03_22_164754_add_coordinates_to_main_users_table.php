<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCoordinatesToMainUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('main_users')) {
            Schema::table('main_users', function (Blueprint $table) {
                $table->text('coordinates')->nullable()->after('zone_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('main_users', function (Blueprint $table) {
            $table->dropColumn('coordinates');
        });
    }
}

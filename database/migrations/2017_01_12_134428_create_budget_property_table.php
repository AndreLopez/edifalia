<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBudgetPropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('budget_property')){
            Schema::create('budget_property', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('budget_id')->unsigned();
                $table->integer('property_id')->unsigned();
                $table->float('general');
                $table->float('amount');
                $table->float('coefficient', 5, 2);
                $table->timestamps();
                $table->foreign('budget_id')->references('id')->on('budgets')
                    ->onDelete('cascade');
                $table->foreign('property_id')->references('id')->on('properties')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('budget_property');
    }
}

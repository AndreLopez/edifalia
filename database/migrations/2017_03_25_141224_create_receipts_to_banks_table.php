<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptsToBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('receipts_to_banks')) {
            Schema::create('receipts_to_banks', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->integer('file_format'); //1=cuaderno19, 2=sepa(core), 3=sepa(core1)
                $table->integer('state'); // 1= previous, 2= later
                $table->date('date_sent_to_bank');
                $table->date('charge_date');
                $table->boolean('bank_request')->default(false);
                $table->date('payment_date')->nullable();
                $table->timestamps();
                $table->softDeletes();
                $table->foreign('user_id')->references('id')->on('users')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipts_to_banks');
    }
}

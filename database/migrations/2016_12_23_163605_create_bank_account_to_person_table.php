<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankAccountToPersonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_account_person', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('person_id')->unsigned();
            $table->foreign('person_id')
            ->references('id')
            ->on('persons')
            ->onDelete('cascade');
            
            $table->integer('bank_account_id')->unique()->unsigned();
            $table->foreign('bank_account_id')
            ->references('id')
            ->on('bank_accounts')
            ->onDelete('restrict')
            ->onUpdate('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_account_person');
    }
}

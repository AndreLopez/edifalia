<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractBankAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contract_bank_accounts', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('contract_id')->unsigned();
            $table->foreign('contract_id')
            ->references('id')
            ->on('contracts')
            ->onDelete('cascade');
            
            $table->integer('bank_account_id')->unique()->unsigned();
            $table->foreign('bank_account_id')
            ->references('id')
            ->on('bank_accounts')
            ->onDelete('restrict')
            ->onUpdate('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('contract_bank_accounts');
    }
}

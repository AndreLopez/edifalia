<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonsContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('persons_contacts')) {
            Schema::create('persons_contacts', function (Blueprint $table) {
                $table->increments('id')->comment('unique number for the record');
                $table->string('contact', 75)->comment('Contact name');
                $table->text('notes')->nullable()->comment('Notes or Observations');
                $table->integer('contact_type_id')->unsigned();
                $table->foreign('contact_type_id')
                    ->references('id')
                    ->on('contact_types')
                    ->onDelete('restrict')
                    ->onUpdate('cascade');
                $table->timestamps();
                $table->softDeletes()->comment('timestamp in that record has deleted');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('persons_contacts');
    }
}

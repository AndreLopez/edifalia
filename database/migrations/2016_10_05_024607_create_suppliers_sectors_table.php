<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliersSectorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('supplier_sectors')) {
            Schema::create('supplier_sectors', function (Blueprint $table) {
                $table->increments('id')->comment('unique number for the record');
                
                $table->integer('supplier_id')->unsigned();
                $table->foreign('supplier_id')->references('id')
                      ->on('suppliers')->onDelete('restrict')->onUpdate('cascade');
                $table->integer('sector_id')->unsigned();
                $table->foreign('sector_id')->references('id')
                      ->on('supplier_activities')->onDelete('restrict')->onUpdate('cascade');
                $table->timestamps();
                $table->softDeletes()->comment('timestamp in that record has deleted');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_sectors');
    }
}

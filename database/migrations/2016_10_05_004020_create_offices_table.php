<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('offices')){
            Schema::create('offices', function (Blueprint $table) {
                $table->increments('id');
                $table->string('identifier')->unique();
                $table->string('name');
                $table->text('address');
                $table->integer('postal_code_id')->unsigned();
                $table->string('phone1');
                $table->string('phone2')->nullable();
                $table->string('fax')->nullable();
                $table->string('email');
                $table->foreign('postal_code_id')->references('id')->on('postal_codes')
                    ->onDelete('cascade');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offices');
    }
}

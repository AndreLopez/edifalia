<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPresidentIdToCommunities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if (Schema::hasTable('communities') && !Schema::hasColumn('communities', 'president_id')) {
            Schema::table('communities', function (Blueprint $table) {
                $table->integer('president_id')
                        ->nullable()
                        ->unsigned()
                        ->after('postal_code_id');
                $table->foreign('president_id')
                      ->references('id')
                      ->on('persons')
                      ->onDelete('restrict')
                      ->onUpdate('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('communities', function (Blueprint $table) {
            $table->dropForeign('communities_president_id_foreign');
            $table->dropColumn('president_id');
        });
    }
}

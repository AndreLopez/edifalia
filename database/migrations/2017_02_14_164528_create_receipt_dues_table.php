<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptDuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('receipt_dues')) {
            Schema::create('receipt_dues', function(Blueprint $table) {
                $table->increments('id');
                $table->integer('community_id')->unsigned();
                $table->integer('invoicing_concept_id')->unsigned();
                $table->float('total_amount');
                $table->integer('month');
                $table->integer('year');
                $table->timestamps();

                $table->foreign('community_id')->references('id')->on('communities');
                $table->foreign('invoicing_concept_id')->references('id')->on('invoicing_concepts');
                $table->softDeletes()->comment('timestamp in that record has deleted');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipt_dues');
    }
}
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessageFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('message_files')) {
            Schema::create('message_files', function (Blueprint $table) {
                $table->increments('id');
                $table->string('filename');
                $table->string('original_filename');
                $table->string('mime');
                $table->string('extension');
                $table->string('complete_path');
                $table->integer('message_id')->unsigned();
                $table->foreign('message_id')
                    ->references('id')
                    ->on('messages')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('message_files');
    }
}

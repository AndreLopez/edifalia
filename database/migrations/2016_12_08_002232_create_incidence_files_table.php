<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncidenceFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('incidence_files')){
            Schema::create('incidence_files', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('incidence_id')->unsigned();
                $table->string('file');
                $table->string('title');
                $table->boolean('publish')->default(false)->nullable();
                $table->timestamps();
                $table->softDeletes();
                $table->foreign('incidence_id')->references('id')->on('incidences')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incidence_files');
    }
}

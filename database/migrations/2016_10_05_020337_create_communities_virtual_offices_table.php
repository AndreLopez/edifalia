<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunitiesVirtualOfficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('communities_virtual_offices')) {
            Schema::create('communities_virtual_offices', function (Blueprint $table) {
                $table->increments('id')->comment('unique number for the record');
                
                $table->integer('community_id')->unsigned();
                $table->foreign('community_id')->references('id')
                      ->on('communities')->onDelete('restrict')->onUpdate('cascade');
                $table->integer('virtual_office_id')->unsigned();
                $table->foreign('virtual_office_id')->references('id')
                      ->on('virtual_offices')->onDelete('restrict')->onUpdate('cascade');
                $table->timestamps();
                $table->softDeletes()->comment('timestamp in that record has deleted');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('communities_virtual_offices');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAccountingAccountToBankAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bank_accounts', function (Blueprint $table) {
            $table->integer('accounting_account_id')
                    ->unsigned()
                    ->nullable()
                    ->comment('relationship with the accounting account');
            $table->foreign('accounting_account_id')
                    ->references('id')
                    ->on('accounting_accounts')
                    ->onDelete('restrict')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bank_accounts', function (Blueprint $table) {
            $table->dropForeign('bank_accounts_accounting_account_id_foreign');
            $table->dropColumn('accounting_account_id');
        });
    }
}

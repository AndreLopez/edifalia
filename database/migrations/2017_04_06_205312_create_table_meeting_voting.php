<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMeetingVoting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('meeting_votings')) {
            Schema::create('meeting_votings', function (Blueprint $table) {
                $table->increments('id');
                $table->text('description');
                $table->string('agreement');
                $table->integer('meeting_id')->unsigned();
                $table->foreign('meeting_id')->references('id')->on('meetings');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meeting_votings');        
    }
}

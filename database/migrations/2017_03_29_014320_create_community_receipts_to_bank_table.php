<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunityReceiptsToBankTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('community_receipts_to_bank')){
            Schema::create('community_receipts_to_bank', function (Blueprint $table){
                $table->increments('id');
                $table->integer('community_id')->unsigned();
                $table->integer('receipts_to_bank_id')->unsigned();
                $table->timestamps();
                $table->foreign('community_id')->references('id')->on('communities')
                    ->onDelete('cascade');
                $table->foreign('receipts_to_bank_id')->references('id')->on('receipts_to_banks')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('community_receipts_to_bank');
    }
}

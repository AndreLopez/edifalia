<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunityIssueReceiptTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('community_issue_receipt')){
            Schema::create('community_issue_receipt', function (Blueprint $table){
                $table->integer('community_id')->unsigned();
                $table->integer('issue_receipt_id')->unsigned();
                $table->timestamps();
                $table->foreign('community_id')->references('id')->on('communities')
                    ->onDelete('cascade');
                $table->foreign('issue_receipt_id')->references('id')->on('issue_receipts')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('community_issue_receipt');
    }
}

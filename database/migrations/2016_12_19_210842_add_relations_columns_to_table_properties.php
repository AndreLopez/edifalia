<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelationsColumnsToTableProperties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if (Schema::hasTable('properties')) 
         {
            Schema::table('properties', function (Blueprint $table) 
            {

                $table->dropColumn('person_relation');
                if (!Schema::hasColumn('properties', 'person_id')) {
                    $table->integer('person_id')->unsigned()->after('group_concepts');
                    $table->foreign('person_id')
                        ->references('id')
                        ->on('persons')
                        ->onDelete('cascade');
                }
                
                if (!Schema::hasColumn('properties', 'payment_method_id')) {
                    $table->integer('payment_method_id')->unsigned()->after('person_id');
                    $table->foreign('payment_method_id')
                            ->references('id')
                            ->on('payment_methods')
                            ->onDelete('restrict')
                            ->onUpdate('cascade');
                }
                
                if (!Schema::hasColumn('properties', 'community_id')) {
                    $table->integer('community_id')->unsigned()->after('payment_method_id');
                    $table->foreign('community_id')
                            ->references('id')
                            ->on('communities')
                            ->onDelete('restrict')
                            ->onUpdate('cascade');
                }

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->dropForeign('properties_person_id_foreign');
            $table->dropColumn('person_id');
            $table->dropForeign('properties_payment_method_id_foreign');
            $table->dropColumn('payment_method_id');
            $table->dropForeign('properties_community_id_foreign');
            $table->dropColumn('community_id');
        });
    }
}

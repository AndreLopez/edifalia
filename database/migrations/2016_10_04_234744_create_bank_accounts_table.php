<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('bank_accounts')) {
            Schema::create('bank_accounts', function (Blueprint $table) {
                $table->increments('id')->comment('unique number for the record');
                $table->string('iban', 4)->unique()->comment('iban code');
                $table->string('code_a', 4)->comment('first iban code');
                $table->string('code_b', 4)->comment('second iban code');
                $table->string('code_c', 4)->comment('third iban code');
                $table->string('code_d', 4)->comment('fourth iban code');
                $table->string('code_e', 4)->comment('fifth iban code');
                $table->string('sufix', 4)->nullable()->comment('account sufix');
                $table->string('bic', 4)->comment('bank international code');
                
                $table->timestamps();
                $table->softDeletes()->comment('timestamp in that record has deleted');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_accounts');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyReadingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('property_reading')){
            Schema::create('property_reading', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('property_id')->unsigned();
                $table->integer('reading_id')->unsigned();
                $table->float('previous');
                $table->float('current');
                $table->float('consumption');
                $table->timestamps();
                $table->foreign('property_id')->references('id')->on('properties')
                    ->onDelete('cascade');
                $table->foreign('reading_id')->references('id')->on('readings')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_reading');
    }
}

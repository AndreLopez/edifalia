<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('contracts')) {
            Schema::create('contracts', function (Blueprint $table) {
                $table->increments('id')->comment('unique number for the record');
                $table->string('reference')->comment('Reference number or contract identifier');
                $table->text('description')->comment('description about the contract');
                $table->boolean('jan')->default(false)->comment('January');
                $table->boolean('feb')->default(false)->comment('February');
                $table->boolean('mar')->default(false)->comment('March');
                $table->boolean('apr')->default(false)->comment('April');
                $table->boolean('may')->default(false)->comment('May');
                $table->boolean('jun')->default(false)->comment('June');
                $table->boolean('jul')->default(false)->comment('July');
                $table->boolean('aug')->default(false)->comment('August');
                $table->boolean('sep')->default(false)->comment('September');
                $table->boolean('oct')->default(false)->comment('October');
                $table->boolean('nov')->default(false)->comment('November');
                $table->boolean('dec')->default(false)->comment('December');
                $table->float('amount', 15, 2)->comment('Contract total amount');
                $table->float('minimum', 15, 2)->comment('Contract minimum amount');
                $table->float('maximum', 15, 2)->comment('Contract maximum amount');
                $table->longText('contract_file')->comment('Contract in digital');
                $table->boolean('publish_virtual_office')->default(false)
                      ->comment('Publish contract in Virtual Office');
                $table->dateTime('contract_date')->comment('Contract date in that has signed');
                $table->integer('duration')->unsigned()->comment('Contract duration');
                $table->string('duration_type', 1)->comment('duration type, Ej. Undefined, Fixed, etc');
                $table->text('observations')->nullable()->comment('Note or Observations about the contract');


                $table->integer('accounting_account_id')->unsigned();
                $table->foreign('accounting_account_id')->references('id')
                      ->on('accounting_accounts')->onDelete('restrict')->onUpdate('cascade');
                $table->integer('community_id')->unsigned();
                $table->foreign('community_id')->references('id')
                      ->on('communities')->onDelete('restrict')->onUpdate('cascade');
                $table->integer('supplier_id')->unsigned();
                $table->foreign('supplier_id')->references('id')
                      ->on('suppliers')->onDelete('restrict')->onUpdate('cascade');
                $table->integer('payment_method_id')->unsigned();
                $table->foreign('payment_method_id')->references('id')
                      ->on('payment_methods')->onDelete('restrict')->onUpdate('cascade');
                $table->integer('bank_account_id')->unsigned();
                $table->foreign('bank_account_id')->references('id')
                      ->on('bank_accounts')->onDelete('restrict')->onUpdate('cascade');
                $table->timestamps();
                $table->softDeletes()->comment('timestamp in that record has deleted');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(Edifalia\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'type' => $faker->numberBetween(1, 4),
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '123456',
        'remember_token' => str_random(10),
    ];
});

/**
 * 
	Create factory for modelo Edifalia\Models\Common\Property

 */
$factory->define(Edifalia\Models\Common\Property::class, function (Faker\Generator $faker) {
    
    return [
    ];

});

/**
 * 
    Create factory for modelo Edifalia\Models\Backend\Person

 */
$factory->define(Edifalia\Models\Backend\Person::class, function (Faker\Generator $faker) {
    

    return [
            
    ];

});

